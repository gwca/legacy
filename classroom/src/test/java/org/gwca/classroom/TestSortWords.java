package org.gwca.classroom;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 */
public class TestSortWords {

	private static String TEXT_DIR = "/src/test/config/main/";
	private static String WORDS_FILE = "58.txt";
	private static String TEXT_FILE = "58-text.txt";

	private String workDir;

	@Before
	public void init() {
		workDir = System.getProperty("user.dir");
		workDir = workDir.replace("\\", "/") + TEXT_DIR;
	}

	/**
	 */
	@Test
	public void sortWordsOfCard() throws Exception {
	  //sortWords();
	}
	
	public void sortWords() throws Exception {

		// get the full text
		StringBuffer text = new StringBuffer();
		List<String> lines = readTextFile(TEXT_FILE);
		for (String line : lines) {
			text.append(line);
		}

		// get the word list and position of the word in the text
		List<TextWord> words = new ArrayList<TextWord>();
		lines = readTextFile(WORDS_FILE);
		for (String line : lines) {
			String[] tokens = line.split("\t");
			TextWord textWord = new TextWord(0, tokens[0], tokens[1]);
			int position = text.indexOf(textWord.word);
			textWord.position = position;
			words.add(textWord);
		}

		// sort the words by position and print them out
		Collections.sort(words, new Comparator<TextWord>() {
			@Override
			public int compare(TextWord lhs, TextWord rhs) {
				return lhs.position == rhs.position ? 0 : (lhs.position > rhs.position) ? 1 : -1;
			}
		});

		for (TextWord word : words) {
			System.out.println(word.toString());
		}
	}

	/**
	 * Gets the text from the specified file.
	 * 
	 * @param name
	 * @return
	 * @throws IOException
	 */
	private List<String> readTextFile(String name) throws IOException {

		String fileName = workDir + name;
		Path path = FileSystems.getDefault().getPath(fileName);
		List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
		return lines;
	}
}

/**
 * A word entry.
 * 
 * @author youping.hu
 */
class TextWord {
	int position;
	String word;
	String entry;

	public TextWord(int position, String word, String entry) {
		this.position = position;
		this.word = word;
		this.entry = entry;
	}

	public String toString() {
		// return position + " " + word + "\t" + entry;
		return word + "\t" + entry;
	}
}
