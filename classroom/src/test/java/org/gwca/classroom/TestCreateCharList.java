package org.gwca.classroom;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.junit.Before;
import org.junit.Test;

/**
 * Creates a list of JSON classes for the new words.
 */
public class TestCreateCharList {

	private static String TEXT_DIR = "/src/test/config/main/";

	private String workDir;
	private CharsetEncoder asciiEncoder;
	private Map<String, String> pinyinMap;
	private Map<String, String> nameListMap;
	private List<String> ignoredWords;

	@Before
	public void initFileList() {

		pinyinMap = new HashMap<String, String>();
		pinyinMap.put("ā", "a");
		pinyinMap.put("á", "a");
		pinyinMap.put("ǎ", "a");
		pinyinMap.put("à", "a");
		pinyinMap.put("ē", "e");
		pinyinMap.put("é", "e");
		pinyinMap.put("ě", "e");
		pinyinMap.put("è", "e");
		pinyinMap.put("ō", "o");
		pinyinMap.put("ó", "o");
		pinyinMap.put("ǒ", "o");
		pinyinMap.put("ò", "o");
		pinyinMap.put("ī", "i");
		pinyinMap.put("í", "i");
		pinyinMap.put("ǐ", "i");
		pinyinMap.put("ì", "i");
		pinyinMap.put("ū", "u");
		pinyinMap.put("ú", "u");
		pinyinMap.put("ǔ", "u");
		pinyinMap.put("ù", "u");
		pinyinMap.put("ǖ", "v");
		pinyinMap.put("ǘ", "v");
		pinyinMap.put("ǚ", "v");
		pinyinMap.put("ǜ", "v");

		nameListMap = new LinkedHashMap<String, String>();
//		nameListMap.put("2-15-1.txt", "NPCR2_15_1");
//		nameListMap.put("2-15-2.txt", "NPCR2_15_2");
//		nameListMap.put("2-16-1.txt", "NPCR2_16_1");
//		nameListMap.put("2-16-2.txt", "NPCR2_16_2");
//		nameListMap.put("2-17-1.txt", "NPCR2_17_1");
//		nameListMap.put("2-17-2.txt", "NPCR2_17_2");
//		nameListMap.put("2-18-1.txt", "NPCR2_18_1");
//		nameListMap.put("2-18-2.txt", "NPCR2_18_2");
//		nameListMap.put("2-19-1.txt", "NPCR2_19_1");
//		nameListMap.put("2-19-2.txt", "NPCR2_19_2");
//		nameListMap.put("2-20-1.txt", "NPCR2_20_1");
//		nameListMap.put("2-21-1.txt", "NPCR2_21_1");
//		nameListMap.put("2-21-2.txt", "NPCR2_21_2");
//		nameListMap.put("2-22-1.txt", "NPCR2_22_1");
//		nameListMap.put("2-22-2.txt", "NPCR2_22_2");
//		nameListMap.put("2-23-1.txt", "NPCR2_23_1");
//		nameListMap.put("2-23-2.txt", "NPCR2_23_2");
//		nameListMap.put("2-24-1.txt", "NPCR2_24_1");
//		nameListMap.put("2-24-2.txt", "NPCR2_24_2");
//		nameListMap.put("2-25-1.txt", "NPCR2_25_1");
//		nameListMap.put("2-25-2.txt", "NPCR2_25_2");
//		nameListMap.put("2-26-1.txt", "NPCR2_26_1");

		asciiEncoder = Charset.forName("ISO-8859-1").newEncoder();
		ignoredWords = Arrays.asList("a", "an", "the", "to");

		workDir = System.getProperty("user.dir");
		workDir = workDir.replace("\\", "/");
	}

	/**
	 * Creates a JSON class for each text character file.
	 */
	@Test
	public void createCharJSON() throws Exception {

		for (String name : nameListMap.keySet()) {
			String[] tokens = readTextFile(name);
			StringBuffer entries = new StringBuffer();
			List<WordEntry> wordEntries = new ArrayList<WordEntry>();
			for (String line : tokens) {
				WordEntry wordEntry = parseLine(line);
				if (wordEntry.isEmpty()) {
					System.err.println("Invalid line:" + line);
				} else {
					if (wordEntry.isSingleChar()) {
						wordEntries.add(wordEntry);
						entries.append(wordEntry.entry + ", ");
					}
				}
			}
			String characters = entries.toString();
			characters = characters.substring(0, characters.lastIndexOf(','));

			String mappedName = nameListMap.get(name);
			String lesson = getLessonInfo(mappedName, true);
			String section = getLessonInfo(mappedName, false);
			System.out.println("var " + mappedName + " = {");
			System.out.println("course : 'npcr2',");
			System.out.println("lesson : '" + lesson + "',");
			System.out.println("section : '" + section + "',");
			System.out.println("title : '第" + lesson + "课生词(" + section + ")',");
			System.out.println("entries : '" + characters + "',");
			System.out.println("quizletId : '',");
			System.out.println("characters : [");

			for (WordEntry wordEntry : wordEntries) {
				if (wordEntry.isSingleChar()) {
					System.out.println(wordEntry.toString());
				}
			}
			System.out.println("]}\n");
		}
	}

	/**
	 * Gets the lesson/section information from the lesson name (NPCR2-15-1)
	 * 
	 * @param name
	 * @param isLesson
	 * @return
	 */
	private String getLessonInfo(String name, boolean isLesson) {
		String[] tokens = name.split("_");
		return (isLesson ? tokens[1] : tokens[2]);
	}

	/**
	 * Parse a line of text to create a word entry for finally printing a JSON
	 * class from it.
	 * 
	 * @param line
	 * @return
	 */
	private WordEntry parseLine(String line) {
		StringTokenizer tokens = new StringTokenizer(line, "\t()");
		if (tokens.countTokens() < 3) {
			return new WordEntry("");
		}

		String entry = tokens.nextToken().trim();
		String[] definition = tokens.nextToken().split("-");
		String pinyin = definition[0].trim();
		String translation = definition[1].trim();
		String id = createId(pinyin, translation);

		StringBuffer words = new StringBuffer();
		while (tokens.hasMoreTokens()) {
			String token = tokens.nextToken();
			words.append((asciiEncoder.canEncode(token)) ? "(" + token + ")" : token);
		}

		WordEntry wordEntry = new WordEntry(id, pinyin, entry, translation, words.toString());

		return wordEntry;
	}

	/**
	 * Creates a character ID using the pinyin and the first word of the
	 * translation.
	 * 
	 * @param pinyin
	 * @param translation
	 * @return
	 */
	private String createId(String pinyin, String translation) {
		StringTokenizer words = new StringTokenizer(translation, ", ");
		String id1 = mapPinyin(pinyin).trim().replace(" ", "_");
		String id2 = "";
		while (words.hasMoreTokens()) {
			String word = words.nextToken();
			if (!ignoredWords.contains(word)) {
				id2 = word;
				break;
			}
		}
		return id1 + "-" + id2;
	}

	/**
	 * Maps a toned pinyin to an ascii string.
	 * 
	 * @param pinyin
	 * @return ascii
	 */
	private String mapPinyin(String pinyin) {
		StringBuffer ascii = new StringBuffer();
		String[] charArray = pinyin.split("");

		for (String letter : charArray) {
			if (letter.length() > 0) {
				String mappedLetter = pinyinMap.get(letter.toLowerCase());
				ascii.append((mappedLetter == null) ? letter : mappedLetter);
			}
		}
		return ascii.toString();
	}

	/**
	 * Gets the text from the specified file.
	 * 
	 * @param name
	 * @return
	 * @throws IOException
	 */
	private String[] readTextFile(String name) throws IOException {

		String fileName = workDir + TEXT_DIR + name;
		Path path = FileSystems.getDefault().getPath(fileName);
		List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);

		StringBuffer code = new StringBuffer();
		for (String line : lines) {
			code.append(line);
		}
		return code.toString().split(";");
	}
}

/**
 * A word entry.
 * 
 * @author youping.hu
 */
class WordEntry {
	String id;
	String pinyin;
	String entry;
	String translation;
	String words;
	String usage;

	public WordEntry(String entry) {
		this.entry = entry;
	}

	public WordEntry(String id, String pinyin, String entry, String translation, String words) {
		this.id = id;
		this.pinyin = pinyin;
		this.entry = entry;
		this.translation = translation;
		this.words = words;
		this.usage = null;
	}

	public boolean isSingleChar() {
		return this.entry.length() == 1;
	}

	public boolean isEmpty() {
		return this.entry.length() == 0;
	}

	public String toString() {
		return "{ id: \"" + id + "\",\n" + "pinyin: \"" + pinyin + "\",\n" + "entry: \"" + entry + "\",\n"
				+ "translation: \"" + translation + "\",\n" + "words: \"" + words + "\",\n" + "usage: null},";
	}
}
