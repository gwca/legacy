package org.gwca.classroom;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Before;
import org.junit.Test;

/**
 * Synchronizes all the HTML files with the content of a common HTML file.
 */
public class TestSyncUpHTML {

  private static String CODE_DIR = "/src/main/webapp/";
  private static String COMMON_FILE = "books/all-text/course";
  private static String TEST_BASE_URL = "https://gwca-classroom-test.appspot.com/";
  private static String PROD_BASE_URL = "https://gwca-classroom.appspot.com/";
  private static String FILE_EXT = ".html";

  private String workDir;
  private String baseUrl;

  // files to be generated with the common code
  private Map<String, String> fileMap;

  // files to be updated with the environment specific base URL.
  private List<String> fileList;

  @Before
  public void initFileList() {

    // the right hand file will be generated with the common code and
    // the left hand file and an environment specific base URL.
    fileMap = new HashMap<String, String>();
    fileMap.put("books/all-text/npcr1-text", "books/npcr1/npcr1");
    fileMap.put("books/all-text/npcr2-text", "books/npcr2/npcr2");
    fileMap.put("books/all-text/npcr3-text", "books/npcr3/npcr3");
    fileMap.put("books/all-text/npcr4-text", "books/npcr4/npcr4");
    fileMap.put("books/all-text/npcr5-text", "books/npcr5/npcr5");
    fileMap.put("books/all-text/npcr6-text", "books/npcr6/npcr6");
    fileMap.put("books/all-text/pinyin-text", "books/pinyin/pinyin");
    fileMap.put("books/all-text/reading-text", "books/reading/reading");
    fileMap.put("books/all-text/workshop-text", "books/workshop/workshop");
    fileMap.put("books/all-text/youping-hu-text", "teachers/youping-hu/youping-hu");

    // the files that will be updated with an environment specific base URL
    fileList =
        Arrays.asList("classroom", "books/books", "books/builder/builder", "teachers/teachers");

    // the work directory
    workDir = System.getProperty("user.dir");
    workDir = workDir.replace("\\", "/") + CODE_DIR;

    // set the environment specific base URL.
    String env = System.getProperty("env");
    if (env == null || env.length() == 0 || env.equals("local")) {
      baseUrl = "file://" + workDir;
    } else if (env.equals("prod")) {
      baseUrl = PROD_BASE_URL;
    } else if (env.equals("test")) {
      baseUrl = TEST_BASE_URL;
    } else {
      baseUrl = "file://" + workDir;
    }
  }

  /**
   * Synchronizes all the HTML files.
   */
  @Test
  public void syncUpHTML() throws Exception {

    // get the common code and set the environment specific base URL.
    String commonCode = readHtmlCode(COMMON_FILE);
    Document commonDom = Jsoup.parse(commonCode);
    commonDom.select("base").attr("href", baseUrl);

    // generate an HTML file with the common code and the course text.
    for (String textName : fileMap.keySet()) {
      String textCode = readHtmlCode(textName);
      String courseCode = createHtmlCode(textCode, commonDom);
      String courseName = fileMap.get(textName);
      updateCourseFile(courseName, courseCode);
    }

    // update the files with the environment specific base URL.
    for (String fileName : fileList) {
      String textCode = readHtmlCode(fileName);
      String courseCode = updateHtmlCode(textCode);
      updateCourseFile(fileName, courseCode);
    }
  }

  /**
   * Creates or updates the course HTML file with the synchronized code.
   * 
   * @param name
   * @param pageCode
   * @throws IOException
   */
  private void updateCourseFile(String name, String pageCode) throws IOException {

    // create an empty or truncate the existing HTML file
    String courseFileName = workDir + name + FILE_EXT;
    File file = new File(courseFileName);
    if (file.exists()) {
      FileOutputStream fos = new FileOutputStream(courseFileName, true);
      FileChannel outChannel = fos.getChannel();
      outChannel.truncate(0);
      outChannel.close();
      fos.close();
    } else {
      file.createNewFile();
    }

    // update the file
    OutputStreamWriter sw = null;
    sw = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8);

    BufferedWriter writer = new BufferedWriter(sw);
    writer.write(pageCode);
    writer.flush();
    writer.close();
  }

  /**
   * Synchronizes the HTML file with the common code.
   * 
   * @param pageCode
   * @param commonDom
   * @return
   * @throws IOException
   */
  private String createHtmlCode(String pageCode, Document commonDom) throws IOException {
    Document textDom = Jsoup.parse(pageCode);
    Element textElement = textDom.select("body").get(0);

    Document clonedDocument = commonDom.clone();
    Element codeBody = clonedDocument.select("body").get(0);
    codeBody.append(textElement.html());
    return clonedDocument.html();
  }

  /**
   * Updates the environment specific base element of the HTML file.
   * 
   * @param pageCode
   * @param commonCode
   * @return
   * @throws IOException
   */
  private String updateHtmlCode(String pageCode) throws IOException {
    Document textDom = Jsoup.parse(pageCode);
    textDom.select("base").attr("href", baseUrl);
    return textDom.html();
  }

  /**
   * Gets the HTML code from the specified file.
   * 
   * @param name
   * @return
   * @throws IOException
   */
  private String readHtmlCode(String name) throws IOException {

    String codeFileName = workDir + name + FILE_EXT;
    Path path = FileSystems.getDefault().getPath(codeFileName);
    List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);

    StringBuffer code = new StringBuffer();
    for (String line : lines) {
      code.append(line + "\n");
    }
    return code.toString();
  }
}
