var DICTIONARY_URL = 'http://www.archchinese.com/chinese_english_dictionary.html?find=';
var QUIZLET_URL = 'https://quizlet.com/QUIZLET_ID';

/** The application for the vocabulary builder */
var app = angular.module('builder', []);

/**
 * The vocabulary builder's controller to bind and update the page properties
 * for the view (build.html)
 */
app.controller('builderController', function($scope, $location, svc) {

	/**
	 * Keeps the mouse position for setting the stroke order panel position
	 * dynamically.
	 */
	var pageX = null;
	var pageY = null;

	/**
	 * Declares and initializes the binding properties.
	 */
	$scope.init = function() {

		// some commonly used URLs
		$scope.dictUrl = DICTIONARY_URL;
		$scope.quizletUrl = QUIZLET_URL;

		// add event listeners to keep the current mouse position
		document.addEventListener('mousemove', $scope.onMouseUpdate, false);
		document.addEventListener('mouseenter', $scope.onMouseUpdate, false);

		// the panel title and the list of the characters to display
		$scope.title = '';
		$scope.entryList = '';
		$scope.characters = [];

		// the character stroke show properties
		$scope.strokeSource = '';
		$scope.strokeShow = false;
		$scope.strokeTop = '0px';

		// the searching parameters to look up a character group.
		$scope.course = "npcr1";
		$scope.lesson = "01";
		$scope.section = "1";
		$scope.pinyin = '';

		// parse the query request to initialize the searching
		// parameters.
		var url = $location.absUrl();
		var parameters = $scope.parseQueryString(url);
		if (parameters.length > 0) {
			switch (parameters.length) {
			case 1:
				$scope.course = parameters[0].value;
				break;
			case 2:
				$scope.course = parameters[0].value;
				$scope.lesson = parameters[1].value;
				break;
			default:
				$scope.course = parameters[0].value;
				$scope.lesson = parameters[1].value;
				$scope.section = parameters[2].value;
				break;
			}
		}

		// the available character groups of the current course
		$scope.characterGroups = svc.getCharGroups($scope.course);
		$scope.selectedCharGroup = svc.getCharGroup($scope.characterGroups,
				$scope.lesson, $scope.section);

		// the available courses for building up TOC
		$scope.courseList = [ {
			id : 'npcr1',
			title : 'NPCR-1',
			bgcolor : '',
		}, {
			id : 'npcr2',
			title : 'NPCR-2',
			bgcolor : ''
		} ]

		// highlight the selected TOC item and load the selected
		// characters to the view.
		$scope.highlightSelectedItem($scope.course);
		$scope.loadSelectedCharacters();
	}

	/**
	 * The event handling function for the ng-click event of the TOC selection.
	 */
	$scope.setCourse = function(course) {
		$scope.course = course;
		$scope.characterGroups = svc.getCharGroups(course);
		$scope.highlightSelectedItem(course);
		if ($scope.characterGroups.length > 0) {
			$scope.selectedCharGroup = $scope.characterGroups[0];
			$scope.loadSelectedCharacters();
		}
	}

	/**
	 * The event handling function for the ng-change event of the lesson
	 * selection element to loads the selected characters.
	 */
	$scope.loadSelectedCharacters = function() {

		$scope.course = $scope.selectedCharGroup.course;
		$scope.lesson = $scope.selectedCharGroup.lesson;
		$scope.section = $scope.selectedCharGroup.section;

		var group = svc.getCharGroup($scope.characterGroups, $scope.lesson,
				$scope.section);
		$scope.setDisplayProperties(group);
	}

	/**
	 * The event handling function for the ng-click event of the Quizlet button
	 */
	$scope.doQuizlet = function() {
		var group = svc.getCharGroup($scope.characterGroups, $scope.lesson,
				$scope.section);
		var quizletId = group.quizletId;
		var url = $scope.quizletUrl.replace('QUIZLET_ID', quizletId);
		var quizlet = window.open(url, '_blank');
		quizlet.focus();
	}

	/**
	 * The event handling function for the ng-click event of the Search by
	 * Pinyin button.
	 */
	$scope.loadCharactersByPinyin = function() {
		var group = svc.getGroupByPinyin($scope.pinyin);
		$scope.setDisplayProperties(group);
	}

	/**
	 * Sets the properties that are bound to the page.
	 * 
	 * @param group
	 */
	$scope.setDisplayProperties = function(group) {
		if (group.length == 0) {
			$scope.characters = [];
			return;
		}

		// the panel title and the list of the new characters
		$scope.title = group.title;
		$scope.entryList = group.entries;

		// split Chinese and English words
		for (var i = 0; i < group.characters.length; i++) {
			var char = group.characters[i];
			char.mixedWords = $scope.splitWords(char.words);
			char.mixedUsage = $scope.splitWords(char.usage);
		}
		$scope.characters = group.characters;
	}

	/**
	 * Plays a flash video to show the character strokes.
	 */
	$scope.showStrokes = function(charId) {
		var top = ($scope.getMouseY() - 120);
		$scope.strokeSource = 'swf/' + charId + '.swf';
		$scope.strokeShow = true;
		$scope.strokeTop = top + 'px';
	}

	/**
	 * Parses the query string of the given URL.
	 * 
	 * @param url
	 * @returns a list of name/value pairs.
	 */
	$scope.parseQueryString = function(url) {
		var parameters = [];
		if (url.indexOf('?') > 0) {
			var query = url.split('?')[1];
			var pairs = query.split('&');
			for (var i = 0; i < pairs.length; i++) {
				var pair = pairs[i].split('=');
				parameters.push({
					name : decodeURIComponent(pair[0]),
					value : decodeURIComponent(pair[1])
				})
			}
		}
		return parameters;
	}

	/**
	 * Highlights the selected TOC item.
	 */
	$scope.highlightSelectedItem = function(course) {
		for (var i = 0; i < $scope.courseList.length; i++) {
			if ($scope.courseList[i].id == course) {
				$scope.courseList[i].bgcolor = 'Yellow';
			} else {
				$scope.courseList[i].bgcolor = '';
			}
		}
	}

	/**
	 * Splits Chinese and English words.
	 * 
	 * @param item
	 * @returns
	 */
	$scope.splitWords = function(item) {
		var items = [];
		if (item == null || item.length == 0) {
			items.push({
				chineseWord : '',
				englishWord : ''
			});
			return items;
		}
		var tokens = item.split(')');
		for (var i = 0; i < tokens.length; i++) {
			var token = tokens[i];
			if (token.length > 0) {
				words = token.split('(');
				if (words.length > 1) {
					items.push({
						chineseWord : words[0].trim(),
						englishWord : '(' + words[1].trim() + ')'
					});
				} else {
					items.push({
						chineseWord : token.trim(),
						englishWord : ''
					});
				}
			}
		}
		return items;
	}

	$scope.onMouseUpdate = function(e) {
		pageX = e.pageX;
		pageY = e.pageY;
	}

	$scope.getMouseX = function() {
		return pageX;
	}

	$scope.getMouseY = function() {
		return pageY;
	}
});

/**
 * The vocabulary builder services to look up characters, which are declared in
 * the build-list.js, by different parameters.
 */
app.service('svc', function() {

	/**
	 * Gets a group of characters by lesson and section.
	 * 
	 * @param groups
	 *            the list of groups of the current course
	 * @param lesson
	 * @param section
	 * @returns
	 */
	this.getCharGroup = function(groups, lesson, section) {
		for (var i = 0; i < groups.length; i++) {
			var group = groups[i];
			if (lesson == group.lesson && section == group.section) {
				return group;
			}
		}
		return groups[0];
	}

	/**
	 * Gets a list of character groups of the given course.
	 * 
	 * @param course
	 * @returns {Array}
	 */
	this.getCharGroups = function(course) {
		var groups = [];
		var group = meta = null;
		for (var i = 0; i < AllCharacterGroups.length; i++) {
			group = AllCharacterGroups[i];
			if (course == group.course) {
				groups.push(group);
			}
		}
		return groups;
	}

	/**
	 * Gets a group of characters by Pinyin
	 * 
	 * @param pinyin
	 * @returns
	 */
	this.getGroupByPinyin = function(pinyin) {
		var matchedCharacters = [];
		var key = pinyin.toLowerCase();
		for (var i = 0; i < AllCharacterGroups.length; i++) {
			var group = AllCharacterGroups[i];
			var characters = group.characters;
			for (var j = 0; j < characters.length; j++) {
				var character = characters[j];
				var id = character.id.toLowerCase();
				if (id.startsWith(key)) {
					matchedCharacters.push(character);
				}
			}
		}
		var sortedCharacters = matchedCharacters.sort(function(a, b) {
			var result = 0;
			if (a.id == b.id) {
				result = 0;
			} else {
				if (a.id > b.id) {
					result = 1;
				} else {
					result = -1;
				}
			}
			return result;
		});

		var group = {
			title : '生词 (characters that start with "' + pinyin + '")',
			entries : '',
			characters : sortedCharacters
		}
		return group;
	}
});