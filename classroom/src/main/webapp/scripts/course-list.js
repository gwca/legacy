var AUDIO_FOLDER = 'https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/audio-clips/';
var VIDEO_FOLDER = 'https://docs.google.com/a/greatwallchineseacademy.org/file/d/';

var PINYIN = {
	id : 'pinyin',
	image : 'images/sites/pinyin.png',
	title : '汉语拼音（Pinyin)',
	description : "Welcome to the fascinating world of Chinese phonetics and tones! You'll gain a general understanding of how Chinese phonetics work and become	familiar with different tones. All the video clips are provided by eChineseLearning.com and made available on YouTube.",
	items : [ {
		lesson : '1-1',
		title : 'Lesson 1-1',
		type : 'video',
		media : 'https://www.youtube.com/embed/5HjfI0n7JIM',
		text : [ 'pinyin-more' ]
	}, {
		lesson : '1-2',
		title : 'Lesson 1-2',
		type : 'video',
		media : 'https://www.youtube.com/embed/0VGF_9LgzXk',
		text : [ 'pinyin-more' ]
	}, {
		lesson : '2-0',
		title : 'Lesson 2-0',
		type : 'video',
		media : 'https://www.youtube.com/embed/TZe_E796uQw',
		text : [ 'pinyin-more' ]
	}, {
		lesson : '2-1',
		title : 'Lesson 2-1',
		type : 'video',
		media : 'https://www.youtube.com/embed/ctRQCh8pi6Q',
		text : [ 'pinyin-more' ]
	}, {
		lesson : '2-2',
		title : 'Lesson 2-2',
		type : 'video',
		media : 'https://www.youtube.com/embed/pQaecPxlFV4',
		text : [ 'pinyin-more' ]
	}, {
		lesson : '3-1',
		title : 'Lesson 3-1',
		type : 'video',
		media : 'https://www.youtube.com/embed/2LFSzZyHeMk',
		text : [ 'pinyin-more' ]
	}, {
		lesson : '3-2',
		title : 'Lesson 3-2',
		type : 'video',
		media : 'https://www.youtube.com/embed/IU1vYMjHQZM',
		text : [ 'pinyin-more' ]
	}, {
		lesson : '4-1',
		title : 'Lesson 4-1',
		type : 'video',
		media : 'https://www.youtube.com/embed/o-5vcOklosE',
		text : [ 'pinyin-more' ]
	}, {
		lesson : '4-2',
		title : 'Lesson 4-2',
		type : 'video',
		media : 'https://www.youtube.com/embed/jYKTlEk3_v0',
		text : [ 'pinyin-more' ]
	}, {
		lesson : '5-1',
		title : 'Lesson 5-1',
		type : 'video',
		media : 'https://www.youtube.com/embed/4fI3lSg6xW0',
		text : [ 'pinyin-more' ]
	}, {
		lesson : '5-2',
		title : 'Lesson 5-2',
		type : 'video',
		media : 'https://www.youtube.com/embed/NpDhoOCyd7g',
		text : [ 'pinyin-more' ]
	}, {
		lesson : '6-1',
		title : 'Lesson 6-1',
		type : 'video',
		media : 'https://www.youtube.com/embed/yhH3fOUFjdU',
		text : [ 'pinyin-more' ]
	}, {
		lesson : '6-2',
		title : 'Lesson 6-2',
		type : 'video',
		media : 'https://www.youtube.com/embed/vCO4oZDczaY',
		text : [ 'pinyin-more' ]
	}, {
		lesson : '7-1',
		title : 'Lesson 7-1',
		type : 'video',
		media : 'https://www.youtube.com/embed/3j6AhMJrA0k',
		text : [ 'pinyin-more' ]
	}, {
		lesson : '7-2',
		title : 'Lesson 7-2',
		type : 'video',
		media : 'https://www.youtube.com/embed/sI-2T2ghl9Q',
		text : [ 'pinyin-more' ]
	}, {
		lesson : '8-1',
		title : 'Lesson 8-1',
		type : 'video',
		media : 'https://www.youtube.com/embed/3U07dDmJfnk',
		text : [ 'pinyin-more' ]
	}, {
		lesson : '8-2',
		title : 'Lesson 8-2',
		type : 'video',
		media : 'https://www.youtube.com/embed/argJcFKLO58',
		text : [ 'pinyin-more' ]
	}, {
		lesson : '9-1',
		title : 'Lesson 9-1',
		type : 'video',
		media : 'https://www.youtube.com/embed/xbS6J4O1MRQ',
		text : [ 'pinyin-more' ]
	}, {
		lesson : '9-2',
		title : 'Lesson 9-2',
		type : 'video',
		media : 'https://www.youtube.com/embed/PwjHheMlbDo',
		text : [ 'pinyin-more' ]
	} ]
};

var NPCR1 = {
	id : 'npcr1',
	title : '新实用汉语课本 (1)',
	description : '《新实用汉语课本》是专门为北美地区留学生编写的汉语教材，分六级。第一册以立波和他的同学、朋友为主要人物，在有趣的情节中，将结构、功能与文化有机地糅合在一起，是一套编排科学的初级汉语课本。本书共十四课，供初级阶段学习使用。',
	items : [ {
		lesson : '01',
		section : '1',
		label : 'Text',
		title : '你好',
		type : 'audio',
		media : 'npcr1/NPCR1-01.mp3',
		text : [ 'npcr1-01' ]
	}, {
		lesson : '01',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/R3QnE8D_b9o',
		text : [ 'npcr1-01' ]
	}, {
		lesson : '01',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr1/NPCR1-Workbook-01.mp3',
		text : [ 'npcr1-workbook' ]
	}, {
		lesson : '02',
		section : '1',
		label : 'Text',
		title : '你忙吗',
		type : 'audio',
		media : 'npcr1/NPCR1-02.mp3',
		text : [ 'npcr1-02-1' ]
	}, {
		lesson : '02',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr1/lesson2-essay.mp3',
		text : [ 'npcr1-02-3' ]
	}, {
		lesson : '02',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/l2BEgmTa-BE',
		text : [ 'npcr1-02-1' ]
	}, {
		lesson : '02',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr1/NPCR1-Workbook-02.mp3',
		text : [ 'npcr1-workbook' ]
	}, {
		lesson : '03',
		section : '1',
		label : 'Text 1',
		title : '她是哪国人',
		type : 'audio',
		media : 'npcr1/NPCR1-03-1.mp3',
		text : [ 'npcr1-03-1' ]
	}, {
		lesson : '03',
		section : '2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr1/NPCR1-03-2.mp3',
		text : [ 'npcr1-03-2' ]
	}, {
		lesson : '03',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr1/lesson3-essay.mp3',
		text : [ 'npcr1-03-3' ]
	}, {
		lesson : '03',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/cpU7FE4vd0M',
		text : [ 'npcr1-03-1', 'npcr1-03-2' ]
	}, {
		lesson : '03',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr1/NPCR1-Workbook-03.mp3',
		text : [ 'npcr1-workbook' ]
	}, {
		lesson : '04',
		section : '1',
		label : 'Text 1',
		title : '认识你很高兴',
		type : 'audio',
		media : 'npcr1/NPCR1-04-1.mp3',
		text : [ 'npcr1-04-1' ]
	}, {
		lesson : '04',
		section : '2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr1/NPCR1-04-2.mp3',
		text : [ 'npcr1-04-2' ]
	}, {
		lesson : '04',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr1/lesson4-essay.mp3',
		text : [ 'npcr1-04-3' ]
	}, {
		lesson : '04',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/xQPAfYoRfIs',
		text : [ 'npcr1-04-1', 'npcr1-04-2' ]
	}, {
		lesson : '04',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr1/NPCR1-Workbook-04.mp3',
		text : [ 'npcr1-workbook' ]
	}, {
		lesson : '05',
		section : '1',
		label : 'Text 1',
		title : '餐厅在哪儿',
		type : 'audio',
		media : 'npcr1/NPCR1-05-1.mp3',
		text : [ 'npcr1-05-1' ]
	}, {
		lesson : '05',
		section : '2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr1/NPCR1-05-2.mp3',
		text : [ 'npcr1-05-2' ]
	}, {
		lesson : '05',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr1/lesson5-essay.mp3',
		text : [ 'npcr1-05-3' ]
	}, {
		lesson : '05',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/a3gOnd6LyO0',
		text : [ 'npcr1-05-1', 'npcr1-05-2' ]
	}, {
		lesson : '05',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr1/NPCR1-Workbook-05.mp3',
		text : [ 'npcr1-workbook' ]
	}, {
		lesson : '06',
		section : '1',
		label : 'Text 1',
		title : '我们去游泳，好吗',
		type : 'audio',
		media : 'npcr1/NPCR1-06-1.mp3',
		text : [ 'npcr1-06-1' ]
	}, {
		lesson : '06',
		section : '2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr1/NPCR1-06-2.mp3',
		text : [ 'npcr1-06-2' ]
	}, {
		lesson : '06',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr1/lesson6-essay.mp3',
		text : [ 'npcr1-06-3' ]
	}, {
		lesson : '06',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/wEpNmTJ7PPI',
		text : [ 'npcr1-06-1', 'npcr1-06-2' ]
	}, {
		lesson : '06',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr1/NPCR1-Workbook-06.mp3',
		text : [ 'npcr1-workbook' ]
	}, {
		lesson : '07',
		section : '1',
		label : 'Text 1',
		title : '你认识不认识他',
		type : 'audio',
		media : 'npcr1/NPCR1-07-1.mp3',
		text : [ 'npcr1-07-1' ]
	}, {
		lesson : '07',
		section : '2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr1/NPCR1-07-2.mp3',
		text : [ 'npcr1-07-2' ]
	}, {
		lesson : '07',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr1/lesson7-essay.mp3',
		text : [ 'npcr1-07-3' ]
	}, {
		lesson : '07',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/BkAHskghoJI',
		text : [ 'npcr1-07-1', 'npcr1-07-2' ]
	}, {
		lesson : '07',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr1/NPCR1-Workbook-07.mp3',
		text : [ 'npcr1-workbook' ]
	}, {
		lesson : '08',
		section : '1',
		label : 'Text 1',
		title : '你们家有几口人',
		type : 'audio',
		media : 'npcr1/NPCR1-08-1.mp3',
		text : [ 'npcr1-08-1' ]
	}, {
		lesson : '08',
		section : '2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr1/NPCR1-08-2.mp3',
		text : [ 'npcr1-08-2' ]
	}, {
		lesson : '08',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr1/lesson8-essay.mp3',
		text : [ 'npcr1-08-3' ]
	}, {
		lesson : '08',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/tu0LhQlwdO0',
		text : [ 'npcr1-08-1', 'npcr1-08-2' ]
	}, {
		lesson : '08',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr1/NPCR1-Workbook-08.mp3',
		text : [ 'npcr1-workbook' ]
	}, {
		lesson : '09',
		section : '1',
		label : 'Text 1',
		title : '他今年二十岁',
		type : 'audio',
		media : 'npcr1/NPCR1-09-1.mp3',
		text : [ 'npcr1-09-1' ]
	}, {
		lesson : '09',
		section : '2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr1/NPCR1-09-2.mp3',
		text : [ 'npcr1-09-2' ]
	}, {
		lesson : '09',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr1/lesson9-essay.mp3',
		text : [ 'npcr1-09-3' ]
	}, {
		lesson : '09',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/mmZOJI5FLoQ',
		text : [ 'npcr1-09-1', 'npcr1-09-2' ]
	}, {
		lesson : '09',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr1/NPCR1-Workbook-09.mp3',
		text : [ 'npcr1-workbook' ]
	}, {
		lesson : '10',
		section : '1',
		label : 'Text 1',
		title : '我在这儿买光盘',
		type : 'audio',
		media : 'npcr1/NPCR1-10-1.mp3',
		text : [ 'npcr1-10-1' ]
	}, {
		lesson : '10',
		section : '2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr1/NPCR1-10-2.mp3',
		text : [ 'npcr1-10-2' ]
	}, {
		lesson : '10',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr1/lesson10-essay.mp3',
		text : [ 'npcr1-10-3' ]
	}, {
		lesson : '10',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/6AL-rzeNqgU',
		text : [ 'npcr1-10-1', 'npcr1-10-2' ]
	}, {
		lesson : '10',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr1/NPCR1-Workbook-10.mp3',
		text : [ 'npcr1-workbook' ]
	}, {
		lesson : '11',
		section : '1',
		label : 'Text 1',
		title : '我会说一点儿汉语',
		type : 'audio',
		media : 'npcr1/NPCR1-11-1.mp3',
		text : [ 'npcr1-11-1' ]
	}, {
		lesson : '11',
		section : '2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr1/NPCR1-11-2.mp3',
		text : [ 'npcr1-11-2' ]
	}, {
		lesson : '11',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr1/lesson11-essay.mp3',
		text : [ 'npcr1-11-3' ]
	}, {
		lesson : '11',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/CLYhx-QWAdg',
		text : [ 'npcr1-11-1', 'npcr1-11-2' ]
	}, {
		lesson : '11',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr1/NPCR1-Workbook-11.mp3',
		text : [ 'npcr1-workbook' ]
	}, {
		lesson : '12',
		section : '1',
		label : 'Text 1',
		title : '我全身都不舒服',
		type : 'audio',
		media : 'npcr1/NPCR1-12-1.mp3',
		text : [ 'npcr1-12-1' ]
	}, {
		lesson : '12',
		section : '2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr1/NPCR1-12-2.mp3',
		text : [ 'npcr1-12-2' ]
	}, {
		lesson : '12',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr1/lesson12-essay.mp3',
		text : [ 'npcr1-12-3' ]
	}, {
		lesson : '12',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/fjK0pm3JjFA',
		text : [ 'npcr1-12-1', 'npcr1-12-2' ]
	}, {
		lesson : '12',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr1/NPCR1-Workbook-12.mp3',
		text : [ 'npcr1-workbook' ]
	}, {
		lesson : '13',
		section : '1',
		label : 'Text 1',
		title : '漂亮的姑娘',
		type : 'audio',
		media : 'npcr1/NPCR1-13-1.mp3',
		text : [ 'npcr1-13-1' ]
	}, {
		lesson : '13',
		section : '2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr1/NPCR1-13-2.mp3',
		text : [ 'npcr1-13-2' ]
	}, {
		lesson : '13',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr1/lesson13-essay.mp3',
		text : [ 'npcr1-13-3' ]
	}, {
		lesson : '13',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/qyTe3o_6UOk',
		text : [ 'npcr1-13-1', 'npcr1-13-2' ]
	}, {
		lesson : '13',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr1/NPCR1-Workbook-13.mp3',
		text : [ 'npcr1-workbook' ]
	}, {
		lesson : '14',
		section : '1',
		label : 'Text',
		title : '祝你圣诞快乐',
		type : 'audio',
		media : 'npcr1/NPCR1-14.mp3',
		text : [ 'npcr1-14' ]
	}, {
		lesson : '14',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/qjt9I1pIGA0',
		text : [ 'npcr1-14' ]
	}, {
		lesson : '14',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr1/NPCR1-Workbook-14.mp3',
		text : [ 'npcr1-workbook' ]
	} ]
};

var NPCR2 = {
	id : 'npcr2',
	title : '新实用汉语课本 (2)',
	description : '本书为《新实用汉语课本》第二册，共十二课（第15—26课）。每课包括课文、生词、注释、练习与运用、阅读和复述、语法、汉字及文化知识等内容，书末附有繁体字课文及词汇、汉字索引以及生词、语法的注释。本书供初级阶段学习使用。',
	items : [ {
		lesson : '15',
		section : '1',
		label : 'Text 1',
		title : '她去上海了',
		type : 'audio',
		media : 'npcr2/NPCR2-15-1.mp3',
		text : [ 'npcr2-15-1' ]
	}, {
		lesson : '15',
		section : '2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr2/NPCR2-15-2.mp3',
		text : [ 'npcr2-15-2' ]
	}, {
		lesson : '15',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr2/NPCR2-15-3.mp3',
		text : [ 'npcr2-15-3' ]
	}, {
		lesson : '15',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/1A1WOi7sSXA',
		text : [ 'npcr2-15-1', 'npcr2-15-2' ]
	}, {
		lesson : '15',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr2/NPCR2-Workbook-01.mp3',
		text : [ 'npcr2-workbook' ]
	}, {
		lesson : '16',
		section : '1',
		label : 'Text 1',
		title : '我把这事儿忘了',
		type : 'audio',
		media : 'npcr2/NPCR2-16-1.mp3',
		text : [ 'npcr2-16-1' ]
	}, {
		lesson : '16',
		section : '2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr2/NPCR2-16-2.mp3',
		text : [ 'npcr2-16-2' ]
	}, {
		lesson : '16',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr2/NPCR2-16-3.mp3',
		text : [ 'npcr2-16-3' ]
	}, {
		lesson : '16',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/jp9pOmg-JPQ',
		text : [ 'npcr2-16-1', 'npcr2-16-2' ]
	}, {
		lesson : '16',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr2/NPCR2-Workbook-02.mp3',
		text : [ 'npcr2-workbook' ]
	}, {
		lesson : '17',
		section : '1',
		label : 'Text 1',
		title : '这件旗袍比那件漂亮',
		type : 'audio',
		media : 'npcr2/NPCR2-17-1.mp3',
		text : [ 'npcr2-17-1' ]
	}, {
		lesson : '17',
		section : '2',
		label : 'Text 2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr2/NPCR2-17-2.mp3',
		text : [ 'npcr2-17-2' ]
	}, {
		lesson : '17',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr2/NPCR2-17-3.mp3',
		text : [ 'npcr2-17-3' ]
	}, {
		lesson : '17',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/O5qPK22dH_8',
		text : [ 'npcr2-17-1', 'npcr2-17-2' ]
	}, {
		lesson : '17',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr2/NPCR2-Workbook-03.mp3',
		text : [ 'npcr2-workbook' ]
	}, {
		lesson : '18',
		section : '1',
		label : 'Text 1',
		title : '我听懂了，可是记错了',
		type : 'audio',
		media : 'npcr2/NPCR2-18-1.mp3',
		text : [ 'npcr2-18-1' ]
	}, {
		lesson : '18',
		section : '2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr2/NPCR2-18-2.mp3',
		text : [ 'npcr2-18-2' ]
	}, {
		lesson : '18',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr2/NPCR2-18-3.mp3',
		text : [ 'npcr2-18-3' ]
	}, {
		lesson : '18',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/LrLgKqlqxVA',
		text : [ 'npcr2-18-1', 'npcr2-18-2' ]
	}, {
		lesson : '18',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr2/NPCR2-Workbook-04.mp3',
		text : [ 'npcr2-workbook' ]
	}, {
		lesson : '19',
		section : '1',
		label : 'Text 1',
		title : '中国画跟油画不一样',
		type : 'audio',
		media : 'npcr2/NPCR2-19-1.mp3',
		text : [ 'npcr2-19-1' ]
	}, {
		lesson : '19',
		section : '2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr2/NPCR2-19-2.mp3',
		text : [ 'npcr2-19-2' ]
	}, {
		lesson : '19',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr2/NPCR2-19-3.mp3',
		text : [ 'npcr2-19-3' ]
	}, {
		lesson : '19',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/R4xLzIF2e4c',
		text : [ 'npcr2-19-1', 'npcr2-19-1' ]
	}, {
		lesson : '19',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr2/NPCR2-Workbook-05.mp3',
		text : [ 'npcr2-workbook' ]
	}, {
		lesson : '20',
		section : '1',
		label : 'Text 1',
		title : '过新年',
		type : 'audio',
		media : 'npcr2/NPCR2-20-1.mp3',
		text : [ 'npcr2-20-1' ]
	}, {
		lesson : '20',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr2/NPCR2-20-3.mp3',
		text : [ 'npcr2-20-3' ]
	}, {
		lesson : '20',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/CR-nBmDazUo',
		text : [ 'npcr2-20-1' ]
	}, {
		lesson : '20',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr2/NPCR2-Workbook-06.mp3',
		text : [ 'npcr2-workbook' ]
	}, {
		lesson : '21',
		section : '1',
		label : 'Text 1',
		title : '我们的队员',
		type : 'audio',
		media : 'npcr2/NPCR2-21-1.mp3',
		text : [ 'npcr2-21-1' ]
	}, {
		lesson : '21',
		section : '2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr2/NPCR2-21-2.mp3',
		text : [ 'npcr2-21-2' ]
	}, {
		lesson : '21',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr2/NPCR2-21-3.mp3',
		text : [ 'npcr2-21-3' ]
	}, {
		lesson : '21',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/jy8ts9nzfJY',
		text : [ 'npcr2-21-1', 'npcr2-21-2' ]
	}, {
		lesson : '21',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr2/NPCR2-Workbook-07.mp3',
		text : [ 'npcr2-workbook' ]
	}, {
		lesson : '22',
		section : '1',
		label : 'Text 1',
		title : '你看过越剧没有',
		type : 'audio',
		media : 'npcr2/NPCR2-22-1.mp3',
		text : [ 'npcr2-22-1' ]
	}, {
		lesson : '22',
		section : '2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr2/NPCR2-22-2.mp3',
		text : [ 'npcr2-22-2' ]
	}, {
		lesson : '22',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr2/NPCR2-22-3.mp3',
		text : [ 'npcr2-22-3' ]
	}, {
		lesson : '22',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/ZRUQq0A0VxQ',
		text : [ 'npcr2-22-1', 'npcr2-22-2' ]
	}, {
		lesson : '22',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr2/NPCR2-Workbook-08.mp3',
		text : [ 'npcr2-workbook' ]
	}, {
		lesson : '23',
		section : '1',
		label : 'Text 1',
		title : '我们爬上长城来了',
		type : 'audio',
		media : 'npcr2/NPCR2-23-1.mp3',
		text : [ 'npcr2-23-1' ]
	}, {
		lesson : '23',
		section : '2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr2/NPCR2-23-2.mp3',
		text : [ 'npcr2-23-2' ]
	}, {
		lesson : '23',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr2/NPCR2-23-3.mp3',
		text : [ 'npcr2-23-3' ]
	}, {
		lesson : '23',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/3nr3XlpLhjY',
		text : [ 'npcr2-23-1', 'npcr2-23-2' ]
	}, {
		lesson : '23',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr2/NPCR2-Workbook-09.mp3',
		text : [ 'npcr2-workbook' ]
	}, {
		lesson : '24',
		section : '1',
		label : 'Text 1',
		title : '你舅妈也开始用电脑了',
		type : 'audio',
		media : 'npcr2/NPCR2-24-1.mp3',
		text : [ 'npcr2-24-1' ]
	}, {
		lesson : '24',
		section : '2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr2/NPCR2-24-2.mp3',
		text : [ 'npcr2-24-2' ]
	}, {
		lesson : '24',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr2/NPCR2-24-3.mp3',
		text : [ 'npcr2-24-3' ]
	}, {
		lesson : '24',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/qADpckOjkKM',
		text : [ 'npcr2-24-1', 'npcr2-24-2' ]
	}, {
		lesson : '24',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr2/NPCR2-Workbook-10.mp3',
		text : [ 'npcr2-workbook' ]
	}, {
		lesson : '25',
		section : '1',
		label : 'Text 1',
		title : '送我们到医院',
		type : 'audio',
		media : 'npcr2/NPCR2-25-1.mp3',
		text : [ 'npcr2-25-1' ]
	}, {
		lesson : '25',
		section : '2',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr2/NPCR2-25-2.mp3',
		text : [ 'npcr2-25-2' ]
	}, {
		lesson : '25',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr2/NPCR2-25-3.mp3',
		text : [ 'npcr2-25-3' ]
	}, {
		lesson : '25',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/phyM4PSmtXI',
		text : [ 'npcr2-25-1', 'npcr2-25-2' ]
	}, {
		lesson : '25',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr2/NPCR2-Workbook-11.mp3',
		text : [ 'npcr2-workbook' ]
	}, {
		lesson : '26',
		section : '1',
		label : 'Text 1',
		title : '你快要成“中国通”了',
		type : 'audio',
		media : 'npcr2/NPCR2-26-1.mp3',
		text : [ 'npcr2-26-1' ]
	}, {
		lesson : '26',
		section : '3',
		label : 'Additional Reading',
		type : 'audio',
		media : 'npcr2/NPCR2-26-3.mp3',
		text : [ 'npcr2-26-3' ]
	}, {
		lesson : '26',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/z-Epwubbb1M',
		text : [ 'npcr2-26-1' ]
	}, {
		lesson : '26',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr2/NPCR2-Workbook-12.mp3',
		text : [ 'npcr2-workbook' ]
	} ]
};

var NPCR3 = {
	id : 'npcr3',
	title : '新实用汉语课本(3)',
	description : '本书为《新实用汉语课本》第三册，共十二课（从第27-38课）。含对话、生词、语法、练习等内容，并附有生词、语法的注释。本书供初级阶段学习者使用。',
	items : [ {
		lesson : '27',
		section : '1',
		label : 'Text',
		title : '入乡随俗',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-01.mp3',
		text : [ 'npcr3-textbook' ]
	}, {
		lesson : '27',
		section : '2',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-01.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '27',
		section : '3',
		type : 'audio',
		media : 'npcr3/lesson3-essay.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '27',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/QZEYEEuimCE',
		text : [ 'npcr3-book' ]
	}, {
		lesson : '27',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr3/NPCR3-Workbook-01.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '28',
		section : '1',
		label : 'Text',
		title : '礼轻情意重',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-02.mp3',
		text : [ 'npcr3-textbook' ]
	}, {
		lesson : '28',
		section : '2',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-03.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '28',
		section : '3',
		type : 'audio',
		media : 'npcr3/lesson3-essay.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '28',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/mlUhWQyXREw',
		text : [ 'npcr3-book' ]
	}, {
		lesson : '28',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr3/NPCR3-Workbook-02.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '29',
		section : '1',
		label : 'Text',
		title : '请多提意见',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-03.mp3',
		text : [ 'npcr3-textbook' ]
	}, {
		lesson : '29',
		section : '2',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-03.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '29',
		section : '3',
		type : 'audio',
		media : 'npcr3/lesson3-essay.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '29',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/8gESEBfMZjY',
		text : [ 'npcr3-book' ]
	}, {
		lesson : '29',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr3/NPCR3-Workbook-03.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '30',
		section : '1',
		label : 'Text',
		title : '他们是练太级剑的',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-04.mp3',
		text : [ 'npcr3-textbook' ]
	}, {
		lesson : '30',
		section : '2',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-04.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '30',
		section : '3',
		type : 'audio',
		media : 'npcr3/lesson4-essay.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '30',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/ULYerbAcFm4',
		text : [ 'npcr3-book' ]
	}, {
		lesson : '30',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr3/NPCR3-Workbook-04.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '31',
		section : '1',
		label : 'Text',
		title : '中国人叫她“母亲河”',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-05.mp3',
		text : [ 'npcr3-textbook' ]
	}, {
		lesson : '31',
		section : '2',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-05.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '31',
		section : '3',
		type : 'audio',
		media : 'npcr3/lesson5-essay.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '31',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/n7KShh_LdL4',
		text : [ 'npcr3-book' ]
	}, {
		lesson : '31',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr3/NPCR3-Workbook-05.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '32',
		section : '1',
		label : 'Text',
		title : '这样的问题现在也不能问了',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-06.mp3',
		text : [ 'npcr3-textbook' ]
	}, {
		lesson : '32',
		section : '2',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-06.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '32',
		section : '3',
		type : 'audio',
		media : 'npcr3/lesson6-essay.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '32',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/z7xJmtDa_EI',
		text : [ 'npcr3-book' ]
	}, {
		lesson : '32',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr3/NPCR3-Workbook-06.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '33',
		section : '1',
		label : 'Text',
		title : '保护环境就是保护我们自己',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-07.mp3',
		text : [ 'npcr3-textbook' ]
	}, {
		lesson : '33',
		section : '2',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-07.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '33',
		section : '3',
		type : 'audio',
		media : 'npcr3/lesson7-essay.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '33',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/J16znTLj_wo',
		text : [ 'npcr3-book' ]
	}, {
		lesson : '33',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr3/NPCR3-Workbook-07.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '34',
		section : '1',
		label : 'Text',
		title : '神女峰的传说',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-08.mp3',
		text : [ 'npcr3-textbook' ]
	}, {
		lesson : '34',
		section : '2',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-08.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '34',
		section : '3',
		type : 'audio',
		media : 'npcr3/lesson8-essay.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '34',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/N-orLr1MZr4',
		text : [ 'npcr3-book' ]
	}, {
		lesson : '34',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr3/NPCR3-Workbook-08.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '35',
		section : '1',
		label : 'Text',
		title : '汽车我先开着',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-09.mp3',
		text : [ 'npcr3-textbook' ]
	}, {
		lesson : '35',
		section : '2',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-09.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '35',
		section : '3',
		type : 'audio',
		media : 'npcr3/lesson9-essay.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '35',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/5pezS34U6J8',
		text : [ 'npcr3-book' ]
	}, {
		lesson : '35',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr3/NPCR3-Workbook-09.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '36',
		section : '1',
		label : 'Text',
		title : '北京热起来了',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-10.mp3',
		text : [ 'npcr3-textbook' ]
	}, {
		lesson : '36',
		section : '2',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-10.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '36',
		section : '3',
		type : 'audio',
		media : 'npcr3/lesson10-essay.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '36',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/j05Nr8_ZWds',
		text : [ 'npcr3-book' ]
	}, {
		lesson : '36',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr3/NPCR3-Workbook-10.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '37',
		section : '1',
		label : 'Text',
		title : '谁来埋单',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-11.mp3',
		text : [ 'npcr3-textbook' ]
	}, {
		lesson : '37',
		section : '2',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-11.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '37',
		section : '3',
		type : 'audio',
		media : 'npcr3/lesson11-essay.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '37',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/UQr5Zkx5Qm8',
		text : [ 'npcr3-book' ]
	}, {
		lesson : '37',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr3/NPCR3-Workbook-11.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '38',
		section : '1',
		label : 'Text',
		title : '你听，他叫我“太太”',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-12.mp3',
		text : [ 'npcr3-textbook' ]
	}, {
		lesson : '38',
		section : '2',
		type : 'audio',
		media : 'npcr3/NPCR3-Textbook-12.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '38',
		section : '3',
		type : 'audio',
		media : 'npcr3/lesson12-essay.mp3',
		text : [ 'npcr3-workbook' ]
	}, {
		lesson : '38',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/5SQft7C7Xk8',
		text : [ 'npcr3-book' ]
	}, {
		lesson : '38',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr3/NPCR3-Workbook-12.mp3',
		text : [ 'npcr3-workbook' ]
	} ]
};

var NPCR4 = {
	id : 'npcr4',
	title : '新实用汉语课本(4)',
	description : '第四册紧接前三册内容，为学习者提供与时俱进的语言知识学习和技能操练的内容板块，同时每课讨论的话题均为与中国国情、习俗和文化密切相关的内容，练习丰富多样。本书可进一步培养学习者运用汉语进行交际的能力。',
	items : [ {
		lesson : '39',
		section : '1',
		label : 'Text',
		title : '第三十九课',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-39.mp3',
		text : [ 'npcr4-textbook' ]
	}, {
		lesson : '39',
		section : '2',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-39.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '39',
		section : '3',
		type : 'audio',
		media : 'npcr4/lesson3-essay.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '39',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/7c9yCy8ay6o',
		text : [ 'npcr4-book' ]
	}, {
		lesson : '39',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr4/NPCR4-Workbook-39.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '40',
		section : '1',
		label : 'Text',
		title : '第四十课',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-40.mp3',
		text : [ 'npcr4-textbook' ]
	}, {
		lesson : '40',
		section : '2',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-40.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '40',
		section : '3',
		type : 'audio',
		media : 'npcr4/lesson3-essay.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '40',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/-k3TrvD7u0A',
		text : [ 'npcr4-book' ]
	}, {
		lesson : '40',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr4/NPCR4-Workbook-40.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '41',
		section : '1',
		label : 'Text',
		title : '第四十一课',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-41.mp3',
		text : [ 'npcr4-textbook' ]
	}, {
		lesson : '41',
		section : '2',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-41.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '41',
		section : '3',
		type : 'audio',
		media : 'npcr4/lesson3-essay.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '41',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/4otoW9UQoAU',
		text : [ 'npcr4-book' ]
	}, {
		lesson : '41',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr4/NPCR4-Workbook-41.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '42',
		section : '1',
		label : 'Text',
		title : '第四十二课',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-42.mp3',
		text : [ 'npcr4-textbook' ]
	}, {
		lesson : '42',
		section : '2',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-42.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '42',
		section : '3',
		type : 'audio',
		media : 'npcr4/lesson4-essay.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '42',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/OPeSvukrVZY',
		text : [ 'npcr4-book' ]
	}, {
		lesson : '42',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr4/NPCR4-Workbook-42.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '43',
		section : '1',
		label : 'Text',
		title : '第四十三课',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-43.mp3',
		text : [ 'npcr4-textbook' ]
	}, {
		lesson : '43',
		section : '2',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-43.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '43',
		section : '3',
		type : 'audio',
		media : 'npcr4/lesson5-essay.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '43',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/pscA6OBxsFU',
		text : [ 'npcr4-book' ]
	}, {
		lesson : '43',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr4/NPCR4-Workbook-43.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '44',
		section : '1',
		label : 'Text',
		title : '第四十四课',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-44.mp3',
		text : [ 'npcr4-textbook' ]
	}, {
		lesson : '44',
		section : '2',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-44.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '44',
		section : '3',
		type : 'audio',
		media : 'npcr4/lesson6-essay.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '44',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/TEPbkoUyv14',
		text : [ 'npcr4-book' ]
	}, {
		lesson : '44',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr4/NPCR4-Workbook-44.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '45',
		section : '1',
		label : 'Text',
		title : '第四十五课',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-45.mp3',
		text : [ 'npcr4-textbook' ]
	}, {
		lesson : '45',
		section : '2',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-45.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '45',
		section : '3',
		type : 'audio',
		media : 'npcr4/lesson7-essay.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '45',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/sJbimPTJJwQ',
		text : [ 'npcr4-book' ]
	}, {
		lesson : '45',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr4/NPCR4-Workbook-45.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '46',
		section : '1',
		label : 'Text',
		title : '第四十六课',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-46.mp3',
		text : [ 'npcr4-textbook' ]
	}, {
		lesson : '46',
		section : '2',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-46.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '46',
		section : '3',
		type : 'audio',
		media : 'npcr4/lesson8-essay.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '46',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/YaL4DFXNwBQ',
		text : [ 'npcr4-book' ]
	}, {
		lesson : '46',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr4/NPCR4-Workbook-46.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '47',
		section : '1',
		label : 'Text',
		title : '第四十七课',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-47.mp3',
		text : [ 'npcr4-textbook' ]
	}, {
		lesson : '47',
		section : '2',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-47.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '47',
		section : '3',
		type : 'audio',
		media : 'npcr4/lesson9-essay.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '47',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/I9bqVu462Gw',
		text : [ 'npcr4-book' ]
	}, {
		lesson : '47',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr4/NPCR4-Workbook-47.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '48',
		section : '1',
		label : 'Text',
		title : '第四十八课',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-48.mp3',
		text : [ 'npcr4-textbook' ]
	}, {
		lesson : '48',
		section : '2',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-48.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '48',
		section : '3',
		type : 'audio',
		media : 'npcr4/lesson10-essay.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '48',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/7Wctv2R35WA',
		text : [ 'npcr4-book' ]
	}, {
		lesson : '48',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr4/NPCR4-Workbook-48.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '49',
		section : '1',
		label : 'Text',
		title : '第四十九课',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-49.mp3',
		text : [ 'npcr4-textbook' ]
	}, {
		lesson : '49',
		section : '2',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-49.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '49',
		section : '3',
		type : 'audio',
		media : 'npcr4/lesson11-essay.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '49',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/8HDJW65ABJM',
		text : [ 'npcr4-book' ]
	}, {
		lesson : '49',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr4/NPCR4-Workbook-49.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '50',
		section : '1',
		label : 'Text',
		title : '第五十课',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-50.mp3',
		text : [ 'npcr4-textbook' ]
	}, {
		lesson : '50',
		section : '2',
		type : 'audio',
		media : 'npcr4/NPCR4-Textbook-50.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '50',
		section : '3',
		type : 'audio',
		media : 'npcr4/lesson12-essay.mp3',
		text : [ 'npcr4-workbook' ]
	}, {
		lesson : '50',
		section : '4',
		label : 'Video',
		type : 'video',
		media : 'https://www.youtube.com/embed/SAWSp5GdmyA',
		text : [ 'npcr4-book' ]
	}, {
		lesson : '50',
		section : '5',
		label : 'Workbook',
		type : 'audio',
		media : 'npcr4/NPCR4-Workbook-50.mp3',
		text : [ 'npcr4-workbook' ]
	} ]
};

var NPCR5 = {
	id : 'npcr5',
	title : '新实用汉语课本(5)',
	description : '本册为中级阶段课本，共十课。以学习者为中心，采用圆周式编排方法，语言结构、功能、文化等因素多次循环重现。全书内容题材广泛，版块式安排，使核心内容和补充内容分割清晰又易于根据需要进行选择。每课包括课文、生词、注释、练习、运用、会话练习、阅读、复述、语音、语音练习、语法、汉字和文化知识等内容。',
	items : [ {
		lesson : '51',
		section : '0',
		label : 'Text 1',
		title : '母爱',
		type : 'audio',
		media : 'npcr5/NPCR5-51-0.mp3',
		text : [ 'npcr5-51-0' ]
	}, {
		lesson : '51',
		section : '1',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr5/NPCR5-51-1.mp3',
		text : [ 'npcr5-51-1' ]
	}, {
		lesson : '51',
		section : '2',
		label : 'Listening 1',
		type : 'audio',
		media : 'npcr5/NPCR5-51-2.mp3',
		text : [ 'npcr5-51-2' ]
	}, {
		lesson : '51',
		section : '3',
		label : 'Listening 2',
		type : 'audio',
		media : 'npcr5/NPCR5-51-3.mp3',
		text : [ 'npcr5-51-3' ]
	}, {
		lesson : '51',
		section : '4',
		label : 'Listening 3',
		type : 'audio',
		media : 'npcr5/NPCR5-51-4.mp3',
		text : [ 'npcr5-51-4' ]
	}, {
		lesson : '52',
		section : '0',
		label : 'Text 1',
		title : '祝你情人节快乐',
		type : 'audio',
		media : 'npcr5/NPCR5-52-0.mp3',
		text : [ 'npcr5-52-0' ]
	}, {
		lesson : '52',
		section : '1',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr5/NPCR5-52-1.mp3',
		text : [ 'npcr5-52-1' ]
	}, {
		lesson : '52',
		section : '2',
		label : 'Listening 1',
		type : 'audio',
		media : 'npcr5/NPCR5-52-2.mp3',
		text : [ 'npcr5-52-2' ]
	}, {
		lesson : '52',
		section : '3',
		label : 'Listening 2',
		type : 'audio',
		media : 'npcr5/NPCR5-52-3.mp3',
		text : [ 'npcr5-52-3' ]
	}, {
		lesson : '52',
		section : '4',
		label : 'Listening 3',
		type : 'audio',
		media : 'npcr5/NPCR5-52-4.mp3',
		text : [ 'npcr5-52-4' ]
	}, {
		lesson : '53',
		section : '0',
		label : 'Text 1',
		title : '五味',
		type : 'audio',
		media : 'npcr5/NPCR5-53-0.mp3',
		text : [ 'npcr5-53-0' ]
	}, {
		lesson : '53',
		section : '1',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr5/NPCR5-53-1.mp3',
		text : [ 'npcr5-53-1' ]
	}, {
		lesson : '53',
		section : '2',
		label : 'Listening 1',
		type : 'audio',
		media : 'npcr5/NPCR5-53-2.mp3',
		text : [ 'npcr5-53-2' ]
	}, {
		lesson : '53',
		section : '3',
		label : 'Listening 2',
		type : 'audio',
		media : 'npcr5/NPCR5-53-3.mp3',
		text : [ 'npcr5-53-3' ]
	}, {
		lesson : '54',
		section : '0',
		label : 'Text 1',
		title : '让我迷恋的北京城',
		type : 'audio',
		media : 'npcr5/NPCR5-54-0.mp3',
		text : [ 'npcr5-54-0' ]
	}, {
		lesson : '54',
		section : '1',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr5/NPCR5-54-1.mp3',
		text : [ 'npcr5-54-1' ]
	}, {
		lesson : '54',
		section : '2',
		label : 'Listening 1',
		type : 'audio',
		media : 'npcr5/NPCR5-54-2.mp3',
		text : [ 'npcr5-54-2' ]
	}, {
		lesson : '54',
		section : '3',
		label : 'Listening 2',
		type : 'audio',
		media : 'npcr5/NPCR5-54-3.mp3',
		text : [ 'npcr5-54-3' ]
	}, {
		lesson : '54',
		section : '4',
		label : 'Listening 3',
		type : 'audio',
		media : 'npcr5/NPCR5-54-4.mp3',
		text : [ 'npcr5-54-4' ]
	}, {
		lesson : '55',
		section : '0',
		label : 'Text 1',
		title : '新素食主义来了',
		type : 'audio',
		media : 'npcr5/NPCR5-55-0.mp3',
		text : [ 'npcr5-55-0' ]
	}, {
		lesson : '55',
		section : '1',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr5/NPCR5-55-1.mp3',
		text : [ 'npcr5-55-1' ]
	}, {
		lesson : '55',
		section : '2',
		label : 'Listening 1',
		type : 'audio',
		media : 'npcr5/NPCR5-55-2.mp3',
		text : [ 'npcr5-55-2' ]
	}, {
		lesson : '55',
		section : '3',
		label : 'Listening 2',
		type : 'audio',
		media : 'npcr5/NPCR5-55-3.mp3',
		text : [ 'npcr5-55-3' ]
	}, {
		lesson : '55',
		section : '4',
		label : 'Listening 3',
		type : 'audio',
		media : 'npcr5/NPCR5-55-4.mp3',
		text : [ 'npcr5-55-4' ]
	}, {
		lesson : '56',
		section : '0',
		label : 'Text 1',
		title : '杂交水稻之父',
		type : 'audio',
		media : 'npcr5/NPCR5-56-0.mp3',
		text : [ 'npcr5-56-0' ]
	}, {
		lesson : '56',
		section : '1',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr5/NPCR5-56-1.mp3',
		text : [ 'npcr5-56-1' ]
	}, {
		lesson : '56',
		section : '2',
		label : 'Listening 1',
		type : 'audio',
		media : 'npcr5/NPCR5-56-2.mp3',
		text : [ 'npcr5-56-2' ]
	}, {
		lesson : '56',
		section : '3',
		label : 'Listening 2',
		type : 'audio',
		media : 'npcr5/NPCR5-56-3.mp3',
		text : [ 'npcr5-56-3' ]
	}, {
		lesson : '56',
		section : '4',
		label : 'Listening 3',
		type : 'audio',
		media : 'npcr5/NPCR5-56-4.mp3',
		text : [ 'npcr5-56-4' ]
	}, {
		lesson : '57',
		section : '0',
		label : '课文',
		title : '初为人妻',
		type : 'audio',
		media : 'npcr5/NPCR5-57-1.mp3',
		text : [ 'npcr5-57-1' ]
	}, {
		lesson : '57',
		section : '2',
		label : 'Listening 1',
		type : 'audio',
		media : 'npcr5/NPCR5-57-2.mp3',
		text : [ 'npcr5-57-2' ]
	}, {
		lesson : '57',
		section : '3',
		label : 'Listening 2',
		type : 'audio',
		media : 'npcr5/NPCR5-57-3.mp3',
		text : [ 'npcr5-57-3' ]
	}, {
		lesson : '57',
		section : '4',
		label : 'Listening 3',
		type : 'audio',
		media : 'npcr5/NPCR5-57-4.mp3',
		text : [ 'npcr5-57-4' ]
	}, {
		lesson : '58',
		section : '0',
		label : 'Text 1',
		title : '背影',
		type : 'audio',
		media : 'npcr5/NPCR5-58-0.mp3',
		text : [ 'npcr5-58-0' ]
	}, {
		lesson : '58',
		section : '1',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr5/NPCR5-58-1.mp3',
		text : [ 'npcr5-58-1' ]
	}, {
		lesson : '58',
		section : '2',
		label : 'Listening 1',
		type : 'audio',
		media : 'npcr5/NPCR5-58-2.mp3',
		text : [ 'npcr5-58-2' ]
	}, {
		lesson : '58',
		section : '3',
		label : 'Listening 2',
		type : 'audio',
		media : 'npcr5/NPCR5-58-3.mp3',
		text : [ 'npcr5-58-3' ]
	}, {
		lesson : '58',
		section : '4',
		label : 'Listening 3',
		type : 'audio',
		media : 'npcr5/NPCR5-58-4.mp3',
		text : [ 'npcr5-58-4' ]
	}, {
		lesson : '59',
		section : '0',
		label : '课文',
		title : '十八年的秘密',
		type : 'audio',
		media : 'npcr5/NPCR5-59-1.mp3',
		text : [ 'npcr5-59-1' ]
	}, {
		lesson : '59',
		section : '2',
		label : 'Listening 1',
		type : 'audio',
		media : 'npcr5/NPCR5-59-2.mp3',
		text : [ 'npcr5-59-2' ]
	}, {
		lesson : '59',
		section : '3',
		label : 'Listening 2',
		type : 'audio',
		media : 'npcr5/NPCR5-59-3.mp3',
		text : [ 'npcr5-59-3' ]
	}, {
		lesson : '59',
		section : '4',
		label : 'Listening 3',
		type : 'audio',
		media : 'npcr5/NPCR5-59-4.mp3',
		text : [ 'npcr5-59-4' ]
	}, {
		lesson : '60',
		section : '0',
		label : 'Text 1',
		title : '珍珠鸟',
		type : 'audio',
		media : 'npcr5/NPCR5-60-0.mp3',
		text : [ 'npcr5-60-0' ]
	}, {
		lesson : '60',
		section : '1',
		label : 'Text 2',
		type : 'audio',
		media : 'npcr5/NPCR5-60-1.mp3',
		text : [ 'npcr5-60-1' ]
	}, {
		lesson : '60',
		section : '2',
		label : 'Listening 1',
		type : 'audio',
		media : 'npcr5/NPCR5-60-2.mp3',
		text : [ 'npcr5-60-2' ]
	}, {
		lesson : '60',
		section : '3',
		label : 'Listening 2',
		type : 'audio',
		media : 'npcr5/NPCR5-60-3.mp3',
		text : [ 'npcr5-60-3' ]
	}, {
		lesson : '60',
		section : '4',
		label : 'Listening 3',
		type : 'audio',
		media : 'npcr5/NPCR5-60-4.mp3',
		text : [ 'npcr5-60-4' ]
	} ]
};

var NPCR6 = {
	id : 'npcr6',
	title : '新实用汉语课本(6)',
	description : '《新实用汉语课本6》是为高级水平成人汉语学习者编写的综合汉语教材，既适合学习者自学也可用于课堂教学。本书的编写目的是通过语言结构、语言功能与相关文化知识的学习和听说读写技能训练，进一步提高学习者的汉语综合水平。',
	items : [ {
		lesson : '61',
		section : '1',
		title : '“二大伯”和他的洋学生',
		label : 'Text 1',
		type : 'audio',
		media : 'npcr6/NPCR6-61-1.mp3',
		text : [ 'npcr6-61-1' ]
	}, {
		lesson : '61',
		section : '2',
		label : 'Listening 1',
		type : 'audio',
		media : 'npcr6/NPCR6-61-2.mp3',
		text : [ 'npcr6-61-2' ]
	}, {
		lesson : '61',
		section : '3',
		label : 'Listening 2',
		type : 'audio',
		media : 'npcr6/NPCR6-61-3.mp3',
		text : [ 'npcr6-61-3' ]
	}, {
		lesson : '61',
		section : '4',
		label : 'Listening 3',
		type : 'audio',
		media : 'npcr6/NPCR6-61-4.mp3',
		text : [ 'npcr6-61-4' ]
	}, {
		lesson : '62',
		section : '1',
		title : '电动自行车',
		label : 'Text 1',
		type : 'audio',
		media : 'npcr6/NPCR6-62-1.mp3',
		text : [ 'npcr6-62-1' ]
	}, {
		lesson : '62',
		section : '2',
		label : 'Listening 1',
		type : 'audio',
		media : 'npcr6/NPCR6-62-2.mp3',
		text : [ 'npcr6-62-2' ]
	}, {
		lesson : '62',
		section : '3',
		label : 'Listening 2',
		type : 'audio',
		media : 'npcr6/NPCR6-62-3.mp3',
		text : [ 'npcr6-62-3' ]
	}, {
		lesson : '62',
		section : '4',
		label : 'Listening 3',
		type : 'audio',
		media : 'npcr6/NPCR6-62-4.mp3',
		text : [ 'npcr6-62-4' ]
	}, {
		lesson : '63',
		section : '1',
		title : '酒鬼砸车',
		label : 'Text 1',
		type : 'audio',
		media : 'npcr6/NPCR6-63-1.mp3',
		text : [ 'npcr6-63-1' ]
	}, {
		lesson : '63',
		section : '2',
		label : 'Listening 1',
		type : 'audio',
		media : 'npcr6/NPCR6-63-2.mp3',
		text : [ 'npcr6-63-2' ]
	}, {
		lesson : '63',
		section : '3',
		label : 'Listening 2',
		type : 'audio',
		media : 'npcr6/NPCR6-63-3.mp3',
		text : [ 'npcr6-63-3' ]
	}, {
		lesson : '64',
		section : '1',
		title : '孔乙己（节选）',
		label : 'Text 1',
		type : 'audio',
		media : 'npcr6/NPCR6-64-1.mp3',
		text : [ 'npcr6-64-1' ]
	}, {
		lesson : '64',
		section : '2',
		label : 'Listening 1',
		type : 'audio',
		media : 'npcr6/NPCR6-64-2.mp3',
		text : [ 'npcr6-64-2' ]
	}, {
		lesson : '64',
		section : '3',
		label : 'Listening 2',
		type : 'audio',
		media : 'npcr6/NPCR6-64-3.mp3',
		text : [ 'npcr6-64-3' ]
	}, {
		lesson : '64',
		section : '4',
		label : 'Listening 3',
		type : 'audio',
		media : 'npcr6/NPCR6-64-4.mp3',
		text : [ 'npcr6-64-4' ]
	}, {
		lesson : '65',
		section : '1',
		title : '中年（节选）',
		label : 'Text 1',
		type : 'audio',
		media : 'npcr6/NPCR6-65-1.mp3',
		text : [ 'npcr6-65-1' ]
	}, {
		lesson : '65',
		section : '2',
		label : 'Listening 1',
		type : 'audio',
		media : 'npcr6/NPCR6-65-2.mp3',
		text : [ 'npcr6-65-2' ]
	}, {
		lesson : '65',
		section : '3',
		label : 'Listening 2',
		type : 'audio',
		media : 'npcr6/NPCR6-65-3.mp3',
		text : [ 'npcr6-65-3' ]
	}, {
		lesson : '65',
		section : '4',
		label : 'Listening 3',
		type : 'audio',
		media : 'npcr6/NPCR6-65-4.mp3',
		text : [ 'npcr6-65-4' ]
	}, {
		lesson : '66',
		section : '1',
		title : '关于名字的随想（节选）',
		label : 'Text 1',
		type : 'audio',
		media : 'npcr6/NPCR6-66-1.mp3',
		text : [ 'npcr6-66-1' ]
	}, {
		lesson : '66',
		section : '2',
		label : 'Listening 1',
		type : 'audio',
		media : 'npcr6/NPCR6-66-2.mp3',
		text : [ 'npcr6-66-2' ]
	}, {
		lesson : '66',
		section : '3',
		label : 'Listening 2',
		type : 'audio',
		media : 'npcr6/NPCR6-66-3.mp3',
		text : [ 'npcr6-66-3' ]
	}, {
		lesson : '66',
		section : '4',
		label : 'Listening 3',
		type : 'audio',
		media : 'npcr6/NPCR6-66-4.mp3',
		text : [ 'npcr6-66-4' ]
	}, {
		lesson : '67',
		section : '1',
		title : '窗（节选）',
		label : 'Text 1',
		type : 'audio',
		media : 'npcr6/NPCR6-67-1.mp3',
		text : [ 'npcr6-67-1' ]
	}, {
		lesson : '67',
		section : '2',
		label : 'Listening 1',
		type : 'audio',
		media : 'npcr6/NPCR6-67-2.mp3',
		text : [ 'npcr6-67-2' ]
	}, {
		lesson : '67',
		section : '3',
		label : 'Listening 2',
		type : 'audio',
		media : 'npcr6/NPCR6-67-3.mp3',
		text : [ 'npcr6-67-3' ]
	}, {
		lesson : '67',
		section : '4',
		label : 'Listening 3',
		type : 'audio',
		media : 'npcr6/NPCR6-67-4.mp3',
		text : [ 'npcr6-67-4' ]
	}, {
		lesson : '68',
		section : '1',
		title : '最后的野骆驼',
		label : 'Text 1',
		type : 'audio',
		media : 'npcr6/NPCR6-68-1.mp3',
		text : [ 'npcr6-68-1' ]
	}, {
		lesson : '68',
		section : '2',
		label : 'Listening 1',
		type : 'audio',
		media : 'npcr6/NPCR6-68-2.mp3',
		text : [ 'npcr6-68-2' ]
	}, {
		lesson : '68',
		section : '3',
		label : 'Listening 2',
		type : 'audio',
		media : 'npcr6/NPCR6-68-3.mp3',
		text : [ 'npcr6-68-3' ]
	}, {
		lesson : '68',
		section : '4',
		label : 'Listening 3',
		type : 'audio',
		media : 'npcr6/NPCR6-68-4.mp3',
		text : [ 'npcr6-68-4' ]
	}, {
		lesson : '69',
		section : '1',
		title : '我的母亲（节选）',
		label : 'Text 1',
		type : 'audio',
		media : 'npcr6/NPCR6-69-1.mp3',
		text : [ 'npcr6-69-1' ]
	}, {
		lesson : '69',
		section : '2',
		label : 'Listening 1',
		type : 'audio',
		media : 'npcr6/NPCR6-69-2.mp3',
		text : [ 'npcr6-69-2' ]
	}, {
		lesson : '69',
		section : '3',
		label : 'Listening 2',
		type : 'audio',
		media : 'npcr6/NPCR6-69-3.mp3',
		text : [ 'npcr6-69-3' ]
	}, {
		lesson : '69',
		section : '4',
		label : 'Listening 3',
		type : 'audio',
		media : 'npcr6/NPCR6-69-4.mp3',
		text : [ 'npcr6-69-4' ]
	}, {
		lesson : '70',
		section : '1',
		title : '傻二舅',
		label : 'Text 1',
		type : 'audio',
		media : 'npcr6/NPCR6-70-1.mp3',
		text : [ 'npcr6-70-1' ]
	}, {
		lesson : '70',
		section : '2',
		label : 'Listening 1',
		type : 'audio',
		media : 'npcr6/NPCR6-70-2.mp3',
		text : [ 'npcr6-70-2' ]
	}, {
		lesson : '70',
		section : '3',
		label : 'Listening 2',
		type : 'audio',
		media : 'npcr6/NPCR6-70-3.mp3',
		text : [ 'npcr6-70-3' ]
	}, {
		lesson : '70',
		section : '4',
		label : 'Listening 3',
		type : 'audio',
		media : 'npcr6/NPCR6-70-4.mp3',
		text : [ 'npcr6-70-4' ]
	} ]
};

var WORKSHOP = {
	id : 'workshop',
	title : '阅读与写作',
	image : 'images/sites/independent-study.jpg',
	description : '今年的阅读与写作课主要是帮助同学们提高阅读和写作的能力。这里是一组文章供大家在这个学年阅读。在每篇文章后，有一些帮助理解文章的问题和作业。另外，除了平时作业中有作文外，每个同学还要写一篇较长的作文《我与长城中文学校》作为毕业班的同学的毕业作文。',
	items : [ {
		lesson : 'ladder',
		title : '天梯（节选）',
		text : [ 'workshop-ladder' ]
	}, {
		lesson : 'dream',
		title : '梦是美丽的',
		text : [ 'workshop-dream' ]
	}, {
		lesson : 'cats',
		title : '无价的狸狸',
		text : [ 'workshop-cats' ]
	}, {
		lesson : 'snow',
		title : '大年初一没下雪',
		text : [ 'workshop-snow' ]
	}, {
		lesson : 'marshmallow',
		title : '棉花糖实验',
		text : [ 'workshop-marshmallow' ]
	}, {
		lesson : 'kite',
		title : '风筝',
		text : [ 'workshop-kite' ]
	}, {
		lesson : 'chanpion',
		title : '赛车冠军',
		text : [ 'workshop-chanpion' ]
	}, {
		lesson : 'lao-wai',
		title : '来了个老外',
		text : [ 'workshop-lao-wai' ]
	}, {
		lesson : 'few-things',
		title : '二、三事（节选）',
		text : [ 'workshop-few-things' ]
	}, {
		lesson : 'bloggers',
		title : '微信、微博',
		text : [ 'workshop-bloggers' ]
	}, {
		lesson : 'gwca-and-me',
		title : '我与长城中文学校',
		text : [ 'workshop-gwca-and-me' ]
	} ]
}

var READING = {
	id : 'reading',
	title : '课外阅读',
	image : 'images/sites/additional-reading.jpg',
	description : '这里是一些程度不同的课外阅读材料，供教师在教学过程中配合教材使用。',
	items : [ {
		lesson : 'SadStory',
		title : '伤心的故事',
		text : [ 'reading-SadStory' ]
	}, {
		lesson : 'PoorFox',
		title : '白忙的狐狸',
		text : [ 'reading-PoorFox' ]
	}, {
		lesson : 'SonAndCloth',
		title : '儿子和衣服',
		text : [ 'reading-SonAndCloth' ]
	}, {
		lesson : 'ChickenOutside',
		title : '鸡笼外的鸡',
		text : [ 'reading-ChickenOutside' ]
	}, {
		lesson : 'WireThePhone',
		title : '接电话线',
		text : [ 'reading-WireThePhone' ]
	}, {
		lesson : 'HeAndShe',
		title : '他和她',
		text : [ 'reading-HeAndShe' ]
	}, {
		lesson : 'Essay',
		title : '作文',
		text : [ 'reading-Essay' ]
	}, {
		lesson : 'Pickpocket',
		title : '扒手',
		text : [ 'reading-Pickpocket' ]
	}, {
		lesson : 'LittleMatchGirl',
		title : '卖火柴的小女孩',
		text : [ 'reading-LittleMatchGirl' ]
	}, {
		lesson : 'Shanghainess',
		title : '上海人',
		text : [ 'reading-Shanghainess' ]
	}, {
		lesson : 'LastDish',
		title : '最后一盘儿',
		text : [ 'reading-LastDish' ]
	}, {
		lesson : 'Saharan',
		title : '撒哈拉的故事',
		text : [ 'reading-Saharan' ]
	}, {
		lesson : 'DuckEggs',
		title : '端午的鸭蛋',
		text : [ 'reading-DuckEggs' ]
	}, {
		lesson : 'BeijingMen',
		title : '爱吹牛的北京男人',
		text : [ 'reading-BeijingMen' ]
	} ]
};

var TEACHER_HU = {
	id : 'youping-hu',
	title : '班里那些事儿',
	image : 'images/sites/youping.jpg',
	description : '自从《明朝那些事》这本书在网上一炮打响之后，一时间冒出来许许多多的从三皇五帝到蒋公毛爷，从街头巷尾到厅堂厨房的那些事儿。现如今，“那些事儿”几乎成了一种特定的文体，于是就有了我们《班里那些事儿》。',
	items : [ {
		lesson : 'ThoseThings',
		title : '引子',
		text : [ 'youping-hu-ThoseThings' ]
	}, {
		lesson : 'OpeningTalk',
		title : '仓促上阵',
		text : [ 'youping-hu-OpeningTalk' ]
	}, {
		lesson : 'WhatsYourName',
		title : '你叫什么名字',
		text : [ 'youping-hu-WhatsYourName' ]
	}, {
		lesson : 'MaternalLove',
		title : '生活中的母爱',
		text : [ 'youping-hu-MaternalLove' ]
	}, {
		lesson : 'SoundOfReading',
		title : '朗朗读书声',
		text : [ 'youping-hu-SoundOfReading' ]
	}, {
		lesson : 'YouAreWrong',
		title : '老师，你说错了',
		text : [ 'youping-hu-YouAreWrong' ]
	}, {
		lesson : 'RecallAndThink',
		title : '忆苦思甜',
		text : [ 'youping-hu-RecallAndThink' ]
	}, {
		lesson : 'DoneEatingYet',
		title : '吃了吗您哪',
		text : [ 'youping-hu-DoneEatingYet' ]
	}, {
		lesson : 'Beijing',
		title : '让人那什么的北京',
		text : [ 'youping-hu-Beijing' ]
	}, {
		lesson : 'Editor',
		title : '当一回编辑',
		text : [ 'youping-hu-Editor' ]
	}, {
		lesson : 'YuanLongping',
		title : '养活全世界',
		text : [ 'youping-hu-YuanLongping' ]
	}, {
		lesson : 'Pleasure',
		title : '不亦说乎',
		text : [ 'youping-hu-Pleasure' ]
	}, {
		lesson : 'XamsParty',
		title : '其乐融融',
		text : [ 'youping-hu-XamsParty' ]
	} ]
};

var AllCourses = {
	title : 'All the courses.',
	itemSet : [ NPCR1, NPCR2, NPCR3, NPCR4, NPCR5, NPCR6, WORKSHOP, READING, PINYIN,
		TEACHER_HU ]
};