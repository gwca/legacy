
var app = angular.module('classroom', []);

app.service('footer', function() {
	this.getFooterText = function() {
		return 'Enjoy Learning Chinese with Great Wall Chinese Academy';
	}
});


app.controller('classroomController', function($scope, footer) {
	$scope.footerText = footer.getFooterText();
});

