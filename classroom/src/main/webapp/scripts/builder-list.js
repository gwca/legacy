var NPCR1_01_1 = {
	course : 'npcr1',
	lesson : '01',
	section : '1',
	title : '第一课生词 ',
	entries : '你,吗,我,和,呢,也,人,手,刀,口,田',
	quizletId : '181741358/',
	characters : [
			{
				id : 'ni-you',
				pinyin : 'Nǐ',
				entry : '你',
				translation : 'you',
				words : '你好? (How are you?)',
				usage : '你是谁? (Who are you?)'
			},
			{
				id : 'hao-good',
				pinyin : 'Hǎo',
				entry : '好',
				translation : 'good, well, fine',
				words : '好人 (nice guy),好看 (pretty)',
				usage : '一双好看的眼睛。 (a pair of pretty eyes.)'
			},
			{
				id : 'ma-ma',
				pinyin : 'Ma',
				entry : '吗',
				translation : 'interrogative particle for question expecting yes-no answer',
				words : '好吗? (alright？)',
				usage : '你好吗？ (How are you?)'
			},
			{
				id : 'wo-me',
				pinyin : 'Wǒ',
				entry : '我',
				translation : 'me',
				words : '自我 (self) 我们 (we, us)',
				usage : '你和我是好朋友 (You and me are good friends.)'
			},
			{
				id : 'hen-very',
				pinyin : 'Hěn',
				entry : '很',
				translation : 'very',
				words : '很好 (very good) 很热 (very hot)',
				usage : '今天的天气很好。 (Nice day today.)'
			},
			{
				id : 'he-and',
				pinyin : 'Hé',
				entry : '和',
				translation : 'and',
				words : '你和我 (you and me) 和平(piece)',
				usage : '你和我是好朋友。 (You and me are good friends.)'
			},
			{
				id : 'ne-ne',
				pinyin : 'Ne',
				entry : '呢',
				translation : 'a modal particle used for elliptical questins',
				words : '你呢? (how about you?)',
				usage : "这是怎么回事呢？(What happened?)"
			},
			{
				id : 'ye-also',
				pinyin : 'Yě',
				entry : '也',
				translation : 'also, too',
				words : '也好 (good too)',
				usage : "他同意，我也同意。(He agreed, so did I.)"
			},
			{
				id : 'ren-human',
				pinyin : 'Rén',
				entry : '人',
				translation : 'human beings,people',
				words : '男人 (man) 女人 (woman) 好人 (nice guy)',
				usage : '那有一个人。 (There is a guy.)'
			},
			{
				id : 'shou-hand',
				pinyin : 'Shǒu',
				entry : '手',
				translation : 'hand',
				words : '双手 (two hands) 把手 (handle)',
				usage : '他手里拿着一把刀。 (He is holding a knife.)'
			},
			{
				id : 'dao-knife',
				pinyin : 'Dāo',
				entry : '刀',
				translation : 'knife',
				words : '菜刀(kitchen knife) 刀刃(Cutting edge)',
				usage : '你去把刀磨一磨。 (Sharpen the knife please.)'
			},
			{
				id : 'kou-mouth',
				pinyin : 'Kǒu',
				entry : '口',
				translation : 'mouth',
				words : '一口 (one bite) 口水 (mouth water) 入口 (entrance)',
				usage : '他一看见好吃的，馋得直流口水。 (That yummy food made his mouth water.)'
			}, {
				id : 'tian-field',
				pinyin : 'Tián',
				entry : '田',
				translation : 'field',
				words : '田野 (farmland), 油田 (oil field)',
				usage : "走进麦田，到处都是绿油油的。(It's all green in the wheat field.)"
			} ]
};

var NPCR1_02_1 = {
	course : 'npcr1',
	lesson : '02',
	section : '1',
	title : '第二课生词 ',
	entries : '忙,爸,妈,他,们,都,不,男,朋,友,哥,要,弟,喝',
	quizletId : '181781999/',
	characters : [ {
		id : 'mang-busy',
		pinyin : 'Máng',
		entry : '忙',
		translation : 'busy',
		words : '很忙 (very busy), 帮忙 (help)',
		usage : '今天你忙吗? (Are you busy today?)'
	}, {
		id : 'ba-father',
		pinyin : 'Bà',
		entry : '爸',
		translation : 'father',
		words : '爸爸 (dad),我爸 (my father)',
		usage : '他爸爸不在家。 (his father is not home.)'
	}, {
		id : 'ma-mother',
		pinyin : 'Mā',
		entry : '妈',
		translation : 'mother',
		words : '妈妈 (mother)',
		usage : '他的妈妈是个老师 (His mother is a teacher.)'
	}, {
		id : 'ta-he',
		pinyin : 'Tā',
		entry : '他',
		translation : 'he',
		words : '他 (he, him) 他们 (they, them)',
		usage : '我和他是好朋友 (He and me are good friends.)'
	}, {
		id : 'men-multiple',
		pinyin : 'Mén',
		entry : '们',
		translation : 'multiple',
		words : '我们 (we, us) 你们 (you) 他们 (they, them)',
		usage : '我们今天去打球。 (We have a ball game today.)'
	}, {
		id : 'ne-ne',
		pinyin : 'Ne',
		entry : '呢',
		translation : 'a modal particle used for elliptical questins',
		words : '你呢? (how about you?)',
		usage : "这是怎么回事呢？(What happened?)"
	}, {
		id : 'dou-all',
		pinyin : 'Dōu',
		entry : '都',
		translation : 'all',
		words : '都是 (everything is)',
		usage : '我们都喜欢游泳 (We all like swimming.)'
	}, {
		id : 'bu-no',
		pinyin : 'Bù',
		entry : '不',
		translation : 'no, not',
		words : '不好 (not good) 不是 (it is not)',
		usage : '她出去了，不在家。 (She is not home.)'
	}, {
		id : 'nan-male',
		pinyin : 'Nán',
		entry : '男',
		translation : 'male',
		words : '男人 (man) 男孩 (boy)',
		usage : '这个小男孩真聪明。 (This boy is very smart.)'
	}, {
		id : 'peng-friend',
		pinyin : 'Péng',
		entry : '朋',
		translation : 'friend',
		words : '朋友 (friend)',
		usage : '他是我的好朋友。 (He is my good friend.)'
	}, {
		id : 'you-friend',
		pinyin : 'Yǒu',
		entry : '友',
		translation : 'friend',
		words : '朋友 (friend), 友好 (friendly, nice)',
		usage : "他对所有的人都很友好。(He is always nice to everybody.)"
	}, {
		id : 'ge-brother',
		pinyin : 'Gē',
		entry : '哥',
		translation : 'brother',
		words : '哥哥 (elder brother)大哥 (eldest brother)',
		usage : '你哥哥是做什么的？ (What does your elder bother do?)'
	}, {
		id : 'yao-want',
		pinyin : 'Yào',
		entry : '要',
		translation : 'want',
		words : '不要 (do not want) 想要 (really want)',
		usage : '你要干什么？ (What do you want to do?)'
	}, {
		id : 'di-brother',
		pinyin : 'Dì',
		entry : '弟',
		translation : 'younger brother',
		words : '弟弟 (younger bother) 小弟 (youngest brother)',
		usage : '他的小弟弟特别淘气。 (His little bother is very naughty.)'
	}, {
		id : 'he-drink',
		pinyin : 'Hē',
		entry : '喝',
		translation : 'drink',
		words : '喝水 (drink water) 喝酒 (drink)',
		usage : '你要少喝那些甜饮料。 (You do not want to drink a lot of sugary drinks.)'
	} ]
};

var NPCR1_03_1 = {
	course : 'npcr1',
	lesson : '03',
	section : '1',
	title : '第三课生词(1)',
	entries : '她,是,哪,国,那,谁,老,师,木,土',
	quizletId : '181783561/',
	characters : [
			{
				id : 'ta-she',
				pinyin : 'Tā',
				entry : '她',
				translation : 'she',
				words : '她们 (they)',
				usage : '她的个子很高 (She is pretty tall.)'
			},
			{
				id : 'shi-yes',
				pinyin : 'Shì',
				entry : '是',
				translation : 'be, yes',
				words : '是非 (right and wrong) 不是 (no, not realy)',
				usage : '是还是不是，那是个问题。 (To be, or not to be, that is the question.)'
			},
			{
				id : 'na-which',
				pinyin : 'Nǎ',
				entry : '哪',
				translation : 'which',
				words : '哪里 (where) 哪个 (which one)',
				usage : '你是哪里人 (Where do you come from.)'
			},
			{
				id : 'guo-country',
				pinyin : 'Guó',
				entry : '国',
				translation : 'country',
				words : '中国 (China) 全国 (entire country)',
				usage : '世界上有二百多个国家 (There are more than 200 countries in the world.)'
			},
			{
				id : 'na-that',
				pinyin : 'Nà',
				entry : '那',
				translation : 'that',
				words : '那天 (that day) 那是 (that is)',
				usage : '那天我们去买东西。 (We went shopping that day.)'
			},
			{
				id : 'lao-old',
				pinyin : 'Lǎo',
				entry : '老',
				translation : 'old',
				words : '老人 (old person) 老师 (teacher)',
				usage : "张老师走进了教室。(Mr. Zhang walked into the classroom)"
			},
			{
				id : 'shui-who',
				pinyin : 'Shǔi',
				entry : '谁',
				translation : 'who',
				words : '那是谁 (who is it)',
				usage : "和张老师一起走进了教室的是谁？(Who is coming with Mr. Zhang walked into the classroom)"
			}, {
				id : 'shi-teacher',
				pinyin : 'Shī',
				entry : '师',
				translation : 'teacher',
				words : '老师 (teacher) 师傅  (master)',
				usage : '张老师走进了教室。(Mr. Zhang walked into the classroom)'
			}, {
				id : 'mu-wood',
				pinyin : 'Mù',
				entry : '木',
				translation : 'wood',
				words : '木材 (wood) 积木 (building blocks)',
				usage : '远处有一座小木屋。(There is a little cabin in distance)'
			}, {
				id : 'tu-earth',
				pinyin : 'Tǔ',
				entry : '土',
				translation : 'earth',
				words : '土地 (land) 领土 (territory)',
				usage : '桌上落了一层土。(The dust is all over the desk.)'
			} ]
};

var NPCR1_03_2 = {
	course : 'npcr1',
	lesson : '03',
	section : '2',
	title : '第三课生词(2)',
	entries : '您,这,外,语,医,生,奶,婆,陈,火,石',
	quizletId : '181783939/',
	characters : [
			{
				id : 'nin-you',
				pinyin : 'Nín',
				entry : '您',
				translation : 'you (plite form)',
				words : '您好 (how are you)',
				usage : '您去哪？ (Where are you going?)'
			},
			{
				id : 'zhe-this',
				pinyin : 'Zhe',
				entry : '这',
				translation : 'this',
				words : '这里 (here) 这是 (this is)',
				usage : '这里是我们的教室 (Here is our classroom.)'
			},
			{
				id : 'wai-outside',
				pinyin : 'Wài',
				entry : '外',
				translation : 'outside',
				words : '外面 (outside)  外语 (foreign language)',
				usage : '他会说三国外语 (He speaks three foreign languages.)'
			},
			{
				id : 'yu-language',
				pinyin : 'Yǔ',
				entry : '语 ',
				translation : 'speak, language',
				words : '语言 (language) 语文 (a language class, like English, Chinese)',
				usage : '世界上有几千种语言 (There are a few thousands languages in the world.)'
			},
			{
				id : 'yi-doctor',
				pinyin : 'Yī',
				entry : '医',
				translation : 'doctor',
				words : '医生 (doctor) 医院 (hospital)',
				usage : '昨天他到医院去看病。 (He went to the hospital to see a doctor yesterday.)'
			}, {
				id : 'nai-milk',
				pinyin : 'Nǎi',
				entry : '奶',
				translation : 'milk',
				words : '奶奶 (grandma) 牛奶  (cow milk)',
				usage : '奶奶给了我一杯牛奶。(Granma gave me a glass of milk.)'
			}, {
				id : 'po-oldlady',
				pinyin : 'Pó',
				entry : '婆',
				translation : 'old lady',
				words : "老婆 (wife) 外婆 (grandma -- mother's mother)",
				usage : '外婆今年七十五岁了。(Granma is 75 years old this year.)'
			}, {
				id : 'chen-old',
				pinyin : 'Chén',
				entry : '陈',
				translation : 'old, family name)',
				words : '姓陈 (family name is Chen) 陈旧 (aged)',
				usage : '我姓胡，你姓黄，他姓陈。(Hu, Huang, Chen)'
			}, {
				id : 'huo-fire',
				pinyin : 'Hǔo',
				entry : '火',
				translation : 'fire',
				words : '点火 (start fire),发火 (get angry),上火',
				usage : '那座房子着火了。(The house is on fire.)'
			}, {
				id : 'shi-rock',
				pinyin : 'Shí',
				entry : '石',
				translation : 'rock',
				words : '石头 (rock) 石油 (oil)',
				usage : '他从地上捡起一块石头。(He picked up a piece of rock.)'
			} ]
};

var NPCR1_04_1 = {
	course : 'npcr1',
	lesson : '04',
	section : '1',
	title : '第四课生词(1)',
	entries : '认,识,可,以,进,来,请,记,者,问,姓,叫,先,生',
	quizletId : '181784599/',
	characters : [
			{
				id : 'ren-realize',
				pinyin : 'Rèn',
				entry : '认',
				translation : 'realize',
				words : '认识 (know) 认为 (think)',
				usage : '他是谁，我不认识他。 (Who is he? I do not know him.)'
			},
			{
				id : 'shi-knowledge',
				pinyin : 'Shi',
				entry : '识',
				translation : 'knowledge',
				words : '认识 (know) 知识 (knowledge)',
				usage : '我们在学校学知识 (We learn at school.)'
			},
			{
				id : 'ke-can',
				pinyin : 'Kě',
				entry : '可',
				translation : 'can, may',
				words : '可以 (can, be able to)  可能 (possible)',
				usage : '一直在下雨，他可能不会来了 (It is been raining, he may not be coming.)'
			},
			{
				id : 'yi-with',
				pinyin : 'Yǐ',
				entry : '以',
				translation : 'with, by',
				words : '可以 (can, be able to) 以为 (think, guess)',
				usage : '我以为他不会来了 (I thought he would not come.)'
			},
			{
				id : 'jin-into',
				pinyin : 'Jìn',
				entry : '进',
				translation : 'get in',
				words : '进来 (get in) 进步 (progress)',
				usage : '进来吧，请坐。 (Come in and have a seat.)'
			},
			{
				id : 'lai-come',
				pinyin : 'Lái',
				entry : '来',
				translation : 'come',
				words : '回来 (come back) 原来 (original)',
				usage : '我哥哥明天就回来了。 (My brother will be back tomorrow.)'
			},
			{
				id : 'qing-please',
				pinyin : 'Qǐng',
				entry : '请',
				translation : 'please',
				words : '请进 (come in please) 请客  (stand treat)',
				usage : '今天的晚饭我请客。(The dinner is on me.)'
			},
			{
				id : 'ji-remember',
				pinyin : 'Jì',
				entry : '记',
				translation : 'remember, take notes',
				words : "记者 (reporter) 记住 (memberize) 记忆 (memory)",
				usage : '我是学生，我的朋友是个记者。(I am a student and my friend is a reporter.)'
			},
			{
				id : 'zhe-person',
				pinyin : 'Zhě',
				entry : '者',
				translation : 'person, someone',
				words : "记者 (reporter) 或者 (or)",
				usage : '我是学生，我的朋友是个记者。(I am a student and my friend is a reporter.)'
			},
			{
				id : 'wen-ask',
				pinyin : 'Wèn',
				entry : '问',
				translation : 'ask, question',
				words : '问题 (question) 问好 (greeting)',
				usage : '大家还有问题吗？(Any questins?)'
			},
			{
				id : 'xing-surname',
				pinyin : 'Xìng',
				entry : '姓',
				translation : 'surname',
				words : '姓名 (full name) 姓王 (famile name is Wang)',
				usage : '你姓什么？我姓王。(What is your family name? Wang.)'
			},
			{
				id : 'jiao-call',
				pinyin : 'Jiào',
				entry : '叫',
				translation : 'call, shout',
				words : '大叫 (shout) 叫做  (to be called)',
				usage : '你叫什么名字？(What is your name?)'
			},
			{
				id : 'xian-prior',
				pinyin : 'Xiān',
				entry : '先',
				translation : 'prior, former',
				words : "先生 (Mr., husband) 事先 (in advance)",
				usage : '我还有事，先走一步了。(I have to run now.)'
			},
			{
				id : 'sheng-raw',
				pinyin : 'Shēng',
				entry : '生',
				translation : 'raw',
				words : '生命 (life) 生字 (new word) 学生 (student)',
				usage : '一回生，二回熟，多练几回就好了。(Practice makes perfect. Just keep practicing!)'
			} ]
};

var NPCR1_04_2 = {
	course : 'npcr1',
	lesson : '04',
	section : '2',
	title : '第四课生词(2)',
	entries : '院,什,么,习,汉,山,日,月,目,云',
	quizletId : '181784857/',
	characters : [
			{
				id : 'yuan-yard',
				pinyin : 'Yuàn',
				entry : '院',
				translation : 'yard',
				words : '学院 (college) 医院 (hospital) 后院 (back yard)',
				usage : '昨天他到医院去看病。 (He went to the hospital to see a doctor yesterday.)'
			},
			{
				id : 'shen-assorted',
				pinyin : 'Shén',
				entry : '什',
				translation : 'assorted',
				words : '什么 (what) 为什么 (why)',
				usage : "他为什么没来上课 (Why didn't he come to the class?)"
			},
			{
				id : 'me-what',
				pinyin : 'Me',
				entry : '么',
				translation : 'what',
				words : '什么 (what) 怎么 (what happened)',
				usage : '这是怎么了，这么乱？ (What happeded, so messy here.)'
			},
			{
				id : 'xi-practice',
				pinyin : 'Xí',
				entry : '习',
				translation : 'practice',
				words : '学习 (study) 练习 (practice) 习惯 (habit)',
				usage : '多练习几遍就好了 (keep practicing, you will be there.)'
			},
			{
				id : 'han-chinese',
				pinyin : 'Hàn',
				entry : '汉',
				translation : 'Chinese, man',
				words : '汉语 (Chinese) 好汉 (hero)',
				usage : '汉语就是中文。'
			},
			{
				id : 'shan-hill',
				pinyin : 'Shān',
				entry : '山',
				translation : 'hill',
				words : '高山 (mountain) 山沟 (revine)',
				usage : '山上有很多树。(There are a lot of trees in the hill.)'
			},
			{
				id : 'ri-sun',
				pinyin : 'Rì',
				entry : '日',
				translation : 'sun',
				words : '日夜 (day and night) 日子 (daily life) 日记 (diary)',
				usage : '和从前比，现在的日子要好过多了。(Life is a lot easier nowadays.)'
			},
			{
				id : 'yue-moon',
				pinyin : 'Yuè',
				entry : '月',
				translation : 'moon, month',
				words : '月亮 (moon) 七月 (July) 月饼 (moon cake)',
				usage : '我的生日在十一月，你的生日是几月份？(My birthday is on November, which month is your birthday on?'
			},
			{
				id : 'mu-eye',
				pinyin : 'Mù',
				entry : '目',
				translation : 'eye',
				words : '目标 (target, goal) 节目 (program)  盲目 (blindly)',
				usage : '做事应该先有目标。 (Be sure you have a goal before start doing something.)'
			}, {
				id : 'yun-cloud',
				pinyin : 'Yūn',
				entry : '云',
				translation : 'cloud',
				words : '云彩 (cloud) 云游 (travelling without a destingnatin)',
				usage : '蓝天上有几朵白云。(there are a few clouds in the blue sky.)'
			} ]
};

var NPCR1_05_1 = {
	course : 'npcr1',
	lesson : '05',
	section : '1',
	title : '第五课生词(1)',
	entries : '餐,厅,在,宿,舍,女,坐,谢,对,起,道,没,关,系,再',
	quizletId : '181785121/',
	characters : [
			{
				id : 'can-meal',
				pinyin : 'Cān',
				entry : '餐',
				translation : 'meal',
				words : '餐厅 (restaurant,cafeteria) 晚餐 (supper)',
				usage : '晚餐吃什么？ (What do we have for the supper.)'
			},
			{
				id : 'ting-hall',
				pinyin : 'Tīng',
				entry : '厅',
				translation : 'hall',
				words : '餐厅 (cafeteria) 客厅 (living room)',
				usage : '请客人在客厅坐 (Ask the guests to have a seat in the living room.)'
			},
			{
				id : 'zai-exist',
				pinyin : 'Zài',
				entry : '在',
				translation : 'at, be, exist',
				words : '在哪 (where is it)  存在(exist)',
				usage : '厕所在哪？往前走，就在那边 (Where is the rest room? Go straight, right there.)'
			},
			{
				id : 'su-night',
				pinyin : 'Sù',
				entry : '宿',
				translation : 'night, lodge',
				words : '宿舍 (dorm) 住宿 (lodge for a night)',
				usage : '你的宿舍很大，住几个人？ (Your dorm is pretty big, how many roommates do you have?)'
			},
			{
				id : 'she-house',
				pinyin : 'Shè',
				entry : '舍',
				translation : 'house, hut',
				words : '宿舍 (dorm) 农舍 (farmhouse)',
				usage : '学生宿舍不远，就在南边。 (The student dorm is not very far, just go south from here.)'
			},
			{
				id : 'nu-famale',
				pinyin : 'Nǚ',
				entry : '女',
				translation : 'famale',
				words : "女生 (girls) 女厕所 (lady's room)",
				usage : '我们班有十二个女生。 (Our class has 12 girls.)'
			},
			{
				id : 'zuo-sit',
				pinyin : 'Zuò',
				entry : '坐',
				translation : 'sit',
				words : '坐下 (sit down) 坐车  (take a bus/car)',
				usage : '咱们骑车去还是坐车去？(How do we go there, by bus or by bike?)'
			},
			{
				id : 'xie-thank',
				pinyin : 'Xiè',
				entry : '谢',
				translation : 'thank',
				words : "谢谢 (thank you) 谢幕 (take a bow)",
				usage : '太谢谢你了。别客气。(Thanks a lot. You are welcome.)'
			},
			{
				id : 'dui-correct',
				pinyin : 'Duì',
				entry : '对',
				translation : 'correct, pair, opposite',
				words : "对不起 (sorry, excuse me) 对了 (it is right) 对面 (opposite)",
				usage : '对不起，请问厕所在哪？(Excuse me, where is the restroom?)'
			},
			{
				id : 'qu-rise',
				pinyin : 'Qǐ',
				entry : '起',
				translation : 'rise, begin, get up',
				words : '起床 (get up) 升起 (rise) 了不起 (amazing)',
				usage : '起床后，把房间收拾好。(Put the room up after you have got up.)'
			},
			{
				id : 'dao-road',
				pinyin : 'Dào',
				entry : '道',
				translation : 'road, principle',
				words : '知道 (know) 道路 (road, path) 道理 (principle)',
				usage : '你知道他是谁吗？(Do you know who he is?)'
			},
			{
				id : 'mei-no',
				pinyin : 'Méi',
				entry : '没',
				translation : 'no, have not, disappear',
				words : '没有 (do not have) 没事  (nothing to do, it is OK)',
				usage : "你没穿大衣，冷吗？我没事。(Are you cold, you don't have a jacket on? I'm good.)"
			},
			{
				id : 'guan-off',
				pinyin : 'Guān',
				entry : '关',
				translation : 'turn off, close',
				words : "关系 (relation) 关门 (close the door)",
				usage : "外面冷，穿件衣服。没关系，我一会儿就回来。(It's cold outside, put on yuor jacket. It's OK, I'll be right back.)"
			},
			{
				id : 'xi-relation',
				pinyin : 'Xì',
				entry : '系',
				translation : 'relation, department',
				words : '关系 (relation) 外语系 (foreign language dept.)',
				usage : '外语系有一百多个学生。(The foreign language department has over a hundred students.)'
			},
			{
				id : 'zai-again',
				pinyin : 'Zài',
				entry : '再',
				translation : 'again, repeat',
				words : "再见 (bye) 再做一次 (do it over)",
				usage : "我没听清楚，请你再说一遍。(I didn't hear you clearly, could you say it again?)"
			} ]
};

var NPCR1_05_2 = {
	course : 'npcr1',
	lesson : '05',
	section : '2',
	title : '第五课生词(2)',
	entries : '小,姐,层,号,用,晚,尺,上,下,大',
	quizletId : '181785305/',
	characters : [ {
		id : 'xiao-small',
		pinyin : 'Xǐao',
		entry : '小',
		translation : 'small',
		words : '小心 (careful) 小丑 (clown) 小孩 (kids)',
		usage : '一不小心，我把杯子掉在地下了。 (I dropped the glass accidentally.)'
	}, {
		id : 'jie-sister',
		pinyin : 'Jiě',
		entry : '姐',
		translation : 'sister',
		words : '姐姐 (elder sister) 小姐 (Miss)',
		usage : '姐姐比我大三岁。 (My sister is 3 years older than me.)'
	}, {
		id : 'ceng-layer',
		pinyin : 'Céng',
		entry : '层',
		translation : 'layer, floor',
		words : '一层 (one layer/floor) 阶层 (statum)',
		usage : '这座楼房有五层。 (This building has 5 floors)'
	}, {
		id : 'hao-number',
		pinyin : 'Hào',
		entry : '号',
		translation : 'number',
		words : '七号 (number 7) 编号 (code)',
		usage : '他家在三楼十五号。 (His appartment number is 315.)'
	}, {
		id : 'yong-use',
		pinyin : 'Yòng',
		entry : '用',
		translation : 'use',
		words : '不用 (do not need to) 用法 (usage) 有用 (usaful)',
		usage : '要不要先休息一会？不用了。 (Take a five? No, I am fine.)'
	}, {
		id : 'wan-late',
		pinyin : 'Wǎn',
		entry : '晚',
		translation : 'late',
		words : '晚上 (evening) 晚了 (late)',
		usage : "快点，要不然就晚了。 (Harry up, we'll be late.)"
	}, {
		id : 'chi-ruler',
		pinyin : 'Chǐ',
		entry : '尺',
		translation : 'ruler',
		words : '尺子 (ruler) 尺寸 (size)',
		usage : '铅笔盒里有一把短短的木尺。 (there is a short roler in the pencil case)'
	}, {
		id : 'shang-up',
		pinyin : 'Shàng',
		entry : '上',
		translation : 'up',
		words : '上面 (up there) 上午 (morning) 上山 (climb mountain)',
		usage : '上山容易下山难。 (climbing up is actually easier than going downhill)'
	}, {
		id : 'xia-down',
		pinyin : 'Xìa',
		entry : '下',
		translation : 'down',
		words : '下去 (go down) 手下 (men who work for me) 下巴 (chin)',
		usage : '下午，天气暖和多了。 (It is a lot warmer in the afternoon.)'
	}, {
		id : 'da-big',
		pinyin : 'Dà',
		entry : '大',
		translation : 'big',
		words : '大家 (everyone) 大方 (generous) 长大 (grow up)',
		usage : '“赶快回来”他大声喊道。 ("Get back now", he shouted.)'
	} ]
};

var NPCR1_06_1 = {
	course : 'npcr1',
	lesson : '06',
	section : '1',
	title : '第六课生词(1)',
	entries : '去,游,泳,昨,天,京,剧,怎,样,有,意,思,今,气,太,时,候,现',
	quizletId : '181785574/',
	characters : [
			{
				id : 'qu-go',
				pinyin : 'Qù',
				entry : '去',
				translation : 'go',
				words : '去除 (remove) 过去 (in the past)',
				usage : '她过去是一个老师 (She used to be a teacher.)'
			},
			{
				id : 'you-tour',
				pinyin : 'Yóu',
				entry : '游',
				translation : 'tour',
				words : '游泳 (swim) 旅游 (travel)',
				usage : '他游泳游得很快。 (He is a fast swimmer.)'
			},
			{
				id : 'yong-swim',
				pinyin : 'Yǒng',
				entry : '泳',
				translation : 'swim',
				words : '泳装 (swimsuit) 自由泳 (free style swimming)',
				usage : '她是个泳装模特。 (She is a swimsuit model)'
			},
			{
				id : 'zuo-yesterday',
				pinyin : 'Zuó',
				entry : '昨',
				translation : 'yesterday',
				words : '昨天 (yesterday)',
				usage : '昨天是星期天。 (Yesterday was Sunday.)'
			},
			{
				id : 'tian-sky',
				pinyin : 'Tīan',
				entry : '天',
				translation : 'sky',
				words : '天气 (weather) 明天 (tomorrow) 聊天 (to chat with)',
				usage : '今天的天气真好。 (Nice day today)'
			},
			{
				id : 'jing-capital',
				pinyin : 'Jīng',
				entry : '京',
				translation : 'capital',
				words : '北京 (Beijing) 京剧 (Beijing Opera)',
				usage : "你是什么时候去的北京？ (When did you go to Beijing?)"
			},
			{
				id : 'ju-drama',
				pinyin : 'Jù',
				entry : '剧',
				translation : 'drama',
				words : '京剧 (Beijing Opera) 剧场 (theater)',
				usage : '昨天你在哪个剧场看得京剧？ (Which theater did you go to watch Beijing opera?)'
			},
			{
				id : 'zen-how',
				pinyin : 'Zěn',
				entry : '怎',
				translation : 'how',
				words : '怎样 (how) 怎么办 (what to do)',
				usage : '现在怎么办，向左还是向右？ (Now what, turn right or left?)'
			},
			{
				id : 'yang-type',
				pinyin : 'Yàng',
				entry : '样',
				translation : 'type, kind, pattern',
				words : '怎样 (how) 一样 (same) 各种各样 (variety)',
				usage : '各种各样的好吃的，都不知道该吃什么了。 (All kinds of good food, not sure what to eat first.)'
			},
			{
				id : 'you-have',
				pinyin : 'Yǒu',
				entry : '有',
				translation : 'have, own, exist',
				words : '没有 (do not have) 有趣 (interesting) 拥有 (own)',
				usage : '明天有时间吗，一起去吃饭？ (Are you free tomorrow, how about we go to lunch together?)'
			},
			{
				id : 'yi-meaning',
				pinyin : 'Yì',
				entry : '意',
				translation : 'meaning, intent, idea',
				words : '意思 (meaning) 意见 (idea,thought) 好意 (kindness)',
				usage : '我的意思是这个电影太没意思了。 (I mean this movie is very boring.)'
			},
			{
				id : 'si-think',
				pinyin : 'Sī',
				entry : '思',
				translation : 'think',
				words : '意思 (meaning) 思想 (thought)',
				usage : '我的意思是这个电影太没意思了。 (I mean this movie is boring.)'
			},
			{
				id : 'jin-now',
				pinyin : 'Jīn',
				entry : '今',
				translation : 'now, this',
				words : '今天 (today) 今后 (from now on)',
				usage : '今天下午，天气会暖和多了。 (It will be a lot warmer this afternoon.)'
			},
			{
				id : 'qi-air',
				pinyin : 'Qì',
				entry : '气',
				translation : 'air, gas',
				words : '天气 (weather) 客气 (polite) 生气 (angry)',
				usage : '该怎么样还是怎么样，你生气也没用。 (That is the way it goes regardless how angry you are.)'
			},
			{
				id : 'tai-too',
				pinyin : 'Tài',
				entry : '太',
				translation : 'too, very',
				words : '太好了 (very good) 太阳 (sun) 太太 (wife)',
				usage : '天气太好了，出去晒晒太阳怎么样？ (It is really nice outside, want to take a sunbathe?)'
			},
			{
				id : 'shi-time',
				pinyin : 'Shí',
				entry : '时',
				translation : 'time, hour',
				words : '时间 (time) 小时 (one hour) 同时 (same time)',
				usage : '时候不早了，咱们得走了。 (It is time to go now.)'
			},
			{
				id : 'hou-wait',
				pinyin : 'Hòu',
				entry : '候',
				translation : 'wait, time',
				words : '时候 (time) 等候 (wait for) 气候 (climate)',
				usage : '这些年，气候越来越暖了。 (The climate has become warmmer these years.)'
			}, {
				id : 'xian-now',
				pinyin : 'Xiàn',
				entry : '现',
				translation : 'now, current',
				words : '现在 (now) 现钱 (cash)',
				usage : '现在买东西基本上不用现钱了。 (We do not use cach much nowadays.)'
			} ]
};

var NPCR1_06_2 = {
	course : 'npcr1',
	lesson : '06',
	section : '2',
	title : '第六课生词(2)',
	entries : '明,间,说,遍,打,球,抱,歉,恐,怕,行',
	quizletId : '181785790/',
	characters : [
			{
				id : 'ming-bright',
				pinyin : 'Míng',
				entry : '明',
				translation : 'bright, next',
				words : '明天 (tomorrow) 明亮 (bright)',
				usage : '明天我就要毕业了 (I will graduate tomorrow.)'
			},
			{
				id : 'jian-between',
				pinyin : 'Jiān',
				entry : '间',
				translation : 'between',
				words : '时间 (time) 房间 (room)',
				usage : '时间过得真快。 (The time flies.)'
			},
			{
				id : 'shuo-talk',
				pinyin : 'Shuō',
				entry : '说',
				translation : 'talk, say',
				words : '说话 (say) 胡说 (nonsense)',
				usage : '他一边说话，一边喝了口水。 (He took a sip of water while talking.)'
			},
			{
				id : 'bian-all',
				pinyin : 'Biàn',
				entry : '遍',
				translation : 'all, everywhere',
				words : '一遍 (one time)',
				usage : '这本书很好，我上个星期又看了一遍。 (This is a good book and I red it again last week.)'
			},
			{
				id : 'da-beat',
				pinyin : 'Dǎ',
				entry : '打',
				translation : 'beat, fight, break',
				words : '打球 (play ball) 打算 (plan) 打架 (fight)',
				usage : '喜欢打篮球吗？不喜欢，我喜欢打冰球。 (Do you like to play basketball? No, but I like hockey.)'
			},
			{
				id : 'qiu-ball',
				pinyin : 'Qiú',
				entry : '球',
				translation : 'ball',
				words : '篮球 (basketball) 球赛 (ball game)',
				usage : "那场球赛什么时候开始？ (When will the game start?)"
			},
			{
				id : 'bao-hold',
				pinyin : 'Bào',
				entry : '抱',
				translation : 'hold, hug',
				words : '抱歉 (apologize) 拥抱 (hug)',
				usage : '她一弯腰，把小孩抱了起来。 (She bended over to pick up the kid.)'
			},
			{
				id : 'qian-apologize',
				pinyin : 'Qiàn',
				entry : '歉',
				translation : 'apologize, deficient',
				words : '抱歉 (apologize) 歉意 (regret)',
				usage : '很抱歉，我不能喝酒。 (Sorry, I cannot drink.)'
			},
			{
				id : 'kong-fear',
				pinyin : 'Kǒng',
				entry : '恐',
				translation : 'fear, be afraid',
				words : '恐怕 (be afraid) 恐龙 (dinosaur)',
				usage : '这样恐怕不行，他会不高兴的。 (I am afraid it will make him unhappy if we do this.)'
			},
			{
				id : 'pa-fear',
				pinyin : 'Pà',
				entry : '怕',
				translation : 'fear, be afraid',
				words : '害怕 (fear) 怕黑 (afraid of dark)',
				usage : '别害怕，慢慢来，你可以做好的。 (Do not be afraid, just be patient, you can do this.)'
			},
			{
				id : 'xing-walk',
				pinyin : 'Xíng',
				entry : '行',
				translation : 'walk, capable',
				words : '不行 (cannot) 旅行 (travel)',
				usage : '不行就算了，我再找别人。 (It is OK if you cannot do it, I will get someone else.)'
			} ]
};

var NPCR1_07_1 = {
	course : 'npcr1',
	lesson : '07',
	section : '1',
	title : '第七课生词(1)',
	entries : '开,高,兴,看,名,片,教,授,张,方,足',
	quizletId : '181786016/',
	characters : [
			{
				id : 'kai-open',
				pinyin : 'Kāi',
				entry : '开',
				translation : 'open',
				words : '开学 (school open) 开始 (start) 打开 (open)',
				usage : '明天就开学了。 (The school opens tomorrow.)'
			},
			{
				id : 'gao-high',
				pinyin : 'Gāo',
				entry : '高',
				translation : 'high',
				words : '高兴 (happy) 高楼 (high rise building)',
				usage : '这样恐怕不行，他会不高兴的。 (I am afraid it will make him unhappy if we do this.)'
			},
			{
				id : 'xin-interest',
				pinyin : 'Xìng',
				entry : '兴',
				translation : 'interest, desire to do',
				words : '高兴 (happy) 兴奋 (exciting)',
				usage : '兴奋得说不出话来了。 (Too exciting to talk.)'
			},
			{
				id : 'kan-see',
				pinyin : 'Kàn',
				entry : '看',
				translation : 'see, read',
				words : '看见 (see) 看书 (read a book)',
				usage : '这本书很好，我上个星期又看了一遍。 (This is a good book and I red it again last week.)'
			},
			{
				id : 'ming-name',
				pinyin : 'Míng',
				entry : '名',
				translation : 'name',
				words : '名片 (business card) 名字 (name)',
				usage : '这是我的名片，上面有我的电话。 (Here is my card with my phone number.)'
			},
			{
				id : 'pian-piece',
				pinyin : 'Piàn',
				entry : '片',
				translation : 'a piece of',
				words : '名片 (business card) 照片 (photo)',
				usage : "照片上的那个男孩是谁？ (Who is that boy in the photo?)"
			},
			{
				id : 'jiao-teach',
				pinyin : 'Jiào',
				entry : '教',
				translation : 'teach',
				words : '教授 (professor) 教材 (textbook)',
				usage : '张教授走进教室，手里拿着一本教材。 (Prof. Zhange walked into the classroom with a textbook in his hand.)'
			},
			{
				id : 'shou-show',
				pinyin : 'Shòu',
				entry : '授',
				translation : 'show, give',
				words : '教授 (professor) 传授 (show sb. the ropes)',
				usage : '张教授走进教室。 (Prof. Zhange walked into the classroom.)'
			},
			{
				id : 'zhang-zhang',
				pinyin : 'Zhāng',
				entry : '张',
				translation : 'a surname',
				words : '老张 (Lao Zhang) 一张(a piece of)',
				usage : '一张纸，一张照片，一张床。 (A piece of paper, a photo, a bed.)'
			},
			{
				id : 'fang-square',
				pinyin : 'Fāang',
				entry : '方',
				translation : 'square',
				words : '方向 (direction) 长方 (rectangle) 方面 (aspect)',
				usage : '他特别大方，总是抢着买单。 (He is very generous, always trying to get the bill.)'
			},
			{
				id : 'zu-foot',
				pinyin : 'Zū',
				entry : '足',
				translation : 'foot',
				words : '手足 (hand & foot, brothers) 满足 (satisfied) 不足 (insufficient)',
				usage : '大吃了一顿，他心里很满足。 (He felt very satisfied after a big meal)'
			} ]
};

var NPCR1_07_2 = {
	course : 'npcr1',
	lesson : '07',
	section : '2',
	title : '第七课生词(2)',
	entries : '介,绍,字,专,业,美,术,文,系,耳, 家',
	quizletId : '181786251/',
	characters : [
			{
				id : 'jie-referral',
				pinyin : 'Jiè',
				entry : '介',
				translation : 'referral',
				words : '介绍 (introduce) 中介 (intermediary, agent)',
				usage : '我来介绍你认识他 (Let me introduce you to him.)'
			},
			{
				id : 'shao-introduce',
				pinyin : 'Shào',
				entry : '绍',
				translation : 'introduce',
				words : '介绍 (introduce) 介绍 (introduce)',
				usage : '我来介绍你认识他。 (Let me introduce you to him.)'
			},
			{
				id : 'zi-character',
				pinyin : 'Zì',
				entry : '字',
				translation : 'character',
				words : '名字 (name) 文字 (text) 数字 (number)',
				usage : '你有一个很好的名字。 (You have a nice name.)'
			},
			{
				id : 'zhuan-specific',
				pinyin : 'Zhuān',
				entry : '专',
				translation : 'specific',
				words : '专业 (major) 专门 (specific)',
				usage : '这本书很好，是专门讲旅游的。 (This is a good book about traveling.)'
			},
			{
				id : 'ye-business',
				pinyin : 'Yè',
				entry : '业',
				translation : 'business, industory',
				words : '失业 (unemployed) 毕业 (graduate)',
				usage : '马上就毕业了。 (I will graduate soon.)'
			},
			{
				id : 'mei-beauty',
				pinyin : 'Měi',
				entry : '美',
				translation : 'beauty, pretty',
				words : '美丽 (beauty) 美国 (U.S.A.)',
				usage : "照片上的那个美丽的女孩是谁？ (Who is that beautful girl in the photo?)"
			},
			{
				id : 'shu-technique',
				pinyin : 'Shù',
				entry : '术',
				translation : 'technique',
				words : '美术 (art, painting) 技术 (technique)',
				usage : '这是个技术问题。 (This is a technial issue.)'
			},
			{
				id : 'wen-text',
				pinyin : 'Wén',
				entry : '文',
				translation : 'text, article',
				words : '文学 (literature) 文章 (article)',
				usage : '他写得一手好文章。 (He is very good at writing.)'
			},
			{
				id : 'xi-department',
				pinyin : 'Xì',
				entry : '系',
				translation : 'department, a series of',
				words : '外语系 (Department of Foreign Languages) 系列(a series of)',
				usage : '外语系，中文系，数学系。 (Departments of Foreign Language, Chinese, Mathematics)'
			}, {
				id : 'er-ear',
				pinyin : 'ěr',
				entry : '耳',
				translation : 'ear',
				words : '耳朵 (ear) 木耳 (wood ears, fungus)',
				usage : '他有一双大耳朵。 (He has a pair of big ears.)'
			}, {
				id : 'jia-home',
				pinyin : 'Jiā',
				entry : '家',
				translation : 'home',
				words : '回家 (go home) 家长 (parents) 大家 (everyone)',
				usage : '在回家的路上，我买了些水果。 (I bought some fruits on my way home.)'
			} ]
};

var NPCR1_08_1 = {
	course : 'npcr1',
	lesson : '08',
	section : '1',
	title : '第八课生词(1)',
	entries : '几,照,个,两,还,共,妹,狗,当,然,可,爱,做,工,作',
	quizletId : '181786673/',
	characters : [
			{
				id : 'ji-few',
				pinyin : 'Jǐ',
				entry : '几',
				translation : 'a few',
				words : '几天 (a few days) 几乎 (almost)',
				usage : '再过几天就开学了。 (The school will open in a few days.)'
			},
			{
				id : 'zhao-shine',
				pinyin : 'Zhào',
				entry : '照',
				translation : 'shine, photo',
				words : '照片 (photo) 照样 (as usual)',
				usage : '这张照片照得真好。 (a very well taken photo.)'
			},
			{
				id : 'ge-one',
				pinyin : 'Gè',
				entry : '个',
				translation : 'messure word (a)',
				words : '几个 (a few) 个别 (exceptions)',
				usage : '一个人，两个饺子，三个书包。 (a person, two dumplings, three bags.)'
			},
			{
				id : 'liang-two',
				pinyin : 'Liǎng',
				entry : '两',
				translation : 'two, Liang (unit of weight:50g)',
				words : '两个 (two of) 三两 (3 Liang, 150g)',
				usage : '桌子上放着两个碗。 (There are two bowls on the table.)'
			},
			{
				id : 'hai-also',
				pinyin : 'Hái',
				entry : '还',
				translation : 'also',
				words : '还有 (in addition to) 还好 (furtunately)',
				usage : '他喜欢看书，还喜欢听音乐。 (In addition to reading, he enjoys music.)'
			},
			{
				id : 'gong-common',
				pinyin : 'Gòng',
				entry : '共',
				translation : 'common, together',
				words : '一共 (altogether) 共同 (in common)',
				usage : "屋里有三个女孩，两个男孩和一个老师，一共六个人？ (3 girs, 2 boys and a teacher, there are 6 people total in the room.)"
			},
			{
				id : 'mei-sister',
				pinyin : 'Mèi',
				entry : '妹',
				translation : 'yonger sister',
				words : '妹妹 (yonger sister)',
				usage : '他的妹妹是个学生。 (His yonger sister is a student.)'
			},
			{
				id : 'gou-dog',
				pinyin : 'Gǒu',
				entry : '狗',
				translation : 'dog',
				words : '黄狗 (a yellow dog) 狗粮 (dog food)',
				usage : '我家的小花狗特别可爱。 (My little dog is very cute.)'
			},
			{
				id : 'dang-at',
				pinyin : 'Dāng',
				entry : '当',
				translation : 'be, work as, at',
				words : '当然 (of course) 当家(in charge of)',
				usage : '他们家是当然是他当家。 (Of course, he is in charge in the family.)'
			},
			{
				id : 'ran-but',
				pinyin : 'Rán',
				entry : '然',
				translation : 'however, but',
				words : '当然 (of course) 虽然 (although)',
				usage : '虽然不愿意，但她还是去了。 (She went anyway, not willingly though.)'
			},
			{
				id : 'ke-but',
				pinyin : 'Kě',
				entry : '可',
				translation : 'but',
				words : '可爱 (lovely, cute) 可是 (but) 可以 (alright, could)',
				usage : '这件事你可以做得更好些。 (You could do it better.)'
			},
			{
				id : 'ai-love',
				pinyin : 'Ài',
				entry : '爱',
				translation : 'love',
				words : '可爱 (lovely, cute) 爱好 (hobby)',
				usage : '他的爱好有：打球，唱歌，读书，钓鱼等。 (His hobbies include play ball, singing, reading, fishing, etc.)'
			},
			{
				id : 'zuo-do',
				pinyin : 'Zuò',
				entry : '做',
				translation : 'do',
				words : '做事 (do) 做饭 (cooking)',
				usage : '她做事特别认真。 (She is very conscientious in her work.)'
			},
			{
				id : 'gong-work',
				pinyin : 'Gōng',
				entry : '工',
				translation : 'work',
				words : '工作 (work, job) 工人 (worker)',
				usage : '工作结束后，大家一起去喝了一杯啤酒。 (They had a beer together after work.)'
			},
			{
				id : 'zuo-work',
				pinyin : 'Zuò',
				entry : '作',
				translation : 'work',
				words : '作业 (homework) 作家 (writer)',
				usage : '因为作业太多，所以他睡得很晚。 (He went to bed very late as he had a lot of homework.)'
			} ]
};

var NPCR1_08_2 = {
	course : 'npcr1',
	lesson : '08',
	section : '2',
	title : '第八课生词(2)',
	entries : '多,少,喜,欢,百,江,入,是,水,走',
	quizletId : '181786868/',
	characters : [
			{
				id : 'duo-more',
				pinyin : 'Duō',
				entry : '多',
				translation : 'more, a lot',
				words : '多少 (how much/many) 不多 (not much/many)',
				usage : '苹果多少钱一斤？ (How much a pound is the apple?)'
			},
			{
				id : 'shao-less',
				pinyin : 'Shǎo',
				entry : '少',
				translation : 'less, a few',
				words : '少量 (a little) 太少了 (too little)',
				usage : '才两个人，太少了，我需要十五个人。 (Only two? Come on, I need at least 15 people.)'
			},
			{
				id : 'xi-happy',
				pinyin : 'Xǐ',
				entry : '喜',
				translation : 'happy',
				words : '喜欢 (like) 喜事 (happy event, wedding)',
				usage : '喜欢吗？那就送给你了。 (Do you like it? It is your then.)'
			},
			{
				id : 'huang-joyous',
				pinyin : 'Huān',
				entry : '欢',
				translation : 'joyous',
				words : '欢乐 (joyous) 欢快 (cheerful)',
				usage : '这是一段欢快的音乐。 (This is a piece of cheerful music.)'
			},
			{
				id : 'bai-hundred',
				pinyin : 'Bǎi',
				entry : '百',
				translation : 'hundred',
				words : '一百 (one hundred) 百万 (million)',
				usage : '电影院里只有不到一百人。 (There are less than a hundred people in the cinema)'
			},
			{
				id : 'jiang-river',
				pinyin : 'Jīang',
				entry : '江',
				translation : 'river',
				words : '长江 (Yangtze River) 江山 (river & mountain, homeland)',
				usage : '长江是中国的第一大河。 (Yangtze River is the longest revier of China.)'
			},
			{
				id : 'ru-into',
				pinyin : 'Rù',
				entry : '入',
				translation : 'into',
				words : '进入 (go into) 入门 (get started)',
				usage : '在电影院的入口处，我碰见了他。 (I bumped heads with him at the entrance of the theatre.)'
			},
			{
				id : 'shi-yes',
				pinyin : 'Shì',
				entry : '是',
				translation : 'be, yes',
				words : '是非 (right and wrong) 不是 (no, not realy)',
				usage : '是还是不是，那是个问题。 (To be, or not to be, that is the question.)'
			},
			{
				id : 'shui-water',
				pinyin : 'Shǔi',
				entry : '水',
				translation : 'water',
				words : '喝水 (drink water) 水果 (fruit) 水平 (level)',
				usage : '他先喝了一口水，然后开始讲故事。(He took a sip of water and then started telling his story.)'
			}, {
				id : 'zou-walk',
				pinyin : 'Zǒu',
				entry : '走',
				translation : 'walk',
				words : '走路 (walk) 走私 (smuggling)',
				usage : '他走得很慢。(He walks very slowly.)'
			} ]
};

var NPCR1_09_1 = {
	course : 'npcr1',
	lesson : '09',
	section : '1',
	title : '第九课生词(1)',
	entries : '年,课,星,期,午,出,属,聚,会,祝,贺,参,加,吃,蛋,糕,买,瓶,红,葡萄,酒',
	quizletId : '181787064/',
	characters : [
			{
				id : 'nian-year',
				pinyin : 'Nián',
				entry : '年',
				translation : 'year',
				words : '两年 (two years) 过年 (celebrate new year)',
				usage : '一年又一年，时间过得真快。 (Year by year, the time flies.)'
			},
			{
				id : 'ke-class',
				pinyin : 'Kè',
				entry : '课',
				translation : 'class, lesson',
				words : '上课 (take a class) 课文 (lesson text)',
				usage : '请把课文读一遍。 (Please read the lesson text.)'
			},
			{
				id : 'xing-star',
				pinyin : 'Xīng',
				entry : '星',
				translation : 'star',
				words : '星期 (a week) 星星 (stars)',
				usage : '一个星期学一课，不算太快吧？ (One lesson a week, is it too fast?)'
			},
			{
				id : 'qi-period',
				pinyin : 'Qī',
				entry : '期',
				translation : 'period',
				words : '过期 (expired) 周期 (time period)',
				usage : '你看看那个东西是不是过期了。 (Could you check the expiration date of it?)'
			},
			{
				id : 'wu-noon',
				pinyin : 'Wǔ',
				entry : '午',
				translation : 'noon',
				words : '上午 (morning) 午饭 (lunch)',
				usage : '吃完饭，我睡了半个小时午觉。 (I took half an hour nap after lunch.)'
			},
			{
				id : 'chu-out',
				pinyin : 'Chū',
				entry : '出',
				translation : 'out',
				words : '出去 (go out) 出口 (export, exit)',
				usage : '我出去一下，一会就回来。 (I need to go somewhere, will be right back.)'
			},
			{
				id : 'shu-belong',
				pinyin : 'Shǔ',
				entry : '属',
				translation : 'belong',
				words : '属虎 (born in tiger year) 属于 (belong to)',
				usage : '我属猴，他属虎，你属兔。 (I was born in the monkey year, he was in the tiger year and you were in the rabbit year.)'
			},
			{
				id : 'ju-gather',
				pinyin : 'Jù',
				entry : '聚',
				translation : 'gather',
				words : '聚会 (party)',
				usage : '今天的聚会上，大家玩得都很高兴。 (Everyone is happy in the party.)'
			},
			{
				id : 'hui-meet',
				pinyin : 'Huì',
				entry : '会',
				translation : 'meet, be able to',
				words : '开会 (have a meeting) 会做 (be able to do something)',
				usage : '一会儿，咱们要开个会。 (We will have a meeting in a few moments.)'
			},
			{
				id : 'zhu-wish',
				pinyin : 'Zhù',
				entry : '祝',
				translation : 'wish',
				words : '祝贺 (congratulate) 庆祝 (celebrate)',
				usage : '祝你生日快乐。 (Happy birthday to you.)'
			},
			{
				id : 'he-congratulate',
				pinyin : 'Hè',
				entry : '贺',
				translation : 'congratulate',
				words : '祝贺 (congratulate) 贺信 (congratulation letter)',
				usage : '祝贺你大学毕业了。 (Congratulations on your graduation.)'
			},
			{
				id : 'can-join',
				pinyin : 'Cān',
				entry : '参',
				translation : 'join',
				words : '参加 (join) 参数 (parameter)',
				usage : '大为上个星期参加了我们的球队。 (Dawei joined our team last week.)'
			},
			{
				id : 'jia-plus',
				pinyin : 'Jiā',
				entry : '加',
				translation : 'plus',
				words : '加法 (addition) 增加 (increase)',
				usage : '加减乘除。(Addition, subtraction, multiplication and division.)'
			},
			{
				id : 'chi-eat',
				pinyin : 'Chī',
				entry : '吃',
				translation : 'eat',
				words : '吃饭 (eat) 小吃 (snack)',
				usage : '护国寺小吃店有很多种好吃的早点。 (Huguosi snack bar carries a lot of good food.)'
			},
			{
				id : 'dan-egg',
				pinyin : 'Dàn',
				entry : '蛋',
				translation : 'egg',
				words : '鸡蛋 (egg) 滚蛋 (get out, go to hell)',
				usage : '做蛋糕要用一杯面，三个鸡蛋 …… (To make a cake, we need a cup of flour, three eggs and ...)'
			},
			{
				id : 'gao-cake',
				pinyin : 'Gāo',
				entry : '糕',
				translation : 'cake',
				words : '蛋糕 (cake) 年糕 (new year cake)',
				usage : '买了很大的生日蛋糕。(Bought a big birthday cake.)'
			},
			{
				id : 'mai-buy',
				pinyin : 'Mǎi',
				entry : '买',
				translation : 'buy',
				words : '买东西 (shopping) 买卖 (business)',
				usage : "快过年了，我们去买些年货。 (New Year is around the corner, let's go shopping.)"
			},
			{
				id : 'ping-bottle',
				pinyin : 'Píng',
				entry : '瓶',
				translation : 'bottle, a bottle of',
				words : '瓶子 (a bottle) 一瓶水 (a bottle of water)',
				usage : '我一口气把一瓶水喝光了。 (I drank a bottle of water in one sip.)'
			},
			{
				id : 'hong-red',
				pinyin : 'Hóng',
				entry : '红',
				translation : 'plus',
				words : '红色 (red) 血红 (blood red)',
				usage : '她穿了一件红色的外衣。(She wears a red jecket.)'
			},
			{
				id : 'putao-grape',
				pinyin : 'Pútáo',
				entry : '葡萄',
				translation : 'grape',
				words : '葡萄 (work, job)',
				usage : '这些葡萄又大又甜。 (These grapes are big and sweet.)'
			},
			{
				id : 'jiu-liqueur',
				pinyin : 'Jiǔ',
				entry : '酒',
				translation : 'liqueur, alcohol',
				words : '喝酒 (drink) 酒精 (alcohol)',
				usage : '每天喝一杯葡萄酒，对身体有好处。 (It is good to your health by drinking a glass of wine every day.)'
			} ]
};

var NPCR1_09_2 = {
	course : 'npcr1',
	lesson : '09',
	section : '2',
	title : '第九课生词(2)',
	entries : '快,乐,漂,亮,烤,鸭,寿,面,东,西',
	quizletId : '181787211/',
	characters : [
			{
				id : 'kuai-fast',
				pinyin : 'Kuài',
				entry : '快',
				translation : 'fast',
				words : '快乐 (happy) 赶快 (harry)',
				usage : '你快点吧，咱们都快晚了。 (Harry up, we are late.)'
			},
			{
				id : 'le-laugh',
				pinyin : 'Lè',
				entry : '乐',
				translation : 'laugh, happy',
				words : '快乐 (happy) 可乐 (funny)',
				usage : '快乐的小狗，每天都要出去玩一会儿。 (The little happy dog goes out to play every day.)'
			},
			{
				id : 'piao-polished',
				pinyin : 'Piào',
				entry : '漂',
				translation : 'polished',
				words : '漂亮 (pretty)',
				usage : '那个小女孩有一双漂亮的大眼睛。 (The little girl has a pair of pretty eyes.)'
			},
			{
				id : 'liang-bright',
				pinyin : 'Liàng',
				entry : '亮',
				translation : 'bright',
				words : '明亮 (bright) 天亮了 (daybreak)',
				usage : "天亮了，该起床了。 (It's daybreak, get up now.)"
			},
			{
				id : 'kao-roast',
				pinyin : 'Kǎo',
				entry : '烤',
				translation : 'roast',
				words : '烤鸭 (roasted duck) 烧烤 (barbecue)',
				usage : '我们在北京吃了两次烤鸭。 (We had Beijing rosted duck twice when we were in Beijing.)'
			},
			{
				id : 'ya-duck',
				pinyin : 'Yā',
				entry : '鸭',
				translation : 'duck',
				words : '鸭子 (duck) 野鸭 (wild duck)',
				usage : '水面上有一群野鸭子。 (There is a bunch of wild ducks on the water.)'
			},
			{
				id : 'shou-age',
				pinyin : 'Shòu',
				entry : '寿',
				translation : 'age, life',
				words : '寿面 (birthday noodle) 长寿 (longevity)',
				usage : '祝你健康长寿。 (Wish you have a long and healthy life.)'
			},
			{
				id : 'mian-flour',
				pinyin : 'Miàn',
				entry : '面',
				translation : 'flour, face',
				words : '面条 (noodle) 外面 (outside)',
				usage : "今天咱们午饭吃面条还是吃米饭？ (What's for lunch, noodle or rice?)"
			},
			{
				id : 'dong-east',
				pinyin : 'Dōng',
				entry : '东',
				translation : 'east',
				words : '东方 (east) 东西 (something)',
				usage : '我买东西去了。 (I went shopping.)'
			},
			{
				id : 'xi-west',
				pinyin : 'Xī',
				entry : '西',
				translation : 'west',
				words : '西方 (west) 西北 (north-west)',
				usage : '中国的西部有很多山 (There are many mountains in western China.)'
			} ]
};

var NPCR1_10_1 = {
	course : 'npcr1',
	lesson : '10',
	section : '1',
	title : '第十课生词(1)',
	entries : '光,盘,音,乐,商,场,常,跟,书,报,本,梁',
	quizletId : '181787485/',
	characters : [
			{
				id : 'guang-light',
				pinyin : 'Guāng',
				entry : '光',
				translation : 'light, empty',
				words : '光盘 (CD) 灯光 (light) 用光 (used up)',
				usage : '家里的灯光很好。 (The lights at home are very good.)'
			},
			{
				id : 'pan-plate',
				pinyin : 'Pán',
				entry : '盘',
				translation : 'plate, disk',
				words : '光盘 (CD) 盘子 (plate)',
				usage : '家里还有一盘饺子。 (We still have a plate of dumpings.)'
			},
			{
				id : 'yin-sound',
				pinyin : 'Yīn',
				entry : '音',
				translation : 'sound',
				words : '音乐 (music) 声音 (sound)',
				usage : '电影的名字是：音乐之声。 (The movie is: Sound of Music.)'
			},
			{
				id : 'yue-music',
				pinyin : 'Yuè',
				entry : '乐',
				translation : 'music',
				words : '音乐 (music) 乐器 (music instrument)',
				usage : '你会玩什么乐器？ (Do you play any music instrument?)'
			},
			{
				id : 'shang-business',
				pinyin : 'Shāng',
				entry : '商',
				translation : 'business',
				words : '商场 (store) 经商 (do business with)',
				usage : "吃完饭，我们去逛商场吧？ (Let's go shopping after lunch.)"
			},
			{
				id : 'chang-field',
				pinyin : 'Chǎng',
				entry : '场',
				translation : 'field',
				words : '蓝球场 (basketball court) 广场 (square)',
				usage : '北京的天安门广场很大。 (Beijing Tiananmen square is huge.)'
			},
			{
				id : 'chang-often',
				pinyin : 'Cháng',
				entry : '常',
				translation : 'often， in general',
				words : '经常 (often) 平常 (usually)',
				usage : '我经常到这家餐厅吃饭。 (I come to this restaurant often.)'
			},
			{
				id : 'gen-with',
				pinyin : 'Gēn',
				entry : '跟',
				translation : 'with, follow',
				words : '跟着 (follow) 脚跟 (heel)',
				usage : '跟我来，我给你看样东西。 (Follow me, let me show you something.)'
			},
			{
				id : 'shu-book',
				pinyin : 'Shū',
				entry : '书',
				translation : 'book',
				words : '读书 (read a book) 写书 (write a book)',
				usage : '我今年要读十本书。 (I plan to read ten books this year.)'
			},
			{
				id : 'bao-newspaper',
				pinyin : 'Bào',
				entry : '报',
				translation : 'newspaper, report',
				words : '报纸 (newspaper) 报告 (report)',
				usage : '现在人们都不看报纸了。 (People do not read newspaper any more nowadays.)'
			},
			{
				id : 'ben-origin',
				pinyin : 'Běn',
				entry : '本',
				translation : 'origin',
				words : '本子 (note book) 本来 (originally)',
				usage : '一本书，两张报纸，三个本子。 (A book, two pieces of newspaper and three note books.)'
			},
			{
				id : 'liang-beam',
				pinyin : 'Liáng',
				entry : '梁',
				translation : 'beam, family name',
				words : '梁祝 (Butterfly Lovers. Liang and Zhu are family names of the main characters in the story) 房梁 (beam)',
				usage : '梁祝是一首很有名的音乐作品。 (Butterfly Lovers (Liangzhu) is a well-known music works.)'
			} ]
};

var NPCR1_10_2 = {
	course : 'npcr1',
	lesson : '10',
	section : '2',
	title : '第十课生词(2)',
	entries : '傅,香,蕉,苹,果,容,易,钱,斤,块,毛,贵,分,送,给,找',
	quizletId : '181787647/',
	characters : [
			{
				id : 'fu-tutor',
				pinyin : 'Fù',
				entry : '傅',
				translation : 'tutor, help',
				words : '师傅 (master)',
				usage : "师傅，我要到后海去。 (Mister, take me to HouHai.)"
			},
			{
				id : 'xiang-incense',
				pinyin : 'Xiāng',
				entry : '香',
				translation : 'incense, scented',
				words : '香蕉 (banana) 香水 (perfume)',
				usage : '这个菜的味道真香。 (The taste of this dish is really fragrant.)'
			},
			{
				id : 'jiao-banana',
				pinyin : 'Jiāo',
				entry : '蕉',
				translation : 'banana',
				words : '香蕉 (banana)',
				usage : '香蕉苹果是有香蕉味的苹果。 (Bananas Apples are apples with the taste of banana.)'
			},
			{
				id : 'ping-apple',
				pinyin : 'Píng',
				entry : '苹',
				translation : 'apple',
				words : '苹果 (apple)',
				usage : "苹果手机比苹果贵多了。 (An Apple phone is a lot more expensive than a real apple.)"
			},
			{
				id : 'guo-fruit',
				pinyin : 'Guǒ',
				entry : '果',
				translation : 'fruit, seed',
				words : '水果 (fruit) 果实 (seed in general)',
				usage : '吃完饭应该吃点水果。 (Should have some fruits after a meal.)'
			},
			{
				id : 'rong-allow',
				pinyin : 'Róng',
				entry : '容',
				translation : 'allow, contain',
				words : '容易 (easy) 容量 (capacity)',
				usage : '这件事很容易办。 (This is easy.)'
			},
			{
				id : 'yi-easy',
				pinyin : 'Yì',
				entry : '易',
				translation : 'easy',
				words : '容易 (easy) 简易 (simple)',
				usage : '说起来容易做起来难。 (Easier said than done.)'
			},
			{
				id : 'qian-money',
				pinyin : 'Qián',
				entry : '钱',
				translation : 'money',
				words : '花钱 (cost) 有钱 (rich)',
				usage : "今天这顿饭谁花钱？ (Who is paying for this meal?)"
			},
			{
				id : 'jin-jin',
				pinyin : 'Jīn',
				entry : '斤',
				translation : 'unit of weight (500g)',
				words : '三斤 (3 Jins) 半斤 (half Jin)',
				usage : '我买了三斤苹果，两斤葡萄。 (I bought 3 Jins of apples and two Jins of grapes.)'
			},
			{
				id : 'kuai-kuai',
				pinyin : 'Kuài',
				entry : '块',
				translation : 'measure word, one Yuan when used in money',
				words : '十块 (10 Yuans) 一块砖 (a brake)',
				usage : "十块钱一斤，是不是太贵了？ (10 Yuans a Jin, isn't it too expensive?)"
			},
			{
				id : 'mao-hair',
				pinyin : 'Máo',
				entry : '毛',
				translation : 'hair, measure word, 10 Fen (1/10 Yuan) when used in money',
				words : '五毛 (5 Maos) 毛发 (hair)',
				usage : "这只狗身上的毛真漂亮。(This dog's hair is so pretty.)"
			},
			{
				id : 'gui-expensive',
				pinyin : 'Guì',
				entry : '贵',
				translation : 'expensive',
				words : '太贵了 (too expensive) 贵重 (precious)',
				usage : "才十块钱一斤，一点儿都不贵。 (Only 10 Yuans a Jin, that's a good deal.)"
			},
			{
				id : 'fen-split',
				pinyin : 'Fēn',
				entry : '分',
				translation : 'split, measure word (1/10 Mao) when used in money ',
				words : '六分 (6 Fens) 分开 (split, devide)',
				usage : '把葡萄分成六份，每人一份 (Divide the grapes into six portions, one per person.)'
			},
			{
				id : 'song-give',
				pinyin : 'Sòng',
				entry : '送',
				translation : 'send, give (as a gift)',
				words : '送礼 (gifting) 送货 (deliver)',
				usage : '他送了一个很大的生日蛋糕。(He sent a big birthday cake.)'
			},
			{
				id : 'gei-give',
				pinyin : 'Gěi',
				entry : '给',
				translation : 'give',
				words : '送给 (give) 不给 (do not give)',
				usage : "这个苹果给你吃。 (This apple is for you.)"
			},
			{
				id : 'zhao-find',
				pinyin : 'Zhǎo',
				entry : '找',
				translation : 'find, seek, change',
				words : '找钱 (give change) 寻找 (seek)',
				usage : '你把那本书放在哪了，我怎么找不到了。 (Where did you put that book, I cannot find it.)'
			} ]
};

var NPCR1_11_1 = {
	course : 'npcr1',
	lesson : '11',
	section : '1',
	title : '第十一课生词(1)',
	entries : '点,司,机,差,刻,回,能,到,教,孙,岁,数',
	quizletId : '181787874/',
	characters : [
			{
				id : 'dian-point',
				pinyin : 'Diǎn',
				entry : '点',
				translation : 'point, hour',
				words : "三点 (3 o'clock) 地点 (place) 优点 (advantage)",
				usage : '请问，现在几点了？ (What time is it please?)'
			},
			{
				id : 'si-manage',
				pinyin : 'Sī',
				entry : '司',
				translation : 'manage, control',
				words : '司机 (driver) 司令 (commander)',
				usage : '这个出租车司机喜欢聊天。 (This texi driver is very chatty.)'
			},
			{
				id : 'ji-machine',
				pinyin : 'Jī',
				entry : '机',
				translation : 'machine',
				words : '机器 (machine) 手机 (cell phone)',
				usage : '现在几乎人人都有手机。 (Now almost every one has a cell phone.)'
			},
			{
				id : 'cha-poor',
				pinyin : 'Chà',
				entry : '差',
				translation : 'poor, short of',
				words : '差一点 (amlost) 差不多 (is about)',
				usage : '差一点就晚了。 (We are amlost late.)'
			},
			{
				id : 'ke-quater',
				pinyin : 'Kè',
				entry : '刻',
				translation : 'quater, moment',
				words : '一刻钟 (a quater)',
				usage : "现在是差一刻八点，不着急，我们还有半个小时。 (Now it is a quater to eight. We are good, still have half an hour.)"
			},
			{
				id : 'hui-return',
				pinyin : 'Huí',
				entry : '回',
				translation : 'return, times',
				words : '回家 (go home) 第二回 (the second time)',
				usage : '回家后，先喝点水。 (Have some water when we get back home.)'
			},
			{
				id : 'neng-able',
				pinyin : 'Néng',
				entry : '能',
				translation : 'be able to, energy',
				words : '可能 (possible) 不能 (cannot)',
				usage : "星期天我们有个聚会，你能来吗？ (We'll have a party at Sunday, would you come?)"
			},
			{
				id : 'dao-arrive',
				pinyin : 'Dào',
				entry : '到',
				translation : 'arrive, go to',
				words : '迟到 (late) 到了 (arrived)',
				usage : '到家了，把外衣脱下来吧。 (We are home, you may take off your jacket.)'
			},
			{
				id : 'jiao-teach',
				pinyin : 'Jiāo',
				entry : '教',
				translation : 'teach',
				words : '教书 (teach (as a teacher)) 教做饭 (teach how to cook)',
				usage : "不会不要紧，我来教你。 (Don't warry, I'll teach you.)"
			},
			{
				id : 'sun-grandson',
				pinyin : 'Sūn',
				entry : '孙',
				translation : 'grandchild',
				words : '孙子 (grandson) 孙女 (grand doughter)',
				usage : '他的小孙子今年都八岁了。 (His little grandson is eight years old this year.)'
			},
			{
				id : 'sui-year',
				pinyin : 'Suì',
				entry : '岁',
				translation : 'one year',
				words : '岁数 (age) 八岁 (eight years old)',
				usage : '他的小孙子今年都八岁了。 (His grandson is eight years old this year.)'
			},
			{
				id : 'shu-number',
				pinyin : 'Shù',
				entry : '数',
				translation : 'number',
				words : '数字 (number, digit) 数学 (math)',
				usage : '现在很多东西都数字化了，如音乐，图像等。 (Many things have been digitalized, like music, image, etc..)'
			} ]
};

var NPCR1_11_2 = {
	course : 'npcr1',
	lesson : '11',
	section : '2',
	title : '第十一课生词(2)',
	entries : '为,昨,玩,半,写,睡,觉,床,应,该,南,北',
	quizletId : '181788030/',
	characters : [
			{
				id : 'wei-for',
				pinyin : 'Wèi',
				entry : '为',
				translation : 'for, as',
				words : '为什么 (why) 为你好 (for your good)',
				usage : "天为什么是蓝的？ (Why the sky is blue?)"
			},
			{
				id : 'zuo-previous',
				pinyin : 'Zuó',
				entry : '昨',
				translation : 'previous, last',
				words : '昨天 (yesterday) 昨日 (yesterday)',
				usage : '我是昨天回来的。 (I came back yesterday)'
			},
			{
				id : 'wan-play',
				pinyin : 'Wán',
				entry : '玩',
				translation : 'play',
				words : '好玩 (fun) 玩笑 (joking)',
				usage : '这个玩具挺好玩的。 (This toy is pretty fun.)'
			},
			{
				id : 'ban-half',
				pinyin : 'Bàn',
				entry : '半',
				translation : 'half',
				words : '一半 (a half) 半路 (half way)',
				usage : "走到半路，他想起来没带手机。 (Half way there, he realized that he did not take his phone with him.)"
			},
			{
				id : 'xie-write',
				pinyin : 'Xiě',
				entry : '写',
				translation : 'write',
				words : '写书 (write a book) 写信 (write a letter)',
				usage : '应该经常给家里写封信。 (Should write a letter home often.)'
			},
			{
				id : 'shui-sleep',
				pinyin : 'Shuì',
				entry : '睡',
				translation : 'sleep, nap',
				words : '睡觉 (sleep) 午睡 (nap)',
				usage : '吃完饭很困，于是他就睡了个午觉。 (Pretty sleepy after lunch, so he took a nap.)'
			},
			{
				id : 'jiao-sleep',
				pinyin : 'Jiào',
				entry : '觉',
				translation : 'sleep',
				words : '睡觉 (sleep)',
				usage : "赶快睡觉，要不明天早上起不来。 (Go to bed now, you won't be able to get up on time otherwise.)"
			},
			{
				id : 'chuang-bed',
				pinyin : 'Chuáng',
				entry : '床',
				translation : 'bed',
				words : '起床 (get up) 大床 (a big bed)',
				usage : "起床后把被子叠好。 (Make your bed when you get up.)"
			},
			{
				id : 'ying-should',
				pinyin : 'Yīng',
				entry : '应',
				translation : 'should, ought to',
				words : '应该 (should, ought to) 答应 (agreed)',
				usage : '你应该八点就起了，怎么到现在还躺在床上。 (You should be up at eight, why you are still lying in bed?)'
			}, {
				id : 'gai-likely',
				pinyin : 'Gāi',
				entry : '该',
				translation : 'most likely',
				words : '应该 (should, ought to)',
				usage : "不该把他一个人留在家里。 (Should not have left him home alone.)"
			}, {
				id : 'nan-south',
				pinyin : 'Nán',
				entry : '南',
				translation : 'south',
				words : '南方 (south) 南风 (south wind)',
				usage : '南方很暖和。 (It is warm in the south.)'
			}, {
				id : 'bei-north',
				pinyin : 'Běi',
				entry : '北',
				translation : 'north',
				words : '北方 (north) 北京 (Beijing)',
				usage : '北京在中国的北方。 (Beijing is in the north of China)'
			} ]
};

var NPCR1_12_1 = {
	course : 'npcr1',
	lesson : '12',
	section : '1',
	title : '第十二课生词(1)',
	entries : '全,身,舒,服,锻,炼,头,疼,嗓,想,病,体,冷,穿,衣',
	quizletId : '181788236/',
	characters : [
			{
				id : 'quan-whole',
				pinyin : 'Quán',
				entry : '全',
				translation : 'whole, full',
				words : "全身 (whole body) 安全 (safe)",
				usage : '开了十个小时的车，终于安全到家了 (Finally we are home safely after 10 hours driving.)'
			},
			{
				id : 'shen-body',
				pinyin : 'Shēn',
				entry : '身',
				translation : 'body',
				words : '身体 (body) 身边 (around)',
				usage : '他的身体非常好，从来不生病。 (He has a good health, never gets sick.)'
			},
			{
				id : 'shu-relax',
				pinyin : 'Shū',
				entry : '舒',
				translation : 'relax, comfortable',
				words : '舒服 (comfortable) 舒展 (stretch)',
				usage : '睡了一大觉，现在感觉舒服多了。 (I had a nice sleep, feel much better now.)'
			},
			{
				id : 'fu-cloth',
				pinyin : 'Fú',
				entry : '服',
				translation : 'cloth, serve',
				words : '服装 (cloth) 服务 (serve)',
				usage : '这是个专卖服装的商店。 (This is a clothing store.)'
			},
			{
				id : 'duan-forge',
				pinyin : 'Duàn',
				entry : '锻',
				translation : 'forge',
				words : '锻炼 (exercise, work out)',
				usage : "我每天早上锻炼半个小时的身体。 (Every morning I work out about half an hour.)"
			},
			{
				id : 'lian-refine',
				pinyin : 'Liàn',
				entry : '炼',
				translation : 'refine',
				words : '锻炼 (exercise, work out) 炼钢 (Steelmaking)',
				usage : '经常锻炼身体有利健康。 (Exercising often is good to you health.)'
			},
			{
				id : 'tou-head',
				pinyin : 'Tóu',
				entry : '头',
				translation : 'head',
				words : '头疼 (headache) 头发 (hair)',
				usage : "今天我头疼，不能上学去了。 (Cannot go to school today as I'm having a headache)"
			},
			{
				id : 'teng-pain',
				pinyin : 'Téng',
				entry : '疼',
				translation : 'pain',
				words : '心疼 (distressed) 头疼 (headache)',
				usage : '见他生病，不舒服，我很心疼。 (I feel sorry for him to he is sick and suffering)'
			},
			{
				id : 'sang-throat',
				pinyin : 'Sǎng',
				entry : '嗓',
				translation : 'throat',
				words : '嗓子 (throat) 嗓音 (voice)',
				usage : "他的嗓音不错，唱歌很好听。 (He has a good voice and sings well.)"
			},
			{
				id : 'xiang-think',
				pinyin : 'Xiǎng',
				entry : '想',
				translation : 'think',
				words : '想法 (think) 思想 (thought)',
				usage : "我们都说完了，你是怎么想的？ (That's all we have, what's your thought.)"
			},
			{
				id : 'bing-disease',
				pinyin : 'Bìng',
				entry : '病',
				translation : 'disease',
				words : '生病 (get sick) 病人 (patient)',
				usage : "穿暖和点，小心生病。 (Put on a heavy jacket, don't get sick.)"
			},
			{
				id : 'ti-body',
				pinyin : 'Tǐ',
				entry : '体',
				translation : 'body',
				words : '身体 (body) 体力 (physical strength)',
				usage : "他的技术很好，就是体力不行。 (He is very skillfull, but doesn't have a good physical strength.)"
			},
			{
				id : 'leng-cold',
				pinyin : 'Lěng',
				entry : '冷',
				translation : 'cold',
				words : '寒冷 (very cold) 冷静 (calm, cool)',
				usage : "天气太冷了，我们要多穿些衣服。 (It's very cold, we need to wear a heavy jacket.)"
			},
			{
				id : 'chuan-wear',
				pinyin : 'Chuān',
				entry : '穿',
				translation : 'wear, go through',
				words : '穿衣服 (wear) 穿过 (go through, pass)',
				usage : "穿上鞋，戴好帽子，我们要出发了。 (Put on the shoes and hat, let's go now.)"
			},
			{
				id : 'yi-cloth',
				pinyin : 'Yī',
				entry : '衣',
				translation : 'cloth, dress',
				words : '衣服 (cloth) 外衣 (outer clothing) 内衣 (underwear)',
				usage : "她有很多衣服，可不知道穿该哪一件。 (She has a lot of dresses, just doesn't know which on to wear.)"
			} ]
};

var NPCR1_12_2 = {
	course : 'npcr1',
	lesson : '12',
	section : '2',
	title : '第十二课生词(2)',
	entries : '休,息,挂,发,炎,烧,感,冒,住,药,愿,意,中',
	quizletId : '181788424/',
	characters : [
			{
				id : 'xiu-rest',
				pinyin : 'Xiū',
				entry : '休',
				translation : 'rest, stop, never',
				words : "休息 (rest) 休想 (don't even think about it)",
				usage : "好好休息，明天我们要走远路。 (Take a good rest, we'll have a long walk tomorrow.)"
			},
			{
				id : 'xi-breath',
				pinyin : 'Xi',
				entry : '息',
				translation : 'breath',
				words : '休息 (rest) 喘息 (pant)',
				usage : "先休息一下，一会接着干。 (Take a five, we'll continue in a few minutes.)"
			},
			{
				id : 'gua-hang',
				pinyin : 'Guà',
				entry : '挂',
				translation : 'hang',
				words : '挂号 (register) 挂钟 (wall clock)',
				usage : '在医院挂号，要排很长的队。 (It has a long line to register at hospital.)'
			},
			{
				id : 'fa-hair',
				pinyin : 'Fā',
				entry : '发',
				translation : 'hair, issue',
				words : '头发 (hair) 发财 (make big money)',
				usage : "她的头发又黑又长。 (Her hair is long and black.)"
			},
			{
				id : 'yan-inflammation',
				pinyin : 'Yán',
				entry : '炎',
				translation : 'inflammation',
				words : '发炎 (inflammation) 肺炎 (pneumonia)',
				usage : '我嗓子发炎了，老是咳嗽。 (My throat is inflamed, so I cough a lot.)'
			},
			{
				id : 'shao-burn',
				pinyin : 'Shāo',
				entry : '烧',
				translation : 'burn, fever',
				words : '发烧 (fever) 烧烤 (barbecue)',
				usage : '他昨天着凉了，今天开始发烧。 (He caught a cold yesterday and the fever began today.)'
			},
			{
				id : 'gan-feel',
				pinyin : 'Gǎn',
				entry : '感',
				translation : 'feel',
				words : '感觉 (feel) 感动 (be moved)',
				usage : "你现在感觉好些了吗？ (Do you feel better now?)"
			},
			{
				id : 'mao-risk',
				pinyin : 'Mào',
				entry : '冒',
				translation : 'risk',
				words : '感冒 (flu, cold) 冒险 (take a risk)',
				usage : "他的感冒好了吗，怎么还没来上课？ (How is his flu, why is he still not coming to the class?)"
			},
			{
				id : 'zhu-live',
				pinyin : 'Zhù',
				entry : '住',
				translation : 'live, reside',
				words : '住院 (Hospitalized) 居住 (reside)',
				usage : '你们家住哪，离这远不远？ (Where do you live, is it far from here?)'
			},
			{
				id : 'yao-medicine',
				pinyin : 'Yào',
				entry : '药',
				translation : 'medicine',
				words : '吃药 (take medicine) 药房 (pharmacy)',
				usage : "你在这等我一会，我到药房去拿药。 (Wait here, I need to go to the pharmacy to get your medicine.)"
			},
			{
				id : 'yuan-willing',
				pinyin : 'Yuàn',
				entry : '愿',
				translation : 'willing',
				words : '愿意 (agree, willing) 心愿 (wish)',
				usage : '你要是愿意，就点一下头。 (Just nod if you agree.)'
			},
			{
				id : 'yi-meaning',
				pinyin : 'Yì',
				entry : '意',
				translation : 'meaning',
				words : '好意 (kindness) 意见 (opinion, idea)',
				usage : "有意见就说，别老让我们猜。 (Shoot your idea, don't let us guess around.)"
			},
			{
				id : 'zhong-central',
				pinyin : 'Zhōng',
				entry : '中',
				translation : 'central, in, during',
				words : '中间 (central, middle) 在 …… 中 (among) 中国 (China)',
				usage : '在我去过的城市中，我最喜欢丹佛。 (Among all the cities I have visited, Denver is my favorite.)'
			} ]
};

var NPCR1_13_1 = {
	course : 'npcr1',
	lesson : '13',
	section : '1',
	title : '第十三课生词(1)',
	entries : '姑,娘,听,告,诉,件,事,散,步,电,影,房,厨,厕,所,公',
	quizletId : '181788621/',
	characters : [
			{
				id : 'gu-aunt',
				pinyin : 'Gū',
				entry : '姑',
				translation : 'aunt',
				words : "姑姑 (aunt) 姑娘 (girl)",
				usage : '这个小姑娘长得真漂亮。 (This little girl is pretty.)'
			},
			{
				id : 'niang-mother',
				pinyin : 'Niang',
				entry : '娘',
				translation : 'mother, woman',
				words : '娘 (mother) 新娘 (bride)',
				usage : '新郎和新娘走出来了。 (Broom and bride are coming out.)'
			},
			{
				id : 'ting-hear',
				pinyin : 'Tīng',
				entry : '听',
				translation : 'hear, listen',
				words : '听说 (heard) 听力 (hearing)',
				usage : '听说你买了一栋房子。 (I heard you just bought a house.)'
			},
			{
				id : 'gao-tell',
				pinyin : 'Gào',
				entry : '告',
				translation : 'tell, sue',
				words : '告诉 (tell) 广告 (advertisement)',
				usage : '告诉他我今天不能去上课了。 (Tell him I cannot go for the class today.)'
			},
			{
				id : 'su-complaint',
				pinyin : 'Sù',
				entry : '诉',
				translation : 'complaint, tell',
				words : '诉说 (narrate) 告诉 (tell)',
				usage : "别告诉他我生病了。 (Don't tell him I'm sick.)"
			},
			{
				id : 'jian-jian',
				pinyin : 'Jiàn',
				entry : '件',
				translation : 'messure word (a cloth, an event)',
				words : '一件衣服 (a cloth) 一件事 (an event)',
				usage : '这件事很有意思。 (This thing is very interesting.)'
			},
			{
				id : 'shi-thing',
				pinyin : 'Shì',
				entry : '事',
				translation : 'head',
				words : '事情 (this) 好事 (good thing)',
				usage : "这是好事，为什么不能告诉他？ (This is a good thing, why cannot we tell him.)"
			},
			{
				id : 'san-scatter',
				pinyin : 'Sàn',
				entry : '散',
				translation : 'scatter, break up',
				words : '散步 (take a walk) 分散 (scatter)',
				usage : '晚饭后，咱们一起去散步吧？ (How about we take a walk after dinner?)'
			},
			{
				id : 'bu-step',
				pinyin : 'Bù',
				entry : '步',
				translation : 'step',
				words : '跑步 (run) 步骤 (procedure)',
				usage : "跑步的时候，别跑得太快。 (Don't run too fast.)"
			},
			{
				id : 'dian-electricity',
				pinyin : 'Diàn',
				entry : '电',
				translation : 'electricity',
				words : '电话 (telephone) 电视 (television)',
				usage : "昨天他给我打了个电话。 (He called me yesterday.)"
			},
			{
				id : 'ying-shadow',
				pinyin : 'Yǐng',
				entry : '影',
				translation : 'shadow',
				words : '电影 (movie) 影子 (shadow)',
				usage : "听说那个电影很好看。 (It is said that it's a good movie.)"
			},
			{
				id : 'fang-house',
				pinyin : 'Fáng',
				entry : '房',
				translation : 'house',
				words : '房子 (house) 房间 (room)',
				usage : "你的房间收拾得挺干净。 (Your room is pretty clean.)"
			},
			{
				id : 'chu-kitchen',
				pinyin : 'Chú',
				entry : '厨',
				translation : 'kitchen',
				words : '厨房 (kitchen) 厨师 (chef)',
				usage : "我们家的厨房很大。 (My kitchen is quite big.)"
			},
			{
				id : 'chuan-toliet',
				pinyin : 'Cè',
				entry : '厕',
				translation : 'toliet',
				words : '厕所 (bathroom) 上厕所 (go to bathroom)',
				usage : "我去上厕所，你们等我一会儿。 (Wait, I need to go to bathroom.)"
			},
			{
				id : 'suo-place',
				pinyin : 'Suǒ',
				entry : '所',
				translation : 'place, spot',
				words : '所以 (so) 所有 (all, everything)',
				usage : "他很忙，所以他今天不能来了。 (He is busy, so he cannot come here today.)"
			}, {
				id : 'gong-public',
				pinyin : 'Gōng',
				entry : '公',
				translation : 'public, common',
				words : '公司 (company) 公共 (public)',
				usage : "我们公司有不少办公室。 (Our company has a lot of offices.)"
			} ]
};

var NPCR1_13_2 = {
	course : 'npcr1',
	lesson : '13',
	section : '2',
	title : '第十三课生词(2)',
	entries : '办,话,让,帮,助,喂,位,经,理,饭,等',
	quizletId : '181788856/',
	characters : [ {
		id : 'ban-do',
		pinyin : 'Bàn',
		entry : '办',
		translation : 'do, process',
		words : "办法 (method, way) 办事 (work)",
		usage : "你一定想办法把他请来。 (Whatever it takes, you get him here.)"
	}, {
		id : 'hua-words',
		pinyin : 'Huà',
		entry : '话',
		translation : 'words, talk',
		words : '说话 (talk) 对话 (dialogue)',
		usage : "别说话了，现在开会了。 (Pay attention please, let's start.)"
	}, {
		id : 'rang-let',
		pinyin : 'Ràng',
		entry : '让',
		translation : 'let',
		words : '让路 (give way) 谦让 (humility)',
		usage : '让老人先过去。 (Please let the seniors go first.)'
	}, {
		id : 'bang-help',
		pinyin : 'Bāng',
		entry : '帮',
		translation : 'help',
		words : '帮助 (help) 帮手 (asistant)',
		usage : "要是没有他的帮助，我做不成这件事。 (I could not do this without his help.)"
	}, {
		id : 'zhu-assist',
		pinyin : 'Zhù',
		entry : '助',
		translation : 'assist',
		words : '助手 (asistant) 求助 (seek help)',
		usage : "坐在那边的就是他的助手。 (That's his asistant who is sitting there.)"
	}, {
		id : 'wei-feed',
		pinyin : 'Wèi',
		entry : '喂',
		translation : 'feed, hello',
		words : '喂 (feed, hello) 喂饭 (feed)',
		usage : "喂，你该给小孩喂奶了。 (Hey, it's time to feed the baby.)"
	}, {
		id : 'wei-position',
		pinyin : 'Wèi',
		entry : '位',
		translation : 'position, digit, messure word',
		words : '哪一位 (who is it) 位置 (position) 座位 (seat)',
		usage : "你好，你们一共几位？ (Hello, how many of you?)"
	}, {
		id : 'jing-through',
		pinyin : 'Jīng',
		entry : '经',
		translation : 'through',
		words : '经理 (manager) 经验 (experience)',
		usage : "他是一位很有经验的经理。 (He is a very experienced manager.)"
	}, {
		id : 'li-manage',
		pinyin : 'Lǐ',
		entry : '理',
		translation : 'manage, logic',
		words : '道理 (reason, principle) 理论 (theory)',
		usage : "你怎么能这么做，还讲不讲道理了。 (How could you do that, it's ridiculous.)"
	}, {
		id : 'fan-meal',
		pinyin : 'Fàn',
		entry : '饭',
		translation : 'meal, food',
		words : '吃饭 (have a meal) 饭碗 (bowl)',
		usage : "今天晚上到外面去吃饭，我请客。 (We'll go out for dinner, my treat.)"
	}, {
		id : 'deng-wait',
		pinyin : 'Děng',
		entry : '等',
		translation : 'wait, equal',
		words : '等待 (wait) 等于 (equal)',
		usage : '三加三等于六。 (Three plus three equals six.)'
	} ]
};

var NPCR1_14_1 = {
	course : 'npcr1',
	lesson : '14',
	section : '1',
	title : '第十四课生词(1)',
	entries : '刚,才,邮,局,寄,扫,脏,洗,旅,行,想',
	quizletId : '181789026/',
	characters : [
			{
				id : 'gang-just',
				pinyin : 'Gāng',
				entry : '刚',
				translation : 'just, barely, firm',
				words : "刚才 (just now) 刚强 (firm)",
				usage : '刚才我在办公室见到他了。 (I saw him in the office just now.)'
			},
			{
				id : 'cai-only',
				pinyin : 'Cái',
				entry : '才',
				translation : 'only, talent',
				words : '刚才 (just now) 有才 (talented)',
				usage : "在这买一件大衣才五十块钱。 (It's only $50 to buy a coat here.)"
			},
			{
				id : 'you-post',
				pinyin : 'Yóu',
				entry : '邮',
				translation : 'mail, post',
				words : '邮局 (post office) 邮箱 (mail box)',
				usage : '我一会儿要去一趟邮局。 (I need to go to the post office in a moment.)'
			},
			{
				id : 'ju-office',
				pinyin : 'Jú',
				entry : '局',
				translation : 'office, set',
				words : '公安局 (police office) 饭局 (dinner party)',
				usage : "今天晚上我有饭局，别等我了。 (I have a dinner party tonight, you don't need to wait for me.)"
			},
			{
				id : 'ji-send',
				pinyin : 'Jì',
				entry : '寄',
				translation : 'send, post',
				words : '寄信 (send a letter) 寄存 (doposit)',
				usage : "今天我要把礼物寄给她。 (I want to send the present to her today.)"
			},
			{
				id : 'sao-sweep',
				pinyin : 'Sǎo',
				entry : '扫',
				translation : 'sweep',
				words : '扫地 (sweep) 打扫 (clean)',
				usage : '雪停了，一会儿把雪扫一扫。 (The snow has stopped, we need to shovel the snow then.)'
			},
			{
				id : 'zang-dirty',
				pinyin : 'Zàng',
				entry : '脏',
				translation : 'dirty',
				words : '很脏 (dirty) 脏东西 (dirty things)',
				usage : "房间很脏，我来打扫一下。 (The room is pretty dirty, let me clean it up.)"
			},
			{
				id : 'xi-wash',
				pinyin : 'Xǐ',
				entry : '洗',
				translation : 'wash',
				words : '清洗 (wash) 洗手 (wash hand)',
				usage : '吃饭前一定要洗手。 (Wash your hands before eating.)'
			},
			{
				id : 'lu-travel',
				pinyin : 'Lǚ',
				entry : '旅',
				translation : 'travel, trip',
				words : '旅行 (travel) 旅途 (journey)',
				usage : "明年咱们去欧洲旅行。 (We'll travel to Europe next year.)"
			},
			{
				id : 'xing-walk',
				pinyin : 'Xíng',
				entry : '行',
				translation : 'walk, go, alright',
				words : '行动 (action) 不行 (no can do)',
				usage : "你这样做不行，会把东西搞坏的。 (You cannot do it this way, things would be damaged.)"
			}, {
				id : 'xiang-think',
				pinyin : 'Xiǎng',
				entry : '想',
				translation : 'think',
				words : '想法 (idea) 思想 (thought)',
				usage : "你这个想法很好。 (It's really a good idea.)"
			} ]
};

var NPCR1_14_2 = {
	course : 'npcr1',
	lesson : '14',
	section : '2',
	title : '第十四课生词(2)',
	entries : '留,楼,念,复,练,法,过,节,礼,物,圣,诞,欧,洲,海',
	quizletId : '181789205/',
	characters : [
			{
				id : 'liu-stay',
				pinyin : 'Liú',
				entry : '留',
				translation : 'stay, remain',
				words : "留学 (Study abroad) 停留 (stay)",
				usage : "现在有很多年轻人到国外去留学。 (Many young people study abroad nowadays.)"
			},
			{
				id : 'lou-building',
				pinyin : 'Lóu',
				entry : '楼',
				translation : 'building',
				words : '楼房 (building) 三楼 (the third floor)',
				usage : "那座楼房是去年才盖的。 (That building was built last year.)"
			},
			{
				id : 'nian-read',
				pinyin : 'Niàn',
				entry : '念',
				translation : 'think, read',
				words : '想念 (miss) 念书 (read, study)',
				usage : '他出国已经两年了，我们都很想念他。 (He has gone abroad two years, we all missed him.)'
			},
			{
				id : 'fu-repeat',
				pinyin : 'Fù',
				entry : '复',
				translation : 'repeat',
				words : '复习 (review) 重复 (repeat)',
				usage : "今天晚上我们一起复习功课。 (Let's review what we have learned tonight.)"
			},
			{
				id : 'lian-practice',
				pinyin : 'Liàn',
				entry : '练',
				translation : 'practice',
				words : '练习 (practice) 熟练 (proficient, skillful)',
				usage : "你再练一会儿，我马上就回来。 (You keep practicing, I'll be right back.)"
			},
			{
				id : 'fa-law',
				pinyin : 'Fǎ',
				entry : '法',
				translation : 'law, method',
				words : '语法 (grammar) 办法 (method) 法律 (law)',
				usage : "这是个很好的办法。 (This is a good way to go.)"
			},
			{
				id : 'guo-pass',
				pinyin : 'Guò',
				entry : '过',
				translation : 'pass, cross',
				words : '过去 (in the past) 做过 (have done)',
				usage : "你过去都做过什么工作？ (What have you done in the past?)"
			},
			{
				id : 'jie-holiday',
				pinyin : 'Jié',
				entry : '节',
				translation : 'holiday, save',
				words : '过节 (holiday) 节约 (save)',
				usage : "今年过春节，我们要节约点，少买些东西。 (We need to cut the budget a bit for this Chinese New Year.)"
			},
			{
				id : 'li-gift',
				pinyin : 'Lǐ',
				entry : '礼',
				translation : 'gift, courtesy',
				words : '礼物 (gift, present) 礼貌 (courtesy)',
				usage : "送这么多礼物，你太客气了。 (That's very kind of you to get us so many presents.)"
			},
			{
				id : 'wu-thing',
				pinyin : 'Wù',
				entry : '物',
				translation : 'thing, matter',
				words : '动物 (animal) 物理 (physics)',
				usage : "他特别喜欢小动物，养得有一只小狗和一只猫。 (He loves pets. He has got a cute dog and a cat.)"
			},
			{
				id : 'sheng-holy',
				pinyin : 'Shèng',
				entry : '圣',
				translation : 'holy, saint',
				words : '圣诞 (Christmas) 圣人 (saint)',
				usage : '每年的十二月二十五号是圣诞节。 (Christmas is on Dec. 25 every year.)'
			},
			{
				id : 'dan-birth',
				pinyin : 'Dàn',
				entry : '诞',
				translation : 'birth',
				words : '圣诞 (Christmas) 诞生 (birth)',
				usage : "每年的十二月二十五号是圣诞节。 (Christmas is on Dec. 25 every year.)"
			},
			{
				id : 'ou-europe',
				pinyin : 'Ōu',
				entry : '欧',
				translation : 'Europe',
				words : '欧洲 (Europe)',
				usage : '欧洲和亚洲挨着。 (Europe is next to Asia.)'
			},
			{
				id : 'zhou-continent',
				pinyin : 'Zhōu',
				entry : '洲',
				translation : 'continent',
				words : '亚洲 (Asia) 美洲 (Americas)',
				usage : "亚洲和美洲中间隔着太平洋。 (Between Asia and Americas, there is a Pacific Ocean.)"
			}, {
				id : 'hai-sea',
				pinyin : 'Hǎi',
				entry : '海',
				translation : 'sea',
				words : '大海 (sea) 海洋 (ocean)',
				usage : '海水是咸的。 (The sea water is salty.)'
			} ]
};

var NPCR2_15_1 = {
	course : 'npcr2',
	lesson : '15',
	section : '1',
	title : '第15课生词(1)',
	entries : '得, 早, 银, 行, 少, 排, 队, 换, 民, 得, 用, 刚, 从, 非, 常, 次, 参, 观, 兵, 信, 该, 千, 数',
	quizletId : '196129774/',
	characters : [ {
		id : "de-de",
		pinyin : "de",
		entry : "得",
		translation : "structural particle",
		words : "做得好 (do well) 起得早 (get up early)",
		usage : null
	}, {
		id : "zao-early",
		pinyin : "zǎo",
		entry : "早",
		translation : "early",
		words : "早晨(morning)  早上好(good morning)",
		usage : null
	}, {
		id : "yin-silver",
		pinyin : "yín",
		entry : "银",
		translation : "silver",
		words : "银子(silver, money)  银行(bank)",
		usage : null
	}, {
		id : "hang-row",
		pinyin : "háng",
		entry : "行",
		translation : "row, profession",
		words : "三行(three rows) 行当(profession)",
		usage : null
	}, {
		id : "shao-few",
		pinyin : "shǎo",
		entry : "少",
		translation : "few, little",
		words : "少量(a small amount) 不少(a lot)",
		usage : null
	}, {
		id : "pai-arrange",
		pinyin : "pái",
		entry : "排",
		translation : "to arrange, to put in order",
		words : "排队(to form a line, to queue up)  排水(train)",
		usage : null
	}, {
		id : "dui-row",
		pinyin : "duì",
		entry : "队",
		translation : "a row of people, line",
		words : "队伍(team) 插队(cut the line)",
		usage : null
	}, {
		id : "huan-exchange",
		pinyin : "huàn",
		entry : "换",
		translation : "to exchange, to change",
		words : "交换(exchange) 换气(ventilation, breathe)",
		usage : null
	}, {
		id : "min-people",
		pinyin : "mín",
		entry : "民",
		translation : "people",
		words : "人民(people) 难民(refugee)",
		usage : null
	}, {
		id : "dei-need",
		pinyin : "děi",
		entry : "得",
		translation : "to need, must, to have to",
		words : "我得走了(I have to go)",
		usage : null
	}, {
		id : "yong-use",
		pinyin : "yòng",
		entry : "用",
		translation : "to use",
		words : "使用(use) 用处(usefulness)",
		usage : null
	}, {
		id : "gang-just",
		pinyin : "gāng",
		entry : "刚",
		translation : "just, only a short while ago",
		words : "刚才(just now) 刚刚(just now)",
		usage : null
	}, {
		id : "cong-from",
		pinyin : "cóng",
		entry : "从",
		translation : "from",
		words : "从前(once upon a time) 从来(always)",
		usage : null
	}, {
		id : "fei-not",
		pinyin : "fēi",
		entry : "非",
		translation : "not",
		words : "非常(very) 是非(right and wrong)",
		usage : null
	}, {
		id : "chang-common",
		pinyin : "cháng",
		entry : "常",
		translation : "common, normal",
		words : "经常(often) 平常(normally)",
		usage : null
	}, {
		id : "ci-times",
		pinyin : "cì",
		entry : "次",
		translation : "measure word for actions",
		words : "三次(three times) 五次(five times)",
		usage : null
	}, {
		id : "can-participate",
		pinyin : "cān",
		entry : "参",
		translation : "participate, join",
		words : "参观(visit) 参加(participate, join)",
		usage : null
	}, {
		id : "guan-view",
		pinyin : "guān",
		entry : "观",
		translation : "view, watch",
		words : "观看(watch) 外观(look and feel)",
		usage : null
	}, {
		id : "bing-soldier",
		pinyin : "bīng",
		entry : "兵",
		translation : "soldier, fighter",
		words : "士兵(soldier) 兵器(weapon)",
		usage : null
	}, {
		id : "xin-letter",
		pinyin : "xìn",
		entry : "信",
		translation : "letter, believe",
		words : "写信(write a letter) 相信(believe)",
		usage : null
	}, {
		id : "gai-should",
		pinyin : "gāi",
		entry : "该",
		translation : "should, to be sb.'s turn to do something",
		words : "该我了(my turn) 应该(should, ought to)",
		usage : null
	}, {
		id : "qian-thousand",
		pinyin : "qiān",
		entry : "千",
		translation : "thousands",
		words : "三千(three thousands) 千万(10 million, a lot)",
		usage : null
	}, {
		id : "shu-count",
		pinyin : "shǔ",
		entry : "数",
		translation : "to count",
		words : "数钱(count money) 数数(count numbers)",
		usage : null
	}, ]
}

var NPCR2_15_2 = {
	course : 'npcr2',
	lesson : '15',
	section : '2',
	title : '第15课生词(2)',
	entries : '久, 见, 发, 展, 快, 话, 轻, 流, 利, 懂, 就, 元, 汇, 率, 城, 市, 菜, 换, 车',
	quizletId : '196288091/',
	characters : [ {
		id : "jiu-long",
		pinyin : "jiǔ",
		entry : "久",
		translation : "long time",
		words : "好久(long time) 长久(long time)",
		usage : null
	}, {
		id : "jian-see",
		pinyin : "jiàn",
		entry : "见",
		translation : "to see, watch",
		words : "看见(see) 见识(knowledge, experience)",
		usage : null
	}, {
		id : "fa-issue",
		pinyin : "fā",
		entry : "发",
		translation : "issue, send, fire",
		words : "发展(develop) 发现(discover)",
		usage : null
	}, {
		id : "zhan-exhibition",
		pinyin : "zhǎn",
		entry : "展",
		translation : "exhibition, unfold, extend",
		words : "展开(unfold) 展览(exhibition)",
		usage : null
	}, {
		id : "kuai-fast",
		pinyin : "kuài",
		entry : "快",
		translation : "fast, quick, rapid",
		words : "很快(very fast) 快餐(fast food)",
		usage : null
	}, {
		id : "hua-dialect",
		pinyin : "huà",
		entry : "话",
		translation : "dialect, language",
		words : "说话(talk, speak) 话剧(play)",
		usage : null
	}, {
		id : "qing-light",
		pinyin : "qīng",
		entry : "轻",
		translation : "small, light",
		words : "年轻(yong) 轻视(contempt, despise)",
		usage : null
	}, {
		id : "liu-flow",
		pinyin : "liú",
		entry : "流",
		translation : "flow, flux",
		words : "流利(fluent) 流水(running water)",
		usage : null
	}, {
		id : "li-profit",
		pinyin : "lì",
		entry : "利",
		translation : "profit, sharp",
		words : "厉害(intensely, powerful) 利益(interest)",
		usage : null
	}, {
		id : "dong-understand",
		pinyin : "dǒng",
		entry : "懂",
		translation : "to understand",
		words : "懂得(understand) 不懂(don't understand)",
		usage : null
	}, {
		id : "jiu-exactly",
		pinyin : "jiù",
		entry : "就",
		translation : "exactly, precisely",
		words : "就是(exactly, that's it)",
		usage : null
	}, {
		id : "yuan-yuan",
		pinyin : "yuán",
		entry : "元",
		translation : "measure word for Chinese currency, first",
		words : "美元(U.S. dollar) 元旦(new year day)",
		usage : null
	}, {
		id : "hui-exchange",
		pinyin : "huì",
		entry : "汇",
		translation : "exchange, collect",
		words : "汇合(confluence, come togather) 汇款(money transfer)",
		usage : null
	}, {
		id : "lv-rate",
		pinyin : "lǜ",
		entry : "率",
		translation : "rate, frequency",
		words : "汇率(exchange rate) 利率(interest rate)",
		usage : null
	}, {
		id : "cheng-city",
		pinyin : "chéng",
		entry : "城",
		translation : "city, place with a closed wall",
		words : "城市(city) 城墙(city wall)",
		usage : null
	}, {
		id : "shi-city",
		pinyin : "shì",
		entry : "市",
		translation : "city",
		words : "市场(market) 北京市(Beijing city)",
		usage : null
	}, {
		id : "cai-dish",
		pinyin : "cài",
		entry : "菜",
		translation : "food, a dish",
		words : "炒菜(cooking, pan-fry) 蔬菜(vegetable)",
		usage : null
	}, {
		id : "huan-exchange",
		pinyin : "huàn",
		entry : "换",
		translation : "exchange, replace",
		words : "交换(exchange) 换车(tranship)",
		usage : null
	}, {
		id : "che-vehicle",
		pinyin : "chē",
		entry : "车",
		translation : "vehicle",
		words : "汽车(automobile) 马车(carriage)",
		usage : null
	}, ]
}

var NPCR2_16_1 = {
	course : 'npcr2',
	lesson : '16',
	section : '1',
	title : '第16课生词(1)',
	entries : '把, 忘, 图, 馆, 上, 先, 借, 证, 下, 带, 填, 表, 拿, 慢, 交',
	quizletId : '196300364/',
	characters : [
			{
				id : "ba-handle",
				pinyin : "bǎ",
				entry : "把",
				translation : "used when the object is the receiver of an action, handle",
				words : "把手(handle) 把事情做好(do things well)",
				usage : null
			},
			{
				id : "wang-forget",
				pinyin : "wàng",
				entry : "忘",
				translation : "to forget",
				words : "忘记(forget) 忘了(forgot)",
				usage : null
			},
			{
				id : "tu-chart",
				pinyin : "tú",
				entry : "图",
				translation : "chart, graphic",
				words : "图画(picture) 图书(books)",
				usage : null
			},
			{
				id : "guan-hall",
				pinyin : "guǎn",
				entry : "馆",
				translation : "term for certain service establishments or places for cultural activities",
				words : "图书馆(library) 展览馆(exhibition hall)",
				usage : null
			}, {
				id : "xian-first",
				pinyin : "xiān",
				entry : "先",
				translation : "first, before",
				words : "先走(go aheard) 祖先(ancestor)",
				usage : null
			}, {
				id : "jie-borrow",
				pinyin : "jiè",
				entry : "借",
				translation : "to borrow, to lend",
				words : "借书(borrow books) 借钱(borrow money)",
				usage : null
			}, {
				id : "zheng-certificate",
				pinyin : "zhèng",
				entry : "证",
				translation : "certificate, card",
				words : "证件(document, credential) 证明(prove, certificate)",
				usage : null
			}, {
				id : "xia-down",
				pinyin : "xià",
				entry : "下",
				translation : "to go down, to get off, next",
				words : "下面(down there) 下楼(go downstairs)",
				usage : null
			}, {
				id : "dai-bring",
				pinyin : "dài",
				entry : "带",
				translation : "to bring, belt",
				words : "带饭 (bring food with) 腰带(belt)",
				usage : null
			}, {
				id : "tian-fill",
				pinyin : "tián",
				entry : "填",
				translation : "to fill in, to write",
				words : "填表(fill in application) 填土(filling with dirt)",
				usage : null
			}, {
				id : "biao-form",
				pinyin : "biǎo",
				entry : "表",
				translation : "form, table, list, watch",
				words : "表格(form) 手表(watch)",
				usage : null
			}, {
				id : "na-take",
				pinyin : "ná",
				entry : "拿",
				translation : "to take, to hold, to get",
				words : "拿起(pick up) 拿走(take away)",
				usage : null
			}, {
				id : "man-slow",
				pinyin : "màn",
				entry : "慢",
				translation : "slow",
				words : "慢跑(jogging) 慢动作(slow motion)",
				usage : null
			}, {
				id : "jiao-cross",
				pinyin : "jiāo",
				entry : "交",
				translation : "to hand over,cross, pay, meet",
				words : "交朋友(make friends) 交作业(submit homework)",
				usage : null
			}, ]
}

var NPCR2_16_2 = {
	course : 'npcr2',
	lesson : '16',
	section : '2',
	title : '第16课生词(2)',
	entries : '长, 考, 新, 出, 还, 过, 期, 罚, 款, 脑, 查',
	quizletId : '196305410/',
	characters : [ {
		id : "chang-long",
		pinyin : "cháng",
		entry : "长",
		translation : "long",
		words : "长头发(long hair) 长方形(rectangle)",
		usage : null
	}, {
		id : "kao-examine",
		pinyin : "kǎo",
		entry : "考",
		translation : "examine, test, check",
		words : "考试(examine) 考虑(consider, think)",
		usage : null
	}, {
		id : "xin-new",
		pinyin : "xīn",
		entry : "新",
		translation : "new",
		words : "新闻(news) 新鲜(fresh)",
		usage : null
	}, {
		id : "chu-out",
		pinyin : "chū",
		entry : "出",
		translation : "to go or come out",
		words : "出去(go out) 走出(walk out)",
		usage : null
	}, {
		id : "huan-return",
		pinyin : "huán",
		entry : "还",
		translation : "to give back, to return",
		words : "还书(return book) 还钱(pay back)",
		usage : null
	}, {
		id : "guo-pass",
		pinyin : "guò",
		entry : "过",
		translation : "to pass",
		words : "过期(overdue) 过去(past)",
		usage : null
	}, {
		id : "qi-period",
		pinyin : "qī",
		entry : "期",
		translation : "a period of time",
		words : "时期(era) 期望(hope, expect)",
		usage : null
	}, {
		id : "fa-punish",
		pinyin : "fá",
		entry : "罚",
		translation : "to punish, to penalize",
		words : "罚款(fine) 惩罚(punish)",
		usage : null
	}, {
		id : "kuan-money",
		pinyin : "kuǎn",
		entry : "款",
		translation : "a sum of money, style",
		words : "贷款(loan) 款式(style)",
		usage : null
	}, {
		id : "nao-brain",
		pinyin : "nǎo",
		entry : "脑",
		translation : "brain",
		words : "大脑(brain) 动脑子(think)",
		usage : null
	}, {
		id : "cha-check",
		pinyin : "chá",
		entry : "查",
		translation : "to check, to look up",
		words : "检查(check) 查找(look up)",
		usage : null
	}, ]
}

var NPCR2_17_1 = {
	course : 'npcr2',
	lesson : '17',
	section : '1',
	title : '第17课生词(1)',
	entries : '旗, 袍, 比, 卖, 试, 商, 店, 极, 差, 套, 帅, 黑, 红, 白, 走',
	quizletId : '196376268/',
	characters : [ {
		id : "qi-flag",
		pinyin : "qí",
		entry : "旗",
		translation : "flag, standard",
		words : "红旗(red flag) 军旗(ensign)",
		usage : null
	}, {
		id : "pao-robe",
		pinyin : "páo",
		entry : "袍",
		translation : "robe, grwn",
		words : "长袍(robe) 旗袍(cheongsam)",
		usage : null
	}, {
		id : "bi-compare",
		pinyin : "bǐ",
		entry : "比",
		translation : "than, indicating comparison",
		words : "比赛(game, match) 比他高(taller than him)",
		usage : null
	}, {
		id : "mai-sell",
		pinyin : "mài",
		entry : "卖",
		translation : "to sell",
		words : "买卖(business) 卖车(sell a car)",
		usage : null
	}, {
		id : "shi-try",
		pinyin : "shì",
		entry : "试",
		translation : "try, type, style",
		words : "尝试(try) 试衣服(try on clothes)",
		usage : null
	}, {
		id : "dian-shop",
		pinyin : "diàn",
		entry : "店",
		translation : "shop, store",
		words : "商店(shop, store) 旅店(hotel)",
		usage : null
	}, {
		id : "ji-extremely",
		pinyin : "jí",
		entry : "极",
		translation : "extremely",
		words : "极好(extremely good) 北极(north pole)",
		usage : null
	}, {
		id : "cha-bad",
		pinyin : "chà",
		entry : "差",
		translation : "not up to standard, poor, bad, difference",
		words : "差不多(almost, nearly) 很差(pretty bad)",
		usage : null
	}, {
		id : "tao-set",
		pinyin : "tào",
		entry : "套",
		translation : "messure word of set, suit, suite",
		words : "一套(one) 三套(three)",
		usage : null
	}, {
		id : "shuai-handsome",
		pinyin : "shuài",
		entry : "帅",
		translation : "handsome, smart, commander",
		words : "帅哥(handsome guy) 统帅(commander in chief)",
		usage : null
	}, {
		id : "hei-black",
		pinyin : "hēi",
		entry : "黑",
		translation : "black",
		words : "黑色(black) 黑夜(dark night)",
		usage : null
	}, {
		id : "hong-red",
		pinyin : "hóng",
		entry : "红",
		translation : "red",
		words : "红花(red flower) 鲜红(bright red)",
		usage : null
	}, {
		id : "bai-white",
		pinyin : "bái",
		entry : "白",
		translation : "white",
		words : "白雪(white snow) 雪白(snow white)",
		usage : null
	}, {
		id : "zou-walk",
		pinyin : "zǒu",
		entry : "走",
		translation : "to walk, to go",
		words : "走路(walk) 走过(pass by)",
		usage : null
	}, ]
}

var NPCR2_17_2 = {
	course : 'npcr2',
	lesson : '17',
	section : '2',
	title : '第17课生词(2)',
	entries : '绿, 售, 货, 短, 高, 双, 鞋, 条, 薄, 页, 布, 表, 已, 及, 产, 丝',
	quizletId : '196381244/',
	characters : [
			{
				id : "lv-green",
				pinyin : "lǜ",
				entry : "绿",
				translation : "green",
				words : "绿色(green) 绿叶(green leaf)",
				usage : null
			},
			{
				id : "shou-sell",
				pinyin : "shòu",
				entry : "售",
				translation : "to sell",
				words : "销售(sell) 售票(sell ticket)",
				usage : null
			},
			{
				id : "huo-goods",
				pinyin : "huò",
				entry : "货",
				translation : "goods",
				words : "货物(goods, merchandise) 货车(truck)",
				usage : null
			},
			{
				id : "duan-short",
				pinyin : "duǎn",
				entry : "短",
				translation : "short",
				words : "长短(length) 短刀(knife)",
				usage : null
			},
			{
				id : "gao-high",
				pinyin : "gāo",
				entry : "高",
				translation : "high, tall",
				words : "高低(height) 高级(advanced, high level) 高楼(high rise)",
				usage : null
			},
			{
				id : "shuang-pair",
				pinyin : "shuāng",
				entry : "双",
				translation : "pair",
				words : "一双手(a pair of hands) 一双眼睛(a pair of eyes) 两双鞋(two pairs of shoes)",
				usage : null
			},
			{
				id : "xie-shoes",
				pinyin : "xié",
				entry : "鞋",
				translation : "shoes",
				words : "球鞋(sneakers) 拖鞋(slippers)",
				usage : null
			},
			{
				id : "tiao-tiao",
				pinyin : "tiáo",
				entry : "条",
				translation : "mw for long narrow objects such as trousers, skirt, snake",
				words : "一条鱼(a fish) 两条毛巾(2 towels) 三根油条(3 fritters)",
				usage : null
			}, {
				id : "bao-thin",
				pinyin : "báo",
				entry : "薄",
				translation : "thin",
				words : "薄冰(thin ice) 薄薄一层土(a thin layer of dust)",
				usage : null
			}, {
				id : "ye-page",
				pinyin : "yè",
				entry : "页",
				translation : "page",
				words : "一页(one page) 网页(web page)",
				usage : null
			}, {
				id : "bu-cloth",
				pinyin : "bù",
				entry : "布",
				translation : "cloth, fabric",
				words : "布鞋(cloth shoes) 白布(white cloth, calico)",
				usage : null
			}, {
				id : "biao-watch",
				pinyin : "biǎo",
				entry : "表",
				translation : "watch, surface, express",
				words : "手表(watch) 表示(said, indicate) 外表(appearance, look)",
				usage : null
			}, {
				id : "yi-already",
				pinyin : "yǐ",
				entry : "已",
				translation : "already",
				words : "已经(already)",
				usage : null
			}, {
				id : "ji-and",
				pinyin : "jí",
				entry : "及",
				translation : "and, reach",
				words : "以及(and something else) 及格(pass a test)",
				usage : null
			}, {
				id : "chan-produce",
				pinyin : "chǎn",
				entry : "产",
				translation : "to produce, to give birth to",
				words : "生产(produce) 产妇(maternal)",
				usage : null
			}, {
				id : "si-silk",
				pinyin : "sī",
				entry : "丝",
				translation : "silk",
				words : "丝绸(silk) 丝巾(scarf)",
				usage : null
			}, ]
}

var NPCR2_18_1 = {
	course : 'npcr2',
	lesson : '18',
	section : '1',
	title : '第18课生词(1)',
	entries : '记, 错, 些, 旧, 包, 往, 取, 别, 门',
	quizletId : '196558442/',
	characters : [ {
		id : "ji-remember",
		pinyin : "jì",
		entry : "记",
		translation : "to remember, to bear in mind",
		words : "记忆(memory, recall) 忘记(forget)",
		usage : null
	}, {
		id : "cuo-wrong",
		pinyin : "cuò",
		entry : "错",
		translation : "wrong, erroneous",
		words : "错误(error, mistake) 出错(go wrong)",
		usage : null
	}, {
		id : "xie-some",
		pinyin : "xiē",
		entry : "些",
		translation : "some",
		words : "一些(some, a few)",
		usage : null
	}, {
		id : "jiu-old",
		pinyin : "jiù",
		entry : "旧",
		translation : "old, past, used",
		words : "陈旧(old) 旧衣服(old cloths)",
		usage : null
	}, {
		id : "bao-wrap",
		pinyin : "bāo",
		entry : "包",
		translation : "to wrap, bag",
		words : "包装(wrap up) 皮包(purse, brifcase)",
		usage : null
	}, {
		id : "wang-toward",
		pinyin : "wǎng",
		entry : "往",
		translation : "to, toward",
		words : "往前(go forward) 来往(back and forth)",
		usage : null
	}, {
		id : "qu-take",
		pinyin : "qǔ",
		entry : "取",
		translation : "to take, to get, to fetch",
		words : "取包裹(take the parcel) 收取(receive)",
		usage : null
	}, {
		id : "bie-donot",
		pinyin : "bié",
		entry : "别",
		translation : "don't, other",
		words : "别动(don't move) 别人(other people)",
		usage : null
	}, {
		id : "men-door",
		pinyin : "mén",
		entry : "门",
		translation : "door, gate",
		words : "大门(gate) 关门(close the door)",
		usage : null
	}, ]
}

var NPCR2_18_2 = {
	course : 'npcr2',
	lesson : '18',
	section : '2',
	title : '第18课生词(2)',
	entries : '路, 里, 站, 封, 贴',
	quizletId : '196558598/',
	characters : [ {
		id : "lu-route",
		pinyin : "lù",
		entry : "路",
		translation : "route",
		words : "走路(walk) 马路(road)",
		usage : null
	}, {
		id : "li-in",
		pinyin : "lǐ",
		entry : "里",
		translation : "in, inside, within",
		words : "里面(inside) 心里(in my heart)",
		usage : null
	}, {
		id : "zhan-station",
		pinyin : "zhàn",
		entry : "站",
		translation : "station, stop",
		words : "车站(bus station) 站住(stop)",
		usage : null
	}, {
		id : "feng-seal",
		pinyin : "fēng",
		entry : "封",
		translation : "measure word for letter, seal",
		words : "信封(envelope) 封闭(close, seal)",
		usage : null
	}, {
		id : "tie-stick",
		pinyin : "tiē",
		entry : "贴",
		translation : "to stick, to paste",
		words : "贴邮票(put the stamp on) 贴心(intimate)",
		usage : null
	}, ]
}

var NPCR2_19_1 = {
	course : 'npcr2',
	lesson : '19',
	section : '1',
	title : '第19课生词(1)',
	entries : '画, 油, 老, 家, 唱, 幅',
	quizletId : '196971670/',
	characters : [ {
		id : "hua-painting",
		pinyin : "huà",
		entry : "画",
		translation : "painting, to paint",
		words : "油画(oil painting) 画画(to paint)",
		usage : null
	}, {
		id : "you-oil",
		pinyin : "yóu",
		entry : "油",
		translation : "oil",
		words : "石油(oil) 酱油(soy sauce)",
		usage : null
	}, {
		id : "lao-old",
		pinyin : "lǎo",
		entry : "老",
		translation : "old, senior",
		words : "老人(elders) 变老(getting old)",
		usage : null
	}, {
		id : "jia-home",
		pinyin : "jiā",
		entry : "家",
		translation : "home, specialist",
		words : "家庭(family) 画家(painter) 科学家(scientist)",
		usage : null
	}, {
		id : "chang-sing",
		pinyin : "chàng",
		entry : "唱",
		translation : "to sing",
		words : "唱歌(sing) 唱戏(singing opera)",
		usage : null
	}, {
		id : "fu-measure",
		pinyin : "fú",
		entry : "幅",
		translation : "a measure word for paintings, cloth, etc.",
		words : "一幅(a painting)",
		usage : null
	}, ]
}

var NPCR2_19_2 = {
	course : 'npcr2',
	lesson : '19',
	section : '2',
	title : '第19课生词(2)',
	entries : '纸, 布, 墨, 只, 匹, 跑, 它, 风, 虾, 游, 骑, 远, 瘦, 管',
	quizletId : '196972914/',
	characters : [ {
		id : "zhi-paper",
		pinyin : "zhǐ",
		entry : "纸",
		translation : "paper",
		words : "白纸（white paper 纸花(paper flower)",
		usage : null
	}, {
		id : "bu-cloth",
		pinyin : "bù",
		entry : "布",
		translation : "cloth",
		words : "黑布(black cloth) 布鞋(cloth shoes)",
		usage : null
	}, {
		id : "mo-ink",
		pinyin : "mò",
		entry : "墨",
		translation : "Chinese ink",
		words : "墨水(ink) 笔墨(pen and ink)",
		usage : null
	}, {
		id : "zhi-only",
		pinyin : "zhǐ",
		entry : "只",
		translation : "only",
		words : "只好(have to) 只要(only if)",
		usage : null
	}, {
		id : "pi-pi",
		pinyin : "pǐ",
		entry : "匹",
		translation : "",
		words : "(a measure word for horses)一匹马(a horse)",
		usage : null
	}, {
		id : "pao-run",
		pinyin : "pǎo",
		entry : "跑",
		translation : "to run",
		words : "跑步(run) 短跑(short distance run, sprint)",
		usage : null
	}, {
		id : "ta-it",
		pinyin : "tā",
		entry : "它",
		translation : "it",
		words : "它是(it is) 其它(others)",
		usage : null
	}, {
		id : "feng-wind",
		pinyin : "fēng",
		entry : "风",
		translation : "wind",
		words : "刮风(windy) 风扇(fan)",
		usage : null
	}, {
		id : "xia-shrimp",
		pinyin : "xiā",
		entry : "虾",
		translation : "shrimp",
		words : "大虾(prawns) 虾球(shrimp ball)",
		usage : null
	}, {
		id : "you-swim",
		pinyin : "yóu",
		entry : "游",
		translation : "to swim",
		words : "游泳(swim) 游走(walk around)",
		usage : null
	}, {
		id : "qi-ride",
		pinyin : "qí",
		entry : "骑",
		translation : "to ride, to sit on the back of",
		words : "骑马(ride a horse) 骑士(knight)",
		usage : null
	}, {
		id : "yuan-far",
		pinyin : "yuǎn",
		entry : "远",
		translation : "far",
		words : "远处(far away) 不远(close by)",
		usage : null
	}, {
		id : "shou-thin",
		pinyin : "shòu",
		entry : "瘦",
		translation : "thin",
		words : "瘦弱(emaciated, thin and weak) 瘦肉(lean meat)",
		usage : null
	}, {
		id : "guan-discipline",
		pinyin : "guǎn",
		entry : "管",
		translation : "to discipline, manage, pipe",
		words : "管理(manage) 水管(water pipe)",
		usage : null
	}, ]
}

var NPCR2_20_1 = {
	course : 'npcr2',
	lesson : '20',
	section : '1',
	title : '第20课生词(1)',
	entries : '火, 锅, 爱, 涮, 羊, 肉, 热, 开, 化, 妆, 西, 器, 演, 奏, 曲, 机, 篇, 又, 春, 江, 月, 夜, 辆, 最, 脚, 完',
	quizletId : '196989782/',
	characters : [ {
		id : "huo-fire",
		pinyin : "huǒ",
		entry : "火",
		translation : "fire, heat",
		words : "点火(start fire) 发火(angry)",
		usage : null
	}, {
		id : "guo-pot",
		pinyin : "guō",
		entry : "锅",
		translation : "pot, pan",
		words : "炒菜锅(wok) 火锅(hot pot)",
		usage : null
	}, {
		id : "ai-love",
		pinyin : "ài",
		entry : "爱",
		translation : "to love",
		words : "爱(love) 爱好(hobby)",
		usage : null
	}, {
		id : "shuan-cook",
		pinyin : "shuàn",
		entry : "涮",
		translation : "to cook thin slices of meat in boiling water",
		words : "涮羊肉(thin slices of mutton boiled in water)",
		usage : null
	}, {
		id : "yang-sheep",
		pinyin : "yang",
		entry : "羊",
		translation : "sheep",
		words : "山羊(golt) 羊皮(sheepskin)",
		usage : null
	}, {
		id : "rou-meat",
		pinyin : "ròu",
		entry : "肉",
		translation : "meat",
		words : "肥肉(fat) 猪肉(pork)",
		usage : null
	}, {
		id : "re-hot",
		pinyin : "rè",
		entry : "热",
		translation : "hot",
		words : "热水(hot water) 天很热(it's hot)",
		usage : null
	}, {
		id : "kai-drive",
		pinyin : "kāi",
		entry : "开",
		translation : "to drive, to operate",
		words : "开车(drive) 开飞机(fly)",
		usage : null
	}, {
		id : "hua-change",
		pinyin : "huà",
		entry : "化",
		translation : "to change",
		words : "变化(change) 现代化(modernization)",
		usage : null
	}, {
		id : "zhuang-make",
		pinyin : "zhuāng",
		entry : "妆",
		translation : "make",
		words : "化妆(make up) 淡妆(light makeup)",
		usage : null
	}, {
		id : "xi-west",
		pinyin : "xī",
		entry : "西",
		translation : "west",
		words : "西方(went) 西瓜(water malon)",
		usage : null
	}, {
		id : "qi-utensil",
		pinyin : "qi",
		entry : "器",
		translation : "utensil",
		words : "乐器(music instrument) 机器(machine)",
		usage : null
	}, {
		id : "yan-perform",
		pinyin : "yǎn",
		entry : "演",
		translation : "to perform, to play",
		words : "演出(performance) 表演(perform) 演员(actor/actress)",
		usage : null
	}, {
		id : "zou-play",
		pinyin : "zòu",
		entry : "奏",
		translation : "to play a musical instrument",
		words : "演奏(play) 独奏(solo)",
		usage : null
	}, {
		id : "qu-tune",
		pinyin : "qǔ",
		entry : "曲",
		translation : "tune, melody",
		words : "乐曲(music) 歌曲(song)",
		usage : null
	}, {
		id : "ji-machine",
		pinyin : "jī",
		entry : "机",
		translation : "machine, engine",
		words : "机器(machine) 电视机(television) 洗衣机(washing machine)",
		usage : null
	}, {
		id : "pian-pian",
		pinyin : "piān",
		entry : "篇",
		translation : "a measure word for essays and articles",
		words : "一篇(a)",
		usage : null
	}, {
		id : "you-again",
		pinyin : "yòu",
		entry : "又",
		translation : "again",
		words : "又是你(you again) 又来了(another time)",
		usage : null
	}, {
		id : "chun-spring",
		pinyin : "chūn",
		entry : "春",
		translation : "spring",
		words : "春天(spring) 早春(early spring)",
		usage : null
	}, {
		id : "jiang-river",
		pinyin : "jiāng",
		entry : "江",
		translation : "river",
		words : "长江(Yangtze river) 江河(rivers)",
		usage : null
	}, {
		id : "yue-moon",
		pinyin : "yuè",
		entry : "月",
		translation : "moon",
		words : "月亮(moon) 五月(May)",
		usage : null
	}, {
		id : "ye-night",
		pinyin : "yè",
		entry : "夜",
		translation : "night",
		words : "夜晚(night) 半夜(midnight)",
		usage : null
	}, {
		id : "liang-liang",
		pinyin : "liàng",
		entry : "辆",
		translation : "a measure word for vehicles",
		words : "三辆车(three cars)",
		usage : null
	}, {
		id : "zui-most",
		pinyin : "zuì",
		entry : "最",
		translation : "most, least, best , to the highest or lowest alegree",
		words : "最好(best) 最亮(lightest)",
		usage : null
	}, {
		id : "jiao-foot",
		pinyin : "jiǎo",
		entry : "脚",
		translation : "foot",
		words : "脚步(steps) 双脚(both feet)",
		usage : null
	}, {
		id : "wan-finish",
		pinyin : "wán",
		entry : "完",
		translation : "to finish",
		words : "完全(entire, whole) 做完(finished, done)",
		usage : null
	}, ]
}

var NPCR2_21_1 = {
	course : 'npcr2',
	lesson : '21',
	section : '1',
	title : '第21课生词(1)',
	entries : '队, 赢, 场, 足, 球, 赛, 提, 踢, 左, 右',
	quizletId : '197063180/',
	characters : [
			{
				id : "dui-team",
				pinyin : "duì",
				entry : "队",
				translation : "team",
				words : "足球队(soccer team) 篮球队(basketball team)",
				usage : null
			},
			{
				id : "ying-win",
				pinyin : "yíng",
				entry : "赢",
				translation : "to win",
				words : "赢球(win a ball game) 打赢了(win a game/match)",
				usage : null
			},
			{
				id : "chang-match",
				pinyin : "chǎng",
				entry : "场",
				translation : "match, set,",
				words : "(measure word for sports, films, performances)一场电影(a movie) 两场比赛(two games)",
				usage : null
			}, {
				id : "zu-foot",
				pinyin : "zú",
				entry : "足",
				translation : "foot",
				words : "足球(soccer) 手足(hand and foot means brothers)",
				usage : null
			}, {
				id : "qiu-ball",
				pinyin : "qiú",
				entry : "球",
				translation : "ball",
				words : "篮球(basketball) 球赛(ball game)",
				usage : null
			}, {
				id : "sai-race",
				pinyin : "sài",
				entry : "赛",
				translation : "race, match/to compete, to race",
				words : "比赛(match) 赛跑(race)",
				usage : null
			}, {
				id : "ti-lift",
				pinyin : "tí",
				entry : "提",
				translation : "to lift",
				words : "手提箱(suitcase) 提高(improve)",
				usage : null
			}, {
				id : "ti-kick",
				pinyin : "tī",
				entry : "踢",
				translation : "to kick",
				words : "踢球(kick the ball) 拳打脚踢(punch and kick)",
				usage : null
			}, {
				id : "zuo-left",
				pinyin : "zuǒ",
				entry : "左",
				translation : "left",
				words : "左边(left side) 左派(left wing)",
				usage : null
			}, {
				id : "you-right",
				pinyin : "yòu",
				entry : "右",
				translation : "right",
				words : "右边(right side) 右派(right wing)",
				usage : null
			}, ]
}

var NPCR2_21_2 = {
	course : 'npcr2',
	lesson : '21',
	section : '2',
	title : '第21课生词(2)',
	entries : '东, 离, 远, 区, 前, 拐, 下, 上, 米, 北, 卧, 输, 山',
	quizletId : '197063508/',
	characters : [ {
		id : "dong-east",
		pinyin : "dōng",
		entry : "东",
		translation : "east",
		words : "东方(east) 东风(east wind)",
		usage : null
	}, {
		id : "li-away",
		pinyin : "lí",
		entry : "离",
		translation : "away, off, from",
		words : "离开(leave) 离婚(devorce)",
		usage : null
	}, {
		id : "yuan-far",
		pinyin : "yuǎn",
		entry : "远",
		translation : "far",
		words : "远方(far away) 不远(close by)",
		usage : null
	}, {
		id : "qu-district",
		pinyin : "qū",
		entry : "区",
		translation : "district, section, area",
		words : "小区(district) 区分(distinction)",
		usage : null
	}, {
		id : "qian-front",
		pinyin : "qián",
		entry : "前",
		translation : "front, ahead, forward",
		words : "向前(go foword) 前方(front)",
		usage : null
	}, {
		id : "guai-turn",
		pinyin : "guǎi",
		entry : "拐",
		translation : "to turn",
		words : "拐弯(turn) 拐骗(abduction)",
		usage : null
	}, {
		id : "xia-down",
		pinyin : "xià",
		entry : "下",
		translation : "down, under",
		words : "下面(below) 下楼(go downstairs)",
		usage : null
	}, {
		id : "mi-meter",
		pinyin : "mǐ",
		entry : "米",
		translation : "meter, rice",
		words : "三米(3 metters) 大米(rice)",
		usage : null
	}, {
		id : "bei-north",
		pinyin : "běi",
		entry : "北",
		translation : "north",
		words : "北方(north) 北风(north wind)",
		usage : null
	}, {
		id : "wo-lie",
		pinyin : "wò",
		entry : "卧",
		translation : "to lie on one's back",
		words : "卧室(bedroom) 卧倒(lying down )(face down)",
		usage : null
	}, {
		id : "shu-lose",
		pinyin : "shū",
		entry : "输",
		translation : "to lose",
		words : "输球(lose the ball game) 认输(throw in the towel)",
		usage : null
	}, {
		id : "shan-hill",
		pinyin : "shān",
		entry : "山",
		translation : "hill, mountain",
		words : "高山(alpine) 山路(mountain road)",
		usage : null
	}, ]
}

var NPCR2_22_1 = {
	course : 'npcr2',
	lesson : '22',
	section : '1',
	title : '第22课生词(1)',
	entries : '过, 剧, 戏, 演, 遍, 部, 排, 见, 梦',
	quizletId : '197296178/',
	characters : [ {
		id : "guo-past",
		pinyin : "gùo",
		entry : "过",
		translation : "indicating a past experience",
		words : "过去(past) 吃过饭了(done eating)",
		usage : null
	}, {
		id : "ju-opera",
		pinyin : "jù",
		entry : "剧",
		translation : "opera, dramatic work, play",
		words : "京剧(Beijing Opera) 话剧(play)",
		usage : null
	}, {
		id : "xi-drama",
		pinyin : "xì",
		entry : "戏",
		translation : "drama, play, show",
		words : "戏曲(drama) 唱戏(sing opera)",
		usage : null
	}, {
		id : "yan-act",
		pinyin : "yǎn",
		entry : "演",
		translation : "to act, to perform, to play",
		words : "演出(perform) 演员(actor/actress)",
		usage : null
	}, {
		id : "bian-times",
		pinyin : "biàn",
		entry : "遍",
		translation : "number of times, through",
		words : "三遍(three times) 走遍……(traveled all over some place)",
		usage : null
	}, {
		id : "bu-bu",
		pinyin : "bù",
		entry : "部",
		translation : "measure word for films, works of literature",
		words : "一部电影(a movie)",
		usage : null
	}, {
		id : "pai-line",
		pinyin : "pái",
		entry : "排",
		translation : "line, row",
		words : "第三排(the 3rd row) 两排座位(two rows of seats)",
		usage : null
	}, {
		id : "jian-see",
		pinyin : "jiàn",
		entry : "见",
		translation : "to see, to meet with",
		words : "看见(see) 见到(see, meet)",
		usage : null
	}, {
		id : "meng-dream",
		pinyin : "mèng",
		entry : "梦",
		translation : "dream",
		words : "做梦(to dream) 梦想(someone's dream)",
		usage : null
	}, ]
}

var NPCR2_22_2 = {
	course : 'npcr2',
	lesson : '22',
	section : '2',
	title : '第22课生词(2)',
	entries : '种, 难, 顿, 诗, 骗, 烧, 哭, 死',
	quizletId : '197296483/',
	characters : [ {
		id : "zhong-kind",
		pinyin : "zhǒng",
		entry : "种",
		translation : "kind, sort, type, plant",
		words : "种类(kind, sort) 种花(plant flowers)",
		usage : null
	}, {
		id : "nan-difficult",
		pinyin : "nán",
		entry : "难",
		translation : "difficult, hard",
		words : "困难(difficulty) 难过(sad, hard time)",
		usage : null
	}, {
		id : "dun-dun",
		pinyin : "dùn",
		entry : "顿",
		translation : "measure word for meals",
		words : "一顿饭(a meal)",
		usage : null
	}, {
		id : "shi-poem",
		pinyin : "shī",
		entry : "诗",
		translation : "poem, poetry",
		words : "写诗(write poetry) 诗意(poetic)",
		usage : null
	}, {
		id : "pian-cheat",
		pinyin : "piàn",
		entry : "骗",
		translation : "to cheat, to deceive",
		words : "骗人(deceive) 欺骗(deceive)",
		usage : null
	}, {
		id : "shao-burn",
		pinyin : "shāo",
		entry : "烧",
		translation : "to burn",
		words : "燃烧(burn) 烧心(heartburn)",
		usage : null
	}, {
		id : "ku-cry",
		pinyin : "kū",
		entry : "哭",
		translation : "to cry, to weep",
		words : "大哭(cry) 哭笑不得(not sure to laugh or cry)",
		usage : null
	}, {
		id : "si-die",
		pinyin : "sǐ",
		entry : "死",
		translation : "to die",
		words : "死人(dead) 死去活来(suffering terribly)",
		usage : null
	}, ]
}

var NPCR2_23_1 = {
	course : 'npcr2',
	lesson : '23',
	section : '1',
	title : '第23课生词(1)',
	entries : '爬, 假, 提, 飞, 机, 山, 顶, 景, 色, 导, 游, 行',
	quizletId : '197318683/',
	characters : [ {
		id : "pa-climb",
		pinyin : "pá",
		entry : "爬",
		translation : "to climb",
		words : "爬山(climb mountain) 爬树(climb tree)",
		usage : null
	}, {
		id : "jia-vacation",
		pinyin : "jià",
		entry : "假",
		translation : "vacation, holiday",
		words : "放假(on vacation) 假期(vacation)",
		usage : null
	}, {
		id : "ti-lift",
		pinyin : "tí",
		entry : "提",
		translation : "to put forward, to raise",
		words : "提前(ahead of time) 提取(extract)",
		usage : null
	}, {
		id : "fei-fly",
		pinyin : "fēi",
		entry : "飞",
		translation : "to fly",
		words : "飞机(aircraft) 飞行(fly)",
		usage : null
	}, {
		id : "ji-machine",
		pinyin : "jī",
		entry : "机",
		translation : "machine in general, occasion",
		words : "机器(machine) 机会(opportunity)",
		usage : null
	}, {
		id : "ding-peak",
		pinyin : "dǐng",
		entry : "顶",
		translation : "peak, tip",
		words : "山顶(top of the mountain) 房顶(roof)",
		usage : null
	}, {
		id : "jing-scene",
		pinyin : "jǐng",
		entry : "景",
		translation : "view, scene",
		words : "景色(scene) 风景(scene)",
		usage : null
	}, {
		id : "se-color",
		pinyin : "sè",
		entry : "色",
		translation : "scene, color",
		words : "颜色(color) 红色(red)",
		usage : null
	}, {
		id : "dao-guide",
		pinyin : "dǎo",
		entry : "导",
		translation : "to guide, to lead",
		words : "领导(leader) 指导(guide, instruct)",
		usage : null
	}, {
		id : "you-travel",
		pinyin : "yóu",
		entry : "游",
		translation : "to travel, to swim",
		words : "旅游(travel) 游泳(swim)",
		usage : null
	}, {
		id : "xing-ok",
		pinyin : "xíng",
		entry : "行",
		translation : "OK, action",
		words : "还行(it's OK) 行动(to act)",
		usage : null
	}, ]
}

var NPCR2_23_2 = {
	course : 'npcr2',
	lesson : '23',
	section : '2',
	title : '第23课生词(2)',
	entries : '加, 累, 条, 龙, 拍, 雪, 零, 度, 帮, 站, 起, 渴, 熊, 棵, 树, 装, 动',
	quizletId : '197318960/',
	characters : [
			{
				id : "jia-add",
				pinyin : "jiā",
				entry : "加",
				translation : "to increase, to add",
				words : "增加(increase) 加法(addition)",
				usage : null
			},
			{
				id : "lei-tired",
				pinyin : "lèi",
				entry : "累",
				translation : "tired",
				words : "累了(tired) 劳累(tired)",
				usage : null
			},
			{
				id : "tiao-strip",
				pinyin : "tiáo",
				entry : "条",
				translation : "strip, long narrow piece, measure word for snakes, rivers, trousers",
				words : "一条河(a river) 星条旗(The flag of the United States)",
				usage : null
			}, {
				id : "long-dragon",
				pinyin : "lóng",
				entry : "龙",
				translation : "dragon",
				words : "恐龙(Dinosaurs) 龙王(Dragon King)",
				usage : null
			}, {
				id : "pai-pat",
				pinyin : "pāi",
				entry : "拍",
				translation : "to pat, to beat, to take a picture",
				words : "拍手(clap) 拍照(take picture)",
				usage : null
			}, {
				id : "xue-snow",
				pinyin : "xuě",
				entry : "雪",
				translation : "to snow",
				words : "下雪(snow) 雪白(snow white)",
				usage : null
			}, {
				id : "ling-zero",
				pinyin : "líng",
				entry : "零",
				translation : "zero",
				words : "零上(above zero degree) 零容忍(zero tolerance)",
				usage : null
			}, {
				id : "du-degree",
				pinyin : "dù",
				entry : "度",
				translation : "degree",
				words : "三十度(30 degrees) 过度(over, excess)",
				usage : null
			}, {
				id : "bang-help",
				pinyin : "bāng",
				entry : "帮",
				translation : "to help, to assist",
				words : "帮助(help) 帮忙(help)",
				usage : null
			}, {
				id : "zhan-stand",
				pinyin : "zhàn",
				entry : "站",
				translation : "to stand, station",
				words : "站起来(stand up) 车站(bus/train station)",
				usage : null
			}, {
				id : "qi-rise",
				pinyin : "qǐ",
				entry : "起",
				translation : "to rise, to get up",
				words : "起来(rise) 起床(get up)",
				usage : null
			}, {
				id : "ke-thirsty",
				pinyin : "kě",
				entry : "渴",
				translation : "thirsty",
				words : "口渴(thirsty) 渴望(desire)",
				usage : null
			}, {
				id : "xiong-bear",
				pinyin : "xióng",
				entry : "熊",
				translation : "bear",
				words : "黑熊(black bear) 熊猫(panda)",
				usage : null
			}, {
				id : "ke-ke",
				pinyin : "kē",
				entry : "棵",
				translation : "measure word for trees, plants",
				words : "一棵树(a tree) 两棵花(two flowers)",
				usage : null
			}, {
				id : "shu-tree",
				pinyin : "shù",
				entry : "树",
				translation : "tree",
				words : "松树(pine) 树叶(tree leaves)",
				usage : null
			}, {
				id : "zhuang-pretend",
				pinyin : "zhuāng",
				entry : "装",
				translation : "to pretend to be, to load",
				words : "装病(pretend to be sick) 装满(filled)",
				usage : null
			}, {
				id : "dong-move",
				pinyin : "dòng",
				entry : "动",
				translation : "to move",
				words : "行动(action) 感动(be moved)",
				usage : null
			}, ]
}
var NPCR2_24_1 = {
	course : 'npcr2',
	lesson : '24',
	section : '1',
	title : '第24课生词(1)',
	entries : '当, 菜, 在, 向, 像',
	quizletId : '197646057/',
	characters : [ {
		id : "dang-serve",
		pinyin : "dāng",
		entry : "当",
		translation : "to serve as, to be",
		words : "当家(manage a household) 当然(of course)",
		usage : null
	}, {
		id : "cai-vegetable",
		pinyin : "cài",
		entry : "菜",
		translation : "vegetable food, dish",
		words : "蔬菜(vegetable) 炒菜(pan-fry)",
		usage : null
	}, {
		id : "zai-being",
		pinyin : "zài",
		entry : "在",
		translation : "being, used to indicate an action in progress",
		words : "在家(is home) 正在(being)",
		usage : null
	}, {
		id : "xiang-towards",
		pinyin : "xiàng",
		entry : "向",
		translation : "towards, to",
		words : "方向(direction) 向前(go forward)",
		usage : null
	}, {
		id : "xiang-alike",
		pinyin : "xiàng",
		entry : "像",
		translation : "to be alike, to take after",
		words : "画像(portrait) 想像(imagine)",
		usage : null
	}, ]
}

var NPCR2_24_2 = {
	course : 'npcr2',
	lesson : '24',
	section : '2',
	title : '第24课生词(2)',
	entries : '雨, 种, 温, 收, 入, 盖, 座, 辆, 村, 低, 接, 停, 冻, 声',
	quizletId : '197646368/',
	characters : [ {
		id : "yu-rain",
		pinyin : "yǔ",
		entry : "雨",
		translation : "rain",
		words : "下雨(rain) 雨水(rainwater)",
		usage : null
	}, {
		id : "zhong-grow",
		pinyin : "zhòng",
		entry : "种",
		translation : "to grow, to plant",
		words : "种菜(grow vegetable) 种树(plant tree)",
		usage : null
	}, {
		id : "wen-warm",
		pinyin : "wēn",
		entry : "温",
		translation : "warm",
		words : "温暖(warm) 温度(temprature)",
		usage : null
	}, {
		id : "shou-accept",
		pinyin : "shōu",
		entry : "收",
		translation : "to accept, to receive",
		words : "收入(income) 接收(accept)",
		usage : null
	}, {
		id : "ru-enter",
		pinyin : "rù",
		entry : "入",
		translation : "to enter",
		words : "进入(go in) 入口(entrance)",
		usage : null
	}, {
		id : "gai-build",
		pinyin : "gài",
		entry : "盖",
		translation : "to build, lid",
		words : "盖房(build house) 锅盖(pot lid)",
		usage : null
	}, {
		id : "zuo-zuo",
		pinyin : "zuò",
		entry : "座",
		translation : "measure word for mountains, buildings",
		words : "一座山(a mountain) 两座大楼(two big buildings)",
		usage : null
	}, {
		id : "liang-liang",
		pinyin : "liàng",
		entry : "辆",
		translation : "measure word for vehicles",
		words : "一辆汽车(a car)",
		usage : null
	}, {
		id : "cun-village",
		pinyin : "cūn",
		entry : "村",
		translation : "village",
		words : "村庄(village) 农村(countryside)",
		usage : null
	}, {
		id : "di-low",
		pinyin : "dī",
		entry : "低",
		translation : "low",
		words : "低头(bow down) 低声(whispered, low voice)",
		usage : null
	}, {
		id : "jie-pickup",
		pinyin : "jiē",
		entry : "接",
		translation : "to pick up someone",
		words : "接人(pick up sb.) 接触(contact)",
		usage : null
	}, {
		id : "ting-stop",
		pinyin : "tíng",
		entry : "停",
		translation : "to stop, to cease",
		words : "停止(stop) 暂停(pause, timeout)",
		usage : null
	}, {
		id : "dong-freeze",
		pinyin : "dòng",
		entry : "冻",
		translation : "to freeze",
		words : "冰冻(frozen) 冻伤(frostabite)",
		usage : null
	}, {
		id : "sheng-sound",
		pinyin : "shēng",
		entry : "声",
		translation : "sound, voice",
		words : "声音(voice, sound) 大声(loudly)",
		usage : null
	}, ]
}

var NPCR2_25_1 = {
	course : 'npcr2',
	lesson : '25',
	section : '1',
	title : '第25课生词(1)',
	entries : '着, 送, 被, 撞, 伤, 第, 重, 完, 腿, 骑, 笑',
	quizletId : '197646730/',
	characters : [ {
		id : "zhe-being",
		pinyin : "zhe",
		entry : "着",
		translation : "being, indicating the continuous aspect",
		words : "看着(watching) 想着(thinking)",
		usage : null
	}, {
		id : "song-give",
		pinyin : "sòng",
		entry : "送",
		translation : "give, to take someone somewhere",
		words : "送礼(give gift to) 送行(see someone off)",
		usage : null
	}, {
		id : "bei-by",
		pinyin : "bèi",
		entry : "被",
		translation : "by, used to indicate passive voice",
		words : "被动(passive) 被打(beaten)",
		usage : null
	}, {
		id : "zhuang-knock",
		pinyin : "zhuàng",
		entry : "撞",
		translation : "to knock, to collide",
		words : "冲撞(collide) 撞车(car crash)",
		usage : null
	}, {
		id : "shang-hurt",
		pinyin : "shāng",
		entry : "伤",
		translation : "to hurt, to wound",
		words : "受伤(hurt, injury)  伤害(hurt)",
		usage : null
	}, {
		id : "di-order",
		pinyin : "dì",
		entry : "第",
		translation : "order, for the ordinal numbers",
		words : "第一(first) 第八(eighth)",
		usage : null
	}, {
		id : "zhong-heavy",
		pinyin : "zhòng",
		entry : "重",
		translation : "serious, heavy",
		words : "重量(collision) 体重(body weight)",
		usage : null
	}, {
		id : "wan-finish",
		pinyin : "wán",
		entry : "完",
		translation : "to finish, to run out of",
		words : "完全(completely) 做完(finish, done)",
		usage : null
	}, {
		id : "tui-leg",
		pinyin : "tuǐ",
		entry : "腿",
		translation : "leg",
		words : "大腿(thigh) 桌子腿(table legs)",
		usage : null
	}, {
		id : "qi-ride",
		pinyin : "qí",
		entry : "骑",
		translation : "to ride",
		words : "骑马(ride a horse) 骑士(knight)",
		usage : null
	}, {
		id : "xiao-laugh",
		pinyin : "xiào",
		entry : "笑",
		translation : "to laugh, to smile",
		words : "微笑(smile) 嘲笑(ridicule, scoff)",
		usage : null
	}, ]
}

var NPCR2_25_2 = {
	course : 'npcr2',
	lesson : '25',
	section : '2',
	title : '第25课生词(2)',
	entries : '躺, 视, 束, 放, 关, 弯, 坏, 偷, 抓, 丢, 补, 吓, 掉',
	quizletId : '197647090/',
	characters : [ {
		id : "tang-lie",
		pinyin : "táng",
		entry : "躺",
		translation : "to lie on, to lie down",
		words : "躺下(lie down) 躺倒(lie down)",
		usage : null
	}, {
		id : "shi-look",
		pinyin : "shì",
		entry : "视",
		translation : "to look at",
		words : "注视(gaze) 电视(TV) 视频(video clip/chat)",
		usage : null
	}, {
		id : "shu-bunch",
		pinyin : "shù",
		entry : "束",
		translation : "bunch of flowers",
		words : "一束花(bunch of flowers)",
		usage : null
	}, {
		id : "fang-put",
		pinyin : "fàng",
		entry : "放",
		translation : "to put, to place",
		words : "放下(put down) 开放(open)",
		usage : null
	}, {
		id : "guan-close",
		pinyin : "guān",
		entry : "关",
		translation : "to close, to turn off",
		words : "关门(close the door) 难关(difficulty)",
		usage : null
	}, {
		id : "wan-bend",
		pinyin : "wān",
		entry : "弯",
		translation : "to bend",
		words : "弯曲(bending) 拐弯(to turn)",
		usage : null
	}, {
		id : "huai-bad",
		pinyin : "huài",
		entry : "坏",
		translation : "bad, broken",
		words : "坏人(bad guys) 坏了(stopped working)",
		usage : null
	}, {
		id : "tou-steal",
		pinyin : "tōu",
		entry : "偷",
		translation : "to steal",
		words : "小偷(thief) 偷看(peep)",
		usage : null
	}, {
		id : "zhua-clutch",
		pinyin : "zhuā",
		entry : "抓",
		translation : "to clutch, to catch, to arrest",
		words : "抓住(catch) 抓人(arrest)",
		usage : null
	}, {
		id : "diu-lose",
		pinyin : "diū",
		entry : "丢",
		translation : "to lose",
		words : "丢失(lose) 丢三落四(careless)",
		usage : null
	}, {
		id : "bu-mend",
		pinyin : "bǔ",
		entry : "补",
		translation : "to mend, to patch, remediation",
		words : "补充(to add) 补衣服(make up cloth)",
		usage : null
	}, {
		id : "xia-scare",
		pinyin : "xià",
		entry : "吓",
		translation : "to scare, to frighten",
		words : "惊吓(scare) 吓一跳(scared)",
		usage : null
	}, {
		id : "diao-drop",
		pinyin : "diào",
		entry : "掉",
		translation : "to drop, to fall",
		words : "掉地上(dropped to the floor) 掉以轻心(take it lightly)",
		usage : null
	}, ]
}

var NPCR2_26_1 = {
	course : 'npcr2',
	lesson : '26',
	section : '1',
	title : '第26课生词(1)',
	entries : '成, 南, 对, 感, 饿, 聊, 才, 陪, 散, 句',
	quizletId : '197647407/',
	characters : [ {
		id : "cheng-become",
		pinyin : "chéng",
		entry : "成",
		translation : "to become",
		words : "成功(success) 成为(become)",
		usage : null
	}, {
		id : "nan-south",
		pinyin : "nán",
		entry : "南",
		translation : "south",
		words : "南方(south) 南下(go south)",
		usage : null
	}, {
		id : "dui-correct",
		pinyin : "duì",
		entry : "对",
		translation : "correct, face, about",
		words : "对于(about) 不对(incorrect)",
		usage : null
	}, {
		id : "gan-feel",
		pinyin : "gǎn",
		entry : "感",
		translation : "to feel, to sense",
		words : "感觉(feeling) 感冒(flu)",
		usage : null
	}, {
		id : "e-hungry",
		pinyin : "è",
		entry : "饿",
		translation : "hungry",
		words : "饥饿(hungry) 饿虎(a hungry tiger, dangerous)",
		usage : null
	}, {
		id : "liao-chat",
		pinyin : "liáo",
		entry : "聊",
		translation : "to chat",
		words : "聊天(chat) 闲聊(chat leisurely)",
		usage : null
	}, {
		id : "cai-just",
		pinyin : "cái",
		entry : "才",
		translation : "just, talent",
		words : "刚才(just now) 天才(genius)",
		usage : null
	}, {
		id : "pei-accompany",
		pinyin : "péi",
		entry : "陪",
		translation : "to accompany, to compensate",
		words : "陪伴(accompany) 赔偿(compensate)",
		usage : null
	}, {
		id : "san-disperse",
		pinyin : "sàn",
		entry : "散",
		translation : "to disperse, to break up",
		words : "分散(scattered) 散发(distribute)",
		usage : null
	}, {
		id : "ju-sentence",
		pinyin : "jù",
		entry : "句",
		translation : "sentence",
		words : "句号（period, stop 一句话(a sentence)",
		usage : null
	}, ]
}

var AllCharacterGroups = [ NPCR1_01_1, NPCR1_02_1, NPCR1_03_1, NPCR1_03_2,
		NPCR1_04_1, NPCR1_05_1, NPCR1_05_2, NPCR1_06_1, NPCR1_06_2, NPCR1_07_1,
		NPCR1_07_2, NPCR1_08_1, NPCR1_08_2, NPCR1_09_1, NPCR1_09_2, NPCR1_10_1,
		NPCR1_10_2, NPCR1_11_1, NPCR1_11_2, NPCR1_12_1, NPCR1_12_2, NPCR1_13_1,
		NPCR1_13_2, NPCR1_14_1, NPCR1_14_2, NPCR2_15_1, NPCR2_15_2, NPCR2_16_1,
		NPCR2_16_2, NPCR2_17_1, NPCR2_17_2, NPCR2_18_1, NPCR2_18_2, NPCR2_19_1,
		NPCR2_19_2, NPCR2_20_1, NPCR2_21_1, NPCR2_21_2, NPCR2_22_1, NPCR2_22_2,
		NPCR2_23_1, NPCR2_23_2, NPCR2_24_1, NPCR2_24_2, NPCR2_25_1, NPCR2_25_2,
		NPCR2_26_1 ];

/**
 * http://www.archchinese.com/chinese_english_dictionary.html?find=大
 * http://www.archchinese.com/chinese_character_strokes.html
 */
