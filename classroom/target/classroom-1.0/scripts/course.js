/** The application of the courses */
var app = angular.module('course', [ 'ngSanitize' ]);

/**
 * The course's controller to bind and update the page properties for the view
 * (course.html)
 */
app.controller('courseController', function($scope, $location, courseService) {

	/*
   * The selected section
   */
	var selectedSection = {
		course : null,
		lesson : null,
		section : null,
		label : '',
		type : '',
		media : '',
		idx : 0,
		text : []
	};

	/**
   * Declares and initializes the binding properties.
   */
	$scope.init = function() {

		// the informational message on the page.
		$scope.message = '';

		// indicators for displaying certain elements conditionally
		$scope.isVideo = false;
		$scope.isAudio = false;
		$scope.isMultiText = false;

		// some dynamically changed values
		$scope.audioSource = '';
		$scope.videoSource = '';
		$scope.lessonText = '';

		$scope.lessonTitles = [];
		$scope.sectionLabels = [];

		/*
     * The selected course {id, title, image, description, lessons[{lesson,
     * section, title, label]
     */
		$scope.selectedCourse = null;

		// parse the query request to initialize the searching parameters.
		var url = $location.absUrl();
		var parameters = $scope.parseQueryString(url);
		if (parameters.length > 0) {
			switch (parameters.length) {
			case 1:
				selectedSection.course = parameters[0].value;
				break;
			case 2:
				selectedSection.course = parameters[0].value;
				selectedSection.lesson = parameters[1].value;
				break;
			default:
				selectedSection.course = parameters[0].value;
				selectedSection.lesson = parameters[1].value;
				selectedSection.section = parameters[2].value;
				break;
			}
		}

		// set the default lesson (the 1st lesson of the course)
		$scope.selectedCourse = courseService.getSelectedCourse(
			selectedSection.course);
		if (selectedSection.lesson == null) {
			var lesson = $scope.selectedCourse.lessons[0];
			selectedSection.lesson = lesson.lesson;
		}
		if (selectedSection.section == null) {
			var section = $scope.selectedCourse.lessons[0];
			selectedSection.section = section.section;
		}

		// update the binding properties with the initial values
		$scope.lessonTitles = courseService.getLessonTitleList($scope.selectedCourse);
		$scope.setLesson(selectedSection.lesson);
	}

	/**
   * Changes the current lesson to the specified one.
   * 
   * @param lesson
   */
	$scope.setLesson = function(lesson) {

		// update the selected section
		selectedSection.lesson = lesson;
		$scope.sectionLabels = courseService.getSectionLabelList(
			$scope.selectedCourse, lesson);
		selectedSection.idx = 0;
		if (selectedSection.section == null && $scope.sectionLabels.length > 0) {
			selectedSection.section = $scope.sectionLabels[0].section;
		}

		// highlight the selected TOC item and set all the
		// binding properties.
		$scope.highlightSelectedItem(lesson);
		$scope.setSection(selectedSection.section);
	}

	/**
   * Sets values to the binding properties.
   * 
   * @param section
   */
	$scope.setSection = function(section) {

		// get the selected section values
		selectedSection = courseService.getSection(
			selectedSection.course, selectedSection.lesson, section);

		if (selectedSection == null) {
			$scope.message = 'No this material:'
				+ selectedSection.course + ', ' + selectedSection.lesson
				+ ', ' + section;
			return;
		} else {
			$scope.message = '';
			selectedSection.section = section;
		}

		// check the media type and set the binding properties accordingly
		$scope.isVideo = (selectedSection.hasOwnProperty('type')
			&& selectedSection.hasOwnProperty('media')
			&& selectedSection.type == 'video');
		$scope.isAudio = (selectedSection.hasOwnProperty('type')
			&& selectedSection.hasOwnProperty('media')
			&& selectedSection.type == 'audio');
		$scope.isMultiText = (selectedSection.hasOwnProperty('text')
			&& selectedSection.text.length > 1);

		// add a video player with the selected clip and remove the audio
		// that is still playing
		if ($scope.isVideo) {
			$scope.audioSource = '';
			$scope.videoSource = selectedSection.media;
			$scope.addAudioPlayer();
			$scope.addVideoPlayer();
		}

		// add an audio player with the selected clip.
		if ($scope.isAudio) {
			$scope.audioSource = courseService.getAudioSrcName(selectedSection.media);
			$scope.addAudioPlayer();
		}

		// bind the selected text
		if (selectedSection.hasOwnProperty('text')) {
			var textId = selectedSection.text[selectedSection.idx];
			$scope.lessonText = courseService.getSectionText(textId);
		}
	}

	/**
   * Checks if the section is the selected section in the drop down list.
   */
	$scope.isSelectedSection = function(section) {
		return (section.section == selectedSection.section);
	}

	/**
   * Gets the next text and shows it on page.
   */
	$scope.getNextText = function() {
		if (selectedSection.idx == (selectedSection.text.length - 1)) {
			selectedSection.idx = 0;
		} else {
			selectedSection.idx++;
		}
		var textId = selectedSection.text[selectedSection.idx];
		$scope.lessonText = courseService.getSectionText(textId);
	}

	/**
   * Highlights the selected TOC item.
   */
	$scope.highlightSelectedItem = function(lessonId) {
		for (var i = 0; i < $scope.lessonTitles.length; i++) {
			var title = $scope.lessonTitles[i];
			if (title.lesson == lessonId) {
				title.bgcolor = 'Yellow';
			} else {
				title.bgcolor = '';
			}
		}
	}

	/**
   * Parses the query string of the given URL.
   * 
   * @param url
   *          https://.../books/npcr2/npcr2.html?lesson=1&section=2
   * @returns a list of name/value pairs.
   */
	$scope.parseQueryString = function(url) {
		var parameters = [];
		var path = null;
		var query = null;
		if (url.indexOf('?') > 0) {
			var items = url.split('?');
			path = items[0];
			query = items[1];
		} else {
			path = url;
		}
		path = path.split('/');
		path = path[path.length - 1];
		path = path.replace('.html', '');
		parameters.push({
			name : 'course',
			value : path
		});

		if (query != null) {
			var pairs = query.split('&');
			for (var i = 0; i < pairs.length; i++) {
				var pair = pairs[i].split('=');
				parameters.push({
					name : decodeURIComponent(pair[0]),
					value : decodeURIComponent(pair[1])
				})
			}
		}
		return parameters;
	}

	/**
   * Have to do it this way to re-load the source for the video and audio
   * players every time the get changed :-(
   */
	$scope.addAudioPlayer = function() {
		var player = document.createElement('audio');
		var source = document.createElement('source');
		player.controls = true;
		player.className = 'side-clip';
		source.type = 'audio/mpeg';
		source.src = $scope.audioSource;
		player.appendChild(source);

		var audioPanel = document.querySelector('#audio-panel');
		audioPanel.innerHTML = '';
		audioPanel.appendChild(player);
		audioPanel.style.display = 'block';
	}

	$scope.addVideoPlayer = function() {
		var videoPanel = document.querySelector('#video-panel');
		var parentPanel = videoPanel.parentNode;
		var player = videoPanel.cloneNode(true);
		parentPanel.removeChild(videoPanel);
		player.src = $scope.videoSource;
		parentPanel.appendChild(player);
	}
});

/**
 * The course services to look up courses, lessons, which are declared in
 * course-list.js, by different parameters.
 */
app.service('courseService', function() {

	/**
   * Forms a fully qualified audio clips URL.
   * 
   * @param src
   * @returns {String}
   */
	this.getAudioSrcName = function(src) {
		return AUDIO_FOLDER + src;
	}

	/**
   * Retrieve a text panel from the document by textId and returns its inner
   * HTML code.
   */
	this.getSectionText = function(textId) {
		var textElement = document.querySelector('#' + textId);
		return textElement.innerHTML;
	}

	/**
   * Gets a course from the course collection.
   * 
   * @param courseId
   * @param lesson
   * @param section
   * @returns a section of the given lesson of the course
   */
	this.getSection = function(courseId, lesson, section) {

		if (section == null || section.length == 0) {
			return this.getLesson(courseId, lesson);
		}

		var chosenSection = {
			course : courseId,
			lesson : lesson,
			section : section,
			label : '',
			type : '',
			media : '',
			idx : 0,
			text : []
		};

		var itemSet = AllCourses.itemSet;
		for (var i = 0; i < itemSet.length; i++) {
			var course = itemSet[i];
			if (course.id == courseId) {
				for (var j = 0; j < course.items.length; j++) {
					var item = course.items[j];
					if (item.lesson == lesson && item.hasOwnProperty('section')
						&& item.section == section) {
						chosenSection.label = item.label;
						chosenSection.type = item.type;
						chosenSection.media = item.media;
						chosenSection.text = item.text;
						return chosenSection;
					}
				}
			}
		}
		return null;
	}

	/**
   * Gets a course from the course data collection.
   * 
   * @param courseId
   * @param lesson
   * @returns a course
   */
	this.getLesson = function(courseId, lesson) {
		var itemSet = AllCourses.itemSet;
		var chosenSection = {
			course : courseId,
			lesson : lesson,
			section : '',
			label : '',
			type : '',
			media : '',
			idx : 0,
			text : []
		};
		for (var i = 0; i < itemSet.length; i++) {
			var course = itemSet[i];
			if (course.id == courseId) {
				for (var j = 0; j < course.items.length; j++) {
					var item = course.items[j];
					if (item.lesson == lesson) {
						chosenSection.section = (item.hasOwnProperty('section') ? item.section : '');
						chosenSection.label = (item.hasOwnProperty('label') ? item.label : '');
						chosenSection.type = item.type;
						chosenSection.media = item.media;
						chosenSection.text = item.text;
						return chosenSection;
					}
				}
			}
		}
		return null;
	}

	/**
   * Gets a course by courseId.
   * 
   * @param courseId
   * @returns
   */
	this.getSelectedCourse = function(courseId) {
		var itemSet = AllCourses.itemSet;
		for (var i = 0; i < itemSet.length; i++) {
			var course = itemSet[i];
			if (course.id == courseId) {
				var chosenCourse = {
					id : course.id,
					title : course.title,
					image : course.image,
					description : course.description,
					lessons : []
				};
				for (var j = 0; j < course.items.length; j++) {
					var item = course.items[j];
					var info = {
						lesson : item.lesson,
						section : (item.hasOwnProperty('section') ? item.section : ''),
						title : (item.hasOwnProperty('title') ? item.title : ''),
						label : (item.hasOwnProperty('label') ? item.label : '')
					}
					chosenCourse.lessons.push(info);
				}
				return chosenCourse;
			}
		}
		return null;
	}

	/**
   * Gets a lesson title list of the specified course for generating TOC lesson
   * titles.
   * 
   * @param course
   * @returns
   */
	this.getLessonTitleList = function(course) {
		var lessons = course.lessons;
		var titleList = [];
		for (var j = 0; j < lessons.length; j++) {
			var lesson = lessons[j];
			if (lesson.hasOwnProperty('title') && lesson.title.length > 0) {
				var title = {
					lesson : lesson.lesson,
					title : lesson.title,
					bgcolor : ''
				}
				titleList.push(title);
			}
		}
		return titleList;
	}

	/**
   * Gets a section label list of the specified lesson for generating a section
   * selection list.
   * 
   * @param course
   * @param lessonId
   * @returns
   */
	this.getSectionLabelList = function(course, lessonId) {
		var lessons = course.lessons;
		var labelList = [];
		for (var j = 0; j < lessons.length; j++) {
			var lesson = lessons[j];
			if (lesson.lesson == lessonId) {
				if (lesson.hasOwnProperty('label') && lesson.label.length > 0) {
					var label = {
						lesson : lesson.lesson,
						section : lesson.section,
						label : lesson.label
					}
					labelList.push(label);
				}
			}
		}
		return labelList;
	}
                });