/**
 * Includes an additional HTML page into the template page, such as a style or a
 * script file.
 * 
 * @param filename
 * @returns
 */
function include(filename) {
	var htmlOutput = null;
	try {
		htmlOutput = HtmlService.createTemplateFromFile(filename).evaluate();
	} catch (error) {
		return '<p>Failed loading file ' + filename + ':' + error + '</p>';
	}
	htmlOutput.setSandboxMode(HtmlService.SandboxMode.IFRAME);
	return htmlOutput.getContent();
}
