/**
 * Some commonly used utilities.
 */
function ServiceUtils() {
	this.constants = new ServiceConstants();
	this.memberService = new MemberService();

	/**
	 * Builds a homework page URL for the homework assignment.
	 */
	this.buildPageUrl = function(memberId, pageName) {
		var pageUrl = this.constants.SCHOOL_ROOT_URL
				+ this.constants.COURSE_WORK_NAME + '/'
				+ this.constants.STUDENT_DIRECTORY + '/' + memberId
				+ '/homework-list/' + pageName;
		return pageUrl;
	}

	/**
	 * Gets an additional content of the email to send to the members.
	 * 
	 * @returns content
	 */
	this.getAdditionalContent = function() {
		try {
			var filename = 'additional-content';
			var src = HtmlService.createTemplateFromFile(filename).evaluate();
			return src.getContent();
		} catch (error) {
			return '';
		}
	}
}

/**
 * Builds an email message body for notifying a student about the a homework
 * assignment.
 * 
 * @param member
 * @param pageName
 */
ServiceUtils.prototype.buildStudentMsgBody = function(member, pageName) {
	var name = member.firstName;
	var memberId = member.id;
	var teacherName = '';
	var teacher = this.memberService.getTeacherInfo(memberId);
	if (teacher.hasError == false) {
		teacherName = teacher.name;
	}
	var pageUrl = this.buildPageUrl(member.id, pageName);
	var body = '<div style="font-size: 20px; font-family: Georgia;">';
	body += '<p>Hello ' + name + ',</p>';
	body += '<p><a href=' + pageUrl
			+ '>Here</a> is the homework assignment of this week.</p>';
	body += '<p>Thanks!</p>';
	body += '<p>' + teacherName + '</p>';
	body += '</div>';
	
	var additionalContent = this.getAdditionalContent();
	return body + additionalContent;
}

/**
 * Builds an email message body for notifying the school administrator about a
 * homework assignment.
 * 
 * @param className
 * @param members
 * @param pageName
 */
ServiceUtils.prototype.buildAdminMsgBody = function(className, members,
		pageName) {
	var today = Utilities.formatDate(new Date(), "MST", "yyyy-MM-dd");
	var body = '<div style="font-size: 20px; font-family: Georgia;">';
	body = '<p>' + className + ' homework assignment of ' + today + '</p>';
	body += '<table style="width: 100%;">';
	for (var i = 0; i < members.length; i++) {
		var member = members[i];
		if (member.role == MEMBER_ROLE.STUDENT) {
			var pageUrl = this.buildPageUrl(member.id, pageName);
			var name = member.firstName + ', ' + member.lastName;
			body += '<tr>';
			body += '<td width="80px">　</td>';
			body += '<td width="120px">' + member.chineseName + '</td>';
			body += '<td width="160px">';
			body += '<a href="' + pageUrl + '">' + name + '</a>';
			body += '</td>';
			body += '</tr>';
		}
	}
	body += '</table></div>';
	return body;
}

/**
 * Sends a simple email
 * 
 * @param to
 * @param subject
 * @param htmlBody
 */
ServiceUtils.prototype.sendSimpleMail = function(to, subject, htmlBody) {
	MailApp.sendEmail({
		to : to,
		subject : subject,
		htmlBody : htmlBody
	});
}

/**
 * Builds a return message with an error code and a text message.
 * 
 * @param code
 * @param text
 */
ServiceUtils.prototype.buildInfoMessage = function(code, text) {
	var message = {
		code : code,
		message : text
	};
	return message;
}

/**
 * Builds a return value with an error code and a value that could be anything
 * 
 * @param code
 * @param value
 */
ServiceUtils.prototype.buildReturnValue = function(code, value) {
	var returnValue = {
		code : code,
		message : value
	};
	return returnValue;
}

/**
 * Checks if the string ends with a suffix.
 * 
 * @param string
 * @param suffix
 * @returns {Boolean}
 */
ServiceUtils.prototype.endsWith = function(string, suffix) {
	return string.indexOf(suffix, string.length - suffix.length) !== -1;
}
