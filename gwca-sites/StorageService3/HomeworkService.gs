/**
 * The homework service.
 */
function HomeworkService() {
	
	this.constants = new ServiceConstants();
	this.dataService = new DataService();
	this.memberService = new MemberService();
	this.serviceUtils = new ServiceUtils();
	this.driveRepository = new DriveRepository();

	/**
	 * Creates a homework object by parsing the URL of the active page for
	 * loading a homework file.<br>
	 * The URL of the active page looks like this:
	 * https://SchoolDomain/course-work/studentId/homework-list/courseName-workNumber
	 */
	this.getFileInfo = function() {
		var activePage = SitesApp.getActivePage();
		var tokens = activePage.getUrl().split('/');
		var lastIdx = tokens.length - 1;
		var names = tokens[lastIdx].split('-');

		var studentId = tokens[lastIdx - 2];
		var courseId = names[0];
		var workNumber = names[1];
		var homework = this.dataService.createHomework(studentId, courseId,
			workNumber);

		homework.workTitle = activePage.getTitle();
		return homework;
	}

	/**
	 * Checks if the url is an image
	 */
	this.isImage = function(url) {
		if (url) {
			var name = url.toLowerCase();
			return (this.serviceUtils.endsWith(name, '.jpg') ||
				this.serviceUtils.endsWith(name, '.png') ||
				this.serviceUtils.endsWith(name, '.gif'));
		} else {
			return false;
		}
	}

	/**
	 * Checks if the url is an audio clip
	 */
	this.isAudio = function(url) {
		if (url) {
			var name = url.toLowerCase();
			return (this.serviceUtils.endsWith(name, '.mp3') ||
				this.serviceUtils.endsWith(name, '.m4a'));
		} else {
			return false;
		}
	}

	/**
	 * Changes an relative URL to an absolute URL.
	 * @param studentId
	 * @param url
	 * @return absolute URL 
	 */
	this.getAbsoluteURL = function(studnetId, url) {
		if (url.indexOf('http') == 0) {
			return url;
		} else {
			return this.constants.SCHOOL_ROOT_URL + this.constants.COURSE_WORK_NAME
				+ '/' + this.constants.STUDENT_DIRECTORY + '/' + studnetId + '/' + url;
		}
	}
}

/**
 * Loads the original or saved homework in JSON format from either the homework library
 * table or the student homework table.<br>
 * 
 * The naming convention of a homework ID is:<br>
 * courseName-workNumber, e.g. npcr5-58_1 (DO NOT use '-' in the workNumber.<br>
 * 
 * The URL structure of a student course work page is:<br>
 * COURSE_WORK: Domain/course-work/student-directory/studentId<br>
 * 
 * The active homework page URL of a student is structured as:<br>
 * ${COURSE_WORK}/homework-list/courseName-workNumber<br>
 * e.g. gwca.org/course-work/student-directory/john-doe/homework-list/npcr5-58_1<br>
 * 
 * If something goes wrong, reads the default error page to inform the student
 * the error.
 * 
 * @returns homework
 */
HomeworkService.prototype.load = function() {

	var fileInfo = this.getFileInfo();
	var courseId = fileInfo.courseId;
	var studentId = fileInfo.studentId;
	var workNumber = fileInfo.workNumber;
	return this.loadFile(studentId, courseId, workNumber);
}

/**
 * Loads a homework from Drive (split this from load() for testing it easier.
 */
HomeworkService.prototype.loadFile = function(studentId, courseId, workNumber) {

	// try to load from the student homework folder first
	var homework = this.dataService.getStudentHomework(studentId, courseId,
		workNumber);
	if (homework.hasError) {
		// load the original assigned homework from the course homework folder
		homework = this.dataService.getAssignedHomework(courseId, workNumber);
	}

	// create a Google Document for the writing work item if needed
	// change the relative URL to an absolute URL if needed.
	var workitems = homework.workitems;
	for (var i = 0; i < workitems.length; i++) {
		var workitem = workitems[i];
		if (workitem.name == 'write' && workitem.hasLink == false) {
			var fileName = workitem.essay;
			var url = this.driveRepository
				.createDocumentByName(studentId, fileName, workitem.instruction);
			workitem.url = url;
			workitem.hasLink = true;
		} else {
			if (workitem.hasOwnProperty('url')) {
				workitem.url = this.getAbsoluteURL(studentId, workitem.url);
			}
		}
	}
	return homework;
}

/**
 * Saves or updates the homework
 * 
 * @param homework
 * @returns homework
 */
HomeworkService.prototype.save = function(homework) {

	var fileInfo = this.getFileInfo();
	homework.courseId = fileInfo.courseId;
	homework.studentId = fileInfo.studentId;
	homework.workNumber = fileInfo.workNumber;
	homework.workTitle = fileInfo.workTitle;

	return this.dataService.saveStudentHomework(homework);
}

/**
 * Checks if the current user is a teacher.
 * 
 * @returns {Boolean}
 */
HomeworkService.prototype.isOwner = function() {
	try {
		var userEmail = Session.getActiveUser().getEmail();
		if (userEmail == null || userEmail.length == 0) {
			return false;
		}

		var owners = SitesApp.getActiveSite().getOwners();
		for (var i = 0; i < owners.length; i++) {
			if (userEmail == owners[i].getEmail()) {
				return true;
			}
		}
	} catch (error) {}
	return false;
}

/**
 * Gets the email address of the current user.
 * 
 * @returns 
 */
HomeworkService.prototype.getUserEmail = function() {
	try {
		return Session.getActiveUser().getEmail();
	} catch (error) {
		return '';
	}
}

/**
 * Sends a simple email to the teacher to notify him the homework is submitted.
 */
HomeworkService.prototype.notifyTeacher = function() {
	var fileInfo = this.getFileInfo();
	var studentId = fileInfo.studentId;
	var pageUrl = SitesApp.getActivePage().getUrl();
	this.notifyTeacherWithId(studentId, pageUrl);
}

/**
 * Sends an email to the teacher (split this from notifyTeacher() for easy
 * testing).
 * 
 * @param studentId
 * @param pageUrl
 */
HomeworkService.prototype.notifyTeacherWithId = function(studentId, pageUrl) {
	try {
		var teacher = this.memberService.getTeacherInfo(studentId);
		if (teacher == null) {
			Logger
				.log("Could not find a teacher for this student:"
					+ studentId);
			return;
		}
		var now = Utilities.formatDate(new Date(), "MST", "MM/dd/yy hh:mm a");
		var subject = studentId + ' submitted his/her homework at ' + now;
		var body = '<p>' + subject + '</p><p><a href="' + pageUrl
			+ '">Click Here</a></p>';

		MailApp.sendEmail({
			to : teacher.email,
			subject : subject,
			htmlBody : body
		});
	} catch (error) {
		Logger.log("Failed sending an email to the teacher of this student:"
			+ studentId + ' : ' + error);
	}
}

/**
 * Sends a simple email to the student to notify the student the teacher has
 * checked the homework.
 */
HomeworkService.prototype.notifyStudent = function() {
	var fileInfo = this.getFileInfo();
	var studentId = fileInfo.studentId;
	var pageUrl = SitesApp.getActivePage().getUrl();
	this.notifyStudentWithId(studentId, pageUrl);
}

/**
 * Sends an email to the student (split this from notifyStudent() for easy
 * testing).
 */
HomeworkService.prototype.notifyStudentWithId = function(studentId, pageUrl) {
	try {
		var member = this.memberService.getMember(studentId);
		var teacher = this.memberService.getTeacherInfo(studentId);
		var studentEmail = member.email;
		if (studentEmail == null || studentEmail.length == 0) {
			Logger.log("This student does not have an email:" + studentId);
			return;
		}

		var now = Utilities.formatDate(new Date(), "MST", "MM/dd/yy hh:mm a");
		var subject = 'I checked your homework at ' + now;
		var body = '<p>' + subject + '</p><p><a href="' + pageUrl
			+ '">Click Here</a> to see my comments.</p>'
			+ '<p>Thanks,</p><p>' + teacher.firstName + ' '
			+ teacher.lastName + '</p>';

		MailApp.sendEmail({
			to : studentEmail,
			subject : subject,
			htmlBody : body
		});
	} catch (error) {
		Logger.log("Failed sending an email to the student:" + studentId
			+ " : " + error);
	}
}

/**
 * Gets the first attached audio file from the current page.
 * 
 * @returns The URL of the first attached audio clip. .mp3 only,
 */
HomeworkService.prototype.getFirstAudioClip = function() {
	var page = SitesApp.getActivePage();
	var attachments = page.getAttachments();
	var clipInfo = null;
	for (var i = 0; i < attachments.length; i++) {
		var url = attachments[i].getUrl();
		if (this.isAudio(url)) {
			return url;
		}
	}
	return null;
}

/**
 * Gets the first attached image from the current page.
 * 
 * @returns The URL of the first attached image: jpg, png, gif,
 */
HomeworkService.prototype.getFirstImage = function() {
	var page = SitesApp.getActivePage();
	var attachments = page.getAttachments();
	var clipInfo = null;
	for (var i = 0; i < attachments.length; i++) {
		var url = attachments[i].getUrl();
		if (this.isImage(url)) {
			return url;
		}
	}
	return null;
}