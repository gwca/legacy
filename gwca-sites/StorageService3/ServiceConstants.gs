function ServiceConstants() {

	/** The school domain name and the root URL */
	this.SCHOOL_DOMAIN = 'greatwallchineseacademy.org';
	this.SCHOOL_ROOT_URL = 'https://sites.google.com/a/greatwallchineseacademy.org/';

	/** The course work site related constants */
	this.COURSE_WORK_NAME = 'course-work';
	this.STUDENT_DIRECTORY = 'student-directory';
	this.COURSE_SITE = 'https://gwca-classroom.appspot.com/';

	/** The homework related constants */
	this.GOOGLE_DOC_URL = 'https://docs.google.com/document/d/';
	this.ESSAY_TEMPLATE_ID = '1PfvdebrWuQNsyAt_8rQ9TiMH6tzP9QO3NYlAUP9dWhY';
	this.STUDENT_WORK_FOLDER_ID = '0B9-duwm5My1ffmVpNDlRVU44QjNMRnVhd1ozc0ZieTNNX0J2MHZ0OGFPYm9NWXJRa2RFMFU';

	/**
	 * The page information of all the pages of the course work site.
	 */
	this.COURSE_WORK_PAGES = [ {
		name : '',
		title : '',
		isHome : true,
		isUpdatable : false,
		template : 'student-template'
	}, {
		name : 'about-me',
		title : '',
		isHome : false,
		isUpdatable : false,
		template : 'about-me-template'
	}, {
		name : 'ask-teacher',
		title : 'Ask Teacher',
		isHome : false,
		isUpdatable : true,
		template : 'ask-teacher-template'
	}, {
		name : 'homework-list',
		title : 'Homework',
		isHome : false,
		isUpdatable : false,
		template : 'homework-list-template',
		subPageTemplate : 'homework-template'
	}, {
		name : 'project',
		title : 'Project',
		isHome : false,
		isUpdatable : true,
		template : 'project-template'
	}, {
		name : 'sandbox',
		title : 'Sandbox',
		isHome : false,
		isUpdatable : true,
		template : 'sandbox-template'
	} ];

	/**
	 * The page information of a homework page.
	 */
	this.HOMEWORK_PAGE = {
		listPage : 'homework-list',
		template : 'homework-template'
	};
}
