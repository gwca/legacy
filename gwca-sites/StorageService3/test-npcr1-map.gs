// update the student homework table
var NPCR1_URL_MAP = [ {
	workId : '1',
	lessonId : '01',
	chapterId : '1',
	readurl : '?item=01&subItem=1',
	pdfurl : 'lesson01.pdf'
}, {
	workId : '2',
	lessonId : '02',
	chapterId : '1',
	moreChapterId : '3',
	readurl : '?item=02&subItem=1',
	moreurl : '?item=02&subItem=3',
	pdfurl : 'lesson02.pdf'
}, {
	workId : '3_1',
	lessonId : '03',
	chapterId : '1',
	readurl : '?item=03&subItem=1',
	pdfurl : 'lesson03.pdf'
}, {
	workId : '3_2',
	lessonId : '03',
	chapterId : '2',
	moreChapterId : '3',
	readurl : '?item=03&subItem=2',
	moreurl : '?item=03&subItem=3',
	pdfurl : 'lesson03.pdf'
}, {
	workId : '4_1',
	lessonId : '04',
	chapterId : '1',
	readurl : '?item=04&subItem=1',
	pdfurl : 'lesson04.pdf'
}, {
	workId : '4_2',
	lessonId : '04',
	chapterId : '2',
	moreChapterId : '3',
	readurl : '?item=04&subItem=2',
	moreurl : '?item=04&subItem=3',
	pdfurl : 'lesson04.pdf'
}, {
	workId : '5_1',
	lessonId : '05',
	chapterId : '1',
	readurl : '?item=05&subItem=1',
	pdfurl : 'lesson05.pdf'
}, {
	workId : '5_2',
	lessonId : '05',
	chapterId : '2',
	moreChapterId : '3',
	readurl : '?item=05&subItem=2',
	moreurl : '?item=05&subItem=3',
	pdfurl : 'lesson05.pdf'
}, {
	workId : '6_1',
	lessonId : '06',
	chapterId : '1',
	readurl : '?item=06&subItem=1',
	pdfurl : 'lesson06.pdf'
}, {
	workId : '6_2',
	lessonId : '06',
	chapterId : '2',
	moreChapterId : '3',
	readurl : '?item=06&subItem=2',
	moreurl : '?item=06&subItem=3',
	pdfurl : 'lesson06.pdf'
}, {
	workId : '7_1',
	lessonId : '07',
	chapterId : '1',
	readurl : '?item=07&subItem=1',
	pdfurl : 'lesson07.pdf'
}, {
	workId : '7_2',
	lessonId : '07',
	chapterId : '2',
	moreChapterId : '3',
	readurl : '?item=07&subItem=2',
	moreurl : '?item=07&subItem=3',
	pdfurl : 'lesson07.pdf'
}, {
	workId : '8_1',
	lessonId : '08',
	chapterId : '1',
	readurl : '?item=08&subItem=1',
	pdfurl : 'lesson08.pdf'
}, {
	workId : '8_2',
	lessonId : '08',
	chapterId : '2',
	moreChapterId : '3',
	readurl : '?item=08&subItem=2',
	moreurl : '?item=08&subItem=3',
	pdfurl : 'lesson08.pdf'
}, {
	workId : '9_1',
	lessonId : '09',
	chapterId : '1',
	readurl : '?item=09&subItem=1',
	pdfurl : 'lesson09.pdf'
}, {
	workId : '9_2',
	lessonId : '09',
	chapterId : '2',
	moreChapterId : '3',
	readurl : '?item=09&subItem=2',
	moreurl : '?item=09&subItem=3',
	pdfurl : 'lesson09.pdf'
}, {
	workId : '10_1',
	lessonId : '10',
	chapterId : '1',
	readurl : '?item=10&subItem=1',
	pdfurl : 'lesson10.pdf'
}, {
	workId : '10_2',
	lessonId : '10',
	chapterId : '2',
	moreChapterId : '3',
	readurl : '?item=10&subItem=2',
	moreurl : '?item=10&subItem=3',
	pdfurl : 'lesson10.pdf'
}, {
	workId : '11_1',
	lessonId : '11',
	chapterId : '1',
	readurl : '?item=11&subItem=1',
	pdfurl : 'lesson11.pdf'
}, {
	workId : '11_2',
	lessonId : '11',
	chapterId : '2',
	moreChapterId : '3',
	readurl : '?item=11&subItem=2',
	moreurl : '?item=11&subItem=3',
	pdfurl : 'lesson11.pdf'
}, {
	workId : '12_1',
	lessonId : '12',
	chapterId : '1',
	readurl : '?item=12&subItem=1',
	pdfurl : 'lesson01.pdf'
}, {
	workId : '12_2',
	lessonId : '12',
	chapterId : '2',
	moreChapterId : '3',
	readurl : '?item=12&subItem=2',
	moreurl : '?item=12&subItem=3',
	pdfurl : 'lesson12.pdf'
}, {
	workId : '13_1',
	lessonId : '13',
	chapterId : '1',
	readurl : '?item=13&subItem=1',
	pdfurl : 'lesson13.pdf'
}, {
	workId : '13_2',
	lessonId : '13',
	chapterId : '2',
	moreChapterId : '3',
	readurl : '?item=13&subItem=2',
	moreurl : '?item=13&subItem=3',
	pdfurl : 'lesson13.pdf'
}, {
	workId : '14_1',
	lessonId : '14',
	chapterId : '1',
	readurl : '?item=14&subItem=1',
	pdfurl : 'lesson14.pdf'
} ];
