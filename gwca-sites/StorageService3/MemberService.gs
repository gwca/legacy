/**
 * The member service
 */
function MemberService() {

	this.constants = new ServiceConstants();
	this.repository = new DataRepository();

	/**
	 * The class information object that maps to the class information page of a
	 * class or student web site.
	 */
	this.classInfo = JSON.stringify({
		className : '',
		classYear : '',
		courseId : '',
		teacherId : '',
		hasError : false,
		error : ''
	});
}

/**
 * Returns a cloned classInfo object.
 * 
 * @returns
 */
MemberService.prototype.cloneClassInfo = function() {
	return JSON.parse(this.classInfo);
}

/**
 * Finds a member by ID from the registered members and returns a cloned member.
 * 
 * @param memberId
 */
MemberService.prototype.getMember = function(memberId) {
	return this.repository.findMember(memberId);
}

/**
 * Finds a course by ID.
 * 
 * @param courseId
 */
MemberService.prototype.getCourse = function(courseId) {
	return this.repository.findCourse(courseId);
}

/**
 * Finds all the active classes.
 * 
 * @returns the list of the active classMember objects
 */
MemberService.prototype.getActiveClasses = function() {
	return this.repository.findActiveClasses();
}

/**
 * Gets the basic information of the active class from the CLASSES table by a
 * memberId.
 * 
 * @param memberId
 * @returns A JSON structure of the classInfo {className, classYear, courseId,
 *          teacherId, error, hasError}.
 */
MemberService.prototype.getClassInfoByMemberId = function(memberId) {

	var classInfo = this.cloneClassInfo();
	var classMember = this.getClassMember(memberId);
	if (classMember == null) {
		classInfo.hasError = true;
		classInfo.error = "No class for this member:" + memberId;
	} else {
		classInfo.className = classMember.className;
		classInfo.classYear = classMember.classYear;
		classInfo.teacherId = classMember.teacherId;
		classInfo.courseId = classMember.courseId;
		classInfo.hasError = false;
	}
	return classInfo;
}

/**
 * Gets the basic information of the active class from the pre-defined
 * CLASS_MEMBERS array by a class name.
 * 
 * @returns A JSON structure of the classInfo {className, classYear, courseId,
 *          teacherId, error, hasError}.
 */
MemberService.prototype.getClassInfoByName = function(className) {

	var classInfo = this.cloneClassInfo();
	var classMember = this.getMembersByClassName(className);
	if (classMember == null) {
		classInfo.hasError = true;
		classInfo.error = "No this class :" + className;
	} else {
		classInfo.className = classMember.className;
		classInfo.classYear = classMember.classYear;
		classInfo.teacherId = classMember.teacherId;
		classInfo.courseId = classMember.courseId;
		classInfo.hasError = false;
	}
	return classInfo;
}

/**
 * Gets the teacher info of a class member (e.g. student).
 * 
 * @returns
 */
MemberService.prototype.getTeacherInfo = function(memberId) {
	var classInfo = this.getClassInfoByMemberId(memberId);
	return this.getMember(classInfo.teacherId);
}

/**
 * Finds the active class by the memberId assuming the member and the active
 * class has a 1-1 relationship.
 * 
 * @param memberId
 * @returns the active classMember that contains the supplied memberId
 */
MemberService.prototype.getClassMember = function(memberId) {
	var classMembers = this.repository.findActiveClasses();
	for (var i = 0; i < classMembers.length; i++) {
		var classMember = classMembers[i];
		var memberIds = classMember.memberIds.split(',');
		for (var j = 0; j < memberIds.length; j++) {
			if (memberId == memberIds[j].trim()) {
				return classMember;
			}
		}
	}
	return null;
}

/**
 * Finds the active class-course-members relationships.
 * 
 * @returns the list of classCourseMembers objects
 */
MemberService.prototype.getActiveClassCourseMembers = function() {

	// find the active classes and all the course and members, for better
	// performance, find all instead of finding individual course and
	// member in the loop to reduce the number of table queries.
	var activeClasses = this.repository.findActiveClasses();
	var classMembers = this.repository.findActiveMembers();
	var courses = this.repository.findActiveCourses();

	var classCourseMembersList = [];
	for (var i = 0; i < activeClasses.length; i++) {
		var clazz = activeClasses[i];

		// lookup the course
		var course = null;
		for (var j = 0; j < courses.length; j++) {
			course = courses[j];
			if (course.id == clazz.courseId) {
				break;
			}
		}

		// lookup the members
		var members = [];
		var ids = clazz.memberIds.split(',');
		for (var j = 0; j < ids.length; j++) {
			var memberId = ids[j].trim();
			var member = null;
			for (k = 0; k < classMembers.length; k++) {
				member = classMembers[k];
				if (memberId == member.id) {
					members.push(member);
				}
			}
		}

		// build a relationship
		var classCourseMembers = {
			'clazz' : clazz,
			'course' : course,
			'members' : members,
		}
		classCourseMembersList.push(classCourseMembers);
	}

	var relationships = {
		'isOwner' : false,
		'courseSite' : this.constants.COURSE_SITE,
		'classCourseMembersList' : classCourseMembersList
	}
	return relationships;
}

/**
 * Finds the active class by the class name.
 * 
 * @param className
 *            the case insensitive class name
 * @returns the active classMember object
 */
MemberService.prototype.getMembersByClassName = function(className) {
	className = className.toUpperCase();
	var classMembers = this.repository.findActiveClasses();
	for (var i = 0; i < classMembers.length; i++) {
		var classMember = classMembers[i];
		if (classMember.className == className) {
			return classMember;
		}
	}
	return null;
}

/**
 * Gets a set of members of this class by looking up the members of the class
 * using the ClassInfo and return an array of the class members
 * 
 * @param className
 *            the case insensitive class name
 * @returns The classInfo object
 */
MemberService.prototype.getClassMemberSet = function(className) {

	var classMemberSet = [];
	var classMember = this.getMembersByClassName(className);
	if (classMember == null) {
		return classMemberSet;
	}

	var members = this.repository.findActiveMembers();
	var memberIds = classMember.memberIds.split(',');
	for (var j = 0; j < memberIds.length; j++) {
		for (var k = 0; k < members.length; k++) {
			var member = members[k];
			if (member.id == memberIds[j].trim()) {
				classMemberSet.push(member);
			}
		}
	}
	return classMemberSet;
}

/**
 * Gets a set of students of this class by looking up the members of the class
 * using the ClassInfo and return an array of the class members
 * 
 * @param className
 *            the case insensitive class name
 * @returns The classInfo object
 */
MemberService.prototype.getClassStudentSet = function(className) {

	var classMemberSet = [];
	var classMember = this.getMembersByClassName(className);

	if (classMember != null) {
		var memberIds = classMember.memberIds.split(',');
		for (var j = 0; j < memberIds.length; j++) {
			var member = this.getMember(memberIds[j].trim());
			if (member != null && member.role == MEMBER_ROLE.STUDENT) {
				classMemberSet.push(member);
			}
		}
	}
	return classMemberSet;
}
