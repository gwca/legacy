/**
 * The test cases for testing DataRepository.
 */
function testInsertHomework() {
	var testData = TestHomework2;
	var repository = getDataRepository();
	var result = repository.createStudentHomework(testData);
	Logger.log('submitted homework:' + JSON.stringify(result));

	var result = repository.createAssignedHomework(testData);
	Logger.log('assigned homework:' + JSON.stringify(result));
}

function testUpdateHomework() {
	var testData = TestHomework1;
	var repository = getDataRepository();
	testData.workTitle = "Changed Title";
	testData.status = "submitted";
	
	var result = repository.updateStudentHomework(testData);
	Logger.log('submitted homework:' + JSON.stringify(result));

	result = repository.updateAssignedHomework(testData);
	Logger.log('assigned homework:' + JSON.stringify(result));
}

function testRemoveHomework() {
	var repository = getDataRepository();
	var clause = "studentId = 'tester' AND courseId = 'npcr5' AND workNumber = '99_2'";
	var rowid = repository.remove(repository.HOMEWORK, clause);
	Logger.log('removed submitted homework rowid:' + rowid);

	clause = "courseId = 'npcr5' AND workNumber = '99_2'";
	rowid = repository.remove(repository.HOMEWORK_LIB, clause);
	Logger.log('removed assigned homework rowid:' + rowid);
}

function testFindHomeworkList() {
	var repository = getDataRepository();
	var memberId = 'john-doe';
	var list = repository.findHomeworkMetaByStudentId(memberId);
	Logger.log('list:' + JSON.stringify(list, '', 2));
}

function testFindHomeworkListWithStatus() {
	var service = getSiteService();
	var memberId = 'john-doe';
	var pageUrl = 'https://sites.google.com/a/greatwallchineseacademy.org/course-work/student-directory/john-doe/homework-list';
	var listPage = SitesApp.getPageByUrl(pageUrl);
	var list = service.getHomeworkListWithStatus(memberId, listPage);
	Logger.log('list:' + JSON.stringify(list, '', 2));
}

function testCourseInfoList() {
	var repository = getDataRepository();
	var list = repository.findCourseInfoList();
	Logger.log('course list:' + JSON.stringify(list, '', 2));
}

function testFindMember() {
	var repository = getDataRepository();
	var member = repository.findMember('john-doe');
	Logger.log('member:' + JSON.stringify(member, '', 2));

	member = repository.findMember('no-body');
	if (member == null) {
		Logger.log('cannot find member no-body');
	}
}

function testFindCourse() {
	var repository = getDataRepository();
	var course = repository.findCourse('npcr5');
	Logger.log('course:' + JSON.stringify(course, '', 2));
}

function testFindActiveClasses() {
	var repository = getDataRepository();
	var classes = repository.findActiveClasses();
	Logger.log('course:' + JSON.stringify(classes, '', 2));
}

function testFindStudentHomework() {
	var repository = getDataRepository();
	var homework = repository.findStudentHomework('john-doe', 'npcr5', 'npcr5 1');
	Logger.log(JSON.stringify(homework, '', 2));
}

function testFindAssignedHomeworkByCourse() {
	var repository = getDataRepository();
	var homework = repository.findAssignedHomework('npcr5', 'npcr5 1');
	Logger.log(JSON.stringify(homework, '', 2));

	var list = repository.findAssignedHomeworkByCourse('npcr5');
	Logger.log(JSON.stringify(list, '', 2));
}

/**
 * This is not a test. It is for assigning the old homework to update
 * the homework table. One time stand. 
 */
//function updateHomework() {
//	var repository = getDataRepository();
//	var classes = repository.findActiveClasses();
//	var classMember = classes[0];
//	var memberIds = classMember.memberIds.split(',');
//	var homeworkList = repository.findAssignedHomeworkByCourse('npcr5');
//	for (var i = 0; i < homeworkList.length; i++) {
//		if (i == 3 || i == 6 || i == 9 || i == 12 || i == 16) {
//			Utilities.sleep(5000);
//		}
//		var homework = homeworkList[i];
//		var workNumber = homework.workNumber;
//		if (homework.createdDate != null && homework.createdDate.length > 0) {
//			for (j = 0; j < memberIds.length; j++) {
//				var memberId = memberIds[j].trim();
//				var studentHomework = repository.findStudentHomework(memberId,
//						'npcr5', workNumber);
//				if (studentHomework == null) {
//					homework.studentId = memberId;
//					var info = repository.createStudentHomework(homework);
//					if (info.hasError) {
//						Logger.log(info.message);
//					}
//				} else {
//					studentHomework.createdDate = homework.createdDate;
//					var info = repository.updateStudentHomework(studentHomework);
//					var info = repository.createStudentHomework(homework);
//					if (info.hasError) {
//						Logger.log(info.message);
//					}
//				}
//			}
//			Logger.log('homework:' + homework.workTitle + ' is processed');
//		}
//	}
//}

/**
 * The test cases for testing DriveRepository.
 */
function testGetTextContent() {
	var repository = new DriveRepository();
	var content = repository.getCourseTextContent('textbook', null);
	Logger.log('textbook.html:' + content);

	content = repository.getCourseTextContent('npcr5', 'NPCR5-51.html');
	Logger.log('NPCR5-51.html:' + content);
}

function testCreateDocument() {
	var repository = new DriveRepository();
	var studentId = 'john-doe';
	var fileName = 'TestNewDoc';
	var instruction = 'This is a testing content of the document.';
	var info = repository
			.createDocumentByName(studentId, fileName, instruction);
	Logger.log('document id:' + info);
}

function getSiteService() {
	return new WorkSiteService();
}

function getDataRepository() {
	return new DataRepository();
}
