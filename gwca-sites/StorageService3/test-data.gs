/**
 * The test cases for testing DataRepository.
 */
var TestReview = {
	"studentId" : "",
	"courseId" : "npcr1",
	"workNumber" : "save_test",
	"workTitle" : "test title",
	"workitems" : [
			{
				"name" : "note",
				"title" : "复习 Review",
				"index" : "0",
				"instructions" : [
						"Let's slow down a bit. We'll use this week to review what we have learned (lessons 1-4) and do an assessment.",
						"The assessment is due by Friday (1/20)" ],
				"url" : "",
				"text" : "",
				"checked" : null,
				"remark" : ""
			},
			{
				"name" : "audio",
				"title" : "听力练习",
				"index" : "10",
				"instructions" : [ "Listen to the recording carefully and do the following exercises: answer questions and multiple choices." ],
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/audio-clips/npcr1/test-1.mp3?attredirects=0&d=1",
				"text" : "听力练习：马大为到哪去了",
				"checked" : null,
				"remark" : ""
			},
			{
				"name" : "answer-question",
				"title" : "回答问题",
				"index" : "11",
				"questions" : [ {
					"question" : "力波请林娜喝什么？",
					"answer" : "",
					"checked" : null,
					"remark" : ""
				}, {
					"question" : "林娜说她要喝什么？",
					"answer" : "",
					"checked" : null,
					"remark" : ""
				}, {
					"question" : "马大为上哪去了？",
					"answer" : "",
					"checked" : null,
					"remark" : ""
				} ]
			},
			{
				"name" : "multi-choice",
				"title" : "选择正确的答案",
				"index" : "12",
				"statements" : [ {
					"question" : "马大为住在 ____ 号。",
					"choices" : [ "三零二号", "二零三号", "三二零号" ],
					"answer" : -1,
					"checked" : null,
					"remark" : ""
				}, {
					"question" : "林娜找过马大为吗？",
					"choices" : [ "找过", "没找过", "不知道" ],
					"answer" : -1,
					"checked" : null,
					"remark" : ""
				}, {
					"question" : "林娜要喝 ____ .",
					"choices" : [ "咖啡", "水", "葡萄酒" ],
					"answer" : -1,
					"checked" : null,
					"remark" : ""
				} ]
			},
			{
				"name" : "phrase-sentence",
				"title" : "组词 create words",
				"index" : "20",
				"words" : [ {
					"word" : "喝",
					"answer" : "",
					"checked" : null,
					"remark" : ""
				}, {
					"word" : "知",
					"answer" : "",
					"checked" : null,
					"remark" : ""
				}, {
					"word" : "很",
					"answer" : "",
					"checked" : null,
					"remark" : ""
				} ]
			},
			{
				"name" : "phrase-sentence",
				"title" : "造句 make setences",
				"index" : "21",
				"words" : [ {
					"word" : "可以",
					"answer" : "",
					"checked" : null,
					"remark" : ""
				}, {
					"word" : "可能",
					"answer" : "",
					"checked" : null,
					"remark" : ""
				}, {
					"word" : "商店",
					"answer" : "",
					"checked" : null,
					"remark" : ""
				} ]
			},
			{
				"name" : "record",
				"title" : "朗读录音",
				"index" : "30",
				"instruction" : "<p>Read the next paragraph and record it</p>\n<p>林娜是个从英国来的留学生，她在中国学医。为了能当一个好医生，她还在语言学院学习中文。在语言学院她认识了几个从不同国家来的留学生。他们中间有从美国来的马大为；从加拿大来的丁力波。她有一个医生朋友叫陆雨平，她还有一个同学叫王小云，也是她的好朋友。\n</p>",
				"checked" : null,
				"remark" : ""
			} ],
	"remark" : "",
	"grade" : "",
	"status" : "assigned",
	"checkTime" : "",
	"hasError" : false,
	"message" : ""
}

var TestHomework1 = {
	"studentId" : "john-doe",
	"courseId" : "npcr1",
	"workNumber" : "test1",
	"workTitle" : "testing updated",
	"workitems" : [ {
		"name" : "read",
		"title" : "阅读与'理\"解",
		"index" : "0",
		"instruction" : "'this'" + "\n" + "is" + "\n" + "an" + "\n" + "essay",
		"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
		"text" : "让我迷恋的北京城",
		"checked" : null,
		"remark" : "",
		"tested" : true
	} ],
	"remark" : "很好",
	"grade" : "",
	"status" : "done",
	"checkTime" : "6/15/2016",
	"hasError" : false,
	"message" : ""
}

var TestHomework2 = {
	"studentId" : "john-doe",
	"courseId" : "npcr5",
	"workNumber" : "npcr5 1",
	"workTitle" : "npcr5 作业-1",
	"workitems" : [
			{
				"name" : "read",
				"title" : "阅读与理解",
				"index" : "0",
				"instruction" : "阅读理解课文的后半部分，（从“北京人的生活水平”到最后）",
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
				"text" : "让我迷恋的北京城",
				"checked" : null,
				"remark" : "",
				"tested" : true
			},
			{
				"name" : "read",
				"title" : "阅读与理解",
				"index" : "1",
				"instruction" : "阅读理解课文的后半部分，（从“北京人的生活水平”到最后）",
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
				"text" : "让我迷恋的北京城",
				"checked" : null,
				"remark" : "",
				"tested" : true
			},
			{
				"name" : "read",
				"title" : "阅读与理解",
				"index" : "2",
				"instruction" : "阅读理解课文的后半部分，（从“北京人的生活水平”到最后）",
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
				"text" : "让我迷恋的北京城",
				"checked" : null,
				"remark" : "",
				"tested" : true
			},
			{
				"name" : "read",
				"title" : "阅读与理解",
				"index" : "3",
				"instruction" : "阅读理解课文的后半部分，（从“北京人的生活水平”到最后）",
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
				"text" : "让我迷恋的北京城",
				"checked" : null,
				"remark" : "",
				"tested" : true
			},
			{
				"name" : "read",
				"title" : "阅读与理解",
				"index" : "4",
				"instruction" : "阅读理解课文的后半部分，（从“北京人的生活水平”到最后）",
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
				"text" : "让我迷恋的北京城",
				"checked" : null,
				"remark" : "",
				"tested" : true
			},
			{
				"name" : "read",
				"title" : "阅读与理解",
				"index" : "5",
				"instruction" : "阅读理解课文的后半部分，（从“北京人的生活水平”到最后）",
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
				"text" : "让我迷恋的北京城",
				"checked" : null,
				"remark" : "",
				"tested" : true
			},
			{
				"name" : "read",
				"title" : "阅读与理解",
				"index" : "6",
				"instruction" : "阅读理解课文的后半部分，（从“北京人的生活水平”到最后）",
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
				"text" : "让我迷恋的北京城",
				"checked" : null,
				"remark" : "",
				"tested" : true
			},
			{
				"name" : "read",
				"title" : "阅读与理解",
				"index" : "7",
				"instruction" : "阅读理解课文的后半部分，（从“北京人的生活水平”到最后）",
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
				"text" : "让我迷恋的北京城",
				"checked" : null,
				"remark" : "",
				"tested" : true
			},
			{
				"name" : "read",
				"title" : "阅读与理解",
				"index" : "8",
				"instruction" : "阅读理解课文的后半部分，（从“北京人的生活水平”到最后）",
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
				"text" : "让我迷恋的北京城",
				"checked" : null,
				"remark" : "",
				"tested" : true
			},
			{
				"name" : "read",
				"title" : "阅读与理解",
				"index" : "9",
				"instruction" : "阅读理解课文的后半部分，（从“北京人的生活水平”到最后）",
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
				"text" : "让我迷恋的北京城",
				"checked" : null,
				"remark" : "",
				"tested" : true
			},
			{
				"name" : "read",
				"title" : "阅读与理解",
				"index" : "0",
				"instruction" : "阅读理解课文的后半部分，（从“北京人的生活水平”到最后）",
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
				"text" : "让我迷恋的北京城",
				"checked" : null,
				"remark" : "",
				"tested" : true
			},
			{
				"name" : "read",
				"title" : "阅读与理解",
				"index" : "1",
				"instruction" : "阅读理解课文的后半部分，（从“北京人的生活水平”到最后）",
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
				"text" : "让我迷恋的北京城",
				"checked" : null,
				"remark" : "",
				"tested" : true
			},
			{
				"name" : "read",
				"title" : "阅读与理解",
				"index" : "2",
				"instruction" : "阅读理解课文的后半部分，（从“北京人的生活水平”到最后）",
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
				"text" : "让我迷恋的北京城",
				"checked" : null,
				"remark" : "",
				"tested" : true
			},
			{
				"name" : "read",
				"title" : "阅读与理解",
				"index" : "3",
				"instruction" : "阅读理解课文的后半部分，（从“北京人的生活水平”到最后）",
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
				"text" : "让我迷恋的北京城",
				"checked" : null,
				"remark" : "",
				"tested" : true
			},
			{
				"name" : "read",
				"title" : "阅读与理解",
				"index" : "4",
				"instruction" : "阅读理解课文的后半部分，（从“北京人的生活水平”到最后）",
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
				"text" : "让我迷恋的北京城",
				"checked" : null,
				"remark" : "",
				"tested" : true
			},
			{
				"name" : "read",
				"title" : "阅读与理解",
				"index" : "5",
				"instruction" : "阅读理解课文的后半部分，（从“北京人的生活水平”到最后）",
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
				"text" : "让我迷恋的北京城",
				"checked" : null,
				"remark" : "",
				"tested" : true
			},
			{
				"name" : "read",
				"title" : "阅读与理解",
				"index" : "6",
				"instruction" : "阅读理解课文的后半部分，（从“北京人的生活水平”到最后）",
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
				"text" : "让我迷恋的北京城",
				"checked" : null,
				"remark" : "",
				"tested" : true
			},
			{
				"name" : "read",
				"title" : "阅读与理解",
				"index" : "7",
				"instruction" : "阅读理解课文的后半部分，（从“北京人的生活水平”到最后）",
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
				"text" : "让我迷恋的北京城",
				"checked" : null,
				"remark" : "",
				"tested" : true
			},
			{
				"name" : "read",
				"title" : "阅读与理解",
				"index" : "8",
				"instruction" : "阅读理解课文的后半部分，（从“北京人的生活水平”到最后）",
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
				"text" : "让我迷恋的北京城",
				"checked" : null,
				"remark" : "",
				"tested" : true
			},
			{
				"name" : "read",
				"title" : "阅读与理解",
				"index" : "9",
				"instruction" : "阅读理解课文的后半部分，（从“北京人的生活水平”到最后）",
				"url" : "https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/?course=npcr5&title=NPCR5-54",
				"text" : "让我迷恋的北京城",
				"checked" : null,
				"remark" : "",
				"tested" : true
			},
			{
				"name" : "fill-blank",
				"title" : "根据事件的先后顺序，用恰当的关联词语完成对话",
				"index" : "50",
				"selections" : "",
				"statements" : [ {
					"tokens" : [ {
						"hasBlank" : false,
						"phrase" : "问：在颐和园，他们游览了哪些地方？ 答：他们",
						"answer" : ""
					}, {
						"hasBlank" : true,
						"phrase" : "游览了长廊，",
						"answer" : "先"
					}, {
						"hasBlank" : true,
						"phrase" : "乘船游览了昆明湖。",
						"answer" : "然后"
					} ],
					"checked" : false,
					"remark" : ""
				}, {
					"tokens" : [ {
						"hasBlank" : false,
						"phrase" : "问：我现在就给你们讲一讲事情的经过，怎么样？答：别着急，你",
						"answer" : ""
					}, {
						"hasBlank" : true,
						"phrase" : "喝点水，",
						"answer" : "先"
					}, {
						"hasBlank" : true,
						"phrase" : "慢漫告诉我们事隋的经过。",
						"answer" : "然后"
					} ],
					"checked" : false,
					"remark" : "还可以用“先”，“再”"
				}, {
					"tokens" : [ {
						"hasBlank" : false,
						"phrase" : "问：他是怎样上车的？答：他走到车前，",
						"answer" : ""
					}, {
						"hasBlank" : true,
						"phrase" : "向四周看了看，",
						"answer" : "先"
					}, {
						"hasBlank" : true,
						"phrase" : "拉开车门坐进去。",
						"answer" : "然后"
					} ],
					"checked" : false,
					"remark" : ""
				}, {
					"tokens" : [ {
						"hasBlank" : false,
						"phrase" : "问：她出机场后，等到出租车了吗？答：在北京首都机场出租车很多，她",
						"answer" : ""
					}, {
						"hasBlank" : true,
						"phrase" : "走出大门",
						"answer" : "先"
					}, {
						"hasBlank" : true,
						"phrase" : "开来一辆空车。",
						"answer" : "然后"
					} ],
					"checked" : false,
					"remark" : "还可以用“一”， “就”"
				} ],
				"tested" : true
			},
			{
				"name" : "answer-question",
				"title" : "回答问题",
				"index" : "60",
				"questions" : [
						{
							"question" : "生活水平是什么意思？",
							"answer" : "生活水平的意思是人们能生活在一个时间和地点而满足他们的需要和想要的容易度。",
							"checked" : true,
							"remark" : "Is it from here? https://zh.wikipedia.org/wiki/生活水平"
						},
						{
							"question" : "为什么说“我真是有点儿眼红”？",
							"answer" : "因为他也想要个好房子和好生活。眼红的意思 Jealous，他有你也要。",
							"checked" : true,
							"remark" : ""
						},
						{
							"question" : "课文中两次用到了“透”这个字，“透”出了什么意思？用“透”这个字造句。",
							"answer" : "透的意思是能看得过去个东西。我的水瓶是透明的。",
							"checked" : true,
							"remark" : ""
						},
						{
							"question" : "什么是“小吃”？说一种你所熟悉的小吃。",
							"answer" : "小吃是指一类在口味上具有特定风格特色的食品的总称。我知道的小吃有汤圆 ，虾卷 ，红豆饼，太阳饼，凤梨酥，饼，等等...",
							"checked" : true,
							"remark" : ""
						},
						{
							"question" : "课文中说“老北京人就好这一口”，其中的“好”字怎么念，是什么意思？",
							"answer" : "这里好的意思是就只知道或就只会。",
							"checked" : true,
							"remark" : ""
						},
						{
							"question" : "课文中提到了北京人业余生活中的几种活动，都是什么？作者是在什么地方看到的？",
							"answer" : "跳交际舞和下棋。他是刚从地铁站走出来，就看见路边儿聚集着许多人，其中还有不少老年人在交际舞。还有，在立交桥的下面也聚集着一堆一堆的人，有的蹲着，有的坐着，都瞪着眼看着地下。走近一看，原来他们是在下棋呢。",
							"checked" : true,
							"remark" : ""
						} ],
				"tested" : true
			} ],
	"remark" : "很好",
	"grade" : "",
	"status" : "done",
	"checkTime" : "6/15/2016",
	"hasError" : false,
	"message" : ""
};
