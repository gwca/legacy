var SCHOOL_ROOT_URL = 'https://sites.google.com/a/greatwallchineseacademy.org/gwca-info/';

/**
 * The test cases for testing the MemberService
 */
function testFindRelationships() {
	var service = new MemberService();
	var members = service.getActiveClassCourseMembers();
	if (members != null) {
		Logger.log(JSON.stringify(members, '', 2));
	}
}

function testGetMemberSet() {
	var service = new MemberService();
	var className = 'C12';
	var info = service.getClassMemberSet(className);
	Logger.log(JSON.stringify(info, '', 2));
}

function testGetClassInfoByMemberId() {
	var service = new MemberService();
	var memberId = 'john-doe';
	var member = service.getClassInfoByMemberId(memberId);
	Logger.log(JSON.stringify(member, '', 2));
}

function testGetClassInfoByName() {
	var service = new MemberService();
	var className = 'workshop';
	var clazz = service.getClassInfoByName(className);
	Logger.log(JSON.stringify(clazz, '', 2));
}

function testGetTeacherInfo() {
	var service = new MemberService();
	var memberId = 'john-doe';
	var teacher = service.getTeacherInfo(memberId);
	Logger.log(JSON.stringify(teacher, '', 2));
}

function testGetClassMember() {
	var service = new MemberService();
	var memberId = 'john-doe';
	var members = service.getClassMember(memberId);
	Logger.log(JSON.stringify(members, '', 2));
}

/**
 * The test cases for testing the DataService
 */
function testGetTitles() {
	var service = getDataService();
	var courseId = 'npcr1';
	var titles = service.getHomeworkTitleList(courseId);
	Logger.log(JSON.stringify(titles, '', 2));
}

function testGetStudentHomework() {
	var service = getDataService();
	var courseId = 'npcr5';
	var studentId = 'john-doe';
	var workNumber = '51_1';
	var homework = service.getStudentHomework(studentId, courseId, workNumber);
	Logger.log(JSON.stringify(homework, '', 2));

	studentId = 'gina-kuo';
	workNumber = '51_1';
	homework = service.getStudentHomework(studentId, courseId, workNumber);
	Logger.log(JSON.stringify(homework, '', 2));
}

function testGetAssignedHomework() {
	var service = getDataService();
	var courseId = 'npcr5';
	var workNumber = '51_1';
	var homework = service.getAssignedHomework(courseId, workNumber);
	Logger.log(JSON.stringify(homework, '', 2));

	workNumber = 'non-exist';
	homework = service.getAssignedHomework(courseId, workNumber);
	Logger.log(JSON.stringify(homework, '', 2));
}

function testSaveStudentHomework() {
	var service = getDataService();
	var homework = service.saveStudentHomework(TestHomework);
	Logger.log(JSON.stringify(homework, '', 2));
}

function testFindAndUpdateStudentHomework() {
	var service = getDataService();
	var courseId = 'workshop';
	var studentId = 'jane-doe';
	var workNumber = 'testing';
	var homework = null;

	Logger.log("Sleep 1.5 second for every update.");
	for (var i = 0; i < 10; i++) {
		homework = service.getStudentHomework(studentId, courseId, workNumber);
		if (homework.hasError) {
			Logger.log("Find " + i + ":" + homework.message);
		} else {
			homework.status = 'test-' + i;
			homework.workTitle = 'changed-' + i;
			service.saveStudentHomework(homework);
			// Utilities.sleep(1500);
			Logger.log("Update " + i + ":" + homework.message);
		}
	}
	Logger.log(JSON.stringify(homework, '', 2));
}

function testUpdateStudentHomeworkLong() {
	var service = getDataService();
	TestHomework.workTitle = 'Changed Title';
	var workitems = JSON.stringify(TestHomework.workitems);
	workitems = JSON.parse(workitems);
	for (var i = 10; i < workitems.length; i++) {
		var workitem = workitems[i];
		TestHomework.workitems.push(workitem);
	}
	homework = service.saveStudentHomework(TestHomework);
	Logger.log(JSON.stringify(homework, '', 2));
}

function testUpdateStudentHomeworkShort() {
	var service = getDataService();
	var workitems = JSON.stringify(TestHomework.workitems);
	workitems = JSON.parse(workitems);
	TestHomework.workitems = [];
	TestHomework.workTitle = 'Changed Title';
	for (var i = 0; i < 2; i++) {
		var workitem = workitems[i];
		TestHomework.workitems.push(workitem);
	}
	homework = service.saveStudentHomework(TestHomework1);
	Logger.log(JSON.stringify(homework, '', 2));
}

function testSaveAssignedHomework() {
	var service = getDataService();
	var homework = service.saveAssignedHomework(TestReview);
	Logger.log(JSON.stringify(homework, '', 2));
}

function testUpdateHomework() {
	var service = getDataService();
	var homework = service.saveAssignedHomework(TestReview);
	Logger.log(JSON.stringify(homework, '', 2));
}

function getDataService() {
	return new DataService();
}

/**
 * The test cases for testing some utilities
 */
function testIsArray() {
	var a = [ "A", "AA", "AAA" ];
	if (Array.isArray(a)) {
		Logger.log('is an array:' + a.length);
	} else {
		Logger.log('is not an array:' + a);
	}
}
