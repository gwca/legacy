/**
 * Some commonly used Google Sites utilities.
 */
function WorkSiteService() {

	this.constants = new ServiceConstants();
	this.repository = new DataRepository();
	this.dataService = new DataService();
	this.serviceUtils = new ServiceUtils();
	this.memberService = new MemberService();

	/**
	 * Gets the student's course work home page and populates it with the info
	 * of the student.
	 * 
	 * @param member
	 * @returns pageInfo
	 */
	this.getStudentHomePage = function(member) {
		var memberId = member.memberId;
		var pageInfo = null;
		for (var i = 0; i < this.constants.COURSE_WORK_PAGES.length; i++) {
			pageInfo = this.constants.COURSE_WORK_PAGES[i];
			if (pageInfo.isHome) {
				pageInfo.name = member.id;
				pageInfo.title = member.firstName + ' ' + member.lastName;
				return pageInfo;
			}
		}
		return pageInfo;
	}

	/**
	 * Finds a pageInfo object by its template name.
	 * 
	 * @param template
	 * @returns pageInfo
	 */
	this.getPageInfoByTemplate = function(template) {
		var pageInfo = null;
		for (var i = 0; i < this.constants.COURSE_WORK_PAGES.length; i++) {
			pageInfo = this.constants.COURSE_WORK_PAGES[i];
			if (pageInfo.template == template) {
				return pageInfo;
			}
		}
		return pageInfo;
	}

	/**
	 * Reads the HTML source from the 'about-me' file that must be located in
	 * the current folder.
	 * 
	 * @param member
	 * @returns HTML source code of the file
	 */
	this.readHtmlSource = function(member) {
		try {
			var filename = 'about-me';
			var src = HtmlService.createTemplateFromFile(filename).evaluate();
			src = src.getContent();
			src = src.replace('FULL-NAME', member.firstName + ' '
					+ member.lastName);
			src = src.replace('CHINESE-NAME', member.chineseName);
			src = src.replace('EMAIL', member.email);
			return src;
		} catch (error) {
			return error;
		}
	}

	/**
	 * Gets the specified page template of the site.
	 * 
	 * @param site
	 * @param templateName
	 * @returns
	 */
	this.getPageTemplate = function(site, templateName) {
		var templates = site.getTemplates();
		var template = null;
		for (var i = 0; i < templates.length; i++) {
			template = templates[i];
			if (template.getName() == templateName) {
				return template;
			}
		}
		return null;
	}

	/**
	 * Gets the course work site.
	 */
	this.getCourseWorkSite = function() {
		var domain = this.constants.SCHOOL_DOMAIN;
		var siteName = this.constants.COURSE_WORK_NAME;
		var site = SitesApp.getSite(domain, siteName);
		return site;
	}

	/**
	 * Builds a homework page name
	 */
	this.createHomeworkPageName = function(courseId, workNumber) {
		return courseId + "-" + workNumber;
	}

	/**
	 * Gets the basic page information for assigning homework.
	 */
	this.getHomeworkPageInfo = function(memberId) {
		var info = null;
		var msg = null;

		// get the member info
		var member = this.memberService.getMember(memberId);
		if (member == null || member.role != MEMBER_ROLE.STUDENT) {
			msg = memberId + ': is not a student.';
			info = this.serviceUtils.buildReturnValue(ERROR_CODE.ERROR, msg);
			return info;
		}

		var site = this.getCourseWorkSite();
		var directory = site.getChildByName(this.constants.STUDENT_DIRECTORY);

		// get the homework list page
		var homePageInfo = this.getStudentHomePage(member);
		var listPage;
		try {
			var homePage = directory.getChildByName(homePageInfo.name);
			listPage = homePage
					.getChildByName(this.constants.HOMEWORK_PAGE.listPage);
		} catch (error) {
			msg = memberId + ': the homework list page does not exist.';
			info = this.serviceUtils.buildReturnValue(ERROR_CODE.ERROR, msg);
			return info;
		}

		// get the homework template
		var template = this.getPageTemplate(site,
				this.constants.HOMEWORK_PAGE.template);
		if (template == null) {
			msg = memberId + ': the homework template does not exist.';
			info = this.serviceUtils.buildReturnValue(ERROR_CODE.ERROR, msg);
			return info;
		}

		info = {
			code : ERROR_CODE.INFO,
			listPage : listPage,
			template : template
		}
		return info;
	}

	/**
	 * Checks if the homework has already been assigned.
	 */
	this.isAssigned = function(listPage, name) {
		var homework = listPage.getChildByName(name);
		return (homework != null);
	}

	/**
	 * Sets the sharing permission of the page to different users (student,
	 * teacher, administrator and parents). Google says setting page level
	 * permission is not supported by Apps Scripts. We have to wait for their
	 * implementation.
	 * 
	 * @page
	 * @email
	 * @role
	 */
	this.setPagePermission = function(page, email, role) {

		if (email == null || email.length == 0) {
			return;
		}

		var users = null;
		switch (role) {
		case SITE_ROLE.OWNER:
			users = page.getOwners();
			break;
		case SITE_ROLE.EDITOR:
			users = page.getEditors();
			break;
		case SITE_ROLE.VIEWER:
			users = page.getViewers();
			break;
		}

		if (users == null || users.length == 0) {
			this.addPermittedUser(page, email, role);
			return;
		}

		for (var i = 0; i < users.length; i++) {
			if (email == users[i].getEmail()) {
				return;
			}
		}
		this.addPermittedUser(page, email, role);
	}

	/**
	 * Adds a user with a desired permission.
	 */
	this.addPermittedUser = function(page, email, role) {
		switch (role) {
		case SITE_ROLE.OWNER:
			page.addOwner(email);
			return;
		case SITE_ROLE.EDITOR:
			page.addEditor(email);
			return;
		case SITE_ROLE.VIEWER:
			page.addViewer(email);
			return;
		}
	}

	/**
	 * Updates page with the HTML content of a template.
	 * 
	 * @param page
	 * @param content
	 * @returns status
	 */
	this.updatePage = function(page, content) {
		var msg = null;
		var status = null;
		var name = page.getParent().getName() + '/' + page.getName();
		try {
			page.setHtmlContent(content);
			msg = name + ' is updated.';
			status = this.serviceUtils.buildReturnValue(ERROR_CODE.INFO, msg);
		} catch (error) {
			msg = 'Updating ' + name + ' failed (' + error + ').';
			status = this.serviceUtils.buildReturnValue(ERROR_CODE.ERROR, msg);
		}
		return status;
	}

	/**
	 * Creates a page for the new homework assigned to a student.
	 * 
	 * @param memberId
	 * @param courseId
	 * @param workNumber
	 * @param title
	 *            the homework title
	 * @returns
	 */
	this.createHomeworkPage = function(memberId, courseId, workNumber, title) {

		// get the page information needed for assigning the homework.
		var info = this.getHomeworkPageInfo(memberId);
		if (info.code != ERROR_CODE.INFO) {
			return info;
		}

		var msg = null;
		var listPage = info.listPage;
		var template = info.template;
		var pageName = this.createHomeworkPageName(courseId, workNumber);
		try {
			// if the homework is already assigned, update it, otherwise
			// create a new homework using the template.
			var homeworkPage = listPage.getChildByName(pageName);
			if (homeworkPage == null) {
				msg = memberId + ': homework is assigned';
			} else {
				homeworkPage.deletePage();
				msg = memberId + ': homework is re-assigned';
			}
			listPage.createPageFromTemplate(title, pageName, template);
		} catch (error) {
			msg = memberId + ': assigning homework failed (' + error + ')';
			info = this.serviceUtils.buildReturnValue(ERROR_CODE.ERROR, msg);
			return info;
		}
		info = this.serviceUtils.buildReturnValue(ERROR_CODE.INFO, msg);
		return info;
	}

	this.getWorkInfo = function(studentId, pageName) {
		var info = pageName.split('-');
		var workInfo = {
			studentId : studentId,
			courseId : info[0],
			workNumber : info[1]
		}
		return workInfo;
	}

	this.getWorkStatus = function(workInfo, metaList) {
		var metaInfo = null;
		for (var i = 0; i < metaList.length; i++) {
			metaInfo = metaList[i];
			if (metaInfo.studentId == workInfo.studentId
					&& metaInfo.courseId == workInfo.courseId
					&& metaInfo.workNumber == workInfo.workNumber) {
				return metaInfo.status;
			}
		}
		return 'unknown';
	}
}

/**
 * Assigns the specified homework to all the students.
 * 
 * @param members
 *            a list of class members (comma delimited string)
 * @param courseId
 * @param workNumber
 * @param title
 */
WorkSiteService.prototype.assignAll = function(members, courseId, workNumber,
		title) {
	var msg = null;
	var info = null;
	var infoList = [];
	try {
		// get the assigned homework from the library
		var assignedHomework = this.dataService.getAssignedHomework(courseId,
				workNumber);
		if (assignedHomework.hasError) {
			msg = assignedHomework.message;
			info = this.serviceUtils.buildReturnValue(ERROR_CODE.ERROR, msg);
			return info;
		}

		// create a page for each assignment
		var ids = members.split(',');
		var studentIds = [];
		for (var i = 0; i < ids.length; i++) {
			var memberId = ids[i].trim();
			info = this.createHomeworkPage(memberId, courseId, workNumber,
					title);
			infoList.push(info);
			if (info.code != ERROR_CODE.ERROR) {
				studentIds.push(memberId);
			}
		}

		// assign it to the students only
		if (studentIds.length > 0) {
			info = this.dataService.saveMultiStudentHomework(studentIds,
					assignedHomework);
			for (var i = 0; i < info.length; i++) {
				if (info[i].hasError) {
					infoList.push(info[i]);
				}
			}
		}
	} catch (error) {
		msg = 'Assigning homework failed (' + error + ')';
		info = this.serviceUtils.buildReturnValue(ERROR_CODE.ERROR, msg);
		infoList.push(info);
	}
	return infoList;
}

/**
 * Notifies the specified member by email.
 * 
 * @param className
 * @param courseId
 * @param workNumber
 */
WorkSiteService.prototype.notify = function(className, courseId, workNumber) {

	var info = [];
	var members = this.memberService.getClassMemberSet(className);
	var today = Utilities.formatDate(new Date(), "MST", "MM/dd/yy");
	var subject = "The latest homework assignment of " + className + " ("
			+ today + ")";

	// email the members
	for (var i = 0; i < members.length; i++) {
		var member = members[i];
		var name = member.firstName + " " + member.lastName;
		var to = member.email;
		var role = member.role;
		var pageName = this.createHomeworkPageName(courseId, workNumber);
		var body = null;
		if (role == MEMBER_ROLE.STUDENT) {
			body = this.serviceUtils.buildStudentMsgBody(member, pageName);
		} else {
			body = this.serviceUtils.buildAdminMsgBody(className, members,
					pageName);
		}

		try {
			this.serviceUtils.sendSimpleMail(to, subject, body);
			var msg = member.id + ': notified';
			var status = this.serviceUtils.buildReturnValue(ERROR_CODE.INFO,
					msg);
			info.push(status);
		} catch (error) {
			var msg = member.id + ': failed to notify (' + error + ')';
			var status = this.serviceUtils.buildReturnValue(ERROR_CODE.INFO,
					msg);
			info.push(status);
		}
	}
	return info;
}

/**
 * Checks the homework status of all the students of the class.
 * 
 * @param className
 * @param fileName
 */
WorkSiteService.prototype.status = function(className, courseId, workNumber) {

	var info = [];
	var members = this.memberService.getClassMemberSet(className);

	// check the homework status of each student
	for (var i = 0; i < members.length; i++) {
		var member = members[i];
		if (member.role == MEMBER_ROLE.STUDENT) {
			// load from the student homework folder
			var homework = this.dataService.getStudentHomework(member.id,
					courseId, workNumber);
			if (homework.hasError) {
				var msg = member.id + ': not assigned';
				var status = this.serviceUtils.buildReturnValue(
						ERROR_CODE.ERROR, msg);
				info.push(status);
			} else {
				var pageName = this
						.createHomeworkPageName(courseId, workNumber);
				var url = this.serviceUtils.buildPageUrl(member.id, pageName);
				var link = '<a target="_blank" href="' + url + '">'
						+ homework.status + '</a>';
				var msg = member.id + ': ' + link;
				var status = this.serviceUtils.buildReturnValue(
						ERROR_CODE.INFO, msg);
				info.push(status);
			}
		}
	}
	return info;
}

/**
 * Creates the course work pages for the students.
 * 
 * @param memberIDs
 */
WorkSiteService.prototype.createCourseWorkPages = function(memberIDs) {
	var info = [];
	var status = null;
	var IDs = memberIDs.split(',');
	for (var i = 0; i < IDs.length; i++) {
		var memberId = IDs[i].trim();
		status = this.createCourseWorkPage(memberId);
		info.push(status);
	}
	return info;
}

/**
 * Creates a student course work page with all the child pages.
 */
WorkSiteService.prototype.createCourseWorkPage = function(memberId) {
	var info = null;
	var msg = null;
	var member = this.memberService.getMember(memberId);
	if (member == null || member.role != MEMBER_ROLE.STUDENT) {
		msg = memberId + ': is not a student.';
		info = this.serviceUtils.buildReturnValue(ERROR_CODE.ERROR, msg);
		return info;
	}

	var site = this.getCourseWorkSite();
	var students = site.getChildByName(this.constants.STUDENT_DIRECTORY);

	// create the course work home page for a student
	var pageInfo = this.getStudentHomePage(member);
	var homePage = students.getChildByName(pageInfo.name);
	if (homePage != null) {
		msg = memberId + ': the home page exists.';
		info = this.serviceUtils.buildReturnValue(ERROR_CODE.ERROR, msg);
		return info;
	}
	var template = this.getPageTemplate(site, pageInfo.template);
	if (template == null) {
		msg = memberId + ': the template ' + pageInfo.template
				+ ' does not exist.';
		info = this.serviceUtils.buildReturnValue(ERROR_CODE.ERROR, msg);
		return info;
	}

	try {
		var title = pageInfo.title;
		var name = pageInfo.name;
		homePage = students.createPageFromTemplate(title, name, template);
	} catch (error) {
		msg = "Creating hmoe page " + pageInfo.name + " failed:" + error;
		info = this.serviceUtils.buildReturnValue(ERROR_CODE.ERROR, msg);
		return info;
	}

	// create the child pages of the student's course work home page
	for (var i = 0; i < this.constants.COURSE_WORK_PAGES.length; i++) {
		pageInfo = this.constants.COURSE_WORK_PAGES[i];
		if (pageInfo.isHome == false) {
			try {
				var name = pageInfo.name;
				var title = pageInfo.title;
				if (pageInfo.name == 'about-me') {
					title = 'About ' + member.firstName + ' ' + member.lastName;
					homePage.createWebPage(title, name, this
							.readHtmlSource(member));
				} else {
					template = this.getPageTemplate(site, pageInfo.template);
					homePage.createPageFromTemplate(title, name, template);
				}
			} catch (error) {
				msg = "Creating page " + pageInfo.name + " for " + memberId
						+ " failed:" + error;
				info = this.serviceUtils
						.buildReturnValue(ERROR_CODE.ERROR, msg);
				return info;
			}
		}
	}

	// set the page permission (Google says setting page level permission is not
	// supported by Apps Scripts. We have to wait for their implementation).
	try {
		this.setPagePermission(homePage, member.email, SITE_ROLE.EDITOR);
		msg = memberId + ": the home page gets created.";
		info = this.serviceUtils.buildReturnValue(ERROR_CODE.INFO, msg);
	} catch (error) {
		msg = memberId
				+ ": the home page gets created, but the permission is not set.";
		info = this.serviceUtils.buildReturnValue(ERROR_CODE.WARN, msg);
	}
	return info;
}

/**
 * Updates one of the course work pages for the students when the page template
 * gets changed.
 * 
 * @param memberIDs
 * @param templateName
 */
WorkSiteService.prototype.updateCourseWorkPages = function(memberIDs,
		templateName) {
	var info = [];
	var status = null;
	var IDs = memberIDs.split(',');
	for (var i = 0; i < IDs.length; i++) {
		var memberId = IDs[i].trim();
		status = this.updateCourseWorkPage(memberId, templateName);
		info.push(status);
	}
	return info;
}

/**
 * Updates one of the course work pages for the specified student when the page
 * template gets changed.
 * 
 * @param memberId
 * @param templateName
 */
WorkSiteService.prototype.updateCourseWorkPage = function(memberId,
		templateName) {

	var msg = null;
	var status = null;
	var site = this.getCourseWorkSite();

	if (templateName == 'about-me-template') {
		msg = 'Cannot update the about-me page.';
		status = this.serviceUtils.buildReturnValue(ERROR_CODE.ERROR, msg);
		return status;
	}

	// get the template from the site
	var template = this.getPageTemplate(site, templateName);
	if (template == null) {
		msg = 'The template ' + templateName + ' does not exist.';
		status = this.serviceUtils.buildReturnValue(ERROR_CODE.ERROR, msg);
		return status;
	}
	var content = template.getHtmlContent();

	// find out the matching web page info (the pages created using the
	// template)
	var pageInfo = this.getPageInfoByTemplate(templateName);
	if (pageInfo == null || pageInfo.isUpdatable == false) {
		msg = 'No matching page or the page is not updatable for this template '
				+ templateName;
		status = this.serviceUtils.buildReturnValue(ERROR_CODE.ERROR, msg);
		return status;
	}

	// get the student's home page from the student directory
	var studentDirectory = site
			.getChildByName(this.constants.STUDENT_DIRECTORY);
	var homePage = studentDirectory.getChildByName(memberId);
	if (homePage == null) {
		msg = 'No page to update for ' + memberId;
		status = this.serviceUtils.buildReturnValue(ERROR_CODE.ERROR, msg);
		return status;
	}

	// update the matching web page using the content of the template
	if (pageInfo.isHome == true) {
		status = this.updatePage(homePage, content);
	} else {
		var subPage = homePage.getChildByName(pageInfo.name);
		if (subPage == null) {
			msg = 'The student page has no sub-page:' + pageInfo.name;
			status = this.serviceUtils.buildReturnValue(ERROR_CODE.ERROR, msg);
		} else {
			status = this.updatePage(subPage, content);
		}
	}
	return status;
}

/**
 * Creates a homework list that contains the most recently assigned homework.
 */
WorkSiteService.prototype.getHomeworkList = function() {

	// get a list of homework pages from the homework list page and
	// sort them by their creation dates descended.
	var listPage = SitesApp.getActivePage();
	var studentId = listPage.getName();
	if (studentId == 'homework-list') {
		studentId = listPage.getParent().getName();
	} else {
		listPage = listPage.getChildByName("homework-list");
	}
	return this.getHomeworkListWithStatus(studentId, listPage);
}

/**
 * Gets a homework list for a student with the work status
 * 
 * @param studentId
 * @param listPage
 * @returns {Array}
 */
WorkSiteService.prototype.getHomeworkListWithStatus = function(studentId,
		listPage) {

	var childPages = listPage.getChildren({
		type : SitesApp.PageType.WEB_PAGE,
		start : 0,
		max : 30,
		includeDrafts : false,
		includeDeleted : false
	});

	childPages = childPages.sort(function(page1, page2) {
		var date1 = page1.getDatePublished();
		var date2 = page2.getDatePublished();
		if (date1 == date2)
			return 0;
		return (date1 < date2 ? 1 : -1);
	});

	// create a list of info blocks for creating the list
	var metaList = this.repository.findHomeworkMetaByStudentId(studentId);
	var pageInfoList = [];
	for (var i = 0; i < childPages.length; i++) {
		var childPage = childPages[i];
		var publishedDate = childPage.getDatePublished();
		var formattedDate = Utilities.formatDate(publishedDate, "MDT",
				"MM/dd/yy");
		var workInfo = this.getWorkInfo(studentId, childPage.getName());
		var status = this.getWorkStatus(workInfo, metaList);
		var pageInfo = {
			workInfo : workInfo,
			title : childPage.getTitle(),
			date : formattedDate,
			url : childPage.getUrl(),
			status : status
		};
		pageInfoList.push(pageInfo);
	}

	return pageInfoList;
}

/**
 * Creates a list of students that have a page under the student directory
 */
WorkSiteService.prototype.getStudentPageList = function() {

	// get all the pages from the student directory
	var site = this.getCourseWorkSite();
	var directory = site.getChildByName(this.constants.STUDENT_DIRECTORY);
	var childPages = directory.getChildren({
		type : SitesApp.PageType.WEB_PAGE,
		start : 0,
		includeDrafts : false,
		includeDeleted : false
	});

	// get some additional information of each student to create a list
	var pageInfoList = [];
	for (var i = 0; i < childPages.length; i++) {
		var memberId = childPages[i].getName();
		var member = this.memberService.getMember(memberId);
		if (member != null) {
			var pageInfo = {
				memberId : memberId,
				firstName : member.firstName,
				lastName : member.lastName,
				chineseName : member.chineseName
			};
			pageInfoList[i] = pageInfo;
		}
	}

	// sort the list by first name and then last name
	var sortedList = pageInfoList.sort(function(info1, info2) {
		if (info1.lastName == info2.lastName) {
			return (info1.firstName > info2.firstName ? 1 : -1);
		}
		return (info1.lastName > info2.lastName ? 1 : -1);
	});

	var students = {
		'isOwner' : false,
		'pageInfoList' : sortedList
	}
	return students;
}

/**
 * Creates a list of all the assigned homework information (courseId, studentId,
 * workNumber and published date)
 */
WorkSiteService.prototype.getStudentHomeworkInfo = function() {

	// get all the pages from the student directory
	var site = this.getCourseWorkSite();
	var directory = site.getChildByName(this.constants.STUDENT_DIRECTORY);
	var childPages = directory.getChildren({
		type : SitesApp.PageType.WEB_PAGE,
		start : 0,
		includeDrafts : false,
		includeDeleted : false
	});

	// get some additional information of each student to create a list
	var pageInfoList = [];
	for (var i = 0; i < childPages.length; i++) {
		var memberPage = childPages[i];
		var memberId = childPages[i].getName();
		var homeworkPage = memberPage.getChildByName("homework-list");
		var listPage = homeworkPage.getChildren({
			type : SitesApp.PageType.WEB_PAGE,
			start : 0,
			includeDrafts : false,
			includeDeleted : false
		})

		for (var j = 0; j < listPage.length; j++) {
			var workPage = listPage[j];
			var pageName = workPage.getName().split('-')
			var courseId = pageName[0];
			var workNumber = pageName[1];
			var publishedDate = workPage.getDatePublished();
			var formattedDate = Utilities.formatDate(publishedDate, "MDT",
					"MM/dd/yy");
			var pageInfo = {
				memberId : memberId,
				courseId : courseId,
				workNumber : workNumber,
				publishedDate : formattedDate
			};
			pageInfoList.push(pageInfo);
		}
	}
	return pageInfoList;
}
