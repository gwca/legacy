
/**
 * The test cases for testing the WorkSiteService
 */
function testLoadHomework() {
	var service = new HomeworkService();
	var memberId = 'john-doe';
	var courseId = 'workshop';
	var workNumber = '1_1';
	var homework = service.loadFile(memberId, courseId, workNumber);
	Logger.log('Assigned workshop homework:');
	Logger.log(JSON.stringify(homework, '', 2));
	
	memberId = 'gina-kuo';
	courseId = 'npcr5';
	workNumber = '51_1';
	homework = service.loadFile(memberId, courseId, workNumber);
	Logger.log('Submitted npcr5 homework:');
	Logger.log(JSON.stringify(homework, '', 2));
}

function testGetHomeworkList() {
	var service = new WorkSiteService();
	var list = service.getHomeworkList();
	Logger.log(JSON.stringify(list, '', 2));
}

function testAssignHomework() {
	var service = new WorkSiteService();
	var memberId = 'john-doe';
	var courseId = 'workshop';
	var workNumber = '1_1';
	var title = 'Testing 作业-1';
	var info = service.assign(memberId, courseId, workNumber, title);
	Logger.log(JSON.stringify(info, '', 2));
}

function testAssignWork() {
	var service = new WorkSiteService();
	var students = 'john-doe, youping-hu, jane-doe, admin-test';
	var courseId = 'workshop';
	var workNumber = '1_1';
	var title = 'Testing 作业-1';
	var info = service.assignWork(students, courseId, workNumber, title);
	Logger.log(JSON.stringify(info, '', 2));
}

function testAssignAllHomework() {
	var service = new WorkSiteService();
	var students = 'john-doe, jane-doe, youping-hu, admin-test';
	var courseId = 'workshop';
	var workNumber = '1_1';
	var title = 'Testing 作业-1';
	var info = service.assignAll(students, courseId, workNumber, title);
	Logger.log(JSON.stringify(info, '', 2));
}

function testNotifyMembers() {
	var service = new WorkSiteService();
	var className = 'workshop';
	var courseId = 'workshop';
	var workNumber = '1_1';
	var info = service.notify(className, courseId, workNumber);
	Logger.log(JSON.stringify(info, '', 2));
}

function testCheckStatus() {
	var service = new WorkSiteService();
	var className = 'C12';
	var courseId = 'npcr5';
	var workNumber = '51_1';
	var info = service.status(className, courseId, workNumber);
	Logger.log(JSON.stringify(info, '', 2));
}

function testCreateStudentPage() {
	var service = new WorkSiteService();
	var studentId = 'jane-doe';
	var info = service.createCourseWorkPage(studentId);
	Logger.log(info.message);
}

function testUpdateStudentPage() {
	var service = new WorkSiteService();
	var memberId = 'john-doe';
	var templateName = 'project-template';
	var info = service.updateCourseWorkPage(memberId, templateName);
	Logger.log(info[0].message);
}

function testUpdateStudentPages() {
	var service = new WorkSiteService();
	var memberIds = 'john-doe, peter-hu';
	var templateName = 'project-template';
	var info = service.updateCourseWorkPages(memberIds, templateName);
	Logger.log(JSON.stringify(info, '', 2));
}

/**
 * The test cases for testing some utilities
 */
function testIsArray() {
	var a = [ "A", "AA", "AAA" ];
	if (Array.isArray(a)) {
		Logger.log('is an array:' + a.length);
	} else {
		Logger.log('is not an array:' + a);
	}
}
