
function testZip() {
	var result = JSON.stringify(TestHomework1, null, '');
	// result = result + result + result + result + result + result + result
	// + result + result + result + result + result;
	// result += result + result + result + result;
	var blob = Utilities.newBlob(result);
	var zippedBlob = Utilities.zip([ blob ]);
	var unzipped = Utilities.unzip(zippedBlob);
	var data = unzipped[0].getDataAsString();

	Logger.log('result len:' + result.length + ' zipped len:'
			+ zippedBlob.getDataAsString().length + ' unzipped len:'
			+ data.length);

	Logger.log(data);
	Logger.log(JSON.parse(data));
}

function testImport() {
	var tableId = "1KermNYoMOFGNDdX9ypNDgcT-qusqX-5r9uuglYYF";
	var result = JSON.stringify(TestHomework1, null, '');

	// remove all the control characters
	result = result.replace(/[\x00-\x1F\x7F-\x9F]/g, "");

	// replace (') and (\") with Chinese single quote (’)
	result = result.replace(/'|\\"/g, "’");

	// replace (~) and (^) with (-)
	result = result.replace(/~|^/g, '-');

	// replace all the newline and tab characters with a space
	result = result.replace(/\n|\t/g, " ");

	// replace all the double quotes with a (~), they will be replaced
	// back to double quote by a query later. doing so because of the
	// importRows API is not able to handle double quotes in the content.
	result = result.replace(/"/g, "~");

	// insert a new row
	var data = [ 'tester2', 'workshop', '1-1', 'test-work', '10/5/2016',
			'10/6/2016', 'submitted', result ].join('^');
	var blob = Utilities.newBlob(data, 'application/octet-stream');
	var options = {
		delimiter : '^'
	};
	var ret = FusionTables.Table.importRows(tableId, blob, options);

	// update a row by deleting it first and insert the new data with the same
	// ID
	var sql = "SELECT ROWID FROM " + tableId + " WHERE studentId = 'tester2'";
	var homework = FusionTables.Query.sqlGet(sql, {
		hdrs : false
	});
	var rowId = homework.rows[0][0];
	sql = "DELETE FROM " + tableId + " WHERE ROWID = '" + rowId + "'";
	homework = FusionTables.Query.sql(sql, {
		hdrs : false
	});

	data = [ 'tester2', 'workshop', '1-1', 'test-work', '10/5/2016',
			'10/6/2016', 'submitted', '{~title~ : ~the updated work~}' ]
			.join('^');
	blob = Utilities.newBlob(data, 'application/octet-stream');
	ret = FusionTables.Table.importRows(tableId, blob, options);

	// query the row to see the result
	sql = "SELECT work FROM " + tableId + " WHERE studentId = 'tester2'";
	homework = FusionTables.Query.sqlGet(sql, {
		hdrs : false
	});
	var work = homework.rows[0][0];

	// replace (~) back to (") before parsing the result.
	work = work.replace(/~/g, '"');
	work = JSON.parse(work);
	Logger.log(work);
}

function replacer(key, value) {
	value = value.replace(/[\x00-\x1F\x7F-\x9F]/g, "");
}

function testStringify() {
	var result = JSON.stringify(TestHomework, null, '');
	Logger.log(result);

	var value = JSON.parse(result);
	Logger.log(value);
}

// function testTransforAssignedWork() {
// var dataService = new DataRepository();
// var zippedService = new ZippedDataRepository();
// var workList = dataService.findAll(dataService.HOMEWORK_LIB);
// for (var i = 0; i < workList.length; i++) {
// var homework = workList[i];
// zippedService.createAssignedHomework(homework);
// }
// }
//
// function testTransforStudentWork1() {
// var dataService = new DataRepository();
// var zippedService = new ZippedDataRepository();
// var workList = dataService.findAll(dataService.HOMEWORK);
// workList = sortWorkList(workList);
// for (var i = 0; i < 100; i++) {
// var homework = workList[i];
// zippedService.createStudentHomework(homework);
// }
// Logger.log("processed the first 100 rows");
// }
//
// function testTransforStudentWork2() {
// var dataService = new DataRepository();
// var zippedService = new ZippedDataRepository();
// var workList = dataService.findAll(dataService.HOMEWORK);
// workList = sortWorkList(workList);
// for (var i = 100; i < 200; i++) {
// var homework = workList[i];
// zippedService.createStudentHomework(homework);
// }
// Logger.log("processed the second 100 rows");
// }
//
// function testTransforStudentWork3() {
// var dataService = new DataRepository();
// var zippedService = new ZippedDataRepository();
// var workList = dataService.findAll(dataService.HOMEWORK);
// workList = sortWorkList(workList);
// for (var i = 200; i < workList.length; i++) {
// var homework = workList[i];
// zippedService.createStudentHomework(homework);
// }
// Logger.log("processed the rest of the rows:" + (workList.length - 200));
// }

function sortWorkList(workList) {
	var sortedList = workList.sort(function(work1, work2) {
		if (work1.studentId == work2.studentId)
			return 0;
		return (work1.studentId > work2.studentId ? 1 : -1);
	});
	return sortedList;
}

var sample = {
	"studentId" : "john-doe",
	"courseId" : "npcr1",
	"workNumber" : "9_2",
	"workTitle" : "第九课 作业 2",
	"workitems" : [
			{
				"name" : "note",
				"title" : "Homework (9-2)",
				"index" : "0",
				"instructions" : [ "Due by Friday (3/11)" ],
				"url" : "",
				"text" : "",
				"checked" : null,
				"remark" : ""
			},
			{
				"name" : "read",
				"title" : "阅读理解 Reading Comprehension",
				"index" : "1",
				"instruction" : "Read and comprehend the lesson 9 (text 2, new words, and the notes sessions).",
				"url" : "https://gwca-classroom.appspot.com/books/npcr1/lesson09.pdf",
				"text" : "他今年二十岁 (2)",
				"checked" : null,
				"remark" : ""
			},
			{
				"name" : "note",
				"title" : "生词 New Words",
				"index" : "2",
				"instructions" : [
						"1. Use Quizlet (learn, spell and test) to master the new characters of the lesson. (Click on the new Quizlet button of the vocabulary builder page to go there)",
						"2. Write out the new characters of the 2nd text 8-10 times.",
						"3. Attach the word paper." ],
				"url" : "https://gwca-classroom.appspot.com/books/builder/builder.html?course=npcr1&lesson=09&section=2",
				"text" : "New Characters of the Lesson 9 (text 2)",
				"checked" : null,
				"remark" : ""
			},
			{
				"name" : "true-false",
				"title" : "选择对错 True/False Check the sentences are grammatically correct",
				"index" : "3",
				"statements" : [ {
					"question" : "他今年多岁？",
					"answer" : null,
					"checked" : null,
					"remark" : ""
				}, {
					"question" : "你今年多大？",
					"answer" : null,
					"checked" : null,
					"remark" : ""
				}, {
					"question" : "我们买大一个蛋糕。",
					"answer" : null,
					"checked" : null,
					"remark" : ""
				}, {
					"question" : "明天二十三号十月。",
					"answer" : null,
					"checked" : null,
					"remark" : ""
				}, {
					"question" : "我喜欢很烤鸭。",
					"answer" : null,
					"checked" : null,
					"remark" : ""
				} ]
			},
			{
				"name" : "phrase-sentence",
				"title" : "造句 Make sentnces with the given words",
				"index" : "5",
				"words" : [ {
					"word" : "漂亮",
					"answer" : "",
					"checked" : null,
					"remark" : ""
				}, {
					"word" : "意思",
					"answer" : "",
					"checked" : null,
					"remark" : ""
				}, {
					"word" : "生日",
					"answer" : "",
					"checked" : null,
					"remark" : ""
				} ]
			},
			{
				"name" : "answer-question",
				"title" : "改成问题 Change the following sentences into questions (吗，是不是， 有没有）",
				"index" : "6",
				"questions" : [ {
					"question" : "他的生日是一月一号。",
					"answer" : "",
					"checked" : null,
					"remark" : ""
				}, {
					"question" : "我买两瓶红酒。",
					"answer" : "",
					"checked" : null,
					"remark" : ""
				}, {
					"question" : "明天我有课。",
					"answer" : "",
					"checked" : null,
					"remark" : ""
				}, {
					"question" : "生日聚会他们吃寿面。",
					"answer" : "",
					"checked" : null,
					"remark" : ""
				}, {
					"question" : "我生日很快乐。",
					"answer" : "",
					"checked" : null,
					"remark" : ""
				} ]
			},
			{
				"name" : "read",
				"title" : "朗读 Read Aloud",
				"index" : "7",
				"instruction" : "Read aloud the text 2 following the audio or video clip until you are fluent. The dialogue in the video clip is in normal speed, you can watch it too.",
				"url" : "https://gwca-classroom.appspot.com/books/npcr1/npcr1.html?item=09&subItem=2",
				"text" : "他今年二十岁  (2)",
				"checked" : null,
				"remark" : ""
			},
			{
				"name" : "note",
				"title" : "阅读 Additional Reading",
				"index" : "10",
				"instructions" : [
						"Read this paragraph that has some additional new characters you may need to look up from a dictionary.",
						"The paragraph is on the side bar of the vocabulary builder page (Additional Reading), click on the next link to go there." ],
				"url" : "https://gwca-classroom.appspot.com/books/npcr1/npcr1.html?item=09&subItem=3",
				"text" : "短文 Short Paragraph",
				"checked" : null,
				"remark" : ""
			},
			{
				"name" : "record",
				"title" : "录音 Record",
				"index" : "11",
				"instruction" : "Record the additional reading and attach the clip here.",
				"checked" : null,
				"remark" : ""
			} ],
	"remark" : "",
	"grade" : "",
	"status" : "assigned",
	"checkTime" : "",
	"hasError" : false,
	"message" : "",
	"sequence" : "92"
}

/**
 * Reads the original homework files from Drive and adds them to the HomeworkLib
 * table. function addHomeworkToTable() {
 * 
 * var service = new DataService(); var folderId =
 * '0B9-duwm5My1ffnFXd25ZUEIwUk5Uanc3N2VMRFVLUmw1YnlLNW5kSlpoRkhORGctTUs5UWM';
 * var workFolder = DriveApp.getFolderById(folderId); var files =
 * workFolder.getFiles(); var counter = 0; while (files.hasNext()) { var file =
 * files.next(); var name = file.getName(); if (name.indexOf('.json') > 0) { var
 * content = file.getBlob().getDataAsString(); var homework =
 * JSON.parse(content); var info = service.saveAssignedHomework(homework);
 * Logger.log(info.message); counter++; } } Logger.log('npcr5 all done:' +
 * counter); }
 */

/**
 * Reads the original student homework files from Drive and adds them to the
 * Homework table. function addHomeworkToStudentTable() {
 * 
 * var studentId = 'gaojie-yang'; // "gaojie-yang" 'gina-kuo' var folderId =
 * '0B9-duwm5My1ffmVpNDlRVU44QjNMRnVhd1ozc0ZieTNNX0J2MHZ0OGFPYm9NWXJRa2RFMFU';
 * 
 * var service = new DataService(); var counter = 0; var workFolder =
 * DriveApp.getFolderById(folderId); var folders =
 * workFolder.getFoldersByName(studentId); if (folders.hasNext()) { folder =
 * folders.next(); var files = folder.getFiles(); while (files.hasNext()) { var
 * file = files.next(); var name = file.getName(); if (name.indexOf('.json') >
 * 0) { var content = file.getBlob().getDataAsString(); var homework =
 * JSON.parse(content); var info = service.saveStudentHomework(homework);
 * Logger.log(info.message); counter++; } } } Logger.log(studentId + ' all
 * done:' + counter); }
 */
