/**
 * The homework data persistence service.
 */
function DataService() {

	this.repository = new DataRepository();

	/**
	 * The structured (JSON) homework object.
	 */
	this.homeworkTemplate = JSON.stringify({
		studentId : '',
		courseId : '',
		workNumber : '',
		workTitle : '',
		createdDate : null,
		lastUpdatedDate : null,
		hasError : false,
		message : ''
	});
}

/**
 * Creates a structured (JSON) homework object.
 * 
 * @param studentId
 * @param courseId
 * @param workNumber
 */
DataService.prototype.createHomework = function(studentId, courseId, workNumber) {
	var homework = JSON.parse(this.homeworkTemplate);
	homework.studentId = studentId;
	homework.courseId = courseId;
	homework.workNumber = workNumber;
	return homework;
}

/**
 * Finds a submitted homework.
 * 
 * @param studentId
 * @param courseId
 * @param workNumber
 * @returns the homework content
 */
DataService.prototype.getStudentHomework = function(studentId, courseId,
		workNumber) {
	var homework = this.repository.findStudentHomework(studentId, courseId,
			workNumber);
	if (homework == null) {
		homework = this.createHomework(studentId, courseId, workNumber);
		homework.hasError = true;
		homework.message = courseId + '_' + workNumber + ' for ' + studentId
				+ ' does not exist.';
	}
	return homework;
}

/**
 * Finds an assigned homework.
 * 
 * @param courseId
 * @param workNumber
 * @returns the homework content
 */
DataService.prototype.getAssignedHomework = function(courseId, workNumber) {
	var homework = this.repository.findAssignedHomework(courseId, workNumber);
	if (homework == null) {
		homework = this.createHomework(null, courseId, workNumber);
		homework.sequence = -1;
		homework.hasError = true;
		homework.message = courseId + '_' + workNumber + ' does not exist.';
	}
	return homework;
}

/**
 * Gets a list of homework titles.
 * 
 * @param courseId
 * @returns a collection of homework info objects
 */
DataService.prototype.getHomeworkTitleList = function(courseId) {

	var fileInfo = {
		'infoList' : [],
		'hasError' : false,
		'message' : 'OK'
	};
	var homeworkList = this.repository.findAssignedHomeworkByCourse(courseId);
	for (var i = 0; i < homeworkList.length; i++) {
		var homework = homeworkList[i];
		var info = {
			'courseId' : homework.courseId,
			'workNumber' : homework.workNumber,
			'description' : homework.workTitle
		}
		fileInfo.infoList.push(info);
	}
	return fileInfo;
}

/**
 * Saves or updates an assigned homework
 * 
 * @param homework
 * @returns homework
 */
DataService.prototype.saveAssignedHomework = function(homework) {

	var courseId = homework.courseId;
	var workNumber = homework.workNumber;
	var result = this.getAssignedHomework(courseId, workNumber);
	var info = null;

	// if non existing, create a new homework entry, otherwise, update the
	// existing one.
	homework.sequence = result.sequence;
	if (result.hasError) {
		info = this.repository.createAssignedHomework(homework);
		homework.message = workNumber + ' of ' + courseId + ' is created';
	} else {
		info = this.repository.updateAssignedHomework(homework);
		homework.message = workNumber + ' of ' + courseId + ' is updated';
	}
	if (info.hasError) {
		homework.message = info.message;
		homework.hasError = true;
	}
	return homework;
}

/**
 * Saves or updates a submitted homework.
 * 
 * @param homework
 * @returns homework
 */
DataService.prototype.saveStudentHomework = function(homework) {

	var studentId = homework.studentId;
	var courseId = homework.courseId;
	var workNumber = homework.workNumber;
	var result = this.getStudentHomework(studentId, courseId, workNumber);
	var info = null;

	// if non existing, create a new homework entry, otherwise, update the
	// existing one.
	if (result.hasError) {
		info = this.repository.createStudentHomework(homework);
		homework.message = workNumber + ' of ' + courseId + ' for ' + studentId
				+ ' is created';
	} else {
		info = this.repository.updateStudentHomework(homework);
		homework.message = workNumber + ' of ' + courseId + ' for ' + studentId
				+ ' is updated';
	}
	if (info.hasError) {
		homework.message = info.message;
		homework.hasError = true;
	}
	return homework;
}

/**
 * Creates a submitted homework entry for each of the members and saves it. 
 * @param studentIds (student ID array)
 * @param assignedHomework
 */
DataService.prototype.saveMultiStudentHomework = function(studentIds, assignedHomework) {
	return this.repository.createMultiSubmittedHomework(studentIds, assignedHomework);
}
