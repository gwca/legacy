/**
 * The fusion table utility class. These fusion table APIs should be a lot
 * faster than those Drive APIs (not really)
 */
function DataRepository() {

	this.constants = new ServiceConstants();

	/** The time zone and date format */
	this.TIMEZONE = "MST";
	this.DATE_FORMAT = "yyyy-MM-dd";

	/**
	 * The number of milliseconds to pause after every "save" operation. Looks
	 * like the importRows() is an asynchronous API and the saved results in the
	 * table will be available to the next query in up to 1.5 seconds. So it has
	 * to wait for a while after a save operation gets executed.
	 */
	this.PAUSE_TIME = 1500;

	/** Some constants */
	this.DELIMITER = "^";
	this.QUOTE_REPLACEMENT = "~";
	this.BLOB_OPTIONS = {
		delimiter : "^"
	};

	/** The table names for looking up the table IDs */
	this.MEMBERS = 'MEMBERS';
	this.COURSES = 'COURSES';
	this.CLASSES = 'CLASSES';
	this.HOMEWORK = 'HOMEWORK';
	this.HOMEWORK_LIB = 'HOMEWORK_LIB';
	this.TEST_HOMEWORK = 'TEST_HOMEWORK';
	this.TEST_HOMEWORK_LIB = 'TEST_HOMEWORK_LIB';

	/**
	 * The length of a homework, which is a JSON object, is ranged from 1 to 20
	 * Kbytes. The fusion table APIs are not able to insert or update a homework
	 * with this length. I believe those APIs call to a restful service and
	 * Google Apps are using GET instead of POST to invoke the services. Since
	 * the queryString of the GET method has a length limit. The POST calls will
	 * have problems when the length of the posted data is longer then 1.5
	 * Kbytes. To get around this issue, I have to use importRwos API instead of
	 * SQL.INSERT and SQL.UPDATE. The importRows also has a problem dealing with
	 * double quotes and therefore I have to replace all the double quotes with
	 * "~" characters to get around that problem.
	 */
	/**
	 * The structured (JSON) tables object. This is subject to change. Go to
	 * Drive::Tables folder to see the latest table schema.
	 */
	this.tableDescriptions = [
			{
				id : '1fAoO5y0gQjp_pQjBdhR6iTsgT94o_60Hg3py_2JD',
				name : this.MEMBERS,
				columns : 'memberId, role, isActive, email, ccEmail, firstName, lastName, chineseName, isPrivate',
				description : 'The detailed info table of all the registered members.'
			},
			{
				id : '11kABL6CjhwldRFSGIkZAsD7KvXztj0VyHiJLpKEO',
				name : this.COURSES,
				columns : 'id, name, alias, brief, image, textFolderId, isActive',
				description : 'The detailed info table of all the courses.'
			},
			{
				id : '1xEaOd5z4QgoY6NDC94ayULPF4TiSFi8eRlHB5VRs',
				name : this.CLASSES,
				columns : 'classId, className, classURL, classYear,	teacherId, courseId, isActive, memberIds',
				description : 'The join table for the relationships of the class, course and members.'
			},
			{
				id : '1WlXx6YHWQMmAjDqw-BLFf3Xc9ICHS7FHE5zpCj8I',
				name : this.HOMEWORK_LIB,
				columns : 'workNumber, courseId, title, work, sequence',
				description : 'The homework library table for the assigned homework.'
			},
			{
				id : '1KermNYoMOFGNDdX9ypNDgcT-qusqX-5r9uuglYYF',
				name : this.HOMEWORK,
				columns : 'studentId, workNumber, courseId, title, status, work',
				description : 'The homework table for the submitted homework of the students.'
			},
			{
				id : '1ybqf7MAmLFMOlv5TqsPG2qct5MhdMemZ2b2rNtLM',
				name : this.TEST_HOMEWORK,
				columns : 'studentId, workNumber, courseId, title, status, work',
				description : 'The testing table for bulk update.'
			}, {
				id : '1UVQT3PXm-3HxiT06QSdoQ5j3aSD9BsecxlMtIb4E',
				name : this.TEST_HOMEWORK_LIB,
				columns : 'workNumber, courseId, title, work, sequence',
				description : 'The testing table for bulk update.'
			} ];

	/**
	 * The class information object that maps to the class information page of a
	 * class or student web site.
	 */
	this.classInfo = JSON.stringify({
		className : '',
		classYear : '',
		courseId : '',
		teacherId : '',
		hasError : false,
		error : ''
	});

	/**
	 * Gets a table description object by table name.
	 */
	this.getTableDesc = function(tableName) {
		var name = tableName.toUpperCase();
		for (var i = 0; i < this.tableDescriptions.length; i++) {
			if (this.tableDescriptions[i].name == name) {
				return this.tableDescriptions[i];
			}
		}
		return null;
	}

	/**
	 * Converts a row into a Member JSON object.
	 * 
	 * @row
	 * @return member
	 */
	this.toMember = function(row) {
		var member = {
			id : row[1],
			role : row[2],
			isActive : row[3],
			email : row[4],
			ccEmail : row[5],
			firstName : row[6],
			lastName : row[7],
			chineseName : row[8],
			isPrivate : row[9]
		};
		return member;
	}

	/**
	 * Converts a row into a ClassMember JSON object.
	 * 
	 * @row
	 * @return classMember
	 */
	this.toClassMember = function(row) {
		var classMember = {
			classId : row[1],
			className : row[2],
			classURL : row[3],
			classYear : row[4],
			teacherId : row[5],
			courseId : row[6],
			isActive : row[7],
			memberIds : row[8]
		};
		return classMember;
	}

	/**
	 * Converts a row into a Course JSON object.
	 * 
	 * @row
	 * @return course
	 */
	this.toCourse = function(row) {
		var course = {
			id : row[1],
			name : row[2],
			alias : row[3],
			desc : row[4],
			image : row[5],
			textFolderId : row[6]
		};
		return course;
	}

	/**
	 * Converts a submitted homework row into a Homework JSON object.
	 * 
	 * @row
	 * @return homework
	 */
	this.toSubmittedHomework = function(row) {
		var work = row[6];
		work = work.replace(/~/g, '"');
		return JSON.parse(work);
	}

	/**
	 * Converts an assigned homework row into a Homework JSON object.
	 * 
	 * @row
	 * @return homework
	 */
	this.toAssignedHomework = function(row) {
		var sequence = row[5];
		var work = row[4];
		work = work.replace(/~/g, '"');
		work = JSON.parse(work);
		work.sequence = sequence;
		return work;
	}

	/**
	 * Filters out some special characters from the JSON string to make sure it
	 * can be written to the table correctly.
	 */
	this.filterWorkValue = function(work) {

		// remove all the control characters
		work = work.replace(/[\x00-\x1F\x7F-\x9F]/g, "");

		// replace (') and (\") with Chinese single quote (’)
		work = work.replace(/'|\\"/g, "’");

		// replace (~) and (^) with (-)
		work = work.replace(/~|\^/g, '-');

		// replace all the newline and tab characters with a space
		work = work.replace(/\n|\t/g, " ");

		// replace all the double quotes with a (~), they will be replaced
		// back to double quote by a query later. doing so because of the
		// importRows API is not able to handle double quotes in the content.
		work = work.replace(/"/g, "~");
		return work;
	}

	/**
	 * Adds a pair of single quotes to the supplied value.
	 * 
	 * @value
	 * @return the quoted value or "''" if the value is empty.
	 */
	this.quotedValue = function(value) {
		if (value != null && value.length > 0) {
			return "'" + value + "'";
		} else {
			return "''";
		}
	}

	/**
	 * Creates a blob for inserting data into the student homework table.
	 */
	this.createSubmittedWorkBlob = function(homework) {
		var work = this.filterWorkValue(JSON.stringify(homework));
		var data = [ homework.studentId, homework.courseId,
				homework.workNumber, homework.workTitle, homework.status, work ]
				.join(this.DELIMITER);
		return Utilities.newBlob(data, 'application/octet-stream');
	}

	/**
	 * Creates a blob for inserting multiple rows into the student homework
	 * table.
	 */
	this.createMultiHomeworkBlob = function(studentIds, homework) {
		var dataArray = [];
		for (var i = 0; i < studentIds.length; i++) {
			homework.studentId = studentIds[i];
			var work = this.filterWorkValue(JSON.stringify(homework));
			var data = [ homework.studentId, homework.courseId,
					homework.workNumber, homework.workTitle, homework.status,
					work ].join(this.DELIMITER);
			dataArray.push(data);
		}
		data = dataArray.join('\n');
		return Utilities.newBlob(data, 'application/octet-stream');
	}

	/**
	 * Creates a blob for inserting data into the homework library table.
	 */
	this.createAssignedWorkBlob = function(homework) {
		var work = JSON.stringify(homework);
		work = this.filterWorkValue(work);
		var data = [ homework.courseId, homework.workNumber,
				homework.workTitle, work, homework.sequence ]
				.join(this.DELIMITER);
		return Utilities.newBlob(data, 'application/octet-stream');
	}

	/**
	 * Creates a blob for replacing all the existing rows of the student
	 * homework table.
	 */
	this.createHomeworkTableBlob = function(homeworkList) {
		var dataArray = [];
		for (var i = 0; i < homeworkList.length; i++) {
			var homework = homeworkList[i];
			var work = this.filterWorkValue(JSON.stringify(homework));
			var data = [ homework.studentId, homework.courseId,
					homework.workNumber, homework.workTitle, homework.status,
					work ].join(this.DELIMITER);
			dataArray.push(data);
		}
		data = dataArray.join('\n');
		return Utilities.newBlob(data, 'application/octet-stream');
	}

	/**
	 * Creates a blob for replacing all the existing rows of the homework
	 * library table.
	 */
	this.createHomeworkLibTableBlob = function(homeworkList) {
		var dataArray = [];
		for (var i = 0; i < homeworkList.length; i++) {
			var homework = homeworkList[i];
			var work = JSON.stringify(homework);
			work = this.filterWorkValue(work);
			var data = [ homework.courseId, homework.workNumber,
					homework.workTitle, work, homework.sequence ]
					.join(this.DELIMITER);
			dataArray.push(data);
		}
		data = dataArray.join('\n');
		return Utilities.newBlob(data, 'application/octet-stream');
	}

	/**
	 * Gets the rowId from the result of an INSERT statement
	 */
	this.getRowId = function(result) {
		var rows = result.rows;
		if (rows == null || rows.length == 0) {
			return null;
		}
		var rowid = rows[0][0];
		return rowid;
	}

	/**
	 * Queries the table with the supplied where clause.
	 * 
	 * @param tableName
	 * @param whereClause
	 * @param orderBy
	 *            order by column name
	 * @return the rows from the table or null if nothing is found.
	 */
	this.findByQuery = function(tableName, whereClause, orderBy) {

		var tableDesc = this.getTableDesc(tableName);
		if (tableDesc == null) {
			return null;
		}

		var sql = 'SELECT ROWID, ' + tableDesc.columns + ' FROM '
				+ tableDesc.id
				+ (whereClause == null ? '' : ' WHERE ' + whereClause)
				+ (orderBy == null ? '' : ' ORDER BY ' + orderBy);
		var result = FusionTables.Query.sqlGet(sql, {
			hdrs : false
		});

		if (result.rows && result.rows.length > 0) {
			return result.rows;
		} else {
			return null;
		}
	}

	/**
	 * Queries the table with the supplied where clause and returns the ROWID of
	 * the first row.
	 * 
	 * @param tableName
	 * @param whereClause
	 * @return the ROWID of the first row or null if nothing is found.
	 */
	this.findRowId = function(tableName, whereClause) {
		var rows = this.findByQuery(tableName, whereClause, null);
		if (rows == null || rows.length == 0) {
			return null;
		} else {
			return rows[0][0];
		}
	}

	/**
	 * Removes a submitted homework for updating it.
	 * 
	 * @param homework
	 * @returns
	 */
	this.cleanSubmittedHomework = function(homework) {
		var studentId = homework.studentId;
		var courseId = homework.courseId;
		var workNumber = homework.workNumber;
		var tableDesc = this.getTableDesc(this.HOMEWORK);
		var whereClause = " WHERE studentId = " + this.quotedValue(studentId)
				+ " AND courseId = " + this.quotedValue(courseId)
				+ " AND workNumber = " + this.quotedValue(workNumber);
		var sql = 'SELECT ROWID FROM ' + tableDesc.id + whereClause;
		var result = FusionTables.Query.sqlGet(sql, {
			hdrs : false
		});

		var rowId = this.getRowId(result);
		if (rowId != null) {
			whereClause = " WHERE ROWID = " + this.quotedValue(rowId);
			sql = "DELETE FROM " + tableDesc.id + whereClause;
			FusionTables.Query.sql(sql, {
				hdrs : false
			});
		}
		return rowId;
	}

	/**
	 * Removes a assigned homework for updating it.
	 * 
	 * @param homework
	 * @returns
	 */
	this.cleanAssignedHomework = function(homework) {
		var courseId = homework.courseId;
		var workNumber = homework.workNumber;
		var tableDesc = this.getTableDesc(this.HOMEWORK_LIB);
		var whereClause = " WHERE courseId = " + this.quotedValue(courseId)
				+ " AND workNumber = " + this.quotedValue(workNumber);

		var sql = 'SELECT ROWID FROM ' + tableDesc.id + whereClause;
		var result = FusionTables.Query.sqlGet(sql, {
			hdrs : false
		});

		var rowId = this.getRowId(result);
		if (rowId != null) {
			whereClause = " WHERE ROWID = " + this.quotedValue(rowId);
			sql = "DELETE FROM " + tableDesc.id + whereClause;
			FusionTables.Query.sql(sql, {
				hdrs : false
			});
		}
		return rowId;
	}
}

/**
 * Finds all the rows of the specified table.
 * 
 * @param tableName
 */
DataRepository.prototype.findAll = function(tableName) {
	var rows = this.findByQuery(tableName, null, null);
	if (rows == null) {
		return null;
	}

	entityList = [];
	for (var i = 0; i < rows.length; i++) {
		var row = rows[i];
		switch (tableName) {
		case this.CLASSES:
			entityList.push(this.toClassMember(row));
			break;
		case this.MEMBERS:
			entityList.push(this.toMember(row));
			break;
		case this.COURSES:
			entityList.push(this.toCourse(row));
			break;
		case this.HOMEWORK:
		case this.TEST_HOMEWORK:
			entityList.push(this.toSubmittedHomework(row));
			break;
		case this.HOMEWORK_LIB:
		case this.TEST_HOMEWORK_LIB:
			entityList.push(this.toAssignedHomework(row));
			break;
		}
	}
	return entityList;
}

/**
 * Finds a member by ID from the registered members.
 * 
 * @param memberId
 */
DataRepository.prototype.findMember = function(memberId) {
	var whereClause = "memberId = " + this.quotedValue(memberId);
	var rows = this.findByQuery(this.MEMBERS, whereClause, null);
	return (rows == null ? null : this.toMember(rows[0]));
}

/**
 * Finds all the active members from the registered members.
 */
DataRepository.prototype.findActiveMembers = function() {
	var whereClause = "isActive = 'true'";
	var rows = this.findByQuery(this.MEMBERS, whereClause, null);
	var members = [];
	if (rows != null) {
		for (var i = 0; i < rows.length; i++) {
			members.push(this.toMember(rows[i]));
		}
	}
	return members;
}

/**
 * Finds all the active classes.
 * 
 * @returns the list of the active classMember objects
 */
DataRepository.prototype.findActiveClasses = function() {
	var whereClause = "isActive = 'true'";
	var rows = this.findByQuery(this.CLASSES, whereClause, null);
	var classList = [];
	if (rows != null) {
		for (var i = 0; i < rows.length; i++) {
			classList.push(this.toClassMember(rows[i]));
		}
	}
	return classList;
}

/**
 * Finds a list of course info objects from the table.
 * 
 * @returns
 */
DataRepository.prototype.findCourseInfoList = function() {
	var whereClause = "id NOT EQUAL TO 'textbook' AND isActive = 'true'";
	var rows = this.findByQuery(this.COURSES, whereClause, 'id');
	if (rows == null || rows.length == 0) {
		return null;
	}

	var infoList = [];
	for (var i = 0; i < rows.length; i++) {
		var course = this.toCourse(rows[i]);
		infoList.push(course);
	}
	return infoList;
}

/**
 * Finds a course info object by courseId
 * 
 * @param courseId
 * @returns
 */
DataRepository.prototype.findCourse = function(courseId) {
	var whereClause = 'id = ' + this.quotedValue(courseId);
	var rows = this.findByQuery(this.COURSES, whereClause, null);
	return (rows == null ? null : this.toCourse(rows[0]));
}

/**
 * Finds all the active courses from the available courses.
 */
DataRepository.prototype.findActiveCourses = function() {
	var whereClause = "isActive = 'true'";
	var rows = this.findByQuery(this.COURSES, whereClause, null);
	var courses = [];
	if (rows != null) {
		for (var i = 0; i < rows.length; i++) {
			courses.push(this.toCourse(rows[i]));
		}
	}
	return courses;
}

/**
 * Gets a piece of homework submitted by a student
 * 
 * @param studentId
 * @param courseId
 * @param workNumber
 * @returns
 */
DataRepository.prototype.findStudentHomework = function(studentId, courseId,
		workNumber) {
	var whereClause = "studentId = " + this.quotedValue(studentId)
			+ " AND courseId = " + this.quotedValue(courseId)
			+ " AND workNumber = " + this.quotedValue(workNumber);

	var rows = this.findByQuery(this.HOMEWORK, whereClause, null);
	return (rows == null ? null : this.toSubmittedHomework(rows[0]));
}

/**
 * Gets a list of the assigned homework by studentId
 * 
 * @param studentId
 * @returns
 */
DataRepository.prototype.findStudentHomeworkByStudentId = function(studentId) {
	var whereClause = "studentId = " + this.quotedValue(studentId);
	var rows = this.findByQuery(this.HOMEWORK, whereClause, null);
	var homework = [];
	if (rows != null) {
		for (var i = 0; i < rows.length; i++) {
			homework.push(this.toSubmittedHomework(rows[i]));
		}
	}
	return homework;
}

/**
 * Gets a list of the assigned homework meta data by studentId
 * 
 * @param studentId
 * @returns
 */
DataRepository.prototype.findHomeworkMetaByStudentId = function(studentId) {
	var whereClause = "studentId = " + this.quotedValue(studentId);
	var rows = this.findByQuery(this.HOMEWORK, whereClause, null);
	var metaList = [];
	var homework = null;
	if (rows != null) {
		for (var i = 0; i < rows.length; i++) {
			homework = this.toSubmittedHomework(rows[i]);
			var meta = {
				studentId : homework.studentId,
				workNumber : homework.workNumber,
				courseId : homework.courseId,
				status : homework.status
			}
			metaList.push(meta);
		}
	}
	return metaList;
}

/**
 * Finds a piece of assigned homework.
 * 
 * @param courseId
 * @param workNumber
 * @returns
 */
DataRepository.prototype.findAssignedHomework = function(courseId, workNumber) {
	var whereClause = "courseId = " + this.quotedValue(courseId)
			+ " AND workNumber = " + this.quotedValue(workNumber);

	var rows = this.findByQuery(this.HOMEWORK_LIB, whereClause, null);
	return (rows == null ? null : this.toAssignedHomework(rows[0]));
}

/**
 * Finds all the assigned homework by courseId
 * 
 * @param courseId
 * @returns
 */
DataRepository.prototype.findAssignedHomeworkByCourse = function(courseId) {
	var whereClause = "courseId = " + this.quotedValue(courseId);
	var orderBy = "sequence";
	var rows = this.findByQuery(this.HOMEWORK_LIB, whereClause, orderBy);
	var homework = [];
	if (rows != null) {
		for (var i = 0; i < rows.length; i++) {
			homework.push(this.toAssignedHomework(rows[i]));
		}
	}
	return homework;
}

/**
 * Creates a new homework entry in the student homework table. Pauses a bit to
 * be sure the newly updated homework is available to the next query.
 * 
 * @param homework
 * @returns the result info of the creation.
 */
DataRepository.prototype.createSubmittedHomework = function(homework) {

	var info = null;
	var tableDesc = this.getTableDesc(this.HOMEWORK);
	if (tableDesc == null) {
		info = {
			hasError : true,
			message : 'Table does not exist:' + this.HOMEWORK
		}
		return info;
	}

	try {
		this.cleanSubmittedHomework(homework);
		var workBlob = this.createSubmittedWorkBlob(homework);
		FusionTables.Table
				.importRows(tableDesc.id, workBlob, this.BLOB_OPTIONS);
		// Utilities.sleep(this.PAUSE_TIME);
		info = {
			hasError : false,
			message : homework.workTitle + ' got updated.'
		}
	} catch (error) {
		info = {
			hasError : true,
			message : 'Failed in updating multiple homework rows.' + ', error:'
					+ error
		}
	}
	return info;
}

/**
 * Creates a set of new homework entries in the student homework table for each
 * student. Pauses a bit to be sure the newly updated homework is available to
 * the next query.
 * 
 * @param studentIds
 *            a student ID array
 * @param homework
 * @returns the result info of the creation.
 */
DataRepository.prototype.createMultiSubmittedHomework = function(studentIds,
		homework) {

	var info = null;
	var infoList = [];

	var tableDesc = this.getTableDesc(this.HOMEWORK);
	if (tableDesc == null) {
		info = {
			hasError : true,
			message : 'Table does not exist:' + this.HOMEWORK
		}
		infoList.push(info);
		return infoList;
	}

	try {
		for (var i = 0; i < studentIds.length; i++) {
			homework.studentId = studentIds[i];
			this.cleanSubmittedHomework(homework);
		}
		var workBlob = this.createMultiHomeworkBlob(studentIds, homework);
		FusionTables.Table
				.importRows(tableDesc.id, workBlob, this.BLOB_OPTIONS);
		// Utilities.sleep(this.PAUSE_TIME);
	} catch (error) {
		info = {
			hasError : true,
			message : 'Failed in creating homework for ' + studentIds + ', '
					+ homework.workNumber + ', error:' + error
		}
		infoList.push(info);
		return infoList;
	}

	for (var i = 0; i < studentIds.length; i++) {
		info = {
			hasError : false,
			message : 'The homework ' + homework.courseId + '_'
					+ homework.workNumber + ' is created for the student:'
					+ studentIds[i]
		}
		infoList.push(info);
	}
	return infoList;
}

/**
 * Creates a new homework entry in the homework library table. Pauses a bit to
 * be sure the newly updated homework is available to the next query.
 * 
 * @param homework
 * @returns the result info of the creation
 */
DataRepository.prototype.createLibraryHomework = function(homework) {

	var info = null;
	var tableDesc = this.getTableDesc(this.HOMEWORK_LIB);
	if (tableDesc == null) {
		info = {
			hasError : true,
			message : 'Table does not exist:' + this.HOMEWORK_LIB
		}
		return info;
	}

	try {
		this.cleanAssignedHomework(homework);
		var workBlob = this.createAssignedWorkBlob(homework);
		FusionTables.Table
				.importRows(tableDesc.id, workBlob, this.BLOB_OPTIONS);
		// Utilities.sleep(this.PAUSE_TIME);
		info = {
			hasError : false,
			message : 'The homework ' + homework.courseId + '_'
					+ homework.workNumber + ' is created.'
		}
	} catch (error) {
		info = {
			hasError : true,
			message : 'Failed in creating homework ' + homework.courseId + '_'
					+ homework.workNumber + ', error:' + error
		}
	}
	return info;
}

/**
 * Creates a new homework entry in the student homework table.
 * 
 * @param homework
 * @returns the result info of the creation.
 */
DataRepository.prototype.createStudentHomework = function(homework) {
	return this.createSubmittedHomework(homework);
}

/**
 * Updates an existing homework in the student homework table.
 * 
 * @param homework
 * @returns the result info of the update
 */
DataRepository.prototype.updateStudentHomework = function(homework) {
	return this.createStudentHomework(homework);
}

/**
 * Creates a new homework entry in the homework library table.
 * 
 * @param homework
 * @returns the result info of the creation
 */
DataRepository.prototype.createAssignedHomework = function(homework) {
	return this.createLibraryHomework(homework);
}

/**
 * Updates an existing homework entry in the homework libraty table.
 * 
 * @param homework
 * @returns
 */
DataRepository.prototype.updateAssignedHomework = function(homework) {
	return this.createLibraryHomework(homework);
}

/**
 * Removes a row, which matches the where clause, from a table.
 * 
 * @param tableName
 * @param whereClause
 * @returns
 */
DataRepository.prototype.remove = function(tableName, whereClause) {

	var tableDesc = this.getTableDesc(tableName);
	if (tableDesc == null) {
		return null;
	}

	var rowid = this.findRowId(tableName, whereClause);
	if (rowid == null || rowid.length == 0) {
		return null;
	}

	var statement = "DELETE FROM " + tableDesc.id + " WHERE ROWID = "
			+ this.quotedValue(rowid);
	var result = FusionTables.Query.sql(statement, {
		hdrs : false
	});

	return rowid;
}

//
// Use the following service with caution as it replaces all the existing
// rows of a table
//
/**
 * Replaces all the existing rows of the homework library table.
 * 
 * @param tableName
 *            one of the HOMEWORK and HOMEWORK_LIB
 * @param homeworkList
 * @returns the result info of the update
 */
DataRepository.prototype.updateHomeworkTable = function(tableName, homeworkList) {

	var info = null;

	// this is only for the following tables
	if (tableName.indexOf('TEST_') < 0 && tableName.indexOf('HOMEWORK') < 0) {
		info = {
			hasError : true,
			message : 'Cannot update this table:' + tableName
		}
		return info;
	}

	// get the table description
	var tableDesc = this.getTableDesc(tableName);
	if (tableDesc == null) {
		info = {
			hasError : true,
			message : 'Table does not exist:' + tableName
		}
		return info;
	}

	// create a blob and update the table
	try {
		if (tableName.indexOf(this.HOMEWORK_LIB) >= 0) {
			var workBlob = this.createHomeworkLibTableBlob(homeworkList);
		} else {
			var workBlob = this.createHomeworkTableBlob(homeworkList);
		}
		FusionTables.Table.replaceRows(tableDesc.id, workBlob,
				this.BLOB_OPTIONS);
	} catch (error) {
		info = {
			hasError : true,
			message : 'Failed in updating table: ' + tableName + ', error:'
					+ error
		}
		return info;
	}

	info = {
		hasError : false,
		message : 'The table ' + tableName + ' is updated with '
				+ homeworkList.length + ' rows.'
	}
	return info;
}
