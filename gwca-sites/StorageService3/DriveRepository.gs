/**
 * Some commonly used file utilities.
 */
function DriveRepository() {

	this.constants = new ServiceConstants();
	this.repository = new DataRepository();

	/**
	 * Finds the course text folder by course id.
	 * 
	 * @returns
	 */
	this.getCourseTextFolder = function(courseId) {

		var folder = null;
		try {
			var courseInfo = this.repository.findCourse(courseId);
			var folderId = (courseInfo == null ? null : courseInfo.textFolderId);
			if (folderId != null) {
				folder = DriveApp.getFolderById(folderId);
			}
		} catch (error) {
			folder = null;
		}
		return folder;
	}

	/**
	 * Finds the homework work folder of the specified student.
	 * 
	 * @param studentId
	 * @returns the homework work folder
	 */
	this.getStudentWorkFolder = function(studentId) {
		var workFolder = null;
		try {
			var parentFolderId = this.constants.STUDENT_WORK_FOLDER_ID;
			var parentFolder = DriveApp.getFolderById(parentFolderId);
			var workFolders = parentFolder.getFoldersByName(studentId);
			if (workFolders.hasNext()) {
				workFolder = workFolders.next();
			}
		} catch (error) {
			workFolder = null;
		}
		return workFolder;
	}

	/**
	 * Finds the homework file by name from the student homework folder.
	 * 
	 * @param studentId
	 * @param fileName
	 * @returns the homework file
	 */
	this.getStudentWorkFile = function(studentId, fileName) {
		var workFile = null;
		try {
			var workFolder = this.getStudentWorkFolder(studentId);
			if (workFolder != null) {
				// locate the first homework file in the folder
				var files = workFolder.getFilesByName(fileName);
				if (files.hasNext()) {
					workFile = files.next();
				}
			}
		} catch (error) {
			workFile = null;
		}
		return workFile;
	}
}

/**
 * Gets the textbook content from the course textbook folder.
 * 
 * @param courseId
 * @param fileName
 * @returns
 */
DriveRepository.prototype.getCourseTextContent = function(courseId, fileName) {
	if (courseId == null || courseId.length == 0) {
		courseId = 'textbook';
		fileName = courseId + '.html';
	} else if (fileName == null || fileName.length == 0) {
		fileName = courseId + '.html';
	}
	var content = null;
	try {
		// locate the course textbook folder
		var folder = this.getCourseTextFolder(courseId);
		if (folder != null) {
			// locate the first textbook file in the folder
			var files = folder.getFilesByName(fileName);
			if (files.hasNext()) {
				var file = files.next();
				content = file.getBlob().getDataAsString();
			}
		}
	} catch (error) {
		content = null;
	}
	return content;
}

/**
 * Creates or retrieves a Google Document. For creating a new document, it opens
 * a pre-built template file and then makes a copy of it in the folder. (the
 * createFile() method with Google.MIMETYPE doesn't work, so make a copy to get
 * around the problem)
 * 
 * @param studentId
 * @param fileName
 * @param instruction
 * @returns documentURL
 */
DriveRepository.prototype.createDocumentByName = function(studentId, fileName,
		instruction) {

	var file = this.getStudentWorkFile(studentId, fileName);
	if (file != null) {
		return this.constants.GOOGLE_DOC_URL + file.getId();
	}

	try {
		var workFolder = this.getStudentWorkFolder(studentId);
		if (workFolder != null) {
			var template = DriveApp
					.getFileById(this.constants.ESSAY_TEMPLATE_ID);
			file = template.makeCopy(fileName, workFolder);
			var fileId = file.getId();
			// Append the instruction as a paragraph into the new document.
			if (instruction != null) {
				var doc = DocumentApp.openById(fileId);
				var body = doc.getBody();
				body.clear();
				body.appendParagraph(instruction);
			}
			return this.constants.GOOGLE_DOC_URL + fileId;
		}
	} catch (error) {
		Logger.log(error);
	}
	return '';
}
