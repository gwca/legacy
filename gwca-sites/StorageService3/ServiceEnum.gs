/**
 * Constants and enumerations
 * 
 * @param constants
 *            The enumerated values that is a list of name/value pairs. <br>
 *            For example: <br>
 *            MEMBER_TITLE_LIST = { <br>
 *            Mr : "Mr.", <br>
 *            Mrs : "Mrs.", <br>
 *            Miss : "Miss", <br>
 *            Ms : "Ms." }; <br>
 *            MEMBER_TITLE = new ServiceEnum(MEMBER_TITLE_LIST);
 */
function ServiceEnum(constants) {
	this.values = constants;
	for ( var i in constants) {
		var constant = constants[i];
		this[i] = constant;
	}
}

/**
 * Finds an enumerated value by name
 * 
 * @param name
 * @returns
 */
ServiceEnum.prototype.find = function(name) {
	for ( var i in this.values) {
		var value = this.values[i];
		if (name == value) {
			return this[i];
		}
	}
	return null;
};

/**
 * The list of error code
 */
var ERROR_CODE_LIST = {
	ERROR : "Error",
	WARN: "Warning",
	INFO: "Info"
};

/**
 * The roles of the school members
 */
var MEMBER_ROLE_LIST = {
	STUDENT : "Student",
	TEACHER : "Teacher",
	TA : "TA",
	ADMIN : "Administrator",
	PARENT : "Parent"
};

/**
 * The roles of a web site
 */
var SITE_ROLE_LIST = {
	OWNER : "Owner",
	EDITOR : "Editor",
	VIEWER : "Viewer"
};

/**
 * The list of the homework status
 */
var WORK_STATUS_LIST = {
	ASSIGNED : "assigned",
	WIP : "work in progress",
	SUBMITTED : "submitted",
	DONE : "done",
	UNKNOWN : "unknown"
};

/**
 * The global Enumeration of the member titles. Usage: MEMBER_TITLE.Mr
 */
ERROR_CODE = new ServiceEnum(ERROR_CODE_LIST);

/**
 * The global Enumeration of the member roles. Usage: MEMBER_ROLE.STUDENT
 */
MEMBER_ROLE = new ServiceEnum(MEMBER_ROLE_LIST);

/**
 * The global Enumeration of the site roles. Usage: SITE_ROLE.OWNER
 */
SITE_ROLE = new ServiceEnum(SITE_ROLE_LIST);

/**
 * The global Enumeration of the homework status
 * Usage: WORK_STATUS.WIP
 */
WORK_STATUS = new ServiceEnum(WORK_STATUS_LIST);
