// update the student homework table
// var APP_URL = 'https://gwca-classroom-test.appspot.com';
var APP_URL = 'https://gwca-classroom.appspot.com';
var TABLE_NAME = 'HOMEWORK_LIB'; 
// TEST_HOMEWORK, TEST_HOMEWORK_LIB, HOMEWORK, HOMEWORK_LIB

function updateHomeworkTable() {
	var service = getDataService();
	var homeworkList = service.findAll(TABLE_NAME);

	for (var i = 0; i < homeworkList.length; i++) {
		var homework = homeworkList[i];
		switch (homework.courseId) {
		case 'npcr1':
			updateNpcr1Url(homework);
			break;
		case 'npcr5':
			updateNpcr5Url(homework);
			break;
		case 'workshop':
			updateWorkshopUrl(homework);
			break;
		}
	}

	var info = service.updateHomeworkTable(TABLE_NAME, homeworkList);
	Logger.log(JSON.stringify(info, '', 2));
}

function updateNpcr1Url(homework) {
	var workitems = homework.workitems;
	for (var i = 0; i < workitems.length; i++) {
		var item = workitems[i];
		if (item.hasOwnProperty('url')) {
			var map = findNpcr1UrlMap(homework.workNumber);
			if (map != null) {
				var url = changeNpcr1Url(item.url, map);
				if (url && url != null) {
					Logger.log(homework.workNumber + ', ' + item.title
							+ '\n\tOld URL:' + item.url + '\n\tNew URL:' + url);
					item.url = url;
				}
			}
		}
	}
	return homework;
}

function updateNpcr5Url(homework) {
	var workitems = homework.workitems;
	for (var i = 0; i < workitems.length; i++) {
		var item = workitems[i];
		if (item.hasOwnProperty('url')) {
			var ids = homework.workNumber.split('_');
			var lessonId = ids[0];
			var chapterId = ids[1];
			var url = changeNpcr5Url(item.url, lessonId, chapterId);
			if (url && url != null) {
				Logger.log(homework.workNumber + ', ' + item.title
						+ '\n\tOld URL:' + item.url + '\n\tNew URL:' + url);
				item.url = url;
			}
		}
	}
	return homework;
}

function updateWorkshopUrl(homework) {
	var workitems = homework.workitems;
	for (var i = 0; i < workitems.length; i++) {
		var item = workitems[i];
		if (item.hasOwnProperty('url')) {
			var ids = homework.workNumber.split('_');
			var chapterId = ids[1];
			var url = changeWorkshopUrl(item.url, chapterId);
			if (url && url != null) {
				Logger.log(homework.workNumber + ', ' + item.title
						+ '\n\tOld URL:' + item.url + '\n\tNew URL:' + url);
				item.url = url;
			}
		}
	}
	return homework;
}

function changeNpcr1Url(url, map) {
	if (url.indexOf(map.pdfurl) > 0 || url.indexOf(map.readurl) > 0) {
		return APP_URL + '?type=course&course=npcr1&lesson=' + map.lessonId
				+ '&chapter=' + map.chapterId;
	}

	if (url.indexOf('builder.html') > 0) {
		return APP_URL + '?type=tool&course=npcr1&lesson=' + map.lessonId
				+ '&chapter=' + map.chapterId;
	}

	if (map.hasOwnProperty('moreurl') && url.indexOf(map.moreurl) > 0) {
		return APP_URL + '?type=course&course=npcr1&lesson=' + map.lessonId
				+ '&chapter=' + map.moreChapterId;
	}
	return null;
}

function findNpcr1UrlMap(workId) {
	for (var i = 0; i < NPCR1_URL_MAP.length; i++) {
		var map = NPCR1_URL_MAP[i];
		if (map.workId == workId) {
			return map;
		}
	}
	return null;
}

function changeNpcr5Url(url, lessonId, chapterId) {
	var oldUrl = 'greatwallchineseacademy.org/site-media-lib';
	var newUrl = 'gwca-classroom.appspot.com/books/npcr5/npcr5.html';
	if (url.indexOf(oldUrl) > 0 || url.indexOf(newUrl)) {
		return APP_URL + '?type=course&course=npcr5&lesson=' + lessonId
				+ '&chapter=' + chapterId;
	}
	return null;
}

function changeWorkshopUrl(url, chapterId) {
	var textUrl = 'greatwallchineseacademy.org/site-media-lib/?course=workshop';
	var readUrl = 'gwca-classroom.appspot.com/books/reading/reading.html';
	if (url.indexOf(textUrl) > 0) {
		var idx = url.indexOf('title=');
		var lessonId = url.substring(idx + 6);
		lessonId = changeTitle(lessonId);
		return APP_URL + '?type=course&course=workshop&lesson=' + lessonId
				+ '&chapter=' + chapterId;
	}
	if (url.indexOf(readUrl) > 0) {
		var idx = url.indexOf('name=');
		var lessonId = url.substring(idx + 5);
		return APP_URL + '?type=course&course=reading&lesson=' + lessonId
				+ '&chapter=1';
	}
	return null;
}

function changeTitle(lessonId) {
	switch (lessonId) {
	case 'chanpion':
		return 'champion';
	case 'proj-gwca-and-me':
		return 'gwcaAndMe';
	default:
		return lessonId;
	}
}

function getDataService() {
	return new DataRepository();
}
