/**
 * Implements the required method doGet()
 */
function doGet() {
	var pageName = 'editor-ui';
	var htmlOutput = HtmlService.createTemplateFromFile(pageName).evaluate();
	htmlOutput.setSandboxMode(HtmlService.SandboxMode.IFRAME);
	return htmlOutput;
}

/**
 * Includes an additional HTML page into the template page, such as a style sheet
 * or a script file.
 * 
 * @param filename
 * @returns
 */
function include(filename) {
	var htmlOutput = null;
	try {
		htmlOutput = HtmlService.createTemplateFromFile(filename).evaluate();
	} catch (error) {
		return '<p>Failed loading file ' + filename + ':' + error + '</p>';
	}
	htmlOutput.setSandboxMode(HtmlService.SandboxMode.IFRAME);
	return htmlOutput.getContent();
}

/**
 * Includes an additional HTML page into the template page, such as a style or a
 * script file.
 * 
 * @param filename
 * @returns
 */
function includeTemplate(filename) {
	return Workitem.include(filename);
}

/**
 * Reads the homework content in JSON by name from the course homework folder.
 * 
 * @param courseId
 * @param workNumber
 * @returns the content of the homework in JSON.
 */
function load(courseId, workNumber) {
	var service = getService();
	var homework = service.getAssignedHomework(courseId, workNumber);
	return JSON.stringify(homework);
}

/**
 * Saves or updates the homework
 * 
 * @param homeworkJson
 * @returns status
 */
function save(homeworkJson) {
	var service = getService();
	var homework = JSON.parse(homeworkJson);
	var savedHomework = service.saveAssignedHomework(homework);
	return JSON.stringify(savedHomework);
}

/**
 * Gets the homework title list of a course.
 * 
 * @param courseId
 *            the course ID for retrieving the assigned homework titles
 * @returns list of titleInfo objects
 */
function getHomeworkTitleList(courseId) {
	var service = getService();
	var titleList = service.getHomeworkTitleList(courseId);
	return JSON.stringify(titleList);
}

/**
 * Creates a new HomeworkService instance
 * 
 * @returns {HomeworkService}
 */
function getService() {
	return new StorageService3.DataService();
}
