/**
 * Implements the required method doGet().
 */
function doGet() {
	var pageName = 'course-work-ui';
	var htmlOutput = HtmlService.createTemplateFromFile(pageName).evaluate();
	htmlOutput.setSandboxMode(HtmlService.SandboxMode.IFRAME);
	return htmlOutput;
}

/**
 * Includes an additional HTML page into the template page, such as a style or a
 * script file. Since HtmlService.createTemplateFromFile can only read file from
 * the current directory, there is no way to make this method as part of a
 * backend service.
 * 
 * @param filename
 * @returns
 */
function include(filename) {
	var htmlOutput = null;
	try {
		htmlOutput = HtmlService.createTemplateFromFile(filename).evaluate();
		htmlOutput.setSandboxMode(HtmlService.SandboxMode.IFRAME);
		return htmlOutput.getContent();
	} catch (error) {
		return '<p>Failed loading file ' + filename + ':' + error + '</p>';
	}
}

/**
 * Gets the class/course/student relationship
 */
function getActiveClassCourseMembers() {
		
	var service = getHomeworkService();
	var isOwner = service.isOwner();
	var userEmail = service.getUserEmail();
	
	// for better performance, try to get the pre-built relationships from the
	// script properties and if it is not built yet, get them from the tables.
	var propertyName = "CLASS-COURSE-MEMBER-RELATIONSHIP";
	var scriptProps = PropertiesService.getScriptProperties();
	var relationships = scriptProps.getProperty(propertyName);
	if (relationships != null) {
		relationships = JSON.parse(relationships);
		relationships.isOwner = isOwner;
		relationships.userEmail = userEmail;
		return JSON.stringify(relationships);
	}
	
	// build the relationships by querying the tables and save them as
	// a script property for later use.
	service = getMemberService();
	relationships = service.getActiveClassCourseMembers();
	relationships.isOwner = isOwner;
	relationships.userEmail = userEmail;
	relationships = JSON.stringify(relationships);
	scriptProps.setProperty(propertyName, relationships);
	return relationships;
}

/**
 * Removes the script property to refresh the landing page with the newly updated data.
 * @returns
 */
function getNewlyUpdatedInfo() {
	var propertyName = "CLASS-COURSE-MEMBER-RELATIONSHIP";
	var scriptProps = PropertiesService.getScriptProperties();
	scriptProps.deleteProperty(propertyName);
	return getActiveClassCourseMembers();
}

/**
 * Creates a new HomeworkService instance
 * 
 * @returns {StorageService3.HomeworkService}
 */
function getHomeworkService() {
	return new StorageService3.HomeworkService();
}

/**
 * Creates a new MemberService instance
 * 
 * @returns {StorageService3.MemberService}
 */
function getMemberService() {
	return new StorageService3.MemberService();
}
