/**
 * Implements the required method doGet()
 */
function doGet() {
	var pageName = 'homework-list-ui';
	var htmlOutput = HtmlService.createTemplateFromFile(pageName).evaluate();
	htmlOutput.setSandboxMode(HtmlService.SandboxMode.IFRAME);
	return htmlOutput;
}

/**
 * Includes an additional HTML page into the template page, such as a style or a
 * script file. Since HtmlService.createTemplateFromFile can only read file from
 * the current directory, there is no way to make this method as part of a
 * backend service.
 * 
 * @param filename
 * @returns
 */
function include(filename) {
	var htmlOutput = null;
	try {
		htmlOutput = HtmlService.createTemplateFromFile(filename).evaluate();
		htmlOutput.setSandboxMode(HtmlService.SandboxMode.IFRAME);
		return htmlOutput.getContent();
	} catch (error) {
		return '<p>Failed loading file ' + filename + ':' + error + '</p>';
	}
}

/**
 * Reads the homework page to get a list of sub-pages.
 * 
 * @returns a list of page info objects in JSON.
 */
function getHomeworkList() {
	var service = getService();
	var infoList = service.getHomeworkList();
	return JSON.stringify(infoList);
}

/**
 * Creates a new SitesService instance
 * 
 * @returns {SitesService}
 */
function getService() {
	return new StorageService3.WorkSiteService();
}
