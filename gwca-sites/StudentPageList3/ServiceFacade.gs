/**
 * Implements the required method doGet()
 */
function doGet() {
	var pageName = 'student-list-ui';
	var htmlOutput = HtmlService.createTemplateFromFile(pageName).evaluate();
	htmlOutput.setSandboxMode(HtmlService.SandboxMode.IFRAME);
	return htmlOutput;
}

/**
 * Includes an additional HTML page into the template page, such as a style or a
 * script file. Since HtmlService.createTemplateFromFile can only read file from
 * the current directory, there is no way to make this method as part of a
 * backend service.
 * 
 * @param filename
 * @returns
 */
function include(filename) {
	var htmlOutput = null;
	try {
		htmlOutput = HtmlService.createTemplateFromFile(filename).evaluate();
		htmlOutput.setSandboxMode(HtmlService.SandboxMode.IFRAME);
		return htmlOutput.getContent();
	} catch (error) {
		return '<p>Failed loading file ' + filename + ':' + error + '</p>';
	}
}

/**
 * Reads the homework page to get a list of sub-pages.
 * 
 * @returns a list of page info objects in JSON.
 */
function getStudentPageList() {

	var service = getHomeworkService();
	var isOwner = service.isOwner();
	
	// for better performance, try to get the pre-built student list from the
	// script properties and if it is not built yet, call the service to get it.
	var propertyName = "COURSE-WORK-STUDENT-LIST";
	var scriptProps = PropertiesService.getScriptProperties();
	var students = scriptProps.getProperty(propertyName);
	if (students != null) {
		students = JSON.parse(students);
		students.isOwner = isOwner;
		return JSON.stringify(students);
	}

	// build the student list and save it as a script property for later use.
	service = getSiteService();
	students = service.getStudentPageList();
	students.isOwner = isOwner;
	students = JSON.stringify(students);
	scriptProps.setProperty(propertyName, students);
	return students;
}

function getUpdatedList() {
	var propertyName = "COURSE-WORK-STUDENT-LIST";
	var scriptProps = PropertiesService.getScriptProperties();
	scriptProps.deleteProperty(propertyName);
	return getStudentPageList();
}

/**
 * This is for testing only
 * @returns
 */
function getStudentHomeworkInfo() {
	var service = getSiteService();
	var infoList = service.getStudentHomeworkInfo();
	return infoList;
}

/**
 * Creates a new HomeworkService instance
 * 
 * @returns {StorageService3.HomeworkService}
 */
function getHomeworkService() {
	return new StorageService3.HomeworkService();
}

/**
 * Creates a new WorkSiteService instance
 * 
 * @returns {WorkSiteService}
 */
function getSiteService() {
	return new StorageService3.WorkSiteService();
}
