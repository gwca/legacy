/**
 * Initializes the HTML page.
 */
function doGet() {
	var pageName = 'homework-ui';
	var htmlOutput = null;
	try {
		htmlOutput = HtmlService.createTemplateFromFile(pageName).evaluate();
		htmlOutput.setSandboxMode(HtmlService.SandboxMode.IFRAME);
	} catch (error) {
		htmlOutput = '<p>Failed loading homework with an error of ' + error
				+ ', contact the teacher please.</p>';
	}
	return htmlOutput;
}

/**
 * Includes an additional HTML page into the template page, such as a style or a
 * script file. Note that the HtmlService.createTemplateFromFile() method can
 * only read a file from the current directory.
 * 
 * @param filename
 * @returns
 */
function include(filename) {
	var htmlOutput = null;
	try {
		htmlOutput = HtmlService.createTemplateFromFile(filename).evaluate();
		htmlOutput.setSandboxMode(HtmlService.SandboxMode.IFRAME);
		return htmlOutput.getContent();
	} catch (error) {
		return '<p>Failed loading file ' + filename + ':' + error + '</p>';
	}
}

/**
 * Includes an additional HTML page into the template page from the work item
 * component that is a library.
 * 
 * @param filename
 * @returns
 */
function includeTemplate(filename) {
	return Workitem.include(filename);
}

/**
 * Loads the assigned or saved homework from either the course's or a student's
 * homework folder.
 * 
 * @returns the homework in JSON string format.
 */
function load() {
	var service = getHomeworkService();
	var homework = service.load();
	return JSON.stringify(homework);
}

/**
 * Saves or updates the homework
 * 
 * @param homeworkJson
 * @returns status
 */
function save(homeworkJson) {
	var service = getHomeworkService();
	var homework = JSON.parse(homeworkJson);
	var status = service.save(homework);
	return JSON.stringify(status);
}

/**
 * Creates a Google Document for writing an essay.
 * 
 * @returns the URL of the Google Document
 */
function createDocumentID() {
	var service = getHomeworkService();
	var studentId = SitesApp.getActiveSite().getName();
	var fileName = SitesApp.getActivePage().getName() + '.doc';
	var homework = service.createDocument(studentId, fileName);
	return JSON.stringify(homework);
}

/**
 * Checks if the current user is the owner of the homework file (usually it is
 * the teacher).
 * 
 * @returns {Boolean}
 */
function isOwner() {
	var service = getHomeworkService();
	return service.isOwner();
}

/**
 * Sends a simple email to the teacher to notify him/her the homework is submitted.
 */
function notifyTeacher() {
	var service = getHomeworkService();
	service.notifyTeacher();
}

/**
 * Sends a simple email to the student to notify him/her the teacher has
 * checked the homework.
 */
function notifyStudent() {
	var service = getHomeworkService();
	return service.notifyStudent();
}

/**
 * Gets the first attached audio file from the current page.
 * 
 * @returns The URL of the first attached audio clip
 */
function getFirstAudioClip() {
	var service = getHomeworkService();
	return service.getFirstAudioClip();
}

/**
 * Gets the first attached image file from the current page.
 * 
 * @returns The URL of the first attached image
 */
function getFirstImage() {
	var service = getHomeworkService();
	return service.getFirstImage();
}

/**
 * Creates a new HomeworkService instance
 * 
 * @returns {HomeworkService}
 */
function getHomeworkService() {
	return new StorageService3.HomeworkService();
}

//
// The following functions are for unit testing only
/**
 * Sends a simple email to the teacher to notify him the homework is submitted.
 */
function notifyTeacherWithId(studentId, page) {
	var service = getHomeworkService();
	return service.notifyTeacher(studentId, page);
}

/**
 * For testing only
 * 
 * @param studentId
 * @param courseId
 * @param workNumber
 * @returns
 */
function loadFile(studentId, courseId, workNumber) {
	var service = getHomeworkService();
	var homework = service.loadFile(studentId, courseId, workNumber);
	return JSON.stringify(homework);
}
