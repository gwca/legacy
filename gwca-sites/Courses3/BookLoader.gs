/**
 * Loads the specified textbook file. The URL looks like this<br>
 * https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib?course=npcr5&title=NPCR5-51
 * 
 * @request the HTTP request
 * @returns
 */
function doGet(request) {
	var course = request.parameter.course;
	var title = request.parameter.title;
	var page = createPageCode(course, title);
	var htmlOutput = null;
	try {
		htmlOutput = HtmlService.createHtmlOutput(page);
		// TODO define the height of the page and set it. Does not work by
		// calling setHeight(). 
		// htmlOutput = htmlOutput.setHeight(100);
	} catch (error) {
		var code = '<p>Error: ' + error + '</p>';
		htmlOutput = HtmlService.createHtmlOutput(code);
	}
	htmlOutput.setSandboxMode(HtmlService.SandboxMode.IFRAME);
	return htmlOutput;
}

/**
 * Creates the course page with the content from Drive
 * 
 * @param course
 * @param title
 * @returns {String}
 */
function createPageCode(course, title) {
	pageCode = null;
	var content = load(course, title);
	if (course == null || course.length == 0 || course == 'textbook') {
		content += createTocCode();
	}
	var script = HtmlService.createHtmlOutputFromFile('book-js').getContent();
	var style = HtmlService.createHtmlOutputFromFile('book-css').getContent();
	var dictionary = HtmlService.createHtmlOutputFromFile('dictionary').getContent();
	var wordEntry = HtmlService.createHtmlOutputFromFile('book-word-entry').getContent();

	pageCode = '<!DOCTYPE html>';
	pageCode += '<html>';
	pageCode += '<head>';
	pageCode += '<base target="_top"';
	pageCode += 'href="https://sites.google.com/a/greatwallchineseacademy.org/site-media-lib/">';
	pageCode += script;
	pageCode += dictionary;
	pageCode += style;
	pageCode += '</head>';
	pageCode += '<body>';
	pageCode += wordEntry;
	pageCode += '<div id="course-content" class="course-content">';
	pageCode += content;
	pageCode += '</div>';
	pageCode += '</body>';
	pageCode += '</html>';
	return pageCode;
}

/**
 * Creates a JSON object from the courses list. This object will be used by the
 * client side java script to generate HTML tags to display TOC.
 * 
 * @returns {String}
 */
function createTocCode() {
	var repository = getDataRepository();
	var toc = repository.findCourseInfoList();
	var tocCode = "<div id='toc-json' style='display: none;'>";
	tocCode += JSON.stringify(toc, '', 2);
	tocCode += '</div>';
	tocCode += '<script>createToc();</script>';
	return tocCode;
}

/**
 * Loads the course textbook file from Drive.
 * 
 * @param courseId
 * @param filename
 * @returns
 */
function load(courseId, filename) {
	if (filename != null && filename.indexOf('.html') < 0) {
		filename += '.html';
	}
	var repository = getDriveRepository();
	var content = repository.getCourseTextContent(courseId, filename);
	if (content == null || content.length == 0) {
		content = '<span style="font-weight:bold; color:Brown; font-size:20px;">';
		content += '<p>Either the course or the title does not exist.</p>';
		content += '<p>Check the URL parameters (?course=courseId&title=title)</p>';
		content += '<p>course:' + courseId + ', title:' + filename + '</p>';
		content += '</span>';
		content += '<div class="back-home">';
		content += '<a href=".">Course Index</a></div>';
	}
	return content;
}

/**
 * Creates a new DriveRepository instance
 * 
 * @returns {DriveRepository}
 */
function getDriveRepository() {
	return new StorageService3.DriveRepository();
}

/**
 * Creates a new DataRepository instance
 * 
 * @returns {DataRepository}
 */
function getDataRepository() {
	return new StorageService3.DataRepository();
}
