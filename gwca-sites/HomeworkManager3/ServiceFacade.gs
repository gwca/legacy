/**
 * The homework management utilities to assign homework and manage the course
 * work pages of the students. This is the service facade for the GUI
 * application (basically those java scripts) to locate and use the backend
 * services (e.g. Google Apps Scripts).
 * 
 * @returns
 */
function doGet() {
	var htmlOutput = null;
	var pageName = 'manager-ui';
	try {
		htmlOutput = HtmlService.createTemplateFromFile(pageName).evaluate();
		htmlOutput.setSandboxMode(HtmlService.SandboxMode.IFRAME);
	} catch (error) {
		htmlOutput = 'Failed loading the homework manager: ' + error;
	}
	return htmlOutput;
}

/**
 * Includes an additional HTML page into the main HTML page, such as a style or
 * a script file. Since HtmlService.createTemplateFromFile can only read file
 * from the current directory, there is no way to make this method as part of a
 * backend service.
 * 
 * @param filename
 * @returns
 */
function include(filename) {
	var htmlOutput = null;
	try {
		htmlOutput = HtmlService.createTemplateFromFile(filename).evaluate();
		htmlOutput.setSandboxMode(HtmlService.SandboxMode.IFRAME);
		return htmlOutput.getContent();
	} catch (error) {
		return '<p>Failed loading file ' + filename + ':' + error + '</p>';
	}
}

/**
 * Includes an additional HTML page into the template page from the work item
 * component that is a library.
 * 
 * @param filename
 * @returns
 */
function includeTemplate(filename) {
	return Workitem.include(filename);
}

/**
 * Assigns homework to a student
 * 
 * @param memberId
 * @param courseId
 * @param workNumber
 * @param title
 *            the homework title
 */
function assign(memberId, courseId, workNumber, title) {
	var service = getWorkSiteService();
	var info = service.assign(memberId, courseId, workNumber, title);
	return JSON.stringify(info);
}

/**
 * Assigns the specified homework to all the students
 * 
 * @param members the class members list (comma delimited string)
 * @param courseId
 * @param workNumber
 * @param title
 *            the homework title
 */
function assignAll(members, courseId, workNumber, title) {
	var service = getWorkSiteService();
	var info = service.assignAll(members, courseId, workNumber, title);
	return JSON.stringify(info);
}

/**
 * Notifies the class members about the latest homework assignment.
 * 
 * @param className
 * @param courseId
 * @param workNumber
 */
function notify(className, courseId, workNumber) {
	var service = getWorkSiteService();
	var info = service.notify(className, courseId, workNumber);
	return JSON.stringify(info);
}

/**
 * Previews the selected homework.
 * 
 * @param courseId
 * @param workNumber
 */
function preview(courseId, workNumber) {
	var service = getDataService();
	var homework = service.getAssignedHomework(courseId, workNumber);
	return JSON.stringify(homework);
}

/**
 * Gets list of info objects that show the homework status of each student.
 * 
 * @param className
 * @param courseId
 * @param workNumber
 */
function status(className, courseId, workNumber) {
	var service = getWorkSiteService();
	var info = service.status(className, courseId, workNumber);
	return JSON.stringify(info);
}

/**
 * Gets the homework title list of a course.
 * 
 * @param courseId
 *            the course ID for retrieving the assigned homework titles
 */
function getHomeworkTitleList(courseId) {
	var dataService = getDataService();
	var titleList = dataService.getHomeworkTitleList(courseId);
	return JSON.stringify(titleList);
}

/**
 * Gets a list of the active classes.
 */
function getActiveClasses() {
	var service = getMemberService();
	var classMembers = service.getActiveClasses();
	return JSON.stringify(classMembers);
}

/**
 * Gets a list of members of the class with the detailed info of each member.
 * 
 * @param className
 * @returns
 */
function getClassMemberSet(className) {
	var service = getMemberService();
	var members = service.getClassMemberSet(className);
	return JSON.stringify(members);
}

/**
 * Creates the course work pages for the students
 * 
 * @param memberIDs
 * @returns
 */
function createCourseWorkPages(memberIDs) {
	var service = getWorkSiteService();
	var info = service.createCourseWorkPages(memberIDs);
	return JSON.stringify(info);
}

/**
 * Updates a course work page for the students
 * 
 * @param memberIDs
 * @param templateName
 * @returns
 */
function updateCourseWorkPages(memberIDs, templateName) {
	var service = getWorkSiteService();
	var info = service.updateCourseWorkPages(memberIDs, templateName);
	return JSON.stringify(info);
	// var info = {
	// code : 'info',
	// message : 'All the pages are updated using ' + templateName
	// };
}

/**
 * Creates a new WorkSiteService instance
 * 
 * @returns {WorkSiteService}
 */
function getWorkSiteService() {
	return new StorageService3.WorkSiteService();
}

/**
 * Creates a new DataService instance
 * 
 * @returns {DataService}
 */
function getDataService() {
	return new StorageService3.DataService();
}

/**
 * Creates a new MemberService instance
 * 
 * @returns {MemberService}
 */
function getMemberService() {
	return new StorageService3.MemberService();
}
