var WORDS = [ 
		{
			id : 'ren-human',
			image : 'ren_human.gif',
			imageId : '0B9-duwm5My1fS1ptbzlWVUZZQ3c',
			pinyin : 'Rén',
			phrase1 : '男人 女人 好人 坏人 人口',
			phrase2 : '判若两人 旁若无人<br>前人栽树，后人乘凉',
			paragraph : '在中国古代的神话传说里，人是女娲用泥土造的。在西方的宗教故事中，上帝创造了人。而根据达尔文的进化论，人是从一种古猿进化来的。在中文里，人字的一撇一捺组成了一个简单而稳定的结构。就在这简单稳定的结构后面，有着无数的酸甜苦辣，悲欢离合。',
			dayOfMonth : 0
		},
		{
			id : 'shou-hand',
			image : 'shou_hand.gif',
			imageId : '0B9-duwm5My1fdjhtT3NPRG9XbmM',
			pinyin : 'Shǒu',
			phrase1 : '双手 手指 手段 手势 把手',
			phrase2 : '手疾眼快 手舞足蹈 人多手杂',
			paragraph : '人和其它动物的一个重要区别是人站着走路，两只手解放了出来，从而就有了我们今天这个到处是人工痕迹的世界。有人手笨，有人手巧，有人手重，有人手轻，有的人手没轻没重，还有人长了三只手。',
			dayOfMonth : 0
		},
		{
			id : 'dao-knife',
			image : 'dao_knife.gif',
			imageId : '0B9-duwm5My1fVXJJckU3b3gyaUk',
			pinyin : 'Dāo',
			phrase1 : '餐刀 菜刀 刀刃 捉刀',
			phrase2 : '手起刀落 刀光剑影 两面三刀',
			paragraph : '如果说刀不是人类使用的第一种工具的话，它也肯定是头几种之一。砍柴打猎，防身护院，互相残杀，哪一样也离不开刀。人从使用刀开始学会了使用工具从而主宰整个世界。',
			dayOfMonth : 0
		},
		{
			id : 'kou-mouth',
			image : 'kou_mouth.gif',
			imageId : '0B9-duwm5My1fZHNWRjVBU25NTDg',
			pinyin : 'Kǒu',
			phrase1 : '一口 口水 入口 进口 出口',
			phrase2 : '众口一词 张口结舌 口蜜腹剑',
			paragraph : '人的这张口，除了吃饭以外，最重要的要数说话了，笨嘴拙舌的有，灵牙利齿的也有。当年孔明在东吴舌战群儒，在阵前骂死王朗，凭的就是一张利口。不过吃饭讲话时，还是不要忘了病从口入，祸从口出的道理。',
			dayOfMonth : 0
		},
		{
			id : 'tian-field',
			image : 'tian_field.gif',
			imageId : '0B9-duwm5My1fc2dyRVZoeHJ5Wlk',
			pinyin : 'Tián',
			phrase1 : '田野 田地 水田 油田',
			phrase2 : '沧海桑田 焚林而田，竭泽而渔',
			paragraph : '小时候放过牛。早上牵一条大水牛顺着田埂走进绿油油的稻田，看着它悠闲地吃草，那日子 ......',
			dayOfMonth : 0
		},
		{
			id : 'mu-wood',
			image : 'mu_wood.gif',
			imageId : '0B9-duwm5My1fLXV3YTEwVTc2Z1k',
			pinyin : 'Mù',
			phrase1 : '木器 树木 木材 积木',
			phrase2 : '木已成舟 呆若木鸡 草木皆兵',
			paragraph : '木星是太阳系九大行星之一，按离太阳远近的次序是第五颗，在九大行星中是体积最大的一颗。它的体积是地球的1,295倍,	有十四颗卫星。中国古代把木星称为岁星。',
			dayOfMonth : 0
		},
		{
			id : 'tu-earth',
			image : 'tu_earth.gif',
			imageId : '0B9-duwm5My1fTm1lQ3JjWUZneGM',
			pinyin : 'Tǔ',
			phrase1 : '土地 黄土 土气 水土 领土',
			phrase2 : '土生土长 乡土气息 风土人情',
			paragraph : '"我家住在黄土高坡，大风从坡上刮过。不管是东南风还是西北风，都是我的歌，我的歌"。这首充满乡土气息的歌曲想必是一位土生土长的陕北人写的，你都能感觉到那呼啸而过的狂风，扑面而来的黄土和那随风飘荡的歌。',
			dayOfMonth : 0
		},
		{
			id : 'huo-fire',
			image : 'huo_fire.gif',
			imageId : '0B9-duwm5My1faTA1TWtiMU9CVms',
			pinyin : 'Hǔo',
			phrase1 : '火焰 发火 放火 上火 走火',
			phrase2 : '隔岸观火 刀山火海 火烧眉毛',
			paragraph : '做饭烧菜，什么最重要？刀功，不对。食材，也不对。作料，越来越离谱了。火候！中国菜里的煎炒烹炸，清蒸水煮，那区别和水平全反映在火候的掌握上。时间，温度，距离以及燃料地选择，哪样错了也不行。没听人说吗，那火候，绝了。',
			dayOfMonth : 0
		},
		{
			id : 'shi-rock',
			image : 'shi_rock.gif',
			imageId : '0B9-duwm5My1fdW1fWHFrNFNwMkk',
			pinyin : 'Shí',
			phrase1 : '石头 岩石 石油 石器',
			phrase2 : '投石问路 落井下石<br>它山之石，可以攻玉',
			paragraph : '除了水以外，世界上最多的就是石头了。按种类分有坚硬的岩石，溜圆的鹅卵石，晶莹温润的玉石和价值连城的金刚石。按用途分有铺路的渣石，盖房用的基石和立传用的碑石。什么用也没有但却大大有名的东海普陀石，黄山飞来石以及青梗峰下那块无才去补天，枉入红尘的顽石。',
			dayOfMonth : 0
		},
		{
			id : 'shan-hill',
			image : 'shan_hill.gif',
			imageId : '0B9-duwm5My1fa3piTDB4WUF2UjQ',
			pinyin : 'Shān',
			phrase1 : '高山 山峰 山沟 山人 侃大山',
			phrase2 : '山清水秀 人山人海 山不转水转<br>车到山前必有路',
			paragraph : '从前有座山，山里有座庙，庙里有个老头在讲故事。讲的什么故事呢？从前有座山，山里有座庙 ...	.... 这山肯定是座仙山，庙想必是座古刹，那老头也一定是位有道之人。要不然这故事怎么讲了那么多年还在讲呢。',
			dayOfMonth : 0
		},
		{
			id : 'sheng-raw',
			image : 'sheng_raw.gif',
			imageId : '0B9-duwm5My1fc254X1pNbldoSGc',
			pinyin : 'Shēng',
			phrase1 : '生命 生产 生字 学生 生育',
			phrase2 : '妙趣横生 生而知之 三生有幸',
			paragraph : '还记得头一回用筷子的情景吗，干着急什么也夹不起来吧。几次以后就好得多了，一回生两回熟吗。用筷子夹过鸽子蛋吗，那得有点巧劲才行，不过熟能生巧，练练还是能行的。试过用象牙筷子从有水的瓶子里往外夹玻璃球吗，一般人到最后都只好抓起瓶子来把球倒出来解气。',
			dayOfMonth : 0
		},
		{
			id : 'ri-sun',
			image : 'ri_sun.gif',
			imageId : '0B9-duwm5My1fUlhVRi1od1lxUm8',
			pinyin : 'Rì',
			phrase1 : '日夜 日子 日光 日记',
			phrase2 : '日理万机 狂犬吠日<br>日有所思，夜有所梦',
			paragraph : '“坐地日行八万里，巡天遥看一千河。”古人骑马，今人驾车，日行千里，已穷其力。诗人乘暇想之风，坐地巡天，可谓潇洒之极。',
			dayOfMonth : 0
		},
		{
			id : 'yue-moon',
			image : 'yue_moon.gif',
			imageId : '0B9-duwm5My1fUWFnWHl2X2hGc1U',
			pinyin : 'Yuè',
			phrase1 : '月亮 七月 月饼 新月',
			phrase2 : '花好月圆 月下老人 嫦娥奔月',
			paragraph : '蓝色天空银河水，有只小白船。船上有棵桂花树，还有小白兔。没有桅杆没有帆，木浆也没有，走呀走呀，向着西天游。',
			dayOfMonth : 0
		},
		{
			id : 'mu-eye',
			image : 'mu_eye.gif',
			imageId : '0B9-duwm5My1feGdJVlktQVFJMU0',
			pinyin : 'Mù',
			phrase1 : '目标 目的 节目 耳目 盲目',
			phrase2 : '鱼目混珠 目空一切 鼠目寸光',
			paragraph : '当一个人能克制自己，不去看那些想看而不应该看或不敢看的东西时，我们说此人能目不斜视。能在众目睽睽下目不斜视确实让人侧目而视，但对着琳琅满目，令人头晕目眩的新奇玩艺，不看上一眼不是太可惜了吗。',
			dayOfMonth : 0
		},
		{
			id : 'yun-cloud',
			image : 'yun_cloud.gif',
			imageId : '0B9-duwm5My1fUWFnWHl2X2hGc1U',
			pinyin : 'Yūn',
			phrase1 : '云彩 云集 云游 云雾',
			phrase2 : '人云亦云 云雾缭绕 不知所云',
			paragraph : '第一次看日出是在北戴河。清早起来兴冲冲地赶到鸽子窝，可天海之际的一抹白云挡住了日出的美景。第二次是在黄山，早起去看云海日出。可那天偏偏无云，只有清清的山岚。最后一次是登泰山观日出，去得太早了些，坐在那竟睡着了。猛然醒时只见云雾之中昏昏然惨白的一个太阳。暗自庆幸这一觉倒也没白睡。',
			dayOfMonth : 0
		},
		{
			id : 'chi-ruler',
			image : 'chi_ruler.gif',
			imageId : '0B9-duwm5My1fcmZYOFY0WUhfNHM',
			pinyin : 'chǐ',
			phrase1 : '尺子 尺度 戒尺 尺寸',
			phrase2 : '近在咫尺 得寸进尺<br>尺有所短，寸有所长',
			paragraph : '记得上小学时，铅笔盒里有一把短短的蛋黄色的木尺。黑色的线条一边标的是厘米，一边是寸。尺子长六寸，等于二十厘米。可是一直到尺子都用坏了，还不知道一寸到底等于几厘米。',
			dayOfMonth : 0
		},
		{
			id : 'shang-up',
			image : 'shang_up.gif',
			imageId : '0B9-duwm5My1fX2p5NmNQQ2t4c00',
			pinyin : 'Shàng',
			phrase1 : '上面 天上 上午 穿上 上火',
			phrase2 : '蒸蒸日上 上方宝剑 箭在弦上<br>登鼻子上脸',
			paragraph : '上和下是反意词，但常常放在一起使用。上知天文，下知地理，形容这人知道的多。下山两腿发抖时说上山容易下山难。遇事心里发慌，不知所措时说十五个吊桶打水，七上八下。题作不出来时说卡住了，那就是不上不下，上不去也下不来的意思。',
			dayOfMonth : 0
		},
		{
			id : 'xia-down',
			image : 'xia_down.gif',
			imageId : '0B9-duwm5My1fQUJfTUw3R19vWWc',
			pinyin : 'Xìa',
			phrase1 : '下去 下手 手下 下巴',
			phrase2 : '对症下药 下里巴人 下不为例<br>三下五除二',
			paragraph : '他下意识地扶了下眼镜，又朝四下看了看，然后从容地走下楼梯，出门向街上走去。望着他渐渐消失在人群中的背影，我一下想起了一个人，那不是那谁吗，那年在乡下的时候，……',
			dayOfMonth : 0
		},
		{
			id : 'da-big',
			image : 'da_big.gif',
			imageId : '0B9-duwm5My1fWVFrU0lJdFdWZTA',
			pinyin : 'Dà',
			phrase1 : '大家 大方 大学 长大',
			phrase2 : '大喜过望 大材小用 宽宏大量',
			paragraph : '无论是体格，力量和能力，鹅和牛都不能相比。可是鹅见到人时总是那么趾高气扬的，而牛却永远是一副俯首甘为的样子。据说这里的原因是在鹅和牛眼睛的区别上。鹅眼小，把人也看小了，所以鹅永远是藐视人这种小东西的。牛眼大，把人也看大了，所以牛对人总是毕恭毕敬的。',
			dayOfMonth : 0
		},
		{
			id : 'xiao-small',
			image : 'xiao_small.gif',
			imageId : '0B9-duwm5My1fNUY3MGVabE9sdHM',
			pinyin : 'Xǐao',
			phrase1 : '小心 小丑 小子 家小 小器',
			phrase2 : '小巧玲珑 略施小计<br>大事化小，小事化了',
			paragraph : '我轻轻地敲着门，一个小姑娘开了门，出来抬头看见我，先愣了一下，然后微笑着，招手叫我进去。门边一个小炭炉，上面放着一个小沙锅，微微地冒着热气。这小姑娘指着炉前的小板凳让我坐了，她自己就蹲在我旁边。不住地打量我。',
			dayOfMonth : 0
		},
		{
			id : 'fang-square',
			image : 'fang_square.gif',
			imageId : '0B9-duwm5My1fZm1FemNtOHN6UFE',
			pinyin : 'Fāang',
			phrase1 : '方向 长方 方面 方才',
			phrase2 : '四面八方 天方夜谈<br>没有规矩，不成方圆',
			paragraph : '洞中方七日，世上已千年。想来我们的古人早就悟通了时空转换的玄机。当你进入了另一维空间，时间就会扭曲。人想长生不老的愿望因而能得以实现。可是只有修炼之人才能进入此等空间，而且修炼程度不同的人能享受到的时空转换的级别还不一样。天上一日，地下一年就是明证。',
			dayOfMonth : 0
		},
		{
			id : 'zu-foot',
			image : 'zu_foot.gif',
			imageId : '0B9-duwm5My1fNnY4MEF6Nk1nVFU',
			pinyin : 'zū',
			phrase1 : '充足 满足 足迹 不足 知足',
			phrase2 : '画蛇添足 知足常乐 心满意足<br>心有余而力不足',
			paragraph : '几个人得了一壶酒，谁都想自己喝，于是大家决定比赛画蛇，谁先画完谁就能独享那壶酒。其中一人手快，最先画好了，他得意地拿起了酒壶。看看其他人还没画完，心说我可以给我的蛇再画上几只脚，于是他就真的画起来了。这时，另外一个人也画好了，他不由分说，抢过酒壶就喝起来了。画蛇添足，多此一举。',
			dayOfMonth : 0
		},
		{
			id : 'er-ear',
			image : 'er_ear.gif',
			imageId : '0B9-duwm5My1fSFRPTzluSjZ1TkU',
			pinyin : 'ěr',
			phrase1 : '耳朵 木耳 耳聋 耳光',
			phrase2 : '耳聪目明 充耳不闻 耳提面命',
			paragraph : '很多历史人物的相貌都很有特点，比如刘备吧，有一双其大无比的耳朵。《三国》中形容刘备是双耳垂肩，两手过膝，目能自顾其耳。据说那是帝王相。不过你要是真在街上见着这么一位，准以为是动物园墙塌了，跑出什么珍奇野生动物来了呢。',
			dayOfMonth : 0
		},
		{
			id : 'jia-home',
			image : 'jia_home.gif',
			imageId : '0B9-duwm5My1fdmtFNzVrUjRPVTg',
			pinyin : 'jā',
			phrase1 : '家庭 回家 家长 大家',
			phrase2 : '四海为家 世家子弟 大家风范',
			paragraph : '那些水果，无论是在店里或摊子上，又都摆列的那么好看，果皮上的白霜一点也没蹭掉，而都被摆成放着香气的立体的图案画，使人感到那些果贩都是些艺术家，他们会使美的东西更美一些。',
			dayOfMonth : 0
		},
		{
			id : 'jiang-river',
			image : 'jiang_river.gif',
			imageId : '0B9-duwm5My1fM3dqemZnVlFSbkE',
			pinyin : 'jīang',
			phrase1 : '长江 江水 渡江 江山',
			phrase2 : '江河日下 翻江倒海 过江之鲫',
			paragraph : '和往常一样，下了公共汽车后，他匆匆赶到码头，乘摆渡到江那边去上班。熙熙攘攘的码头上挤满了等船的人，还有几个小贩，在人群中钻来钻去，高声地叫卖着。一会儿，摆渡来了，码头上顿时热闹了起来。上下船的人朝着不同方向走去，有人在小声抱怨着什么；有几个熟人在大声打着招呼。又过了一会儿，船开走了，渐渐消失在江雾之中。码头上一下子冷清了下来，只有那几个小贩还留在那，耐心等待着下一班船。',
			dayOfMonth : 0
		},
		{
			id : 'ru-into',
			image : 'ru_into.gif',
			imageId : '0B9-duwm5My1fUWMwdG5pZmk4WEU',
			pinyin : 'rù',
			phrase1 : '进入 入门 入时',
			phrase2 : '泥牛入海 登堂入室 入木三分',
			paragraph : '小时候常听西游记的故事，曾深深地为孙悟空那上天入地，钻山过海的本事所吸引。小孩子们一起玩的时候，也常要舞枪弄棒，嘴里喊着大胆的毛神，吃老孙一棒之类的话。现在儿子都快过那个年龄了，可有一次重听孙敬修讲西游记，仍有些童心萌动，跃跃欲试的感觉。',
			dayOfMonth : 0
		},
		{
			id : 'shi-yes',
			image : 'shi_yes.gif',
			imageId : '0B9-duwm5My1fR1drV3duLUVoa3c',
			pinyin : 'shì',
			phrase1 : '是非 不是 就是 是否',
			phrase2 : '实事求是 是非之地 似是而非',
			paragraph : '天是蓝的，水是清的。墨是黑的，纸是白的。糖是甜的，药是苦的。我是从来不说谎的，你是除了谎话什么也不说的。莎翁说：是还是不是，那是个问题。我说：不管是还是不是，这个问题是已经解决了的。',
			dayOfMonth : 0
		},
		{
			id : 'wo-me',
			image : 'wo_me.gif',
			imageId : '0B9-duwm5My1fZE9ZU3dfaURicnM',
			pinyin : 'wǒ',
			phrase1 : '忘我 自我 我们',
			phrase2 : '你来我往 我行我素 舍我其谁也',
			paragraph : '读书看报，“我”字到处都是，但脑子里印象最深的一个“我”字是小时候读过的一首歌谣：	<blockquote><dl><dt>天上没有玉皇，</dt>	<dt>地上没有龙王，</dt>	<dt>我就是玉皇，</dt><dt>我就是龙王，</dt><dt>喝令三山五岳开道，</dt><dt>我来了。</dt></dl></blockquote>',
			dayOfMonth : 0
		},
		{
			id : 'tian-sky',
			image : 'tian_sky.gif',
			imageId : '0B9-duwm5My1feDVYcHpVY0VxaGs',
			pinyin : 'Tīan',
			phrase1 : '天空 天气 明天 聊天',
			phrase2 : '天南海北 普天同庆<br>三天打鱼，两天晒网',
			paragraph : '中秋前后是北平最美丽的时候。天气正好不冷不热，昼夜的长短也划分得平匀。没有冬季从蒙古吹来的黄风，也没有伏天里挟着冰雹的暴雨。天是那么高，那么蓝，那么亮，好象是含着笑告诉北平的人们：在这些天里，大自然是不会给你们什么威胁与损害的。',
			dayOfMonth : 0
		},
		{
			id : 'shui-water',
			image : 'shui_water.gif',
			imageId : '0B9-duwm5My1fdkVGM1pfSHkzX2s',
			pinyin : 'Shǔi',
			phrase1 : '喝水 水果 水平 水银 铁水 下水',
			phrase2 : '水到渠成 水涨船高 水火无情<br>井水不犯河水',
			paragraph : '<blockquote><blockquote><dl><dt>你这是醉翁之意不在酒，在乎于山水之间。</dt><dt>到底你想说什么吧？</dt><dt>我是说咱俩从此井水不犯河水，谁也别碍谁的事。</dt><dt>这不是大水冲了龙王庙，一家人不认一家人了吗。有话咱们好好说吗。</dt></dl></blockquote></blockquote>',
			dayOfMonth : 0
		},
		{ // repeat the first word for easier look-up
			id : 'ren-human',
			image : 'ren_human.gif',
			imageId : '0B9-duwm5My1fS1ptbzlWVUZZQ3c',
			pinyin : 'Rén',
			phrase1 : '男人 女人 好人 坏人 人口 人情',
			phrase2 : '判若两人 旁若无人<br>前人栽树，后人乘凉',
			paragraph : '在中国古代的神话传说里，人是女娲用泥土造的。在西方的宗教故事中，上帝创造了人。而根据达尔文的进化论，人是从一种古猿进化来的。在中文里，人字的一撇一捺组成了一个简单而稳定的结构。就在这简单稳定的结构后面，有着无数的酸甜苦辣，悲欢离合。',
			dayOfMonth : 0
		} ];
