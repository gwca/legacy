
// The property name of this script
var WORD_PROP = 'WordOfTheWeek';

/**
 * Creates the Word of the Week HTML page
 * 
 * @param request
 * @returns
 */
function doGet() {
	var filename = 'word-ui';
	var htmlOutput = HtmlService.createTemplateFromFile(filename).evaluate();
	htmlOutput.setSandboxMode(HtmlService.SandboxMode.IFRAME);
	return htmlOutput;
}

/**
 * Includes an additional HTML page into the template page, such as a style or a
 * script file.
 * 
 * @param filename
 * @returns
 */
function include(filename) {
	var htmlOutput = null;
	try {
		htmlOutput = HtmlService.createTemplateFromFile(filename).evaluate();
	} catch (error) {
		return '<p>Failed loading file ' + filename + ':' + error + '</p>';
	}
	htmlOutput.setSandboxMode(HtmlService.SandboxMode.IFRAME);
	return htmlOutput.getContent();
}

/**
 * Returns the next word of the week in JSON format.
 * 
 * @returns
 */
function getWordOfWeek() {

	var properties = PropertiesService.getScriptProperties();
	var currentWord = properties.getProperty(WORD_PROP);
	var defaultWord = WORDS[0];
	
	// return the first word if nothing there
	if (currentWord == null || currentWord.length == 0) {
		currentWord = updateWordProp(defaultWord);
		return currentWord;
	}

	// update it on Sunday
	var today = new Date();
	var jsonWord = JSON.parse(currentWord);
	var weekDay = today.getDay();
	var dayOfMonth = today.getDate();
	
	if (weekDay == 0 && dayOfMonth != jsonWord.dayOfMonth) {
		jsonWord = getNextWord(jsonWord.id);
		currentWord = updateWordProp(jsonWord);
	}
	return currentWord;
}

/**
 * Updates the script property with a new word
 * @param word
 * @returns
 */
function updateWordProp(jsonWord) {
	var today = new Date();
	var properties = PropertiesService.getScriptProperties();
	jsonWord.dayOfMonth = today.getDate();
	var stringWord = JSON.stringify(jsonWord);
	properties.setProperty(WORD_PROP, stringWord);
	return stringWord;
}

/**
 * Finds out the next word in the array
 * @param wordId
 * @returns
 */
function getNextWord(wordId) {
	var numOfWords = WORDS.length - 1;
	for (var i = 0; i < numOfWords; i++) {
		var word = WORDS[i];
		if (wordId == word.id) {
			word = WORDS[i+1];
			return word;
		}
	}
	return WORDS[0];
}

/**
 * A test case
 */
function testGetWord() {
	var nextWord = getWordOfWeek();
	var jsonWord = JSON.parse(nextWord);
	Logger.log("nextWord:" + nextWord);
	Logger.log("wordId:" + jsonWord.id + ', ' + jsonWord.paragraph);
}
